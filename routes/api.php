<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'API'], function () {

	Route::get('login', function(Request $request) {
		if($request->wantsJson())
			return response()->json(['error','401 Unauthorized Access.']);
		else
			return 'Access Denied';
	});

	Route::post('login', [
    	'as' => 'login.driver', 
    	'uses' => 'AuthController@postLogin'
	]);

	Route::post('/forget', [
		'as' => 'forget.password',
    	'uses' => 'AuthController@postForget'
	]);

	Route::group(['middleware' => 'ApiToken'], function () {

		Route::post('/logout', [
	    	'as' => 'logout.driver', 
	    	'uses' => 'AuthController@logout'
		]);

		Route::post('/get-driver', [
	    	'as' => 'get.driver.info',
	    	'uses' => 'AuthController@getDriver'
		]);

		Route::post('/driver/current/location', [
			'as' => 'post.current.location',
			'uses' => 'LocationController@currentLocation'
		]);

	    Route::post('/orders', [
			'as'   => 'get.orders',
            'uses' => 'OrderController@listOrders'
		]);

		Route::post('/order', [
			'as'   => 'get.order',
            'uses' => 'OrderController@getorder'
		]);

		Route::post('/order/accept', [
			'as'   => 'accept.order',
            'uses' => 'OrderController@acceptOrder'
		]);

		Route::post('/order/reject', [
			'as'   => 'reject.order',
            'uses' => 'OrderController@rejectOrder'
		]);

		Route::post('/order/delivery/failer', [
			'as'   => 'order.delivery.failer',
            'uses' => 'OrderController@reportFailer'
		]);

		Route::post('/order/delivery/confirm', [
			'as'   => 'order.delivery.confirm',
            'uses' => 'OrderController@confirmDelivery'
		]);

		Route::post('/assignments', [
			'as'   => 'get.assignments',
            'uses' => 'OrderController@assignments'
		]);

		Route::post('/order-with-status', [
			'as' => 'get.order.status',
			'uses' => 'OrderController@getOrderWithStatus'
		]);

		Route::post('/estimate-time', [
			'as' => 'post.order.estimate.time',
			'uses' => 'OrderController@postEstimateTime'
		]);
    });
});