<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Sentinel::Check()) {
        return redirect('/products');
    } else {
        return view('landing');
    }
});
Route::get('/products', 'LandingController@getProducts');

Route::post('/process-search', 'LandingController@postSearch');

Route::get('/signup', 'RegistrationController@getSignup');
Route::post('/signup', 'RegistrationController@postSignup');
Route::get('/signin', 'LoginController@getLogin');
Route::post('/signin', 'LoginController@postLogin');
Route::post('/logout', 'LoginController@postLogout');
Route::post('/signup/customerfileupdate','RegistrationController@customerfileupdate');

Route::get('/forgot-password', 'ForgotPasswordController@getForm');
Route::post('/forgot-password', 'ForgotPasswordController@postForm');
Route::get('/reset/{email}/{code}', 'ForgotPasswordController@resetPassword');
Route::post('/reset/{email}/{code}', 'ForgotPasswordController@postResetPassword');

Route::get('/page/become-a-driver', function () {
    return Redirect::to('/blog/become-a-driver/', 301); 
});
Route::get('/page/{slug}', 'PageController@show');

Route::post('/page/contact-kushly', 'ContactController');


Route::get('map', function(){
    return view('map');
});

Route::post('/addtocart', 'CartController@addToCart')->middleware('ajax');
Route::post('/addtocartlocation', 'CartController@addToCartLocation')->middleware('ajax');
Route::post('/addtocart/update', 'CartController@updateAddToCart')->middleware('ajax');
Route::post('/product/updatePriceByVariant','CartController@updatePriceByVariant')->middleware('ajax');

Route::get('/cart', 'CartController@cart');

// Route::get('/cart/o/{id}', 'CartController@allocateOrder');
// Route::get('/aod', 'CartController@aod');

Route::get('/cart/payment', 'CartController@getcartpayment')->middleware('checkAuth');
Route::post('/cart/payment', 'CartController@cartpayment')->middleware('checkAuth');

Route::post('/cart/payment/complete', 'CartController@placeorder')->middleware('checkAuth');
Route::get('/cart/payment/complete', function(){
    Session::flash('info', 'No direct access for requested page.');
    return redirect('/');
})->middleware('checkAuth');

Route::get('/cart/thankyou','CartController@thankyou');


Route::get('/search', 'SearchController');

Route::post('/repeatOrder', 'CartController@repeatAddToCart')->middleware('ajax');

/**
 * Route group with products prefix
 */
Route::group(['prefix'=>'product'], function() {
    Route::get('/{id}', 'ProductController@singleProduct');
});

Route::get('/customer/profile', 'CustomerController@index');

/**
 * Route group which needs user login to access the page.
 * Middleware check the user logined or not.
 */
Route::group(['middleware' => 'checkAuth'], function() {

    Route::post('/user/profile/image', 'CustomerController@uploadUserImage');
    Route::post('/user/profile/image/del', 'CustomerController@delUserImage');

    Route::get('/customer/location/add', 'CustomerController@getLocationForm');

    Route::post('/customer/location/add', 'CustomerController@postLocationForm');
    
    Route::get('/customer/locations/json','CustomerController@getLocationJSON')->middleware('ajax');

    Route::get('/customer/locations/{id}', [
        'as'=>'location.edit', 
        'uses'=>'CustomerController@getlocation'
    ]);

    Route::post('/customer/locations/{id}', 'CustomerController@updateLocation');

    Route::post('/customer/locations/delete', 'CustomerController@deleteLocation')->middleware('ajax');
    Route::post('/customer/locations/restored', 'CustomerController@restoreLocation')->middleware('ajax');

    Route::get('/customer/order/{id}', 'CustomerController@viewOrder');

    // ANET Profile
    Route::post('/customer/create/anet/profile', 'CustomerController@createANETProfile');
    Route::post('/customer/delete/anet/profile', 'CustomerController@deleteANETProfile');

});

/**
 * Dispensary Admin Routes
 * Middleware check the user role and authentication.
 */
Route::group([
        'prefix' => 'dispensary', 
        'middleware' => ['dispensaryAdmin'], 
        'namespace' => 'DispensaryAdmin'
    ], function () {

    Route::get('/home', 'DispensaryController@index');

    Route::get('/account/{id}', 'DispensaryController@edit');
    Route::post('/account', 'DispensaryController@update');

    Route::get('/orders', 'OrderController@index');
    Route::get('/orders/json', 'OrderController@getOrdersJSON')->middleware('ajax');
    Route::get('/order/{id}', 'OrderController@show');

    Route::post('/order/accept', 'OrderController@acceptByDispensary')->middleware('ajax');
    Route::post('/order/picked', 'OrderController@pickedAtDispensary')->middleware('ajax');
    
    Route::get('/reports', 'ReportController@index');
    Route::get('/reports/order-json', 'ReportController@getOrdersJSON')->middleware('ajax');

    Route::get('/product/create', 'ProductController@create');
    Route::post('/product/create', 'ProductController@store');

    Route::get('/product/{id}', 'ProductController@edit');
    Route::post('/product/{id}', 'ProductController@update');

    Route::post('/product/deactive', 'ProductController@deactive')->middleware('ajax');
    Route::post('/product/active', 'ProductController@active')->middleware('ajax');
    Route::post('/product/trash', 'ProductController@trash')->middleware('ajax');
    Route::post('/product/restore', 'ProductController@restore')->middleware('ajax');
    Route::post('/product/imagedelete', 'ProductController@imagedelete')->middleware('ajax');
    Route::post('/product/image-featured', 'ProductController@imageFeatured')->middleware('ajax');
    
    Route::get('/locations/create', 'DispensaryController@addLocation');
    Route::post('/locations/store', 'DispensaryController@postLocation');
    
    Route::get('/manage-locations', 'DispensaryController@getLocations');

    Route::get('/manage-locations/{id}', 'DispensaryController@editLocation');
    Route::post('/manage-locations/{id}', 'DispensaryController@updateLocation');
});

/**
 * Driver Role Routes
 * Middleware check the user role and authentication.
 */
Route::group([
        'prefix' => 'driver', 
        'middleware' => 'driverRole', 
        'namespace' => 'Driver'
    ], function () {

    Route::get('/', 'DriverController@index');
    Route::get('/my-assignments', 'DriverController@assignments');

    Route::get('/order/{id}', 'DriverController@getOrder');
    Route::get('/order/{id}/detail', 'DriverController@getOrderDetail');

    Route::post('/accept/assignment', 'DriverController@acceptAssignment')->middleware('ajax');

    Route::get('/map-view', function(){
        Session::flash('danger','Unauthorized Access!');
        return back();
    });
    Route::post('/map-view', 'DriverController@mapview');

    Route::get('/profile', 'DriverController@profile');
	
	Route::post('/save/customer-photo', 'DriverController@saveCustomerPhoto');
});



/**
 * Admin routes using super instead of admin in url
 * Middleware check the user role is super admin.
 */
Route::post('/super/logout', 'LoginController@postLogout');
Route::group([
        'prefix' => 'super', 
        'middleware' => 'superAdmin',
        'namespace' => 'Admin'
    ], function () {

    Route::get('/', 'AdminController@index');
    /**
     * Route for user/roles/permissions
     */
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UserController@getUsers');
        Route::get('/create', 'UserController@createUser');
        Route::post('/create', 'UserController@storeUser');
        
        Route::get('/view/{id}', 'UserController@getUserDetails');
        Route::get('/edit/{id}', 'UserController@editUserDetails');
        Route::post('/edit/{id}', 'UserController@updateUserDetails');
        Route::post('/delete', 'UserController@deleteUser')->middleware('ajax');
        Route::post('/restored', 'UserController@restoreUser')->middleware('ajax');
        Route::get('/json', 'UserController@getUsersJSON')->middleware('ajax');
    });
    Route::group(['prefix' => 'roles'], function () {
        Route::get('/', 'UserController@getRoles');
        Route::get('/edit/{id}', 'UserController@editRoles');
        Route::post('/edit', 'UserController@updateRole');
    });
    Route::get('/permissions', 'UserController@getPermissions');

    /**
     * Route for orders
     */
    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', 'OrderController@index');
        Route::get('/json', 'OrderController@getOrdersJSON')->middleware('ajax');
        Route::get('/{id}', 'OrderController@show');
        Route::post('/getcustomername', 'OrderController@getCustomerName')->middleware('ajax');
    });

    /**
     * Route for reports
     */
    Route::group(['prefix' => 'reports'], function () {
        Route::get('/', 'ReportsController@index');

        Route::get('/allocated-drivers', 'ReportsController@allocatedDrivers');
    });

    /**
     * Route for drivers
     */
    Route::group(['prefix' => 'drivers'], function () {
        Route::resource('/', 'DriverController', ['only' => ['index', 'create',]]);
        Route::get('/json', 'DriverController@getDriversJSON')->middleware('ajax');

        Route::post('/store', 'DriverController@store');
        Route::get('/edit/{id}', 'DriverController@edit');
        Route::post('/update/{id}', 'DriverController@update');
        Route::post('/delete', 'DriverController@deleteDriver')->middleware('ajax');
        Route::post('/restored', 'DriverController@restoreDriver')->middleware('ajax');
    });    

    /**
     * Route for dispensaries / groups
     */
    Route::group(['prefix' => 'dispensaries'], function () {
        Route::get('/', 'GroupsController@index');
        Route::get('/create', 'GroupsController@create');
        Route::post('/create', 'GroupsController@store');
        Route::get('/edit/{id}', 'GroupsController@edit');
        Route::post('/edit/{id}', 'GroupsController@update');
        Route::post('/delete', 'GroupsController@destroy');
        Route::post('/restored', 'GroupsController@restore');

        Route::get('/json', 'GroupsController@getJSON');

    });

    /**
     * Route for customers
     */
    Route::group(['prefix' => 'customers'], function () {
        
        Route::get('/', 'CustomerController@index');
        Route::get('/json', 'CustomerController@getCustomersJSON')->middleware('ajax');

        Route::get('/create', 'CustomerController@createCustomer');
        Route::post('/store', 'CustomerController@storeCustomer');
        Route::get('/edit/{id}', 'CustomerController@edit');
        Route::post('/update/{id}', 'CustomerController@update');
        Route::post('/delete', 'CustomerController@deleteCustomer')->middleware('ajax');
        Route::post('/restored', 'CustomerController@restoreCustomer')->middleware('ajax');

    });

    /**
     * Route for locations
     */
    Route::group(['prefix' => 'locations'], function () {
        Route::get('/', 'LocationsController@index');
        Route::get('/create', 'LocationsController@create');
        Route::post('/store', 'LocationsController@store');
        Route::get('/edit/{id}', 'LocationsController@edit');
        Route::post('/edit/{id}', 'LocationsController@update');
    });
   

    /**
     * Route for page management
     */
    Route::group(['prefix' => 'manage-pages'], function () {
        Route::get('/', 'PageController@index');
        Route::get('/add-new', 'PageController@create');
        Route::post('/add-new', 'PageController@store');
        Route::get('/edit/{id}', 'PageController@edit');
        Route::post('/edit/{id}', 'PageController@update');
        Route::post('/delete', 'PageController@destroy')->middleware('ajax');
        Route::get('/json', 'PageController@getPagesJSON')->middleware('ajax');
    });

    //verification route start
    Route::group(['prefix' => 'verifications'], function () {
        Route::get('/','VerificationController@index');
        Route::get('/json', 'VerificationController@getVerificationJSON')->middleware('ajax');
        Route::get('/getdetails/json', 'VerificationController@getVerificationDetailsJSON')->middleware('ajax');
        Route::get('/updatestatus/json', 'VerificationController@gupdateStatusJSON')->middleware('ajax');
    });

    /*Admin Dashboard*/
    Route::get('/json', 'AdminController@getOrdersJSON')->middleware('ajax');
    Route::get('/allocated-drivers', 'AdminController@allocatedDrivers')->middleware('ajax');
    Route::post('/chartjson', 'AdminController@chartdata')->middleware('ajax');
    Route::post('/piechartjson', 'AdminController@piechartdata')->middleware('ajax');
    Route::post("/allocatedriver", 'AdminController@allocatedriver')->middleware("ajax");
    Route::post("/addriver", "AdminController@addriver")->middleware("ajax");
});

// Ajax Request
Route::group(['prefix' => 'ajax', 'middleware' => 'ajax'], function() {

    // Home Page
    Route::post('/load-product-modal', 'AjaxController@loadProductModalSlider');
    Route::post('/load-more-product', 'AjaxController@loadMoreProducts');
    Route::get('/load-view-cart', 'AjaxController@loadViewCart');
    Route::get('/remove-product-from-cart', 'AjaxController@removeProductFromCart');
    Route::get('/update-cart', 'AjaxController@updateCartAfterRemove');
    
    // Customer Profile
    Route::get('/customer/profile/update', 'AjaxController@updateCustomerForm');
    Route::post('/customer/profile/update/info', 'AjaxController@updateCustomerDetails');
    Route::post('/active-role', 'AjaxController@aRole');
    Route::post('/product/filter', 'AjaxController@productFilter');

    Route::post('/product/filter/location', 'AjaxController@locSearch');

    Route::post('/load/gmap','AjaxController@getGM');

    Route::post('/get/loc-by-city/','AjaxController@getLocByCity');
});

Route::get('/nevada-medical-marijuana', function () {
    return Redirect::to('/blog/nevada-medical-marijuana', 301); 
});
Route::get('/las-vegas-dispensaries', function () {
    return Redirect::to('/blog/las-vegas-dispensaries', 301); 
});

Route::get('/weed-delivery-las-vegas-nevada', function () {
    return Redirect::to('/blog/weed-delivery-las-vegas-nevada', 301); 
});
Route::get('/arizona-medical-marijuana', function () {
    return Redirect::to('/blog/arizona-medical-marijuana', 301); 
});
Route::get('/weed-delivery-phoenix-arizona', function () {
    return Redirect::to('/blog/weed-delivery-phoenix-arizona', 301); 
});
Route::get('/medicinal-marijuana-on-demand', function () {
    return Redirect::to('/blog/medicinal-marijuana-on-demand', 301); 
});
Route::get('/finding-perfect-marijuana-strand/', function () {
    return Redirect::to('/blog/finding-perfect-marijuana-strand/', 301); 
});
Route::get('/thc-cbd-stepping-into-the-spotlight/', function () {
    return Redirect::to('/blog/thc-cbd-stepping-into-the-spotlight/', 301); 
});
Route::get('/marijuana-thc-potency-much-good-thing/', function () {
    return Redirect::to('/blog/marijuana-thc-potency-much-good-thing/', 301); 
});
Route::get('/marijuana-industry-changing/', function () {
    return Redirect::to('/blog/marijuana-industry-changing/', 301); 
});
Route::get('/medical-marijuana-blog/', function () {
    return Redirect::to('/blog/medical-marijuana-blog/', 301); 
});
Route::get('/wp-content/{any}', function ($any) {
    return Redirect::to('/blog/'.$any, 301); 

})->where('any', '.*');
Route::get('/blog/uploads/{any}', function ($any) {
    return Redirect::to('/blog/wp-content/uploads/'.$any, 301); 

})->where('any', '.*');

 Route::get('/send-test-email', function(){
      Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)
      {
          $message->subject('Mailgun and Laravel are awesome!');
           $message->from('wjgrass@website_name.com', 'Website Name');
          $message->to('wjgrass@gmail.com');
      });

     Mail::to('wjgrass@gmail.com')->send(new App\Mail\CustomerWelcomeMessage());
	 return "Mail sent successfully";
 });

// Route::get('/welcome-customer-mail', function(){
//     $user = App\User::where('id',1)->first();
//     $product = App\Product::select('products.id','products.product_name','product_images.path')
//         ->join('product_images', 'product_images.product_id','=','products.id')
//         ->join('product_price', 'product_proce.product_id','=','products.id')
//         ->limit(4)
//         ->get();
//     return view('emails.users.welcome')->with(['user'=>$user,'product'=>$product]);
// });

// Route::get('/forget-pass-mail', function(){
//     return view('emails.forgetpassword');
// });
// Route::get('/dispensary-account-mail', function(){
//     return view('emails.admin.dispensaryaccount');
// });
// Route::get('/driver-account-mail', function(){
//     return view('emails.admin.driveraccount');
// });
// Route::get('/verification-email-mail', function(){
//     return view('emails.users.verificationemail');
// });
// Route::get('/thank-you-mail', function(){
//     return view('emails.users.thankyoumail');
// });
Route::get('/added-new-product-mail', function(){
    Mail::to('davidw.tws@gmail.com')->send(new App\Mail\AddNewProduct($product));
    return view('emails.admin.addednewproduct');
});

// Route::get('/dispensary-thank-you-mail', function(){
//     return view('emails.dispensary.thankyoumail');
// });
// Route::get('/admin-thank-you-mail', function(){
//     return view('emails.admin.thankyoumail');
// });
// Route::get('/new-product-confirmation-mail', function(){
//     return view('emails.dispensary.newproductconfirmation');
// });
// Route::get('/driver-accepts-order-mail', function(){
//     return view('emails.admin.driveracceptsorder');
// });
// Route::get('/customer-notified-mail', function(){
//     return view('emails.users.customernotified');
// });