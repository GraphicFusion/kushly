const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
  mix.sass('app.scss')
     .webpack('app.js');

  mix
    .styles([
      'datatables/jquery.dataTables.min.css',
      'datatables/dataTables.bootstrap.min.css',
      'datatables/responsive.bootstrap.min.css'
    ], 'public/css/dataTables.css')
    .scripts([
      'datatables/jquery.dataTables.min.js',
      'datatables/dataTables.bootstrap.min.js',
      'datatables/dataTables.responsive.min.js',
      'datatables/responsive.bootstrap.min.js'
    ], 'public/js/dataTables.js');

});
