
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./jasny-bootstrap');

$(document).ready(function() {

	/**
	 * Control footer visibilty as per sticky footer
	 */
	$('.footer').css('visibility', 'visible');

	/**
	 * Ajax loader image, Adding a div on body as
	 * overlay with loader image on every ajax request.
	 */
	$(document).ajaxStart(function(){
	    $("#ajaxLoader").show();
	}).ajaxStop(function(){
	    $("#ajaxLoader").fadeOut();
	});

	/**
	 * Close alert box message shown via Session
	 * Template flash.blade.php
	 */
	window.setTimeout(function() {
	    $(".alert-dismissible").fadeTo(500, 0).slideUp(500, function(){
	        $(this).remove(); 
	    });
	}, 4000);

	/**
	 * Change body and footer styles to manage overlaping
	 * Also add an overlay to body for disable elements.
	 */
	$('#myNavmenu').on('show.bs.offcanvas', function(){
		$('.footer').css('position', 'static');
		$('body').css('margin-bottom', '0');
		$('#overlay').show('slow');
		// $('.navbar-rside').parent('li').find('svg').css('display', 'none');
		// $('.navbar-rside').parent('li').append('<div class="icon-close">&times;</div>');
	});
	$('#myNavmenu').on('hidden.bs.offcanvas', function(){
		$('.footer').css('position', 'absolute');
		dynamic_margin_body();
		dynamic_pdt_body();
		$('#overlay').hide('slow');
		// $('.navbar-rside').parent('li').find('svg').css('display', 'inline-block');
		// $('.navbar-rside').parent('li').find('.icon-close').remove();
	});

	/**
	 * Change current user role
	 */
	$.ajaxSetup({
	  	headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  	}
	});
    $('#activeRole li a').click(function(event) {
        event.preventDefault();
        $(this).parent().addClass('active').siblings().removeClass('active');
        var dRole = $(this).data('role');
        var redirectURL = $(this).data('url');
        var aUrl = $(this).data('aurl');
        var dataX = '';
        if(dRole == 'dispensary-admin') {
        	dataX = {
        		role: dRole,
        		group: $(this).data('group-id')
        	};
        } else {
        	dataX = {
        		role: dRole
        	};
        }

        $.ajax({
            url: aUrl,
            type: "POST",
            data: dataX,
            success: function(data){
                // console.log(data);
                $(this).parent().addClass('active').siblings().removeClass('active');
                if(redirectURL == '') {
					location.reload();           	
                } else {
	                location.href = redirectURL;
                }
            },
            error: function(xhr, status, error){
                console.log(xhr + status + error);
            }
        });
    });

    /**
     * Delete confirmation popover
     */
    $('[data-toggle="popover"]').popover({
    	html: true
    });
    $('.btn').on('click', function (e) {
        $('.btn').not(this).popover('hide');
    });

    /**
     * Back to top scroll
     */
    $("a.btop").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	/**
	 * Input field validate
	 * Number Accept Only
	 */
 	$('.numbersOnly').keypress(function (event) {
	    var keycode = event.which;
	    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
	        event.preventDefault();
	    }
	});	
    /**
	 * Sticky header for Phone
	**/
	$(window).scroll(function() {
		var viewportHeight = $(window).height();
		// if(viewportWidth <= 767) {
		    if ($(this).scrollTop() > 1){  
		        $('.navbar-mobile').addClass("sticky");
	            var navbar_height = $(".navbar-xs").innerHeight() + 70;
		        var sticky_height = $(".sticky").innerHeight();
				var body_padding_top = navbar_height - sticky_height;
				$('.filterSidebar').css('top', body_padding_top);
		    } else {
		        $('.navbar-mobile').removeClass("sticky");
		        navbar_height = $(".navbar-xs").innerHeight() + 8;
			    var body_padding_top = navbar_height;
			    
			    // $('body').css('margin-top', body_padding_top);
			    //$('.filterSidebar').css('top', body_padding_top);	
                if($(window).width() > 768){

				    	 $("#filterNav").css('top', '68px');
				}else{
				 		 $("#filterNav").css('top', '133px');
				}
             }
		// }
	});

});


/**
 * close confirmation popover
 */
$(document).on("click", '.po-close', function (e) {
	$('.po, .po-restore').popover('hide');
});

/**
 * focus on input group
 */ 
$(document).on('focus', '.input-group .form-control', function () {
  	$(this).closest('.input-group, .form-group').addClass('focus');
}).on('blur', '.form-control', function () {
  	$(this).closest('.input-group, .form-group').removeClass('focus');
});

/**
 * Input field validate
 * Number Accept Only
 */
$(document).on('keypress', '.numbersOnly', function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});


function dynamic_margin_body() {
	var footer_height = $(".footer").innerHeight();
	var body_margin_bottom = footer_height;
	$('body').css('margin-bottom', body_margin_bottom);
	// $('body').css('margin-top', 68);
}
function dynamic_pdt_body() {
	var viewportWidth = $(window).width();
	var viewportHeight = $(window).height();
	var navbar_height = '';

	// if(viewportWidth < 767) {
	// 	navbar_height = $(".navbar-xs").innerHeight() + 8;
	// } else {
	// 	navbar_height = $(".navbar-all").innerHeight() + 8;
	// }
	// var body_padding_top = navbar_height;
	// $('body').css('margin-top', body_padding_top);
	// $('.filterSidebar').css('top', body_padding_top);

	if(viewportWidth < 767) {
		navbar_height = $(".navbar-xs").innerHeight() + 8;
		var body_padding_top = navbar_height;
		// $('body').css('margin-top', body_padding_top);
		$('.filterSidebar').css('top', body_padding_top);
	}
	if(viewportWidth > 767) {
		navbar_height = $(".navbar-all").innerHeight() + 10;
		var body_padding_top = navbar_height;
		$('body').css('margin-top', body_padding_top);
		$('.filterSidebar').css('top', body_padding_top);
	}

}

function preloadFunc()
{
	/**
	 * Set margin bottom dynamically to body as per footer and navbar height
	 */ 
	dynamic_margin_body();
	dynamic_pdt_body();
	$(window).resize(function(event) {
		dynamic_margin_body();
		dynamic_pdt_body();
	});
}
window.onpaint = preloadFunc();