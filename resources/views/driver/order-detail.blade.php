{{-- Driver --}}

@extends('layouts.driver')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('driver'); ?>>
			<a href="{{ url('/driver') }}">All Orders</a>
		</li>
		<li <?php echo navActive('driver/my-assignments'); ?>>
			<a href="{{ url('/driver/my-assignments') }}">My Assignments</a>
		</li>
	</ul>
</nav>	

<div class="all-orders">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12 order">
				<div class="bg-white">
					<div class="head">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p>Delivery Pending Driver</p>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<?php
									$odr = App\OrderDriver::where('order_id', $order->id)->whereNotNull('order_accept_timestamp')->get();
								?>
								@if(sizeof($odr) == 1)
									{!! Form::open(['method'=>'post', 'url'=>'driver/save/customer-photo', 'class'=>'form', 'id'=>'save_customer_photo', 'enctype'=>'multipart/form-data']) !!}	
								
									<input value="{{ $order->id }}" type="hidden" name="order_id" />
									<span class="con">
										<span class="btn btn-primary btn-file cam_con">
										<span class="cam_icon glyphicon glyphicon-camera"></span>Select photo<input type="file" name="customer_photo" id="customer_photo" />
										</span>
									</span>
									<button type="button" id="confirmDelivery" data-orderid="{{ $order->id }}" data-driverid="{{ getUserId() }}" class="btn btn-primary btn-block" onclick="document.getElementById('save_customer_photo').submit()"> Confirm Delivery </button>								
									{!! Form::close() !!}
								@else
									<button type="button" id="acceptAssignment" data-orderid="{{ $order->id }}" data-driverid="{{ getUserId() }}" class="btn btn-primary btn-block"> Accept </button>
								@endif
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								@if(sizeof($odr) == 1)
									<button type="button" class="btn btn-link btn-block">Report Failer</button>
								@else
									<button type="button" class="btn btn-link btn-block">Reject Assignment</button>
								@endif
							</div>
						</div>
					</div>
					
					<div class="order-details-row clearfix">
						<div class="col-lg-12 title">
							<p><span>Order#</span>{{ $order->id }}</p>
						</div>

						<div class="col-lg-12 time">
							<?php 
		                        echo embed_svg(
		                            $class = 'obj-svg', 
		                            $svgurl = 'img/time.svg', 
		                            $height = '17px', 
		                            $width = '17px', 
		                            $link = ''
		                        ); 
		                    ?>
							{{ $order->timestamp_order_made }}
						</div>

						<div class="col-lg-12 loc">
							@php 
								$loc = App\Locations::where('id', $order->locations_id)->first();

								$loc_to = $loc->street_address.', '.$loc->city.', '.$loc->state.', '.$loc->zip_code;
							@endphp
							<?php 
		                        echo embed_svg(
		                            $class = 'obj-svg', 
		                            $svgurl = 'img/location.svg', 
		                            $height = '17px', 
		                            $width = '17px', 
		                            $link = ''
		                        ); 
		                    ?>


							<?php
						 		// get dispensary/group location
						       	$oProduct = App\OrderProduct::select('product_id')->where('order_id',$order->id)->first();

						       	$gid = \DB::table('products')->select('group_id')->where('id',$oProduct['product_id'])->get();

						        $group = App\Groups::where('id', $gid[0]->group_id)->first();

						        $groupLoc = $group->location()->first();

						      	$location_from = $groupLoc->street_address.', '.$groupLoc->unit.', '.$groupLoc->city.', '.$groupLoc->state.', '.$groupLoc->zip_code.', '.$groupLoc->country;

							?>

							From:<br/>
							{{ $location_from }}
							<br/>
		                    To:<br/>
							{{ $loc_to }}
							<br/>

							<form action="{{ url('/driver/map-view') }}" method="post" id="getDirections">
			                    {{ csrf_field() }}
			                    <input type="hidden" name="order_id" value="{{ $order->id }}">
			                    <input type="hidden" name="flat" value="{{ $groupLoc->lat }}">
			                    <input type="hidden" name="flng" value="{{ $groupLoc->lng }}">
			                    <input type="hidden" name="tlat" value="{{ $loc->lat }}">
			                    <input type="hidden" name="tlng" value="{{ $loc->lng }}">
			                    <a href="#" onclick="document.getElementById('getDirections').submit()">Directions</a>
			                </form>
						</div>

						<div class="col-lg-12 estimate">
							<p>
								<span>Delivery Estimate</span>
								@php
									$est = App\OrderDriver::select('estimate_time')
										->where('order_id', $order->id)
										->where('driver_id', getUserId())
										->first();
									echo !empty($est['estimate_time']) ? $est['estimate_time'] : 'Estimate time not updated.';
								@endphp
							</p>
							<a href="#x">Change</a>
						</div>

						<div class="col-lg-12">
							<a href="{{ url('/driver/order/'.$order->id.'/detail') }}">View Order Details</a>
						</div>
					</div>
									
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php
// $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=45-46+Star+Enclave+Near+Green+Dhandra+Rd+Opp+Mehmudpura+Gate+Golden+Avenue+Duggri+Ludhiana+Punjab+141116&key='.\Config::get('app.gmap').'';
// $ch = curl_init($url);
// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
// curl_setopt($ch, CURLOPT_URL,$url);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// $output = curl_exec ($ch);
// $info = curl_getinfo($ch);
// $http_result = $info;
// curl_close ($ch);

// pr($http_result);
?>

@endsection


@section('customJs')
<script type="text/javascript">
$(document).ready(function($) {
	$('#acceptAssignment').click(function(){
		var $this = $(this);
		var uid = $this.data('driverid');
		var oid = $this.data('orderid');
		
		$.ajax({
			url: '{{ url('/driver/accept/assignment') }}',
			type: 'POST',
			data: {
				driver_id: uid,
				order_id: oid
			},
		})
		.done(function(data) {
			$this.replaceWith('<button type="button" id="confirmDelivery" data-orderid="{{ $order->id }}" data-driverid="{{ getUserId() }}" class="btn btn-primary btn-block"> Confirm Delivery </button>');
		})
		.fail(function() {
			console.log("error");
		});
		
	});
});
</script>
@endsection