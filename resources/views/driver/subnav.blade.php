<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('driver'); ?>>
			<a href="{{ url('/driver') }}">Deliveries</a>
		</li>
		<!--li <?php echo navActive('driver/map-view'); ?>>
			<a href="{{ url('/driver/map-view') }}">Map View</a>
		</li-->
		<li <?php echo navActive('driver/profile'); ?>>
			<a href="{{ url('/driver/profile') }}">Profile</a>
		</li>
	</ul>
</nav>	