{{-- Driver Map View --}}

@extends('layouts.driver')

@section('content')

@include('driver.subnav')

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h5>Order Delivery Estimate Time</h5>
				</div>
			</div>
		</div>		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
		        <div id="dvMap" style="width: 100%; height: 450px">
			</div>
			<div class="col-lg-12">

				<h5>Order id {!! $geo['order_id'] !!} estimated deliver time is <span id="distance_tm"></span></h5>

			</div>
		</div>
	</div>
</div>

@endsection

@section('customJs')

<script src="https://maps.googleapis.com/maps/api/js?key={!! \Config::get('app.gmap') !!}"></script>
<script>
	var directionsDisplay = new google.maps.DirectionsRenderer();
    var markers = [
        {
            "title": 'Dispensary',
            "lat": '{!! $geo['flat'] !!}',
            "lng": '{!! $geo['flng'] !!}',
            "description": 'Dispensary Location'
        }, {
            "title": 'Customer',
            "lat": '{!! $geo['tlat'] !!}',
            "lng": '{!! $geo['tlng'] !!}',
            "description": 'Customer Location'
        }
    ];
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: '{{ asset('/img/map-marker.png') }}',
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                    directionsDisplay.setMap(map);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
 
        //***********ROUTING****************//
 
        //Initialize the Path Array
        var path = new google.maps.MVCArray();
 
        //Initialize the Direction Service
        var service = new google.maps.DirectionsService();
        
 
        //Set the Path Stroke Color
        var poly = new google.maps.Polyline({ map: map, strokeColor: '#21DB00' });
 
        //Loop and Draw Path Route between the Points on MAP
        for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) < lat_lng.length) {
                var src = lat_lng[i];
                var des = lat_lng[i + 1];
                path.push(src);
                poly.setPath(path);

                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }

						directionsDisplay.setDirections(result);
						// var km = parseInt(result.routes[0].legs[0].distance.value) / 1000;
						// document.getElementById('distance_val').textContent = km+" K.M";

						var tm = result.routes[0].legs[0].duration.text;
						document.getElementById('distance_tm').textContent = tm;
                    }
                });
            }
        }
    }
</script>
@endsection