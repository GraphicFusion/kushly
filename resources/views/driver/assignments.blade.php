{{-- Driver Deliveries --}}

@extends('layouts.driver')

@section('content')

{{-- @include('driver.subnav') --}}

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('driver'); ?>>
			<a href="{{ url('/driver') }}">All Orders</a>
		</li>
		<li <?php echo navActive('driver/my-assignments'); ?>>
			<a href="{{ url('/driver/my-assignments') }}">My Assignments</a>
		</li>
	</ul>
</nav>	

<div class="all-orders">
	<div class="container">
		<div class="row">
			
			@foreach($orders as $key => $order)
				<div class="order col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="bg-white">
						<div class="rw title">
							<a href="{{ url('/driver/order/'.$order->id) }}"></a>
							<div class="pull-left"><span>Order#</span>{{ $order->id }}</div>
							<div class="icon pull-right">&#8250;</div>
						</div>
						<div class="rw time">
							<?php 
		                        echo embed_svg(
		                            $class = '', 
		                            $svgurl = 'img/time.svg', 
		                            $height = '17px', 
		                            $width = '17px', 
		                            $link = ''
		                        ); 
		                    ?>
							{{ $order->timestamp_order_made }}
						</div>
						<div class="rw loc">
							@php 
								$loc = App\Locations::where('id', $order->locations_id)->first();
							@endphp
							<?php 
		                        echo embed_svg(
		                            $class = '', 
		                            $svgurl = 'img/location.svg', 
		                            $height = '17px', 
		                            $width = '17px', 
		                            $link = ''
		                        ); 
		                    ?>
							{{ $loc->street_address }}<br/>{{ $loc->city.', '.$loc->state.', '.$loc->zip_code }}
						</div>
					</div>
				</div>
			@endforeach

		</div>
	</div>
</div>
@endsection