{{-- Driver Profile --}}

@extends('layouts.driver')

@section('content')

@include('driver.subnav')

<div class="bg-white driver-account">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5>Driver Account</h5>
				</div>
				<div class="col-lg-6">

				</div>
			</div>
		</div>		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<div class="thumbnail">
					<div class="img-wrap">
						<?php
							$user_image = Sentinel::findById(getUserId())->profile_pic;
							$user_image = (!empty($user_image)) ? URL::to('/profile-pics/'.$user_image) : 'http://placehold.it/300x300?text=No Image Available';
						?>
						<img src="{!! $user_image !!}" class="img-circle" alt="">
						<?php 
			                echo embed_svg(
			                    $class = 'pencil-icon picForm',
			                    $svgurl = 'img/pencil-icon.svg',
			                    $height = '16px', 
			                    $width = '16px', 
			                    $link = 'javascript:void(x)'
			                ); 
			            ?>
						
						<div class="well" id="uploadPicture">
							<a href="javascript:void(0)" class="btn btn-primary openForm">Upload Picture</a>

							<form action="{{ url('/user/profile/image/del') }}" method="post" id="delUserImage">
	                            {{ csrf_field() }}
	                            <a class="btn btn-primary-outline" href="#" onclick="document.getElementById('delUserImage').submit()">Delete</a>
	                        </form>

							<a href="#x" class=""></a>

				            {!! Form::open(['url' => 'user/profile/image', 'method' => 'post', 'class' => 'form', 'id' => 'profileImageFrom', 'enctype'=>'multipart/form-data']) !!}
				            	@if (count($errors) > 0)
								    <div class="alert alert-danger">
								        <ul>
								            @foreach ($errors->all() as $error)
								                <li>{{ $error }}</li>
								            @endforeach
								        </ul>
								    </div>
								@endif
								<div class="form-group">
									{!! Form::hidden('id', getUserId()) !!}
									<input type="file" name="profile_pic" id="profile_pic" class="inputfile" />
									<label for="profile_pic">JPG, GIF or PNG. Max size 800kb</label>
								</div>
								{!! Form::submit('Upload Now', ['class'=>'btn btn-primary']) !!}
								{!! Form::button('Cancel', ['class'=>'btn btn-primary-outline closeForm']) !!}
							{!! Form::close() !!}
						</div>
					</div>

					<div class="caption">
						<ul class="list-unstyled list-actions">
							<li>
								<form action="{{ url('/logout') }}" method="post" id="formLogout">
				                    {{ csrf_field() }}
				                    <a href="#" onclick="document.getElementById('formLogout').submit()">Sign-Out</a>
				                </form>
							</li>
						</ul>
					</div>
				</div>

			</div>
			<div class="col-sm-7 col-sm-offset-1">
				<?php $user = Sentinel::getUser(); ?>
				<ul class="list-unstyled user-details">
					<li class="customer_name">
						<label>Name</label>
						<p>{!! $user->first_name.' '.$user->last_name !!}</p>
						<?php
			                echo embed_svg(
			                    $class = 'pencil-icon',
			                    $svgurl = 'img/pencil-icon.svg',
			                    $height = '16px', 
			                    $width = '16px', 
			                    $link = makeURL('/ajax/customer/profile/update')
			                ); 
			            ?>
			            <div class="ajaxform"></div>
					</li>
					<li class="customer_phone">
						<label>Phone</label>
						<p>{!! $user->phone_no !!}</p>
						<?php 
			                echo embed_svg(
			                    $class = 'pencil-icon',
			                    $svgurl = 'img/pencil-icon.svg',
			                    $height = '16px', 
			                    $width = '16px', 
			                    $link = makeURL('/ajax/customer/profile/update')
			                ); 
			            ?>
			            <div class="ajaxform"></div>
					</li>
					<li class="customer_email">
						<label>Email</label>
						<p>{!! $user->email !!}</p>
						<?php 
			                echo embed_svg(
			                    $class = 'pencil-icon',
			                    $svgurl = 'img/pencil-icon.svg',
			                    $height = '16px', 
			                    $width = '16px', 
			                    $link = makeURL('/ajax/customer/profile/update')
			                ); 
			            ?>
			            <div class="ajaxform"></div>
					</li>
					<li class="customer_birthday">
						<label>Birthday</label>
						<p>{!! $user->birthday !!}</p>
						<?php 
			                echo embed_svg(
			                    $class = 'pencil-icon',
			                    $svgurl = 'img/pencil-icon.svg',
			                    $height = '16px', 
			                    $width = '16px', 
			                    $link = makeURL('/ajax/customer/profile/update')
			                ); 
			            ?>
			            <div class="ajaxform"></div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

@endsection

@section('customCss')
<style>
#profileImageFrom,
#uploadPicture,
.closeForm { 
	display: none; 
}
#uploadPicture {
    margin-top: 25px;
}
#profileImageFrom {
	margin-top: 20px;
}
</style>
@endsection


@section('customJs')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> --}}
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('.picForm .object-link').click(function(event) {
			$('#uploadPicture').toggle('slow');
		});

		var openform = $('#uploadPicture .openForm');
		var closeform = $('#uploadPicture .closeForm');
		var uploadform = $('#profileImageFrom');

		openform.click(function(event) {
			uploadform.slideDown('slow');
			$(this).hide('slow');
			closeform.show();
		});
		closeform.click(function(event) {
			uploadform.slideUp('slow');
			$(this).hide('slow');
			openform.show();
		});

  		// Ajax call to open inline edit form
	    function callAjax(type, url, data, target, hideelem) {
	    	$.ajaxSetup({
			  	headers: {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  	}
			});
	    	$.ajax({
	    		url: url,
	    		type: type,
	    		data: data,
	    		success: function(data){
	                // console.log(data);
	                $(hideelem).hide();
	                $(target).append(data);
	            },
	            error: function(xhr, status, error){
	                console.log(xhr);
	                console.log(status);
	                console.log(error);
	            }
	    	});
	    }

	    $('.customer_name .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'name'}
	    	callAjax('GET', url, data, '.customer_name .ajaxform', '.customer_name .pencil-icon');
	    });

	    $('.customer_phone .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'phone'}
	    	callAjax('GET', url, data, '.customer_phone .ajaxform', '.customer_phone .pencil-icon');
	    });

	    $('.customer_email .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'email'}
	    	callAjax('GET', url, data, '.customer_email .ajaxform', '.customer_email .pencil-icon');
	    });

	    $('.customer_birthday .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'dob'}
	    	callAjax('GET', url, data, '.customer_birthday .ajaxform', '.customer_birthday .pencil-icon');
	    });
	});

	/**
	 * Update Customer Name
	 */
	function updateRecordAJAX(id, formid, statuswrapper, formwrapper, icon) {
		$(document).on('click', id, function(){
			$.ajaxSetup({
			  	headers: {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  	}
			});
			$.ajax({
				url: $(formid).attr('action'),
				type: 'POST',
				data: $(formid).serialize(),
			})
			.done(function(msg) {
				// console.log(msg);
				switch(msg.action) {
					case 'name':
						$(statuswrapper).text(msg.firstName+' '+msg.lastName);
						break;
					case 'phone':
						$(statuswrapper).text(msg.phone_no);
						break;
					case 'email':
						$(statuswrapper).text(msg.email);
						break;
					case 'dob':
						$(statuswrapper).text(msg.birthday);
						break;
				}
				$(formwrapper).html('');
				$(icon).show();
			})
			.fail(function() {
				console.log("Error");
			});
		});
	}
	updateRecordAJAX('#updateName','#updateCustomerName','li.customer_name p','.customer_name .ajaxform','.customer_name .pencil-icon');

	updateRecordAJAX('#updatePhone','#updateCustomerPhone','li.customer_phone p','.customer_phone .ajaxform','.customer_phone .pencil-icon');

	updateRecordAJAX('#updateEmail','#updateCustomerEmail','li.customer_email p','.customer_email .ajaxform','.customer_email .pencil-icon');

	updateRecordAJAX('#updateDOB','#updateCustomerBirthday','li.customer_birthday p','.customer_birthday .ajaxform','.customer_birthday .pencil-icon');

	function closeAjaxForm(id, formwrapper, icon) {
		$(document).on('click', id, function(){
			$(formwrapper).html('');
			$(icon).show();
		});		
	}
	closeAjaxForm('#cancelName','.customer_name .ajaxform','.customer_name .pencil-icon');
	closeAjaxForm('#cancelPhone','.customer_phone .ajaxform','.customer_phone .pencil-icon');
	closeAjaxForm('#cancelEmail','.customer_email .ajaxform','.customer_email .pencil-icon');
	closeAjaxForm('#cancelDOB','.customer_birthday .ajaxform','.customer_birthday .pencil-icon');

</script>
@endsection