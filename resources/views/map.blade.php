@extends('layouts.master')

@section('content')
	<div class="container p-b-30">
		{{-- <div class="row p-b-30">
			<div class="col-lg-12">
				<div class="page-content">
					<div class="page-header">
						<h3>Map Setup</h3>
					</div>
					<div id="map" style="height: 400px;"></div>
				</div>
			</div>
		</div>

		<div class="row p-b-30">
			<div class="col-lg-12">
				<div class="page-header">
					<h3>GEO Location</h3>
				</div>
				<form class="form-inline" method="post" id="geocoding_form">
					<div class="form-group">
						<input type="text" class="form-control" id="address" name="address" required/>
					</div>
					<button type="submit" class="btn btn-primary">Search</button>
				</form>
			</div>
		</div> --}}

		<div class="row p-b-30">
			<div class="col-lg-12">
                <div class="page-header">
                    <h3>Arriving Time Demo</h3>
                    <p class="help-block">This timming/distance is changed as per the driver current location.</p>
                </div>
		        <div id="dvMap" style="width: 100%; height: 500px">
			</div>
			<input type="text" id="distance_val" value="">
			<input type="text" id="distance_tm" value="">
		</div>
	</div>
@endsection

@section('customJs')

<script src="https://maps.googleapis.com/maps/api/js?key={!! \Config::get('app.gmap') !!}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>
<script>
/* Map Object */
var mapObj = new GMaps({
	el: '#map',
	lat: 48.857,
	lng: 2.295
});

var m = mapObj.addMarker({
	lat: 48.8583701,
	lng: 2.2944813,
	title: 'Eiffel Tower',
	icon: '{{ asset('/img/map-marker.png') }}',
	infoWindow: {
		content: '<h4>Eiffel Tower</h4><div>Paris, France</div>',
		maxWidth: 100
	}
});
</script>


<script>
	var directionsDisplay = new google.maps.DirectionsRenderer();
    var markers = [
        {
            "title": 'Mumbai',
            "lat": '18.964700',
            "lng": '72.825800',
            "description": 'Mumbai formerly Bombay, is the capital city of the Indian state of Maharashtra.'
        }, {
            "title": 'Pune',
            "lat": '18.523600',
            "lng": '73.847800',
            "description": 'Pune is the seventh largest metropolis in India, the second largest in the state of Maharashtra after Mumbai.'
        }
    ];
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: '{{ asset('/img/map-marker.png') }}',
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                    directionsDisplay.setMap(map);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
 
        //***********ROUTING****************//
 
        //Initialize the Path Array
        var path = new google.maps.MVCArray();
 
        //Initialize the Direction Service
        var service = new google.maps.DirectionsService();
        
 
        //Set the Path Stroke Color
        var poly = new google.maps.Polyline({ map: map, strokeColor: '#21DB00' });
 
        //Loop and Draw Path Route between the Points on MAP
        for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) < lat_lng.length) {
                var src = lat_lng[i];
                var des = lat_lng[i + 1];
                path.push(src);
                poly.setPath(path);

                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }

						directionsDisplay.setDirections(result);
						var km = parseInt(result.routes[0].legs[0].distance.value) / 1000;
						document.getElementById('distance_val').value = km+" K.M";

						var tm = result.routes[0].legs[0].duration.text;
						document.getElementById('distance_tm').value = tm;
                    }
                });
            }
        }
    }
</script>
@endsection