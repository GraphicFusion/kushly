<div id="modal-product-slider" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
	
	<?php
		$set = '';
		$cartVariants = [];
		$cartProductIDs = [];
		$nocp = sizeof(Session::get('addToCart'));
		if(Session::get('addToCart')) {
			$cartVariant = Session::get('addToCart');
			foreach ($cartVariant as $key => $cv) {
			 	$cartVariants[] = $cv['variant_id'];
			 	$cartProductIDs[] = $cv['product_id'];
			}
		}
		$minLimit = 4;
		$maxLimit = 8;
		$countNextProducts = App\Product::where('id', '>=', $id)->limit($minLimit)->count();
		$countPrevProducts = App\Product::where('id', '<', $id)->limit($minLimit)->count();
		if($countPrevProducts == 0){
			$nextProduct = App\Product::where('id', '>=', $id)->limit($maxLimit)->get();
		} else {
			$nextProduct = App\Product::where('id', '>=', $id)->limit($minLimit)->get();
		}

		if($countNextProducts == 0){
			$prevProduct = App\Product::where('id', '<', $id)->limit($maxLimit)->get();
		} else {
			$prevProduct = App\Product::where('id', '<', $id)->limit($minLimit)->get();
		}

		$active = '';
	?>

	@if(sizeof($prevProduct) > 0)
		@foreach($prevProduct as $key => $pProduct)
			
			<?php
				if($pProduct->id == $id):
					$active = 'active';
				else:
					$active = '';
				endif;
			?>

		    <div class="item {{ $active }}">
				<div class="thumbnail">
					
					<?php
						$proImg = App\ProductImages::select('id', 'path')
		                    ->where('product_id', $pProduct->id)
		                    ->where('featured', 1)                
		                    ->first();
	                	$featuredImage = (!empty($proImg['path'])) ? url('product-images/'.$proImg['path']) : url('img/product-thumb.jpg');
					?>
			        <img src="{{ $featuredImage }}" alt="" style="height:160px;">
	
		      		<div class="caption">
		      			<span class="category">
							{{ $pProduct->type }}
						</span>
						<h3>{{ $pProduct->product_name }}</h3>
						<ul class="list-inline">
							<li>
								<select name="product-opt" id="product-opt_OS_<?php echo $pProduct->id; ?>" class="form-control product-opt-x">
									<option value="">Select Variant</option>
									@foreach($pProduct->prices as $key => $varient)
										<?php
											if(in_array($varient->id, $cartVariants)) {
												$set="selected='selected'";		
											} else {
												$set="";
											}
										?>
										<option {{ $set }} value="{{ $varient->id.'-'.$varient->unit_price.'-'.$varient->quantity_per_unit }}">
											{{ $varient->unit_price.'('.$varient->quantity_per_unit.')' }}
										</option>
									@endforeach
								</select>
							</li>
							<li>
								<form action="{{ url('addtocart') }}" method="post" id="addtocartOS_<?php echo $pProduct->id; ?>">
				                    {{ csrf_field() }}
									<input type="hidden" id="product_id" name="product_id" value="{{ $pProduct->id }}"/>
									<input type="hidden" id="unit_price" name="unit_price" value="" />
									<input type="hidden" id="variant_id" name="variant_id" value="" />
									<input type="hidden" id="quantity_per_unit" name="quantity_per_unit" value="" />
									<input type="hidden" id="quantity" name="quantity" value="1" />
									<input type="hidden" id="user_id" name="user_id" value="<?php echo getUserId(); ?>">
									<input type="hidden" id="group_id" name="group_id" value="{{ $pProduct->group_id }}">
									<?php
										if(in_array($pProduct->id, $cartProductIDs)) {
											echo '<div class="icon"><a title="View Cart" class="viewCart btn btn-primary-outline" id="viewCart" href="#">View Cart</a></div>';
										} else {
											echo '<div class="icon"><a class="btn btn-primary" href="#" id="add2cartOnSlider" data-nocp="'.$nocp.'" title="Add to cart">Add to Cart</a></div>';
										}
									?>
				                </form>
							</li>
						</ul>
						<div class="error"></div>

						@if($pProduct->is_marijuana == 1)
							<ul class="list-inline list-stat">
								<li><span>THC</span>{{ $pProduct->thc }}%</li>
								<li><span>CBD</span>{{ $pProduct->cbd }}%</li>
								<li><span>CBN</span>{{ $pProduct->cbn }}%</li>
							</ul>
						@endif

						<?php
							$groupID = $pProduct->group_id;
							$group_logo = App\Groups::select('id', 'logo')->where('id', $groupID)->get()->first();
							if(!empty($group_logo['logo'])) {
								echo '<div class="text-center"><img src="'.url('groups-logo/'.$group_logo['logo']).'" width="120px" height="31px" alt="Group Logo"></div>';
							}
						?>

						<div class="link-more">
							<a href="{{ url('/product/'.$pProduct->id) }}">More Info</a>
						</div>
		      		</div>
		      	</div>
		    </div>

		@endforeach
	@endif

	@if(sizeof($nextProduct) > 0)
		@foreach($nextProduct as $key => $nProduct)
			
			<?php
				if($nProduct->id == $id):
					$active = 'active';
				else:
					$active = '';
				endif;
			?>

		    <div class="item {{ $active }}">
				<div class="thumbnail">
		      		
					<?php
						$proImg = App\ProductImages::select('id', 'path')
		                    ->where('product_id', $nProduct->id)
		                    ->where('featured', 1)                
		                    ->first();
	                	$featuredImage = (!empty($proImg['path'])) ? url('product-images/'.$proImg['path']) : url('img/product-thumb.jpg');
					?>
			        <img src="{{ $featuredImage }}" alt="" style="height:160px;">


		      		<div class="caption">
		      			<span class="category">
							{{ $nProduct->type }}
						</span>
						<h3>{{ $nProduct->product_name }}</h3>
						<ul class="list-inline">
							<li>
								<select name="product-opt" id="product-opt_OS_<?php echo $nProduct->id; ?>" class="form-control product-opt-x">
									<option value="">Select Variant</option>
									@foreach($nProduct->prices as $key => $varient)
										<?php
											if(in_array($varient->id, $cartVariants)) {
												$set="selected='selected'";		
											} else {
												$set="";
											}
										?>
										<option {{ $set }} value="{{ $varient->id.'-'.$varient->unit_price.'-'.$varient->quantity_per_unit }}">
											{{ $varient->unit_price.'('.$varient->quantity_per_unit.')' }}
										</option>
									@endforeach
								</select>
							</li>
							<li>
								<form action="{{ url('addtocart') }}" method="post" id="addtocartOS_<?php echo $nProduct->id; ?>">
				                    {{ csrf_field() }}
									<input type="hidden" id="product_id" name="product_id" value="{{ $nProduct->id }}"/>
									<input type="hidden" id="unit_price" name="unit_price" value="" />
									<input type="hidden" id="variant_id" name="variant_id" value="" />
									<input type="hidden" id="quantity_per_unit" name="quantity_per_unit" value="" />
									<input type="hidden" id="quantity" name="quantity" value="1" />
									<input type="hidden" id="user_id" name="user_id" value="<?php echo getUserId(); ?>">
									<input type="hidden" id="group_id" name="group_id" value="{{ $nProduct->group_id }}">
									<?php
										if(in_array($nProduct->id, $cartProductIDs)) {
											echo '<div class="icon"><a title="View Cart" class="viewCart btn btn-primary-outline" id="viewCart" href="#">View Cart</a></div>';
										} else {
											echo '<div class="icon"><a class="btn btn-primary" href="#" id="add2cartOnSlider" data-nocp="'.$nocp.'" title="Add to cart">Add to Cart</a></div>';
										}
									?>
				                </form>
							</li>
						</ul>
						<div class="error"></div>

						@if($nProduct->is_marijuana == 1)
							<ul class="list-inline list-stat">
								<li><span>THC</span>{{ $nProduct->thc }}%</li>
								<li><span>CBD</span>{{ $nProduct->cbd }}%</li>
								<li><span>CBN</span>{{ $nProduct->cbn }}%</li>
							</ul>
						@endif
						
						<?php
							$groupID = $nProduct->group_id;
							$group_logo = App\Groups::select('id', 'logo')->where('id', $groupID)->get()->first();
							if(!empty($group_logo['logo'])) {
								echo '<div class="text-center"><img src="'.url('groups-logo/'.$group_logo['logo']).'" width="120px" height="31px" alt="Group Logo"></div>';
							}
						?>
						
						<div class="link-more">
							<a href="{{ url('/product/'.$nProduct->id) }}">More Info</a>
						</div>
		      		</div>
		      	</div>
		    </div>

		@endforeach
	@endif
	
  </div>
  <a class="left carousel-control" href="#modal-product-slider" role="button" data-slide="prev">
    <img src="img/arrow-left.png" alt="Previous Product">
  </a>
  <a class="right carousel-control" href="#modal-product-slider" role="button" data-slide="next">
    <img src="img/arrow-right.png" alt="Next Product">
  </a>
</div>