<style>
.view_cart
{
	padding-left: 20px;
	padding-right: 20px;
}
.view_cart .box_list1 {
    background-color: #ddd;
    width: 28px;
    height: 37px;
    margin: 0 auto;
}
.view_cart .box_list1:after {
    content: '';
    position: absolute;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    border-top: 10px solid #ddd;
    left: 18px;
    top: 47px;
}
.view_cart .box1 h4 {
    font-family: 'Lato', sans-serif;
    font-weight: 400;
    font-size: 18px;
    text-align: center;
    line-height: 2;
    color: #fff;
}
.box_list_active1
{
	background-color: #333 !important;
    width: 40px;
    height: 50px;
    margin: 0 auto;
}
.box_list_active1:after {
    content: '';
    position: absolute;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    border-top: 10px solid #333 !important;
    left: 18px;
    top: 47px;
}
</style>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h6 class="modal-title" id="myModalLabel">
		<svg version="1.1" id="svg-cart" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 20" style="enable-background:new 0 0 24 20;" xml:space="preserve">
	    <path class="st0" d="M23.5,0h-2.6c-0.5,0-0.9,0.4-1,0.9L18.1,14H3.5C3.2,14,3,13.8,3,13.5S3.2,13,3.5,13h12c0.8,0,1.5-0.7,1.5-1.5
	        S16.3,10,15.5,10h-13C2.2,10,2,9.8,2,9.5S2.2,9,2.5,9H16c0.8,0,1.5-0.7,1.5-1.5S16.8,6,16,6H1.5C1.2,6,1,5.8,1,5.5S1.2,5,1.5,5h16
	        C17.8,5,18,4.8,18,4.5S17.8,4,17.5,4h-16C0.7,4,0,4.7,0,5.5S0.7,7,1.5,7H16c0.3,0,0.5,0.2,0.5,0.5S16.3,8,16,8H2.5
	        C1.7,8,1,8.7,1,9.5S1.7,11,2.5,11h13c0.3,0,0.5,0.2,0.5,0.5S15.8,12,15.5,12h-12C2.7,12,2,12.7,2,13.5S2.7,15,3.5,15h14.6
	        c0.5,0,0.9-0.4,1-0.9L20.9,1h2.6C23.8,1,24,0.8,24,0.5S23.8,0,23.5,0z M6,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S7.1,16,6,16z
	         M6,19c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S6.6,19,6,19z M17,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S18.1,16,17,16z M17,19
	        c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S17.6,19,17,19z"/>
	    </svg>
		Your Cart
	</h6>
</div>
<div class="modal-body">
	<?php
		$total = '';
		if(isset($carts)) {
	        foreach ($carts as $key => $cart) {
	            // pr($cart);
	            $total += $cart['unit_price'];

	            $pro = App\Product::select('product_name', 'type')
	                    ->where('id', $cart['product_id'])
	                    ->first();

                $proImg = App\ProductImages::select('id', 'path')
	                    ->where('product_id', $cart['product_id'])
	                    ->where('featured', 1)                
	                    ->first();
                $featuredImage = (!empty($proImg['path'])) ? url('product-images/'.$proImg['path']) : url('img/product-thumb.jpg');
	            ?>
					<div class="row bdr-b">
						<div class="col-sm-12 product-details pb15">
							<button type="button" class="close removeFromCart" title="Remove this item from cart" data-toggle="tooltip" data-placement="left" data-key="{{ $key }}"><span>&times;</span></button>
							<img src="{{ $featuredImage }}" width="80px" height="55px" alt="">
							<span class="help-block">{{ $pro->type }}</span>
							<h5>{{ $pro->product_name }}</h5>
						</div>

						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-9">
									<?php
										$pPrice = App\Price::where('product_id', $cart['product_id'])->get();
									?>
									{{-- class="js-select-wos" --}}
									<div class="row view_cart">
										@foreach($pPrice as $key => $varient)
												<?php
												$selected = '';
												if($varient->id == $cart['variant_id']) {
													$selected = 'box_list_active1';
												} else {
													$selected = '';
												}
											?>
											<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 box1" data-product="{{$varient->product_id}}" data-varient="{{$varient->id}}" data-price="{{$varient->unit_price}}" data-unit="{{$varient->quantity_per_unit}}">
												<div class="box_list1 {{$selected}}"  data-varient="{{$varient->id}}">
													<h4>{{$varient->quantity_per_unit}}</h4>
												</div>
											</div>
										<!-- <br/>----------- {{$varient->id}}-- 2-- {{$cart['variant_id']}}  <br/> -->
										@endforeach
									</div>
								</div>
								<div class="col-sm-3">
									<div class="priceinfo">
										 $ {{ $cart['unit_price'] }}	
									</div>
								</div>									
							</div>
						</div>


						{{-- <div class="row view_cart">
							<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 box1">
								<div class="box_list1 box_list_active1">
									<h4>1g</h4>
								</div>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 box1">
								<div class="box_list1">
									<h4>1g</h4>
								</div>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 box1">
								<div class="box_list1">
									<h4>1g</h4>
								</div>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 box1">
								<div class="box_list1">
									<h4>1g</h4>
								</div>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 box1">
								<div class="box_list1">
									<h4>1g</h4>
								</div>
							</div>
						</div> --}}		
					</div>
	            <?php
	        }
		} else {
	    	echo '<h1>Cart is empty.</h1>';
	    }
	?>
	<div class="subtotals">
		<label>SUBTOTAL</label>
		${{ $total }}
	</div>
</div>
<div class="modal-footer">
	<a href="{{ url('cart') }}" id="checkout" class="btn btn-cart-payment btn-block">Checkout</a>
	<button type="button" class="btn btn-cart-link btn-block" data-dismiss="modal">Continue Shopping</button>
</div>