<div id="filterNav" class="filterSidebar">
	<div class="filterOptions">
		<span class="closebtn" onclick="closeFilterNav()">&times;</span>

		<div class="filterResults">
			<a href="#" onclick="openFilterNav()"></a>
			<?php 
                echo embed_svg(
                    $class = 'filter-icon pull-left', 
                    $svgurl = 'img/filter-icon.svg', 
                    $height = '24px', 
                    $width = '24px', 
                    $link = ''
                ); 
            ?>
			Filter Results
		</div>

		<input type="hidden" id="groups" />
		<input type="hidden" id="viewAll" value="1"/>
		<input type="hidden" id="options" />

		<div class="disp-dropdown">
			<p>Dispensary</p>
			<select class="js-select-ws" id="dispensary" data-placeholder="Select Dispensary">
				<option value=""></option>
				<?php
					$groups = App\Groups::where('type', 'Dispensary')->get();
					pr($groups);
					foreach ($groups as $key => $group) {
						echo '<option value="'.$group->id.'">'.$group->name.'</option>';
					}
				?>	
			</select>
		</div>

		<div class="options">
			<div class="checkbox-x">
		  		<input type="checkbox" class="viewall" value="1" id="checkboxInput" name="viewall" checked="true" />
			  	<label for="checkboxInput">
			  		<span id="ico"></span>
			  		View All
			  	</label>
		  	</div>			
		</div>

		<div class="list-options">
			<?php
				$options = [
					'Sativa' => 'Sativa',
					'Indica' => 'Indica',
					'Hybrid' => 'Hybrid',
					'Extract' => 'Extract',
					'High Cbd' => 'High-Cbd',
					'Flowers' => 'Flowers',
					'Pre Rolls' => 'Pre-Rolls',
					'Edibles' => 'Edibles',
					'Concentrates' => 'Concentrates',
					'Cartridges' => 'Cartridges',
					'Topicals' => 'Topicals',
					'Grow' => 'Grow',
					'Gear' => 'Gear'
				];
			?>
			<ul class="list-unstyled">
				@foreach ($options as $key => $value)
					<li>
						<div class="checkbox-x">
					  		<input type="checkbox" class="opt" value="{!! str_slug($value) !!}" id="checkboxInput_{!! $value !!}" name="opt[]" />
						  	<label for="checkboxInput_{!! $value !!}">
						  		<span id="ico"></span>
						  		{!! $key !!}
						  	</label>
					  	</div>	
					</li>	
				@endforeach
					<li class="visible-xs visible-sm">&nbsp;</li>
					<li class="visible-xs visible-sm">&nbsp;</li>
			</ul>
		</div>		
	</div>
</div>