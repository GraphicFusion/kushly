@extends('layouts.master')

@section('content')
<div class="content-pages">
	<div class="container">
		<div class="row is-flex">
<!--			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div class="sidebar">
					<ul class="list-unstyled">
						<?php $pages = App\Pages::get(); ?>
						@foreach ($pages as $key => $pg)
							<li <?php echo navActive($pg->slug); ?>>
								<a href="{{ url('/page/'.$pg->slug) }}">{{ $pg->page_title }}</a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
-->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-white">
				<div class="page-header">
					<h1>{!! $page->page_title !!}</h1>
				</div>
				<div class="page-content">
					@if($page->slug == 'contact-kushly')
						
						{!! Form::open(['url'=>'/page/contact-kushly','method' => 'post', 'class' => 'form', 'id' => 'contactform']) !!}
							<div class="form-group {{ $errors->has('first_name') ? 'has-error' :'' }}">
							    {!! Form::label('first_name', 'First Name') !!}
							    {!! Form::text('first_name', '', ['class' => 'form-control']) !!}
							    {!! $errors->first('first_name','<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-group {{ $errors->has('last_name') ? 'has-error' :'' }}">
							    {!! Form::label('last_name', 'Last Name') !!}
							    {!! Form::text('last_name', '', ['class' => 'form-control']) !!}
							    {!! $errors->first('last_name','<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
							    {!! Form::label('email', 'E-Mail Address') !!}
							    {!! Form::text('email', '', ['class' => 'form-control']) !!}
							    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
							</div>
							
							<div class="form-group {{ $errors->has('phone_no') ? 'has-error' :'' }}">
							    {!! Form::label('phone_no', 'Phone No') !!}
							    {!! Form::text('phone_no', '', ['class' => 'form-control']) !!}
							    {!! $errors->first('phone_no','<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-group {{ $errors->has('message') ? 'has-error' :'' }}">
							    {!! Form::label('Message', 'Message') !!}
							    {!! Form::textarea('message', '', ['class' => 'form-control ']) !!}
							    {!! $errors->first('message','<span class="help-block">:message</span>') !!}
							</div>
						    {!! Form::submit('Submit', ['class'=>'btn btn-primary-outline']) !!}
						{!! Form::close() !!}

					@else
						{!! $page->page_content !!}
					@endif
				</div>

			</div>
		</div>
	</div>
</div>
@endsection

@if(!empty($page->custom_css))
	@section('customCss')
		<style type ="text/css">
			{{ $page->custom_css }}
		</style>
	@endsection
@endif