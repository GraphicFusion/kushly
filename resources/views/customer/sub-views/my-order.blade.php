<div class="container">
	<div class="row">
		<div class="col-sm-12">

			<div class="order-details">
					
				<?php
				$orders = App\Order::where('user_id', getUserId())->orderBy('id','desc')->get();
				?>
				@foreach ($orders as $key => $order)
					<div class="row">
						<div class="col-md-5 col-sm-8 col-xs-6">
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="box">
										<div class="title">
											Order Date/Time
										</div>
										<div class="matter">
											<p>{!! $order->timestamp_order_made !!}</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="box">
										<div class="title">
											Delivery Status
										</div>
										<div class="matter">
											<?php
												switch ($order->status) {
													case 1:
														echo '<p>Order Placed</p>';
														break;
													case 2:
														echo '<p>Allocated</p>';
														break;
													case 3:
														echo '<p>Rejected</p>';
														break;
													case 4:
														echo '<p>Failure</p>';
														break;
													case 5:
														echo '<p>Accepted</p>';
														break;
													case 6:
														echo '<p>Delivered</p>';
														break;
													case 7:
														echo '<p>Accepted by dispensary</p>';
														break;
													case 8:
														echo '<p>Pickup at dispensary</p>';
														break;
												}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-2 col-md-push-5 col-sm-4 col-xs-6">
							<div class="box">
								<div class="matter">
									<ul class="list-unstyled list-action">
					  					<li>
					  						<form method="post" id="addtocart_<?php echo $order->id; ?>">
							                    {{ csrf_field() }}
												<?php
													$orderProducts = $order->orderProducts;
												?>
												<input type="hidden" name="locations_id" value="{{ $order->locations_id }}">

												@foreach($orderProducts as $key => $product)
													@php
														// Get Variant ID
														$vid = App\Price::select('id')->where('quantity_per_unit', $product->quantity_per_unit)->where('product_id', $product->product_id)->get()->first();
													@endphp

													<input type="hidden" name="product_id[]" value="{{ $product->product_id }}"/>
													<input type="hidden" name="unit_price[]" value="{{ $product->unit_price }}" />
													<input type="hidden" name="variant_id[]" value="{{ $vid->id }}" />
													<input type="hidden" name="quantity_per_unit[]" value="{{ $product->quantity_per_unit }}" />
													<input type="hidden" name="quantity[]" value="{{ $product->quantity }}" />
													<input type="hidden" name="user_id[]" value="<?php echo getUserId(); ?>">
													
												@endforeach

												@if(Session::get('repeatCart') == 'active')
													<button type="button" title="Repeat order in cart already." data-toggle="tooltip" data-placement="top" class="btn btn-link" disabled="disabled">Repeat Order <span class="ico">&#8250;</span></button>
												@else
													<button type="button" class="btn btn-link">Repeat Order <span class="ico">&#8250;</span></button>
												@endif
											</form>

					  					<li><a href="{{ url('/customer/order/'.$order->id) }}" class="btn btn-link">View Details <span class="ico">&#8250;</span></a></li>
					  				</ul>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-md-pull-2 col-sm-12 col-xs-12">
							<div class="row">
								<div class="col-lg-7 col-md-7 col-sm-7">
									<div class="box">
										<div class="title">
											Products Ordered
										</div>
										<div class="matter">
											<div class="order-info">
												<?php
													$orderProducts = $order->orderProducts;
												?>
												@foreach($orderProducts as $key => $product)
													@php
														$pro = App\Product::select('product_name')->where('id', $product->product_id)->get()->first();
													@endphp
													<div class="product">
									  					<span>{{ $product->quantity_per_unit.'($'.$product->unit_price.')' }}</span>
									  					<p>
															{!! $pro->product_name !!}
														</p>
								  					</div>
												@endforeach
							  				</div>
										</div>
									</div>
								</div>
								<div class="col-lg-5 col-md-5 col-sm-5">
									<div class="box">
										<div class="title">
											Location
										</div>
										<div class="matter">
											<?php
												$loc = $order->locations;
											?>
											<p>
												{!! $loc->street_address.', '.$loc->city.', '.$loc->state.', '.$loc->zip_code !!}
							  				</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				{{-- {{ $orders->links() }} --}}
			</div>
		</div>
	</div>
</div>