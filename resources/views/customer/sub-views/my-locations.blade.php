<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<a href="{{ url('/customer/location/add') }}" class="btn btn-primary btn-disp">Add New Location</a>
		</div>
		<div class="col-sm-6">
			<div class="form-search dtb btn-disp-input">
				<div class="form-group">
			        <input type="text" class="form-control" id="search_dtable" placeholder="Search for...">
			        <button type="button" class="btn btn-default">
			        	<?php 
			                echo embed_svg(
			                    $class = '', 
			                    $svgurl = 'img/search-icon.svg', 
			                    $height = '20px', 
			                    $width = '20px', 
			                    $link = ''
			                ); 
			            ?>
			        </button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-12">

			<table class="table table-hover" id="dTableLocations">
				<thead>
					<tr>
						<th>ID</th>
						<th width="20%">Street Address</th>
						<th>City</th>
						<th>State</th>
						<th>Zip Code</th>
						<th>Type</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$locations = App\User::find(Sentinel::getUser()->id)->locations()->withTrashed()->get();
					?>
					@foreach($locations as $loc)
						<tr>
							<td>{{ $loc->id }}</td>
							<td>{{ $loc->street_address }}</td>
							<td>{{ $loc->city }}</td>
							<td>{{ $loc->state }}</td>
							<td>{{ $loc->zip_code }}</td>
							<td>{{ $loc->type }}</td>
							<td class="text-center">
								@if($loc->trashed())
									<a href="javascript:void(0)" class="text-success po-restore" title="Restore" data-toggle="tooltip" data-placement="top"><i class='glyphicon glyphicon-check'></i></a>
								@else
									<a href="javascript:void(0)" class="text-danger po" title="Delete" data-toggle="tooltip" data-placement="top"><i class='glyphicon glyphicon-remove'></i></a>
								@endif

								<a href="{{ route('location.edit', $loc->id) }}" class="text-info loc-edit" title="Edit" data-toggle="tooltip" data-placement="top"><i class='glyphicon glyphicon-pencil'></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>

		</div>
	</div>
</div>

