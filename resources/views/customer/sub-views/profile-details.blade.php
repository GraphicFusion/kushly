<?php
use App\Identification;
?>
<div class="container">
	<div class="row">
		<div class="col-sm-4">
			<div class="thumbnail">
				<div class="img-wrap">
					<?php
						$user_image = Sentinel::findById(getUserId())->profile_pic;
						$user_image = (!empty($user_image)) ? URL::to('/profile-pics/'.$user_image) : 'http://placehold.it/300x300?text=No Image Available';
					?>
					<img src="{!! $user_image !!}" class="img-circle" alt="">
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon picForm',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = 'javascript:void(x)'
		                ); 
		            ?>
					
					<div class="well" id="uploadPicture">
						<a href="javascript:void(0)" class="btn btn-primary openForm">Upload Picture</a>

						<form action="{{ url('/user/profile/image/del') }}" method="post" id="delUserImage">
                            {{ csrf_field() }}
                            <a class="btn btn-primary-outline" href="#" onclick="document.getElementById('delUserImage').submit()">Delete</a>
                        </form>

						<a href="#x" class=""></a>

			            {!! Form::open(['url' => 'user/profile/image', 'method' => 'post', 'class' => 'form', 'id' => 'profileImageFrom', 'enctype'=>'multipart/form-data']) !!}
			            	@if (count($errors) > 0)
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							<div class="form-group">
								{!! Form::hidden('id', getUserId()) !!}
								<input type="file" name="profile_pic" id="profile_pic" class="inputfile" />
								<label for="profile_pic">JPG, GIF or PNG. Max size 800kb</label>
							</div>
							{!! Form::submit('Upload Now', ['class'=>'btn btn-primary']) !!}
							{!! Form::button('Cancel', ['class'=>'btn btn-primary-outline closeForm']) !!}
						{!! Form::close() !!}
					</div>
				</div>

				<div class="caption">
					<ul class="list-unstyled list-actions">
						<li><a class="btn btn-primary btn-tab" href="#my-orders">My Orders</a></li>
						<li><a class="btn btn-primary btn-tab" href="#my-locations">My Locations</a></li>
						<li>
							<form action="{{ url('/logout') }}" method="post" id="formLogout">
			                    {{ csrf_field() }}
			                    <a href="#" onclick="document.getElementById('formLogout').submit()">Sign-Out</a>
			                </form>
						</li>
					</ul>
				</div>
			</div>

		</div>
		<div class="col-sm-7 col-sm-offset-1">
			<?php $user = Sentinel::getUser(); 
				  $identification = App\Identification::where('user_id',$user->id)->first();
			?>
			<ul class="list-unstyled user-details">
				<li class="customer_name">
					<label>Name</label>
					<p>{!! $user->first_name.' '.$user->last_name !!}</p>
					<?php
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
		            <div class="ajaxform"></div>
				</li>
				<li class="customer_phone">
					<label>Phone</label>
					<p>{!! $user->phone_no !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
		            <div class="ajaxform"></div>
				</li>
				<li class="customer_email">
					<label>Email</label>
					<p>{!! $user->email !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
		            <div class="ajaxform"></div>
				</li>
				<li class="customer_birthday">
					<label>Birthday</label>
					<p>{!! $user->birthday !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
		            <div class="ajaxform"></div>
				</li>
				<?php
					$userId = Sentinel::findById(getUserId())->id;
				?>

				<li class="customer_state_id_front">
					<label>State Id Front</label>
					<p>{!! Html::image(Identification::getVerificationImage($userId,'state_id_front_path'),'',['style'=>"width:300px;height:200px"]) !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
		            <div class="ajaxform"></div>
				</li>


				<li class="customer_state_id_back">
					<label>State Id Back</label>
					<p>{!! Html::image(Identification::getVerificationImage($userId,'state_id_back_path'),'',['style'=>"width:300px;height:200px"]) !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
		            <div class="ajaxform"></div>
				</li>

				<li class="customer_medical_card_front">
					<label>Medical Card Front</label>
					<p>{!! Html::image(Identification::getVerificationImage($userId,'med_card_front_path'),'',['style'=>"width:300px;height:200px"]) !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
		            <div class="ajaxform"></div>
				</li>

				<li class="customer_medical_card_back">
					<label>Medical Card Back</label>
					<p>{!! Html::image(Identification::getVerificationImage($userId,'med_card_back_path'),'',['style'=>"width:300px;height:200px"]) !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
		            <div class="ajaxform"></div>
				</li>
				
				{{-- <li class="payment_opt"> --}}
				<li class="payment_opt" style="display:none;">
					@php
						$anet = App\User::getAnetProfile();
					@endphp
					@if($anet)
						<div class="row">
							<div class="col-md-6">
								<a href="javascript:void()" data-toggle="modal" data-target="#modalAnetProfileUpdate">Update Authorize.net Profile</a>
							</div>
							<div class="col-md-6">
								<a href="javascript:void()" data-toggle="modal" data-target="#modalAnetProfileDelete">Delete Authorize.net Profile</a>
							</div>
						</div>
					@else
						<div class="row">
							<div class="col-md-12">
								<a href="javascript:void()" data-toggle="modal" data-target="#modalPaymentOptions">Create Authorize.net Profile</a>
							</div>
						</div>
					@endif
				</li>
				
				<li class="credit_card_info">
					<label>Credit Card</label>	
					<p>{!! (!empty($user->cardnumber)) ? 'xxxx-xxxx-xxxx-'.substr($user->cardnumber, -4) : 'xxxx-xxxx-xxxx-0000' !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
					<div class="ajaxform"></div>
				</li>

				<li class="bank_info">
					<label>Bank Details</label>	
					<p>{!! (!empty($user->bank)) ? $user->bank : 'xxx-xxx-xxx-0' !!}</p>
					<?php 
		                echo embed_svg(
		                    $class = 'pencil-icon',
		                    $svgurl = 'img/pencil-icon.svg',
		                    $height = '16px', 
		                    $width = '16px', 
		                    $link = makeURL('/ajax/customer/profile/update')
		                ); 
		            ?>
					<div class="ajaxform"></div>
				</li>


			</ul>
		</div>
	</div>
</div>

{{-- Update authorize.net profiles --}}
<div class="modal fade" id="modalAnetProfileUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
  	{!! Form::open(['url'=>'/customer/update/anet/profile', 'method'=>'post', 'name'=>'delpayopt', 'id'=>'delpayout']) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Authorize.net Customer & Payment Profile</h4>
      </div>
      <div class="modal-body">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, temporibus, labore! Nulla qui soluta repudiandae veniam nihil voluptate consequatur, necessitatibus explicabo tempore libero ex quae. Reprehenderit repellendus voluptates mollitia. Suscipit?</p>
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary-outline" data-dismiss="modal">Close</button>
        <button type="button" id="upPaymentOpt" class="btn btn-primary">Save changes</button>
      </div>
	{!! Form::close() !!}
    </div>
  </div>
</div>

@php
	$anet = App\User::getAnetProfile();
@endphp
@if($anet!='')

{{-- Modal for delete authorise.net profile --}}
<div class="modal fade" id="modalAnetProfileDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
  	{!! Form::open(['url'=>'/customer/delete/anet/profile', 'method'=>'post', 'name'=>'delpayopt', 'id'=>'delpayout']) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete Authorize.net Customer & Payment Profile</h4>
      </div>
      <div class="modal-body">

		{!! Form::hidden('profileID', $anet->anet_customer_profile_id, ['id' => 'profileID']) !!}
		{!! Form::hidden('paymentProfileID', $anet->anet_customer_payment_profile_id, ['id' => 'paymentProfileID']) !!}
		{!! Form::hidden('user_id', $anet->id, ['id' => 'user_id']) !!}
	
		<label class="radio-inline">
			{!! Form::radio('anet_profile', 'completeProfile', true) !!} Complete Authorise.net Profile
		</label>
		<label class="radio-inline">
			{!! Form::radio('anet_profile', 'paymentProfileOnly') !!} Only Payment Profile
		</label>

	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary-outline" data-dismiss="modal">Close</button>
        <button type="submit" id="delPaymentOpt" class="btn btn-primary">Save changes</button>
      </div>
	{!! Form::close() !!}
    </div>
  </div>
</div>
@endif

{{-- Modal for new authorize.net profile --}}
<div class="modal fade" id="modalPaymentOptions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      {!! Form::open(['url'=>'/customer/create/anet/profile', 'method'=>'post', 'name'=>'payopt', 'id'=>'payout']) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Payment Options</h4>
      </div>
      <div class="modal-body">
      	
      	<div class="row">
      		<div class="col-md-12">
				<div class="form-group">
					{!! Form::label('username', 'Customer Name') !!}
					{!! Form::text('username', '', ['class' => 'form-control']) !!}
					<span class="error"></span>
				</div>
      		</div>
      	</div> 
		
		{{-- <div class="row m-b-20">
			<div class="col-md-12 text-center">
				<div class="btn-group btn-opt" data-toggle="buttons">
				  <label class="btn btn-primary active">
				    <input type="radio" name="options" id="card" autocomplete="off" value="card" checked> Credit Card
				  </label>
				  <label class="btn btn-primary">
				    <input type="radio" name="options" id="bank" autocomplete="off" value="bank"> Bank Details
				  </label>
				</div>		
			</div>
		</div> --}}

      	<div class="row" id="ccinfo">
		    <div class="col-md-6">
				<div class="form-group">
					{!! Form::label('cardnumber', 'Card Number') !!}
					{!! Form::text('cardnumber', '', ['class' => 'form-control', 'onkeyup'=>'validateCard(document.payopt.cardnumber)']) !!}
					<span id="stat" class="badge badge-success"></span>
					<span class="error"></span>
				</div>
		    </div>
		    <div class="col-md-3">
				<div class="form-group">
					{!! Form::label('expdate', 'Expiration Date') !!}
					{!! Form::text('expdate', '', ['class' => 'form-control ll-skin-melon', 'id'=>'datepicker','required' => 'true']) !!}
					<span class="error"></span>
				</div>    
		    </div>
		    <div class="col-md-3">
		      	<div class="form-group">
		        	{!! Form::label('cardcode', 'Card Code (CVV)') !!}
		        	{!! Form::text('cardcode', '', ['class' => 'form-control']) !!}
		        	<span class="error"></span>
		      	</div>
		    </div>
		</div>

		{{-- <div class="row" id="bankinfo" style="display: none;">
			<div class="col-md-12">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque in obcaecati voluptatem debitis, harum magnam unde suscipit similique, nemo, corrupti animi culpa molestias voluptates. Perferendis facere odit iure impedit corporis.</p>
			</div>
		</div>
		 --}}
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary-outline" data-dismiss="modal">Close</button>
        <button type="button" id="addPaymentOpt" class="btn btn-primary">Save changes</button>
      </div>
	{!! Form::close() !!}
    </div>
  </div>
</div>