@extends('layouts.master')

@section('content')

	<div class="profile-tabs">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a>
						</li>
						
						@if(Sentinel::findById(getUserId())->orders->count() > 0)
							<li role="presentation">
								<a href="#my-orders" class="customorder" aria-controls="my-orders" role="tab" data-toggle="tab">My Orders</a>
							</li>
						@endif
	
						<li role="presentation">
							<a href="#my-locations" class="customlocation" aria-controls="my-locations" role="tab" data-toggle="tab">My Locations</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="settings">
				@include('customer.sub-views.profile-details')
			</div>
			@if(Sentinel::findById(getUserId())->orders->count() > 0)
				<div role="tabpanel" class="tab-pane" id="my-orders">
					@include('customer.sub-views.my-order')
				</div>
			@endif
			<div role="tabpanel" class="tab-pane" id="my-locations">
				@include('customer.sub-views.my-locations')
			</div>
		</div>
	</div>

@endsection

@section('customCss')
{{-- <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"> --}}
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
<style>
#profileImageFrom,
#uploadPicture,
.closeForm { 
	display: none; 
}
#uploadPicture {
    margin-top: 25px;
}
#profileImageFrom {
	margin-top: 20px;
}
</style>
@endsection

@section('customJs')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
{!!Html::script('js/fileupload.js')!!}
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> --}}
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('[data-toggle="tooltip"]').tooltip();

		$('.picForm .object-link').click(function(event) {
			$('#uploadPicture').toggle('slow');
		});

		var openform = $('#uploadPicture .openForm');
		var closeform = $('#uploadPicture .closeForm');
		var uploadform = $('#profileImageFrom');

		openform.click(function(event) {
			uploadform.slideDown('slow');
			$(this).hide('slow');
			closeform.show();
		});
		closeform.click(function(event) {
			uploadform.slideUp('slow');
			$(this).hide('slow');
			openform.show();
		});

		// Add hashtag in url on bootstrap tabs
		var hash = window.location.hash;
		hash && $('ul.nav-tabs a[href="' + hash + '"]').tab('show');

		$('ul.nav-tabs a').click(function (e) {
		    $(this).tab('show');
		    var scrollmem = $('body').scrollTop();
		    window.location.hash = this.hash;
		    $('html,body').scrollTop(scrollmem);
		});

		$('.list-actions li a.btn-tab').click(function (e) {
			var href = $(this).attr('href');
			if(href == '#my-orders'){ 
				$('.customorder').click(); 
			}
			if(href == '#my-locations'){
				$('.customlocation').click();
			}
			$("html, body").animate({ scrollTop: 0 }, "slow");
  		});

  		// Ajax call to open inline edit form
	    function callAjax(type, url, data, target, hideelem) {
	    	$.ajaxSetup({
			  	headers: {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  	}
			});
	    	$.ajax({
	    		url: url,
	    		type: type,
	    		data: data,
	    		success: function(data){
	                // console.log(data);
	                $(hideelem).hide();
	                $(target).append(data);
	            },
	            error: function(xhr, status, error){
	                console.log(xhr);
	                console.log(status);
	                console.log(error);
	            }
	    	});
	    }

	    $('.customer_name .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'name'}
	    	callAjax('GET', url, data, '.customer_name .ajaxform', '.customer_name .pencil-icon');
	    });

	    $('.customer_phone .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'phone'}
	    	callAjax('GET', url, data, '.customer_phone .ajaxform', '.customer_phone .pencil-icon');
	    });

	    $('.customer_email .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'email'}
	    	callAjax('GET', url, data, '.customer_email .ajaxform', '.customer_email .pencil-icon');
	    });

	    $('.customer_birthday .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'dob'}
	    	callAjax('GET', url, data, '.customer_birthday .ajaxform', '.customer_birthday .pencil-icon');
	    });

	    $('.customer_state_id_front .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'stateidfront'}
	    	callAjax('GET', url, data, '.customer_state_id_front .ajaxform', '.customer_state_id_front .pencil-icon');
	    });

	     $('.customer_state_id_back .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'stateidback'}
	    	callAjax('GET', url, data, '.customer_state_id_back .ajaxform', '.customer_state_id_back .pencil-icon');
	    });

	     $('.customer_medical_card_front .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'medicalcardfront'}
	    	callAjax('GET', url, data, '.customer_medical_card_front .ajaxform', '.customer_medical_card_front .pencil-icon');
	    });

	      $('.customer_medical_card_back .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'medicalcardback'}
	    	callAjax('GET', url, data, '.customer_medical_card_back .ajaxform', '.customer_medical_card_back .pencil-icon');
	    });

	    $('.credit_card_info .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'ccinfo'}
	    	callAjax('GET', url, data, '.credit_card_info .ajaxform', '.credit_card_info .pencil-icon');
	    });

	    $('.bank_info .object-link').click(function(e){
	    	e.preventDefault(); 
	    	var url = $(this).attr('href');
	    	var data = {id: '{{ getUserId() }}', action: 'bankinfo'}
	    	callAjax('GET', url, data, '.bank_info .ajaxform', '.bank_info .pencil-icon');
	    });

	    var oTable = $('#dTableLocations').DataTable({
			"sDom": '<"top">rt<"bottom"p><"clear">',
			"stateSave": true,
			"lengthMenu": [ 10, 25, 50, 75, 100 ],
		    "iDisplayLength": 10,
		    "aaSorting": [[0, 'asc']],
		    "columnDefs": [ 
		    	{ "targets": -1, "orderable": false }
	    	],
		    "destroy": true,
			"language": {
			    "oPaginate": {
		          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
		          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
		        }
		    },
		    "createdRow": function( row, data, dataIndex ) {
		    	$(row).attr('id', 'row-'+data[0]);
		  	}
	  	});

		$('#search_dtable').keyup(function(){
	        oTable.search($(this).val()).draw();
	    });

		$('#dTableLocations tbody').on( 'click', '.po', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
        	$('a').not(this).popover('hide');
	        $(this).popover({
	        	container: 'body',
	        	trigger:'manual',
	        	html: true,
	        	placement: 'left',
	        	title:'Are you sure!!!',
	        	content:'<a class="btn btn-sm btn-danger po-delete" href="{{ url('/customer/locations/delete') }}" data-id="'+data[0]+'">Yes I`m sure</a> <button class="btn btn-sm po-close">No</button>'
	        });
	        $(this).popover('show');
	    });

	    $('#dTableLocations tbody').on( 'click', '.po-restore', function () {
	        var data = oTable.row( $(this).parents('tr') ).data();
	        $('a').not(this).popover('hide');
	        $(this).popover({
	        	container: 'body',
	        	trigger:'manual',
	        	html: true,
	        	placement: 'left',
	        	title:'Restore this group!!!',
	        	content:'<a class="btn btn-sm btn-info po-restored" href="{{ url('/customer/locations/restored') }}" data-id="'+data[0]+'">Yes, Restore this...</a> <button class="btn btn-sm po-close">No</button>'
	        });
	        $(this).popover('show');
	    });

	});

    $(document).on("click", '.btn-link', function(e) {
    	var formID = $('#'+$(this).parent('form').attr('id'));
    	var formData = formID.serialize();
    	$.ajax({
    		url: '/repeatOrder',
    		type: 'POST',
    		data: formData,
    	})
    	.done(function(data) {
    		if(data.message == 'success') {
				$('.order-details').find('.btn-link').attr({
					'title': 'Repeat order in cart already.',
					'data-toggle': 'tooltip',
					'data-placement': 'top',
					'disabled': 'disabled'
				});
				$('.cart-counter').text(data.count);
				$( "body" ).prepend( '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Repeat order in process, please complete you cart process.</div>' );
				window.location.href = "{{ url('/cart') }}";

			}
    		if(data.message == 'danger') {
				$( "body" ).prepend( '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Repeat order in processing failed, please try again.</div>' );
    		}
    	});
    });

	$(document).on("click", '.po-delete', function (e) {
		e.preventDefault();
		$.ajaxSetup({
		  	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		});
		$.ajax({
			url: $(this).attr('href'),
			type: 'POST',
			data: {
				id: $(this).data('id')
			},
		}).done(function(data) {
			// console.log(data);
			if(data.message == 'success') {
				$('.po').popover('hide');
				// $('#dTableLocations').DataTable().ajax.reload(null, false);
				window.location.reload();
			}
		});
	});

	$(document).on("click", '.po-restored', function (e) {
		e.preventDefault();
		$.ajaxSetup({
		  	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		});
		$.ajax({
			url: $(this).attr('href'),
			type: 'POST',
			data: {
				id: $(this).data('id')
			},
		}).done(function(data) {
			// console.log(data);
			if(data.message == 'success') {
				$('.po-restore').popover('hide');
				// $('#dTableLocations').DataTable().ajax.reload(null, false);
				window.location.reload();
			}
		});
	});
	/**
	 * Update Customer
	 */
	function updateRecordAJAX(id, formid, statuswrapper, formwrapper, icon) {
		$(document).on('click', id, function(){
			$.ajaxSetup({
			  	headers: {
			    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  	}
			});
			$.ajax({
				url: $(formid).attr('action'),
				type: 'POST',
				data: $(formid).serialize(),
			})
			.done(function(msg) {
				// console.log(msg);
				switch(msg.action) {
					case 'name':
						$(statuswrapper).text(msg.firstName+' '+msg.lastName);
						break;
					case 'phone':
						$(statuswrapper).text(msg.phone_no);
						break;
					case 'email':
						$(statuswrapper).text(msg.email);
						break;
					case 'dob':
						$(statuswrapper).text(msg.birthday);
						break;
					case 'ccinfo':
					 	var cn = msg.cardnumber.substr(msg.cardnumber.length - 4);
						$(statuswrapper).text('xxxx-xxxx-xxxx-'+cn);
						break;
					case 'bankInfo':
						$(statuswrapper).text(msg.bank);
						break;
					case 'stateIdFront':
						$(statuswrapper).replaceWith('<p><img src="'+msg.stateidfront+'" style="width:300px;height:200px"></p>');
						break;
					case 'stateIdBack':
					$(statuswrapper).replaceWith('<p><img src="'+msg.stateidback+'" style="width:300px;height:200px"></p>');
						break;
						$(statuswrapper).text(msg.stateidback);
						break;
					case 'medicalCardFront':
					$(statuswrapper).replaceWith('<p><img src="'+msg.medicalcardfront+'" style="width:300px;height:200px"></p>');
						break;
						$(statuswrapper).text(msg.bank);
						break;
					case 'medicalCardBack':
					$(statuswrapper).replaceWith('<p><img src="'+msg.medicalcardback+'" style="width:300px;height:200px"></p>');
						break;
						$(statuswrapper).text(msg.bank);
						break;
				}
				$(formwrapper).html('');
				$(icon).show();
			})
			.fail(function() {
				console.log("Error");
			});
		});
	}
	updateRecordAJAX('#updateName','#updateCustomerName','li.customer_name p','.customer_name .ajaxform','.customer_name .pencil-icon');

	updateRecordAJAX('#updatePhone','#updateCustomerPhone','li.customer_phone p','.customer_phone .ajaxform','.customer_phone .pencil-icon');

	updateRecordAJAX('#updateEmail','#updateCustomerEmail','li.customer_email p','.customer_email .ajaxform','.customer_email .pencil-icon');

	updateRecordAJAX('#updateDOB','#updateCustomerBirthday','li.customer_birthday p','.customer_birthday .ajaxform','.customer_birthday .pencil-icon');

	updateRecordAJAX('#updateInfo','#updateCCInfo','li.credit_card_info p','.credit_card_info .ajaxform','.credit_card_info .pencil-icon');

	updateRecordAJAX('#updateBankInfo','#updateBankInfoFrom','li.bank_info p','.bank_info .ajaxform','.bank_info .pencil-icon');

	updateRecordAJAX('#updateStateIdFront','#updateCustomerStateIdFront','li.customer_state_id_front p','.customer_state_id_front .ajaxform','.customer_state_id_front .pencil-icon');

	updateRecordAJAX('#updateStateIdBack','#updateCustomerStateIdBack','li.customer_state_id_back p','.customer_state_id_back .ajaxform','.customer_state_id_back .pencil-icon');

	updateRecordAJAX('#updateMedicalCardFront','#updateCustomerMedicalCardFront','li.customer_medical_card_front p','.customer_medical_card_front .ajaxform','.customer_medical_card_front .pencil-icon');
	
	updateRecordAJAX('#updateMedicalCardBack','#updateCustomerMedicalCardBack','li.customer_medical_card_back p','.customer_medical_card_back .ajaxform','.customer_medical_card_back .pencil-icon');

	function closeAjaxForm(id, formwrapper, icon) {
		$(document).on('click', id, function(){
			$(formwrapper).html('');
			$(icon).show();
		});		
	}
	closeAjaxForm('#cancelName','.customer_name .ajaxform','.customer_name .pencil-icon');
	closeAjaxForm('#cancelPhone','.customer_phone .ajaxform','.customer_phone .pencil-icon');
	closeAjaxForm('#cancelEmail','.customer_email .ajaxform','.customer_email .pencil-icon');
	closeAjaxForm('#cancelDOB','.customer_birthday .ajaxform','.customer_birthday .pencil-icon');

	closeAjaxForm('#cancelStateIdFront','.customer_state_id_front .ajaxform','.customer_state_id_front .pencil-icon');
	closeAjaxForm('#cancelStateIdBack','.customer_state_id_back .ajaxform','.customer_state_id_back .pencil-icon');
	closeAjaxForm('#cancelMedicalCardFront','.customer_medical_card_front .ajaxform','.customer_medical_card_front .pencil-icon');
	
	closeAjaxForm('#cancelMedicalCardBack','.customer_medical_card_back .ajaxform','.customer_medical_card_back .pencil-icon');

	closeAjaxForm('#cancelInfo','.credit_card_info .ajaxform','.credit_card_info .pencil-icon');
	closeAjaxForm('#cancelBankInfo','.bank_info .ajaxform','.bank_info .pencil-icon');


// $(document).on('click','.btn-opt', function(e) {
// 	e.preventDefault();
// 	var rdio = $(this).find('input[type="radio"]:checked').val();
// 	if(rdio == 'card') {
// 		$('#ccinfo').show('slow');
// 		$('#bankinfo').hide();
// 	} else if(rdio == 'bank') {
// 		$('#ccinfo').hide();
// 		$('#bankinfo').show('slow');
// 	}
// });

function validateCard(cardno) {
	var ae_regexp = /^(?:3[47][0-9]{13})$/; // Amercican Express
	var visa_regexp = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/; // Visa
	var mc_regexp = /^(?:5[1-5][0-9]{14})$/; // Master Card
	var d_regexp = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/; // Discover
	var din_regexp = /^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$/; // Diner
  	var jcb_regexp = /^(?:(?:2131|1800|35\d{3})\d{11})$/; // JCB

  	if(cardno.value.match(ae_regexp)) {
  		$('#stat').html('<span class="info">Amercican Express Card.</span>');
  	} else if(cardno.value.match(visa_regexp)) {
  		$('#stat').html('<span class="info">Visa Card.</span>');
  	} else if(cardno.value.match(mc_regexp)) {
  		$('#stat').html('<span class="info">Master Card.</span>');
  	} else if(cardno.value.match(d_regexp)) {
  		$('#stat').html('<span class="info">Discover Card.</span>');
  	} else if(cardno.value.match(din_regexp)) {
  		$('#stat').html('<span class="info">Diner Card.</span>');
  	} else if(cardno.value.match(jcb_regexp)) {
  		$('#stat').html('<span class="info">JCB Card.</span>');
  	} else {
  		$('#stat').html('<span class="error">Card no is not valid.</span>');
  	}
}

$('#modalPaymentOptions').on('shown.bs.modal', function () {
	var name = $('.customer_name p').text();
  	$('#username').val(name).attr('value', name);
});

$(document).on('click', '#addPaymentOpt', function(event) {
	event.preventDefault();
	var error = 0;

	if($('#username').val() == '') {
		error = 1;
		$('#username').parent('.form-group').addClass('has-error');
		$('#username').parent('.form-group').find('.error').text('Username is required.');
	} else {
		error = 0;
		$('#username').parent('.form-group').find('.error').text('');
		$('#username').parent('.form-group').removeClass('has-error');
	}

	if($('#cardnumber').val() == '') {
		error = 1;
		$('#cardnumber').parent('.form-group').addClass('has-error');
		$('#cardnumber').parent('.form-group').find('.error').text('Card Number is required.');
	} else {
		error = 0;
		$('#cardnumber').parent('.form-group').find('.error').text('');
		$('#cardnumber').parent('.form-group').removeClass('has-error');
	}

	if($('#datepicker').val() == '') {
		error = 1;
		$('#datepicker').parent('.form-group').addClass('has-error');
		$('#datepicker').parent('.form-group').find('.error').text('Expiration Date is required.');
	} else {
		error = 0;
		$('#datepicker').parent('.form-group').find('.error').text('');
		$('#datepicker').parent('.form-group').removeClass('has-error');
	}

	if($('#cardcode').val() == '') {
		error = 1;
		$('#cardcode').parent('.form-group').addClass('has-error');
		$('#cardcode').parent('.form-group').find('.error').text('Card code (cvv) is required.');
	} else {
		error = 0;
		$('#cardcode').parent('.form-group').find('.error').text('');$('.error').text('');
		$('#cardcode').parent('.form-group').removeClass('has-error');
	}

	if(error == 0) {
		$('#payout').submit();
	}
});

$(document).on('change','#stateIdFront',function(){
	var val = $('#stateIdFront').val();
	$('#heddinStateId').val(val);
});

function closeprogress(){
	$('.signupsubmit').show();
	setTimeout(function(){ $('.progress').next().css('width','0%').text(''); $('.progress').hide(); }, 1000);
}

$(document).on('click','.stateIdFrontFile', function(){
$('.stateIdFrontFile').fileupload({
    url: '/signup/customerfileupdate',
    headers: { 'X-XSRF-Token': $('meta[name="_token"]').attr('content'), },
    dataType: 'json',
    formData: { path:'' },
     dataType: 'json',
        done: function (e, data) {
    	console.log(data);
    	$('.sf_emsg').remove();
    	var iferror = data.result.files[0].error;
    	console.log(iferror);
    	if(iferror != undefined){
    		//alert(iferror); return false;
    		$('#stateIdFront').after('<em style="color:red;" class="sf_emsg">'+iferror+'</em>');
    		return false;
    	}
        var baseurl = document.location.origin;
        var src = baseurl+'/'+data.result.files[0].url;
        $('#heddinStateId').val(src);
        $('#stateIdFront').after('<a target="_blank" style="color:green;" href="'+src+'" class="sf_emsg">'+data.files[0].name+'</a>');
       console.log(src);
       $('#signupbutton').show();
    },
    error: function(e){
    	$('.sf_emsg').remove();
      console.log(e.responseText);
    }
}).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(document).on('change','#stateIdBack',function(){
	var val = $('#stateIdBack').val();
	$('#heddinStateId').val(val);
});


$(document).on('click','.stateIdBackFile', function(){
$('.stateIdBackFile').fileupload({
    url: '/signup/customerfileupdate',
    headers: { 'X-XSRF-Token': $('meta[name="_token"]').attr('content'), },
    dataType: 'json',
    formData: { path:'' },
     dataType: 'json',
        done: function (e, data) {
    	$('#signupbutton').hide();
    	console.log(data);
    	$('.sb_emsg').remove();
    	var iferror = data.result.files[0].error;
    	console.log(iferror);
    	if(iferror != undefined){
    		//alert(iferror); return false;
    		$('#stateIdBack').after('<em style="color:red;" class="sb_emsg">'+iferror+'</em>');
    		return false;
    	}
        var baseurl = document.location.origin;
        var src = baseurl+'/'+data.result.files[0].url;
        $('#heddinStateId').val(src);
        $('#stateIdBack').after('<a target="_blank" style="color:green;" href="'+src+'" class="sb_emsg">'+data.files[0].name+'</a>');
       console.log(src);
       $('#signupbutton').show();
    },
   error: function(e){
    	$('.sf_emsg').remove();
      console.log(e.responseText);
    }
}).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');
});



$(document).on('click','.medicalCardFrontFile', function(){
$('.medicalCardFrontFile').fileupload({
    url: '/signup/customerfileupdate',
    headers: { 'X-XSRF-Token': $('meta[name="_token"]').attr('content'), },
    dataType: 'json',
    formData: { path:'' },
     dataType: 'json',
        done: function (e, data) {
    	console.log(data);
    	$('.sb_emsg').remove();
    	var iferror = data.result.files[0].error;
    	console.log(iferror);
    	if(iferror != undefined){
    		//alert(iferror); return false;
    		$('#medicalCardFront').after('<em style="color:red;" class="sb_emsg">'+iferror+'</em>');
    		return false;
    	}
        var baseurl = document.location.origin;
        var src = baseurl+'/'+data.result.files[0].url;
        $('#heddinStateId').val(src);
        $('#medicalCardFront').after('<a target="_blank" style="color:green;" href="'+src+'" class="sb_emsg">'+data.files[0].name+'</a>');
       console.log(src);
    },
   error: function(e){
    	$('.sf_emsg').remove();
      console.log(e.responseText);
    }
}).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');
});



$(document).on('click','.medicalCardBackFile', function(){
$('.medicalCardBackFile').fileupload({
    url: '/signup/customerfileupdate',
    headers: { 'X-XSRF-Token': $('meta[name="_token"]').attr('content'), },
    dataType: 'json',
    formData: { path:'' },
     dataType: 'json',
        done: function (e, data) {
    	console.log(data);
    	$('.sb_emsg').remove();
    	var iferror = data.result.files[0].error;
    	console.log(iferror);
    	if(iferror != undefined){
    		//alert(iferror); return false;
    		$('#medicalCardBack').after('<em style="color:red;" class="sb_emsg">'+iferror+'</em>');
    		return false;
    	}
        var baseurl = document.location.origin;
        var src = baseurl+'/'+data.result.files[0].url;
        $('#heddinStateId').val(src);
        $('#medicalCardBack').after('<a target="_blank" style="color:green;" href="'+src+'" class="sb_emsg">'+data.files[0].name+'</a>');
       console.log(src);
    },
   error: function(e){
    	$('.sf_emsg').remove();
      console.log(e.responseText);
    }
}).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>
@endsection