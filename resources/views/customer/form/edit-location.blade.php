@extends('layouts.master')
@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('customer/locations/{id}'); ?>>
			<a href="{{ url('/customer/locations/{id}') }}">Edit Location</a>
		</li>
	</ul>
</nav>

<div class="bg-white p-t-30 p-b-30">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="locationsForm">
					<h5>Update Location</h5>
					{!! Form::open(['method' => 'post', 'class' => 'form', 'id' => 'locations']) !!}
						{!! Form::hidden('id', $loc->id) !!}
						<div class="form-group {{ $errors->has('zip_code') ? 'has-error' :'' }}">
						    {!! Form::label('zip_code', 'Zip Code') !!}
						    {!! Form::text('zip_code', $loc->zip_code, ['class' => 'form-control numbersOnly','readonly'=>true]) !!}
						    {!! $errors->first('zip_code','<span class="help-block">:message</span>') !!}
						</div>

						<div class="row">
							<div class="col-lg-6">
								<div class="form-group {{ $errors->has('city') ? 'has-error' :'' }}">
								    {!! Form::label('city', 'City') !!}
								    {!! Form::text('city', $loc->city, ['class' => 'form-control','readonly'=>true]) !!}
								    {!! $errors->first('city','<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group {{ $errors->has('state') ? 'has-error' :'' }}">
								    {!! Form::label('state', 'State') !!}
								    {!! Form::text('state', $loc->state, ['class' => 'form-control','readonly'=>true]) !!}
								    {!! $errors->first('state','<span class="help-block">:message</span>') !!}
								</div>			
							</div>
						</div>

						<div class="row">
							<div class="col-lg-9">
								<div class="form-group {{ $errors->has('street_address') ? 'has-error' :'' }}">
								    {!! Form::label('street_address', 'Street Address') !!}
								    {!! Form::text('street_address', $loc->street_address, ['class' => 'form-control']) !!}
								    {!! $errors->first('street_address','<span class="help-block">:message</span>') !!}
								    <span class="error"></span>
								</div>
							</div>
							<div class="col-lg-3">
								<a href="javascript:void()" id="updateMap" class="pull-right btn btn-primary-outline" style="margin-top: 26px;">Update Map</a>
							</div>
						</div>
						
						<div class="form-group">
						    {!! Form::label('country', 'Country') !!}
						    {!! Form::text('country', $loc->country, ['class' => 'form-control', 'disabled' => true]) !!}
						</div>
						<div class="form-group {{ $errors->has('type') ? 'has-error' :'' }}">
						    {!! Form::label('type', 'Type') !!}
							<select class="styleDropdown form-control" name="type" id="type">
						    	@php
						    		$type = ['home'=>'Home','office'=>'Office','public'=>'Public'];
						    		foreach ($type as $key => $value) {
						    			if($loc->type == $key) {
											echo '<option selected="selected" value="'.$key.'">'.$value.'</option>';
										} else {
											echo '<option value="'.$key.'">'.$value.'</option>';
										}
						    		}
						    	@endphp
							</select>
							{!! $errors->first('type','<span class="help-block">:message</span>') !!}
						</div>

						<div class="row" style="display: none;">
							<div class="col-lg-6">
								<div class="form-group">
								    {!! Form::label('lat', 'Latitude') !!}
									{!! Form::text('lat',$loc->lat,['id'=>'lat','class'=>'form-control','readonly'=>true]) !!}
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
								    {!! Form::label('lng', 'Longtitude') !!}
									{!! Form::text('lng',$loc->lng,['id'=>'lng','class'=>'form-control','readonly'=>true]) !!}
								</div>
							</div>
						</div>

						{!! Form::submit('Update Location', ['id'=>'editLocation','class'=>'btn btn-primary-outline']) !!}
					{!! Form::close() !!}
				</div>
			</div>

			<div class="col-md-6 col-sm-12 col-xs-12">
				<h5>Current location map.</h5>
				<p class="help-block">If map is not shown your exact location, you can drag the marker to exact location. Note that after dragging the map marker your filled location (street address) is updated with the new location.</p>
				<div id="gMap" style="width: 100%;height: 380px;"></div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('customJs')
<script src="https://maps.googleapis.com/maps/api/js?key={!! \Config::get('app.gmap') !!}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#street_address').on('blur', function(event) {
		$('#updateMap').trigger('click');
	});

	$('#street_address').on('focus', function(event) {
		$('#editLocation').hide();
	});

	$('#updateMap').on('click', function(event) {
		event.preventDefault();
		getMapLocation();
		$('#editLocation').show();
	});
});

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

window.onload = function () { getMapLocation(); }

function getMapLocation() {
	var geocoder;
	var map;
	var addr = $('#street_address').val()+', '+$('#city').val()+', '+$('#state').val()+', '+$('#zip_code').val()+', '+$('#country').val();
	var address = replaceAll(addr, ' ', '+');
	var pin = $('#zip_code').val();

	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng($("#lat").val(), $("#lng").val());
	var myOptions = {
		zoom: 14,
		center: latlng,
		mapTypeControl: true,
		mapTypeControlOptions: {
		  	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		},
		navigationControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("gMap"), myOptions);
	if (geocoder) {
		geocoder.geocode({
	  		'address': address
		}, function(results, status) {
	  		if (status == google.maps.GeocoderStatus.OK) {
	    		if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
					map.setCenter(results[0].geometry.location);

					var infowindow = new google.maps.InfoWindow({
						content: address,
						size: new google.maps.Size(150, 50)
					});

					var marker = new google.maps.Marker({
						position: results[0].geometry.location,
						map: map,
						title: address,
						icon: '{{ asset('/img/map-marker.png') }}',
						draggable: true,
        				animation: google.maps.Animation.DROP
					});
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map, marker);
					});

					google.maps.event.addListener(marker, "dragend", function (e) {
	                    var lat, lng, address;
	                    geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
	                        if (status == google.maps.GeocoderStatus.OK) {
	                            // lat = marker.getPosition().lat();
	                            // lng = marker.getPosition().lng();
	                            // address = results[0].formatted_address;
	                            $('#lat').attr('value', marker.getPosition().lat()).val(marker.getPosition().lat());
	                            $('#lng').attr('value', marker.getPosition().lng()).val(marker.getPosition().lng());

	                            // alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);
	                            addressComponent = results[0].address_components;
                            	var street_addressX = '';
	                            for (var i = 0; i < addressComponent.length; i++) {
									if(addressComponent[i].types == 'street_number') {
										street_addressX += addressComponent[i].long_name+' ';
									}
									if(addressComponent[i].types == 'route') {
										street_addressX += addressComponent[i].long_name;
									}
								}
								// $('#street_address').attr('value', street_addressX);
								$('#street_address').val(street_addressX).attr('value', street_addressX);
	                        }
	                    });
	                });

	    		} else {
	      			alert("No results found");
	    		}
	  		} else {
	    		alert("Geocode was not successful for the following reason: " + status);
	  		}
		});
	}
}
</script>
@endsection