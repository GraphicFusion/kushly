<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <?php if(Request::is('/')): ?>
		<title>Kushly | Medical Marijuana Delivery &amp; Dispensary in Arizona and Nevada</title>
		<meta name="description" content="Kushly is an on-demand medical marijuana delivery service in Nevada and Arizona. We partner with the best dispensaries to serve Phoenix and Las Vegas."/>
    <?php else: ?>
        <title>Kushly</title>
    <?php endif; ?>
	
	{{-- Google Font --}}
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One|Roboto+Mono:400,500,700|Lato:300,400,700,900" rel="stylesheet"> 

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    
	<link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('lib/select2/css/select2-bootstrap.min.css') }}">

    @yield('customCss')
    <script src="{{ asset('/js/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
{{-- 			<style>
				.navbar-default {
					box-shadow: none !important;
				}
                .header-message{
                    background-color:black;
                    text-align:center;
                    color:white;
                    text-transform:uppercase;
                    letter-spacing:1px;
                    font-family: 'Lato', sans-serif;
                    font-weight: 700;
                    padding:6px 12px;
                    font-size:14px;
                }
            </style> --}}

</head>
<body class="landing-page">

    <div class="main-app">
		{{-- Sidebar --}}
		@include('sidebar-right')
        <div id="app">

        	<header class="header">
	            {{-- Navbar --}}
	            @include('navs.landing')
{{-- 
	           	<div class="header-message">
	                Opening Soon! Not accepting orders at this time.
	            </div>
 --}}
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-8 col-lg-offset-2 form-wrapper">
							<h3 class="title">Kushly Delivers</h3>
							{!! Form::open(['url'=>'/process-search','method'=>'post','class'=>'form-inline','id'=>'formSearch']) !!}
								<div class="form-group">
									<select name="locations" class="js-select-ws" id="locations" data-placeholder="Select Location">
										<option value="Phoenix">Phoenix</option>
									</select>
								</div>
								{!! Form::submit('Search', ['class'=>'btn btn-primary']) !!}
							{!! Form::close() !!}
        				</div>
        			</div>
        		</div>
        	</header>
			
            <div class="content">
				
				<div class="bg-base info-wrap">
					<div class="container">
						<h6 class="info-title">How Kushly Works</h6>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="info">
									<div class="icon">
										<?php 
							                echo embed_svg(
							                    $class = 'svg-icon', 
							                    $svgurl = 'img/order_online.svg', 
							                    $height = '40px', 
							                    $width = '80px', 
							                    $link = makeURL('/')
							                ); 
							            ?>
									</div>
									<h3>Order Online</h3>
									<p>View menus from the nearest local dispensaries. Select the cannabis products you want and submit your order.</p>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="info">
									<div class="icon">
										<?php 
							                echo embed_svg(
							                    $class = 'svg-icon', 
							                    $svgurl = 'img/track_delivery.svg', 
							                    $height = '40px', 
							                    $width = '80px', 
							                    $link = makeURL('/')
							                ); 
							            ?>
									</div>
									<h3>Track Delivery</h3>
									<p>You will receive a notification once your order is fulfilled and you can track its progress right to your doorstep!</p>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="info">
									<div class="icon">
										<?php 
							                echo embed_svg(
							                    $class = 'svg-icon', 
							                    $svgurl = 'img/receive_goods.svg', 
							                    $height = '40px', 
							                    $width = '80px', 
							                    $link = makeURL('/')
							                ); 
							            ?>
									</div>
									<h3>Receive Goods</h3>
									<p>A text message will notify you that your driver is arriving. Reveive your delivery and enjoy the medicine!</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="bg-white info-wrap2">
					<div class="container">
	                    <div class="row content">
	                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 content-page">
	                            <div class="page-header">
	                            	<?php 
						                echo embed_svg(
						                    $class = 'svg-icon', 
						                    $svgurl = 'img/home2.svg', 
						                    $height = '40px', 
						                    $width = '40px', 
						                    $link = makeURL('/')
						                ); 
						            ?>
	                                <h1>Unlock the Convenience of On-Demand Marijuana Delivery</h1>
	                            </div>
	                            <div class="page-content" style="margin-bottom:75px; text-align:left;">
	                                <p>Kushly is bringing premier on-demand medical marijuana delivery to the California, Arizona, and Nevada. With a focus on patient care, superior customer service, and advanced technology, Kushly is seeking to dramatically enhance the way patients procure the natural medicine they need. Our app provides the easiest, quickest, and most convenient access to the purest medical marijuana products available. Most importantly, we only partner with <a href="https://en.wikipedia.org/wiki/Cannabis_dispensaries_in_the_United_States">dispensaries</a> that are reputable and have proven standards of quality and reliability for the medical marijuana they distribute.</p>
	                            </div>
	                        </div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="thumbnail ">
									<img src="img/landing-phone-alt.png" alt="Landing Page Mobile Image">
								</div>								
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-lg-12 m-b-30">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<h3>Join Kushly Today</h3>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
										<a class="btn btn-primary" href="{{ url('/signup') }}">Sign Up</a>
									</div>
								</div>
							</div>

							<div class="col-lg-12 m-t-30">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="text-wrapper p-r-30">
											<p>As more and more studies indicate, marijuana is fast becoming a proven alternative to more harmful opioid based prescription drugs. In fact, according to <a href="https://www.engadget.com/2017/01/13/massive-meta-study-confirms-the-health-benefits-of-cannabis/">Engadgent.com</a>, there have been more than 10,000 separate clinical studies regarding cannabis use. These studies overwhelmingly conclude that Cannabis can relieve anything from restless leg syndrome to Fibromyalgia, and even been shown to alleviate certain cancer conditions.</p>	
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="text-wrapper p-l-30">
											<p>Kushly is dedicated to making it as easy as possible to order medical marijuana online. Our exceptional technology, user friendly platform, and well-established relationships with the most reputable dispensaries on the west coast make Kushly the superior choice for your medical marijuana delivery needs. We aim to provide our patients with first-class treatment and except nothing but the highest standards from the Arizona, California and <a href="https://kushly.com/blog/las-vegas-dispensaries/">Las Vegas dispensaries</a> we partner with. Simply put, Kushly is the most easiest, transparent, reliable, and convenient medical marijuana delivery service available.</p>
										</div>
									</div>
								</div>
 							</div>
						</div>
					</div>
				</div>

            </div>
        </div>
        {{-- Footer --}}

        <footer class="footer alt">
            <div class="bg-primary">
                <div class="info">
                    <?php 
                        echo embed_svg(
                            $class = 'footer-logo', 
                            $svgurl = 'img/logo.svg', 
                            $height = '42px', 
                            $width = '113px', 
                            $link = makeURL('/')
                        ); 
                    ?>
                </div>
            </div>
           
            <div class="bg-base">
                <div class="container">
                    <div class="row credits">
                        <div class="col-sm-8 col-xs-12">
                            <p class="text-left">&copy; 2017 Kushly. All Rights Reserved.  <a href="/page/privacy-policy">Privacy Policy</a>, <a href="/page/terms-and-conditions">Legal Terms</a>. Design by <a href="#x">Sonder</a></p>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p class="text-right"><a class="btop" href="javascript:void(0)">Back to top</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        
    </div>

    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>

	<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key={!! \Config::get('app.gmap') !!}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			// Customize select box via select2 js adding bootstrap stheme
			$.fn.select2.defaults.set( "theme", "bootstrap" );
			// Customize select box via select2 js
			$(".js-select-ws").select2({
			  	placeholder: function(){
			        $(this).data('placeholder');
			    },
		  		allowClear: true
			});
		});
	</script>
</body>
</html>
