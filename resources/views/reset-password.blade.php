{{-- Login Form --}}
@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				
				<div class="signin-from">
					<h3>Reset Password</h3>
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					@if(!empty($msg))
						<div class="alert alert-success">{{ $msg }}</div>
					@endif

					{!! Form::open(['url' => 'reset/'.$email.'/'.$code, 'method' => 'post', 'class' => 'form', 'id' => 'forgotPassForm']) !!}
						<div class="form-group">
						    {!! Form::label('password', 'Password') !!}
						    {!! Form::password('password', ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
						    {!! Form::label('re_password', 'Confirm Password') !!}
						    {!! Form::password('re_password', ['class' => 'form-control']) !!}
						</div>

						{!! Form::submit('Reset Password', ['class'=>'btn btn-primary-outline']) !!}
					{!! Form::close() !!}
				</div>

			</div>
		</div>
	</div>
@endsection