<h5>New contact request from {{ $contact->first_name.' '.$contact->last_name }}</h5>

<p>Details given below:</p>

<table>
	<tbody>
		<tr>
			<td>Name</td>
			<td>{{ $contact->first_name.' '.$contact->last_name }}</td>
		</tr>
		<tr>
			<td>Email</td>
			<td>{{ $contact->email }}</td>
		</tr>
		<tr>
			<td>Phone No</td>
			<td>{{ $contact->phone_no }}</td>
		</tr>
		<tr>
			<td>Message</td>
			<td>{{ $contact->message }}</td>
		</tr>
	</tbody>
</table>
