<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Driver Accepts Order</title>
      <style type="text/css">
         body,#bodyTable,#bodyCell{
         height:100% !important;
         margin:0;
         padding:0;
         color:#575459;
         width:100% !important;
         font-family:Arial, Helvetica;
         }
         table{
         border-collapse:collapse;
         }
         img,a img{
         border:0;
         outline:none;
         text-decoration:none;
         }
         h1,h2,h3,h4,h5,h6{
         margin:0;
         padding:0;
         }
         p{
         margin:1em 0;
         padding:0;
         }
         a{
         word-wrap:break-word;
         }
         .ReadMsgBody{
         width:100%;
         }
         .ExternalClass{
         width:100%;
         }
         .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
         line-height:100%;
         }
         table,td{
         mso-table-lspace:0pt;
         mso-table-rspace:0pt;
         }
         #outlook a{
         padding:0;
         }
         img{
         -ms-interpolation-mode:bicubic;
         }
         body,table,td,p,a,li,blockquote{
         -ms-text-size-adjust:100%;
         -webkit-text-size-adjust:100%;
         }
         #bodyCell{
         padding:20px;
         }
         .themezyImage{
         vertical-align:bottom;
         }
         .themezyTextContent img{
         height:auto !important;
         }
         body,#bodyTable{
         background-color:#e1e1e1;
         }
         #bodyCell{
         border-top:0;
         }
         #templateContainer{
         border:0;
         }
         h1{
         color:#575459 !important;
         display:block;
         font-family:Arial, Helvetica;
         font-size:22px;
         font-style:normal;
         font-weight:bold;
         line-height:200%;
         letter-spacing:normal;
         margin:0;
         text-align:center;
         }
         h2{
         color:#575459 !important;
         display:block;
         font-family:Arial, Helvetica;
         font-size:19px;
         font-style:normal;
         font-weight:bold;
         line-height:125%;			
         margin:0;
         text-align:center;
         }
         h3{
         color:#606060 !important;
         display:block;
         font-family:Arial, Helvetica;
         font-size:18px;
         font-style:normal;
         font-weight:bold;
         line-height:125%;
         letter-spacing:-.5px;
         margin:0;
         text-align:left;
         }
         h4{
         color:#808080 !important;
         display:block;
         font-family:Arial, Helvetica;
         font-size:16px;
         font-style:normal;
         font-weight:bold;
         line-height:125%;
         letter-spacing:normal;
         margin:0;
         text-align:left;
         }
         #templatePreheader{
         background-color:#e1e1e1;
         border-top:0;
         border-bottom:0;
         }
         .preheaderContainer .themezyTextContent,.preheaderContainer .themezyTextContent p{
         color:#7b7b7b;
         font-family:Arial, Helvetica;
         font-size:11px;
         line-height:125%;
         text-align:left;
         }
         .preheaderContainer .themezyTextContent a{
         color:#7b7b7b;
         font-weight:normal;
         text-decoration:underline;
         }
         #templateHeader{
         background-color:#FFFFFF;
         border-top:0;
         border-bottom:0;
         }
         .headerContainer .themezyTextContent,.headerContainer .themezyTextContent p{
         color:#606060;
         font-family:Arial, Helvetica;
         font-size:15px;
         line-height:150%;
         text-align:left;
         }
         .headerContainer .themezyTextContent a{
         color:#6DC6DD;
         font-weight:normal;
         text-decoration:underline;
         }
         #templateBody{
         background-color:#FFFFFF;
         border-top:0;
         border-bottom:0;
         }
         .bodyContainer .themezyTextContent,.bodyContainer .themezyTextContent p{
         color:#575459;
         font-family:Arial, Helvetica;
         font-size:14px;
         line-height:150%;
         text-align:left;
         }
         .bodyContainer .themezyTextContent a{
         color:#6DC6DD;
         font-weight:normal;
         text-decoration:underline;
         }
         #templateFooter{
         background-color:#e1e1e1;
         border-top:0;
         border-bottom:0;
         }
         .footerContainer .themezyTextContent,.footerContainer .themezyTextContent p{
         color:#7b7b7b;
         font-family:Arial, Helvetica;
         font-size:14px;
         line-height:200%;
         text-align:center;
         }
         .footerContainer .themezyTextContent a{
         color:#7b7b7b;
         font-weight:normal;
         text-decoration:underline;
         }
         @media only screen and (max-width: 630px){
         body,table,td,p,a,li,blockquote{
         -webkit-text-size-adjust:none !important;
         }
         body{
         width:100% !important;
         min-width:100% !important;
         }
         td[id=bodyCell]{
         padding:10px !important;
         }
         table[class=themezyTextContentContainer]{
         width:100% !important;
         }
         table[class=themezyBoxedTextContentContainer]{
         width:100% !important;
         }
         table[class=mcpreview-image-uploader]{
         width:100% !important;
         display:none !important;
         }
         img[class=themezyImage]{
         width:100% !important;
         }
         table[class=themezyImageGroupContentContainer]{
         width:100% !important;
         }
         td[class=themezyImageGroupContent]{
         padding:9px !important;
         }
         td[class=themezyImageGroupBlockInner]{
         padding-bottom:0 !important;
         padding-top:0 !important;
         }
         tbody[class=themezyImageGroupBlockOuter]{
         padding-bottom:9px !important;
         padding-top:9px !important;
         }
         table[class=themezyCaptionTopContent],table[class=themezyCaptionBottomContent]{
         width:100% !important;
         }
         table[class=themezyCaptionLeftTextContentContainer],table[class=themezyCaptionRightTextContentContainer],table[class=themezyCaptionLeftImageContentContainer],table[class=themezyCaptionRightImageContentContainer],table[class=themezyImageCardLeftTextContentContainer],table[class=themezyImageCardRightTextContentContainer]{
         width:100% !important;
         }
         td[class=themezyImageCardLeftImageContent],td[class=themezyImageCardRightImageContent]{
         padding-right:18px !important;
         padding-left:18px !important;
         padding-bottom:0 !important;
         }
         td[class=themezyImageCardBottomImageContent]{
         padding-bottom:9px !important;
         }
         td[class=themezyImageCardTopImageContent]{
         padding-top:18px !important;
         }
         td[class=themezyImageCardLeftImageContent],td[class=themezyImageCardRightImageContent]{
         padding-right:18px !important;
         padding-left:18px !important;
         padding-bottom:0 !important;
         }
         td[class=themezyImageCardBottomImageContent]{
         padding-bottom:9px !important;
         }
         td[class=themezyImageCardTopImageContent]{
         padding-top:18px !important;
         }
         table[class=themezyCaptionLeftContentOuter] td[class=themezyTextContent],table[class=themezyCaptionRightContentOuter] td[class=themezyTextContent]{
         padding-top:9px !important;
         }
         td[class=themezyCaptionBlockInner] table[class=themezyCaptionTopContent]:last-child td[class=themezyTextContent]{
         padding-top:18px !important;
         }
         td[class=themezyBoxedTextContentColumn]{
         padding-left:18px !important;
         padding-right:18px !important;
         }
         td[class=themezyTextContent]{
         padding-right:18px !important;
         padding-left:18px !important;
         }
         table[id=templateContainer],table[id=templatePreheader],table[id=templateHeader],table[id=templateBody],table[id=templateFooter]{
         max-width:600px !important;
         width:100% !important;
         }
         h1{
         font-size:24px !important;
         line-height:125% !important;
         }
         h2{
         font-size:20px !important;
         line-height:125% !important;
         }
         h3{
         font-size:18px !important;
         line-height:125% !important;
         }
         h4{
         font-size:16px !important;
         line-height:125% !important;
         }
         table[class=themezyBoxedTextContentContainer] td[class=themezyTextContent],td[class=themezyBoxedTextContentContainer] td[class=themezyTextContent] p{
         font-size:15px !important;
         line-height:125% !important;
         }
         table[id=templatePreheader]{
         display:block !important;
         }
         td[class=preheaderContainer] td[class=themezyTextContent],td[class=preheaderContainer] td[class=themezyTextContent] p{
         font-size:14px !important;
         line-height:115% !important;
         text-align:center !important;        
         }
         td[class=headerContainer] td[class=themezyTextContent],td[class=headerContainer] td[class=themezyTextContent] p{
         font-size:18px !important;
         line-height:125% !important;
         }
         td[class=bodyContainer] td[class=themezyTextContent],td[class=bodyContainer] td[class=themezyTextContent] p{
         font-size:18px !important;
         line-height:125% !important;
         }
         td[class=footerContainer] td[class=themezyTextContent],td[class=footerContainer] td[class=themezyTextContent] p{
         font-size:14px !important;
         line-height:115% !important;
         }
         }
      </style>
   </head>
   <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
      <center>
         <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            <tr>
               <td align="center" valign="top" id="bodyCell">
                  <!-- BEGIN TEMPLATE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                     <tr>
                        <td align="center" valign="top">
                           <!-- BEGIN PREHEADER // -->
                           <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                              <tr>
                                 <td valign="top" class="preheaderContainer" style="padding-top:9px;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                                       <tbody class="themezyTextBlockOuter">
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="366" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;">
                                                            Use this area to offer a short preview of your email's content.
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <table align="right" border="0" cellpadding="0" cellspacing="0" width="197" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:0;">
                                                            <a href="#" >View this email in your browser</a>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                           <!-- // END PREHEADER -->
                        </td>
                     </tr>
                     <tr>
                        <td align="center" valign="top">
                           <!-- BEGIN HEADER // -->
                           <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                              <tr>
                                 <td valign="top" class="headerContainer">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyImageBlock">
                                       <tbody class="themezyImageBlockOuter">
                                          <tr>
                                             <td valign="top" style="padding:9px" class="themezyImageBlockInner">
                                                <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="themezyImageContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td class="themezyImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 25px; padding-bottom: 15px; text-align:center; background-color:#21DB00">
                                                            <object class="svg-object" type="image/svg+xml" data="{!! asset("img/logo.svg") !!}
                                                            " width="113px" height="42px">Your browser doesnot support SVG</object>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                           <!-- // END HEADER -->
                        </td>
                     </tr>
                     <tr>
                        <td align="center" valign="top">
                           <!-- BEGIN BODY // -->
                           <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                              <tr>
                                 <td valign="top" class="bodyContainer">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                                       <tbody class="themezyTextBlockOuter">
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding:9px 18px 0;text-align:center;">
                                                            <h1>[Driver Name]</h1>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding:9px 18px 0;text-align:center; font-size:16px;text-align:left;">
                                                            <b>Driver details</b>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding:9px 18px;text-align:center;">
                                                            <p style="margin:0px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding:9px 18px 0;text-align:center; text-align:left;">
                                                            <h2 style="text-align:left;">Order details</h2>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>

                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                                       <tbody class="themezyTextBlockOuter">
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding: 10px 18px 20px;text-align: left;font-size:16px;">
                                                            <b style="text-align:left">Product Name</b>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                                       <tbody class="themezyTextBlockOuter">
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:0px;padding-right:18px; padding-bottom: 9px; padding-left: 18px;text-align:center;">
                                                         <img align="center" alt="" src="{!! asset("img/product-thumb-lg.jpg") !!}"  style="max-width:740px; padding-bottom: 0; display: inline !important; vertical-align: bottom; width:350px;" class="themezyImage">
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:10px; padding-right: 18px; padding-bottom: 0px; padding-left: 18px;width:50%">
                                                         <b style="text-align:left;">Price</b>
                                                         </td>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:10px; padding-right: 18px; padding-bottom: 0px; padding-left: 18px;width:50%">
                                                         <p style="text-align:left;margin:0px;">$190</p>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:10px; padding-right: 18px; padding-bottom: 10px; padding-left: 18px;width:50%">
                                                         <b style="text-align:left;">Taxes</b>
                                                         </td>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:10px; padding-right: 18px; padding-bottom: 10px; padding-left: 18px;width:50%">
                                                         <p style="text-align:left;margin:0px;">$0</p>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:0px; padding-right: 18px; padding-bottom: 10px; padding-left: 18px;width:50%">
                                                         <b style="text-align:left;">Delivery</b>
                                                         </td>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:0px; padding-right: 18px; padding-bottom: 10px; padding-left: 18px;width:50%">
                                                         <p style="text-align:left;margin:0px;">$0</p>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:10px; padding-right: 18px; padding-bottom: 10px; padding-left: 18px;width:50%;border-top:1px solid #ddd;">
                                                         <b style="text-align:left;font-size:18px;">Total</b>
                                                         </td>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:10px; padding-right: 18px; padding-bottom: 10px; padding-left: 18px;width:50%;border-top:1px solid #ddd;">
                                                         <b style="font-size:18px;text-align:left;margin:0px;">$190</b>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyDividerBlock">
                                       <tbody class="themezyDividerBlockOuter">
                                          <tr>
                                             <td class="themezyDividerBlockInner" style="padding: 10px 18px;">
                                                <table class="themezyDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td style=";padding-bottom:10px;padding-top:0px;">
                                                            <b style="font-family: Arial, Helvetica">Delivery Time</b>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>
                                                            <span style="font-family: Arial, Helvetica;">2 hours</span>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyDividerBlock">
                                       <tbody class="themezyDividerBlockOuter">
                                          <tr>
                                             <td class="themezyDividerBlockInner" style="padding: 10px 18px;">
                                                <table class="themezyDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td>
                                                            <span></span>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                </td>
                              </tr>
                           </table>
                           <!-- // END BODY -->
                        </td>
                     </tr>
                     <tr>
                        <td align="center" valign="top">
                           <!-- BEGIN FOOTER // -->
                           <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
                              <tr>
                                 <td valign="top" class="footerContainer" style="padding-bottom:9px;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                                       <tbody class="themezyTextBlockOuter">
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:20px; padding-right: 18px; padding-bottom: 0px; padding-left: 18px;background-color:#21DB00;text-align: center; ">
                                                            <object class="svg-object" type="image/svg+xml" data="{!! asset("img/logo.svg") !!}" width="113px" height="42px">Your browser doesnot support SVG</object>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="themezyTextBlock">
                                       <tbody class="themezyTextBlockOuter">
                                          <tr>
                                             <td valign="top" class="themezyTextBlockInner">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="themezyTextContentContainer">
                                                   <tbody>
                                                      <tr>
                                                         <td valign="top" class="themezyTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;background-color:#222222">
                                                            <span style="color:#fff"><span style="font-size:12px">&copy; 2017 Kushly. All Rights Reserved. Privacy Policy, Legal Terms. Design by <a href="#" style="color:#21DB00">Sonder Agency</a></span></span>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                           <!-- // END FOOTER -->
                        </td>
                     </tr>
                  </table>
                  <!-- // END TEMPLATE -->
               </td>
            </tr>
         </table>
      </center>
   </body>
</html>

