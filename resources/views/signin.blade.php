{{-- Login Form --}}
@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				
				<div class="signin-from">
					<h3>Sign In</h3>

					{!! Form::open(['method' => 'post', 'class' => 'form', 'id' => 'signinfrom']) !!}
						@if(isset($_GET['ref']))
							<input type="hidden" name="ref" value="{{ $_GET['ref'] }}">
						@endif
						<div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
						    {!! Form::label('email', 'E-Mail Address') !!}
						    {!! Form::text('email', '', ['class' => 'form-control']) !!}
						    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
						</div>
						<div class="form-group {{ $errors->has('password') ? 'has-error' :'' }}">
						    {!! Form::label('password', 'Password') !!}
						    {!! Form::password('password', ['class' => 'form-control']) !!}
						    {!! $errors->first('password','<span class="help-block">:message</span>') !!}
						</div>
						
						<div class="checkbox">
						    <label>
						      	{!! Form::checkbox('rememberMe', 'ON', ['class' => 'form-control']) !!} Remember Me
						    </label>
					  	</div>

						<a href="{{ url('/forgot-password') }}">Forgot Password?</a>

						{!! Form::submit('Sign In', ['class'=>'btn btn-primary-outline pull-right']) !!}
						<div class="clearfix"></div>
					{!! Form::close() !!}
				</div>

			</div>
		</div>
	</div>
@endsection


@section('customJs')

@endsection