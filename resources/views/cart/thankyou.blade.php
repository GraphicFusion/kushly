@extends('layouts.cart')
@section('content')
<div class="cart">
<nav class="cart-page-nav">
   <ul class="list-inline">
      <li class="default">
         <a href="{{ url('/cart') }}">CART</a>
      </li>
      <li class="">
         <a href="#x">Customer & Delivery Info</a>
      </li>
      <li class="">
         <a href="#x">Payment</a>
      </li>
      <li class="active">
         <a href="#x">Complete Payment</a>
      </li>
   </ul>
</nav>
<div class="bg-white p-b-30">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="thankyou-c clearfix">
               <object class="logo" type="image/svg+xml" height="42" width="42"  data="{!! asset('img/ok.svg') !!}">Your browser doesnot support SVG</object>
               <h1>Thankyou</h1>
               <p>The transaction was successful money is kept by the admin</p>
               <p>Please check the into below</p>
               <div class="order-details clearfix">
                  @foreach($authResponse as $key => $value)
                  <div class="row">
                     <div class="col-xs-6 text-right">
                        <span>{{ $key }}</span>
                     </div>
                     <div class="col-xs-6 text-left">
                        <em>{{ $value }}</em>
                     </div>
                  </div>
                  @endforeach
               </div>
               <p>You product is currently in process</p>
               <div class="col-sm-6 col-sm-offset-3 ">
                  <a href="{{ url('/customer/profile#my-orders') }}"class="btn btn-cart-payment btn-block btn-lg">CHECK IT</a> 
                  </div
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('customCss')
<style>
   body {margin-top: 0 !important;}
</style>
@endsection

