@extends('layouts.cart')

@section('content')

@if(count($cart) == 0 || empty($cart))
	<div class="cart-error">
		<h1>Cart is Empty.</h1>
		<a href="{{ url('/') }}">Return to shop</a>
	</div>
@else

<div class="cart">
	<nav class="cart-page-nav">
		<ul class="list-inline">
			<li class="default">
				<a href="{{ url('/cart') }}">CART</a>
			</li>
			<li class="active">
				<a href="#x">Customer & Delivery Info</a>
			</li>
			<li class="">
				<a href="{{ url('/cart/payment') }}">Payment</a>
			</li>
		</ul>
	</nav>	

	<div class="bg-white p-b-30">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="product-ordered-title">
						{{-- <span class="help-block">Order#</span> --}}
						{{-- <h5>123456789</h5> --}}
						<h5>Order#</h5>
					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="box">
								<div class="title">Customer</div>
								<div class="matter">
									<?php 
										if(!Sentinel::check()):
											echo 'Login Required to complete this order. <a href="'.url('signin?ref=cart').'">Click Here</a> to login.';
										else:
									?>

											<?php
												$user_id = '';
												foreach ($cart as $key => $pr) {
													$user_id = $pr['user_id'];
												}
												if(empty($user_id)) {
													$user_id = getUserId();
												}
												$user = Sentinel::findById($user_id);
												// pr($user);
												echo '<h5>'.$user->first_name.' '.$user->last_name.'</h5>';
											?>
											<div class="row">
												<div class="col-md-6">
													<?php
														$locations = $user->locations()->get();
														// pr($locations);
													?>
													<div class="form-group">
														<label for="location">Select Delivery Location:</label>
													
														@if(count($locations) > 0)
														<select name="location" id="location" class="form-control">
															@foreach($locations as $key => $loc)
																<?php
																	$stat = '';
																	if(Session::get('addToCartLocation') == $loc->id):
																		$stat = 'selected=selected';
																	else:
																		$stat = '';
																	endif;
																?>
																<option {{ $stat }} value="{{ $loc->id }}">{{ $loc->street_address.', '.$loc->city.', '.$loc->state.'-'.$loc->zip_code.', '.$loc->country.', '.$loc->type }}</option>
															@endforeach
														</select>
														@else
															<p>No location found for this account. Please add location to process further. <a href="{{ url('customer/location/add') }}">Click Here</a> to add location.</p>
														@endif

													</div>
												</div>
												<div class="col-md-6">
													<p>{{ $user->email }}<br/>{{ $user->phone_no }}</p>
												</div>
											</div>

									<?php endif; ?>


								</div>
							</div>

						</div>

						<div class="col-md-6">
							<div class="box">
								<div class="title">Products Ordered</div>
								<div class="matter">
									<table class="table product-ordered-tbl">
										<tbody>
											<?php 
												$total = 0;
												$delivery = 0;
												$taxes = 0;
												$order_value = 0;
											?>
											@foreach ($cart as $key => $pr)
												<?php
													// pr($pr);
													$total += $pr['unit_price'];
												?>
												<tr>
													<td width="50%">
														<a href="{{ url('/product/'.$pr['product_id']) }}"><img src="{{ url('/img/product1-thumb.jpg') }}" alt=""></a>
														<p>
														<?php $pro = App\Product::select('product_name', 'type')->where('id', $pr['product_id'])->first(); ?>
															<b>{{ $pro->product_name }}</b>
															<span class="help-block">{{ title_case($pro->type) }}</span>
														</p>
													</td>
													<th width="25%">{{ $pr['quantity_per_unit'] }}</th>
													<th width="25%">
														${{ $pr['unit_price'] }}
														<button type="button" class="close removeFromCart" title="Remove this item from cart" data-toggle="tooltip" data-placement="top" data-key="{{ $key }}"><span>&times;</span></button>
													</th>
												</tr>
												
											@endforeach

											<tr>
												<td colspan="3">
													<table class="table order-totals">
														<tbody>
															<tr>
																<td>Taxes</td>
																<th>${!! $taxes !!}</th>
															</tr>
															<tr>
																<td>Delivery</td>
																<th>${!! $delivery !!}</th>
															</tr>
															<tr>
																<td>Total</td>
																<th>${{ $order_value = $taxes + $delivery + $total }}</th>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-4 col-lg-offset-4">
							<form action="{{ url('cart/payment') }}" method="post" id="placeOrder">
								{{ csrf_field() }}
								{!! Form::hidden('taxes', $taxes) !!}
								{!! Form::hidden('delivery', $delivery) !!}
								{!! Form::hidden('order_value', $order_value) !!}
								<a href="#x" onclick="document.getElementById('placeOrder').submit()" class="btn btn-cart-payment btn-block btn-lg">Confirm Payment</a>
							</form>							
							<br/>
							<a href="{{ url('/') }}" class="btn btn-cart-link btn-block btn-lg">Continue Shopping</a>
						</div>
					</div>				

				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endsection

@section('customCss')
<style>
	body {
		margin-top:0 !important;
	}
</style>
@endsection

@section('customJs')
<script type="text/javascript">
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	$('#location').change(function(){
		$.ajax({
			url: '{{ url('addtocartlocation') }}',
			type: 'POST',
			data: {
				location_id: $('#location').val()
			}
		})
		.done(function(data) {
			// Success Message
		});
	});
});
$(document).on('click', '.removeFromCart', function(e){
	e.preventDefault();
	var $this = $(this);
	$.ajax({
		url: '{{ url('/ajax/remove-product-from-cart') }}',
		type: 'GET',
		data: {key: $this.data('key')},
	}).done(function(data) {
		window.location.reload();
    });
});
</script>
@endsection