@extends('layouts.cart')

@section('content')
<div class="cart payment">
	<nav class="cart-page-nav">
		<ul class="list-inline">
			<li class="default">
				<a href="{{ url('/cart') }}">CART</a>
			</li>
			<li class="">
				<a href="{{ url('/cart') }}">Customer & Delivery Info</a>
			</li>
			<li class="active">
				<a href="{{ url('/cart/payment') }}">Payment</a>
			</li>
		</ul>
	</nav>	
	
	<div class="bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">Order Information</div>
					<div class="amount"><b>Total: US ${!! $cartOptions['order_value'] !!}</b></div>

					<div class="title">Payment Information</div>

					@php
						$user = Sentinel::findById(getUserId())->first();
					@endphp

					{!! Form::open(['url'=>'cart/payment/complete', 'method'=>'post']) !!}
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group {{ $errors->has('payoptions') ? 'has-error' :'' }}">
									<div class="radio">
									  <label>
									    <input type="radio" name="payoptions" value="cc" {{ (old('payoptions') == 'cc') ? 'checked' : '' }}>
									    Credit Card
									  </label>
									</div>
									<div class="radio">
									  <label>
									    <input type="radio" name="payoptions" value="bank" {{ (old('payoptions') == 'bank') ? 'checked' : '' }}>
									    Bank Account (USA Only)
									  </label>
									</div>
									{!! $errors->first('payoptions','<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-sm-8">
								<div class="row">
									<div class="col-xs-3"><img class="img-responsive" src="{{ asset('img/card1.png') }}" alt="American Express"></div>
									<div class="col-xs-3"><img class="img-responsive" src="{{ asset('img/card2.png') }}" alt="Visa"></div>
									<div class="col-xs-3"><img class="img-responsive" src="{{ asset('img/card3.png') }}" alt="Master Card"></div>
									<div class="col-xs-3"><img class="img-responsive" src="{{ asset('img/card4.png') }}" alt="Discover"></div>
								</div>
							</div>
						</div>

						<div class="row cc">
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('cardnumber') ? 'has-error' :'' }}">
									<label for="cardnumber">Card Number</label>
									<input type="text" class="form-control" name="cardnumber" id="cardnumber" placeholder="Enter Card Number" value="{!! (!empty($user->cardnumber)) ? $user->cardnumber : old('cardnumber') !!}">
									{!! $errors->first('cardnumber','<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group {{ $errors->has('expdate') ? 'has-error' :'' }}">
									<label for="expdate">Expiration Date</label>
									<input type="text" class="form-control" name="expdate" id="expdate" placeholder="MM/YY" value="{!! (!empty($user->expdate)) ? $user->expdate : old('expdate') !!}">
									{!! $errors->first('expdate','<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group {{ $errors->has('cardcode') ? 'has-error' :'' }}">
									<label for="cardcode">Card Code</label>
									<input type="text" class="form-control" name="cardcode" id="cardcode" placeholder="000" value="{!! (!empty($user->cardcode)) ? $user->cardcode : old('cardcode') !!}">
									{!! $errors->first('cardcode','<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="What`s this?" style="position: relative;margin-top: 22px;">What's this?</button>
							</div>
						</div>

						<div class="row bank">
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('bank') ? 'has-error' :'' }}">
									<label for="bank">Bank Name</label>
									<input type="text" class="form-control" name="bank" id="bank" placeholder="Enter Bank Name" value="{!! (!empty($user->bank)) ? $user->bank : old('bank') !!}">
									{!! $errors->first('bank','<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group {{ $errors->has('routingno') ? 'has-error' :'' }}">
									<label for="routingno">Routing No</label>
									<input type="text" class="form-control" name="routingno" id="routingno" placeholder="123456789" value="{!! (!empty($user->routingno)) ? $user->routingno : old('routingno') !!}">
									{!! $errors->first('routingno','<span class="help-block">:message</span>') !!}
								</div>
							</div>       
							<div class="col-md-3">
								<div class="form-group {{ $errors->has('accountno') ? 'has-error' :'' }}">
									<label for="accountno">Account No</label>
									<input type="text" class="form-control" name="accountno" id="accountno" placeholder="123456789" value="{!! (!empty($user->accountno)) ? $user->accountno : old('accountno') !!}">
									{!! $errors->first('accountno','<span class="help-block">:message</span>') !!}
								</div>
							</div>       
						</div>
						
						<?php
							$loc = App\Locations::getLocation($location, 'AR');
						?>
						<div class="row">
							<div class="col-xs-12">
								<a href="{{ url('/customer/profile') }}">Update Payment Details</a>
							</div>
							<div class="col-xs-12">
								<div class="title">
									Billing Information
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group {{ $errors->has('first_name') ? 'has-error' :'' }}">
									<label for="first_name">First Name</label>
									<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="{{ App\User::getName(getUserId(), 'FN') }}">
									{!! $errors->first('first_name','<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group {{ $errors->has('last_name') ? 'has-error' :'' }}">
									<label for="last_name">Last Name</label>
									<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="{{ App\User::getName(getUserId(), 'LN') }}">
									{!! $errors->first('last_name','<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="address">Address</label>
									<input type="text" class="form-control" name="address" id="address" placeholder="Address" value="{{ $loc->street_address.', '.$loc->city.', '.$loc->state }}">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group {{ $errors->has('zipcode') ? 'has-error' :'' }}">
									<label for="zipcode">Zip Code</label>
									<input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zip Code" value="{{ $loc->zip_code }}">
									{!! $errors->first('zipcode','<span class="help-block">:message</span>') !!}
								</div>
							</div>
						</div>

						{!! Form::hidden('user_id', getUserId()) !!}
						{!! Form::hidden('location', $location) !!}
						{!! Form::hidden('taxes', $cartOptions['taxes']) !!}
						{!! Form::hidden('delivery', $cartOptions['delivery']) !!}
						{!! Form::hidden('order_value', $cartOptions['order_value']) !!}

						@foreach($cart as $cart_p)
							<input type="hidden" name="product_id[]" value="{{$cart_p['product_id']}}">
							<input type="hidden" name="variant_id[]" value="{{$cart_p['variant_id']}}">
							<input type="hidden" name="quantity[]" value="{{$cart_p['quantity']}}">
							<input type="hidden" name="quantity_per_unit[]" value="{{$cart_p['quantity_per_unit']}}">
							<input type="hidden" name="unit_price[]" value="{{$cart_p['unit_price']}}">
						@endforeach

						<hr>
						<div class="row">
							<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
								{!! Form::submit('Submit', ['class'=>'btn btn-cart-payment btn-block btn-lg']) !!}
								{!! Form::reset('Reset From', ['class'=>'btn btn-cart-link btn-block btn-lg']) !!}
							</div>
						</div>
					{!! Form::close() !!}

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<style>
	.ui-datepicker-buttonpane button[data-handler='today'],
	.ui-datepicker-calendar {
		display: none;
	}
	.ui-datepicker .ui-datepicker-buttonpane { margin: 0; }
	.ui-datepicker .ui-datepicker-buttonpane button {
	    width: 100%;
	    margin: 3px 0;
	    padding: 0 .2em;
	    border: none;
	    font-weight: normal;
	    border-radius: 0;
	    text-transform: uppercase;
	    font-size: 14px;
	    font-family: 'Lato', sans-serif;
		font-weight: 400;
	}
	.bank,
	.cc {
		display: none;
	}
	body {
		margin-top:0 !important;
	}
</style>
@endsection

@section('customJs')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function($) {
	$('[data-toggle="tooltip"]').tooltip();

	$('#expdate').datepicker({
      	changeMonth: true,
      	changeYear: true,
      	dateFormat: 'mm/yy',
      	showButtonPanel: true,
      	yearRange: "c:+20",
      	onClose: function(dateText, inst) {  
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
            $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
		},
      	beforeShow: function(input, inst) {
			$('#ui-datepicker-div').wrap("<div class='datepicker ll-skin-melon hasDatepicker'></div>");
			if ((datestr = $(this).val()).length > 0) {
                year = datestr.substring(datestr.length-4, datestr.length);
                month = datestr.substring(0, 2);
                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                $(this).datepicker('setDate', new Date(year, month-1, 1));
            }
		},
		afterClose: function(input, inst) {
			$('#ui-datepicker-div').unwrap();
		}
    }).focus(function () {
    	$("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });    
	});

	$('input:radio[name=payoptions]').change(function(event) {
		var $this = $(this);
		var payOpt = $this.val();
		if(payOpt == 'cc') {
			$('.cc').slideDown('slow');
			$('.bank').slideUp('slow');
		}
		if(payOpt == 'bank'){
			$('.bank').slideDown('slow');
			$('.cc').slideUp('slow');
		}
	});

	var payopt = '<?php echo old('payoptions'); ?>';
	if(payopt == 'cc') {
		$('.cc').css('display', 'block');
	} else {
		$('.cc').css('display', 'none');
	}
	if(payopt == 'bank') {
		$('.bank').css('display', 'block');
	} else {
		$('.bank').css('display', 'none');
	}
});


</script>
@endsection