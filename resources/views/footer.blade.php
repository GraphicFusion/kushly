<footer class="footer">
	<div class="bg-primary">
		<div class="info">
			<?php 
                echo embed_svg(
                    $class = 'footer-logo', 
                    $svgurl = 'img/logo.svg', 
                    $height = '42px', 
                    $width = '113px', 
                    $link = makeURL('/')
                ); 
            ?>
		</div>
	</div>
	<!-- div class="container">
		<div class="row links">
			<div class="col-lg-12">
				<ul class="list-inline">
					<li><a href="#x">Search Products</a></li>
					<li><a href="#x">Track Order</a></li>
					<li><a href="#x">Shopping Cart</a></li>
					<li><a href="#x">My Account</a></li>
				</ul>
				<ul class="list-inline">
					<li><a href="#x">How it works</a></li>
					<li><a href="#x">Our dispensaries</a></li>
					<li><a href="#x">Become a partner</a></li>
					<li><a href="#x">Become a driver</a></li>
				</ul>
				<ul class="list-inline">
					<li><a href="#x">Weed Education</a></li>
					<li><a href="#x">About Us</a></li>
					<li><a href="#x">Contact Us</a></li>
				</ul>
			</div>
		</div>
		<div class="row credits">
			<div class="col-sm-6 col-xs-12">
			</div>
			<div class="col-sm-6 col-xs-12">
				<p class="text-right">Design by <a href="#x">Sonder</a></p>
			</div>
		</div>
	</div -->
	<div class="footer-bottom">
		<div class="credits">
			<p class="text-left">&copy; 2017 Kushly. All Rights Reserved. <a href="/page/privacy-policy">Privacy Policy</a>, <a href="/page/terms-and-conditions">Legal Terms</a>.</p>
		</div>
		<a href="#x" class="to-top">BACK TO TOP</a>
	</div>
</footer>