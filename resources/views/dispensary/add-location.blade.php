@extends('layouts.dispensary-admin')

@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('dispensary/manage-locations'); ?>>
			<a href="{{ url('/dispensary/manage-locations') }}">Manage Locations</a>
		</li>
		<li <?php echo navActive('dispensary/locations/create'); ?>>
			<a href="{{ url('/dispensary/locations/create') }}">Create Location</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5>Add New Location</h5>
				</div>
				<div class="col-lg-6"></div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="locationsForm">

					@if($errors->any())
						<div class="alert alert-danger alert-dismissible text-center" role="alert">
					   		@foreach ($errors->all() as $error)
					      		<p>{{ $error }}</p>
							@endforeach
				        </div>
					@endif

					{!! Form::open(['url'=>'dispensary/locations/store','method' => 'post', 'class' => 'form', 'id' => 'locations']) !!}

						<div class="row">
							<div class="col-lg-9">
								<div class="form-group {{ $errors->has('zip_code') ? 'has-error' :'' }}">
								    {!! Form::label('zip_code', 'Zip Code') !!}
								    {!! Form::text('zip_code', '', ['id'=>'zip_code','class' => 'form-control numbersOnly']) !!}
								    {!! $errors->first('zip_code','<span class="help-block">:message</span>') !!}
								    <span class="error"></span>
								</div>
							</div>
							<div class="col-lg-3">
								<a href="javascript:void()" id="viewMap" class="pull-right btn btn-primary-outline" style="margin-top: 26px;">View Map</a>
							</div>
						</div>
						
						<div class="formWrap" style="display: none;">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group {{ $errors->has('city') ? 'has-error' :'' }}">
								    {!! Form::label('city', 'City') !!}
								    {!! Form::text('city', '', ['id'=>'city','class' => 'form-control','readonly'=>true]) !!}
								    {!! $errors->first('city','<span class="help-block">:message</span>') !!}
								    <span class="error"></span>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group {{ $errors->has('state') ? 'has-error' :'' }}">
								    {!! Form::label('state', 'State') !!}
								    {!! Form::text('state', '', ['id'=>'state','class' => 'form-control','readonly'=>true]) !!}
								    {!! $errors->first('state','<span class="help-block">:message</span>') !!}
								    <span class="error"></span>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-9">
								<div class="form-group {{ $errors->has('street_address') ? 'has-error' :'' }}">
								    {!! Form::label('street_address', 'Street Address') !!}
								    {!! Form::text('street_address', '', ['id'=>'street_address','class' => 'form-control']) !!}
								    {!! $errors->first('street_address','<span class="help-block">:message</span>') !!}
								    <span class="error"></span>
								</div>
							</div>
							<div class="col-lg-3">
								<a href="javascript:void()" id="updateMap" class="pull-right btn btn-primary-outline" style="margin-top: 26px;">Update Map</a>
							</div>
						</div>

						<div class="form-group">
						    {!! Form::label('country', 'Country') !!}
						    {!! Form::text('country', '', ['id'=>'country','class' => 'form-control', 'readonly'=>true]) !!}
						</div>
						<div class="form-group {{ $errors->has('type') ? 'has-error' :'' }}">
						    {!! Form::label('type', 'Type') !!}
						    <select class="styleDropdown form-control" name="type" id="type">
						    	<option value="home">Home</option>
								<option value="office">Office</option>
								<option value="public">Public</option>
							</select>
							{!! $errors->first('type','<span class="help-block">:message</span>') !!}
						</div>

						<div class="row" style="display: none;">
							<div class="col-lg-6">
								<div class="form-group">
								    {!! Form::label('lat', 'Latitude') !!}
									{!! Form::text('lat','',['id'=>'lat','class'=>'form-control','readonly'=>true]) !!}
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
								    {!! Form::label('lng', 'Longtitude') !!}
									{!! Form::text('lng','',['id'=>'lng','class'=>'form-control','readonly'=>true]) !!}
								</div>
							</div>
						</div>

						{!! Form::submit('Add New Location', ['id'=>'addLocation','class'=>'btn btn-primary-outline']) !!}
						</div>
					{!! Form::close() !!}

					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="formWrap" style="display: none;">
					<h5>Current location map.</h5>
					<p class="help-block">If map is not shown your exact location, you can drag the marker to exact location. Note that after dragging the map marker your filled location (street address) is updated with the new location.</p>
				</div>
				<div id="gMap" style="width: 100%;height: 380px;"></div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<style>.error{ color:#a94442; font-size: 12px; }</style>
@endsection

@section('customJs')
<script src="https://maps.googleapis.com/maps/api/js?key={!! \Config::get('app.gmap') !!}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#addLocation').hide();

	$('#viewMap').on('click', function(event) {
		event.preventDefault();
		
		if($('#zip_code').val() == '') {
			$('#zip_code').parent('.form-group').addClass('has-error');
			$('#zip_code').parent('.form-group').find('span.error').text('Zip Code is required to view map.');
		} else {
			$('#zip_code').parent('.form-group').removeClass('has-error');
			$('#zip_code').parent('.form-group').find('span.error').text('');
		}

		// if(($('#street_address').val() != '') && ($('#city').val() != '') && ($('#state').val() != '') && ($('#zip_code').val() != '')) {
		if(($('#zip_code').val() != '')) {
			$(this).attr('disabled', 'disabled');
			$('#zip_code').attr('readonly', true);
			$('.formWrap').show('slow');

			// Implementing google map api
			var addr = $('#street_address').val()+', '+$('#city').val()+', '+$('#state').val()+', '+$('#zip_code').val()+', '+$('#country').val();
			var address = replaceAll(addr, ' ', '+');
			var pin = $('#zip_code').val();

			// var gMapApiURL = 'https://maps.googleapis.com/maps/api/geocode/json';
			// var gMapApiKEY = 'AIzaSyABI-7lp-qnW-6EnoB6k6Xhy6bBu1gIE1U';
			$.post('/ajax/load/gmap', {
				address: pin, 
				pin: pin
			}, function(data,status) {
				for (var i = 0; i < data['address_components'].length; i++) {
					if(data['address_components'][i].types == 'locality,political') {
						// City
						$("#city").attr('value', data['address_components'][i].short_name).val(data['address_components'][i].short_name);
					}
					if(data['address_components'][i].types == 'administrative_area_level_1,political') {
						// state
						$("#state").attr('value', data['address_components'][i].short_name).val(data['address_components'][i].short_name);
					}
					if(data['address_components'][i].types == 'country,political') {
						// country
						$("#country").attr('value', data['address_components'][i].short_name).val(data['address_components'][i].short_name);
					}
				}
				$("#lat").attr('value', data['geometry']['location'].lat).val(data['geometry']['location'].lat);
				$("#lng").attr('value', data['geometry']['location'].lng).val(data['geometry']['location'].lng);
			});

			googleMap($("#lat").val(), $("#lng").val(), address, pin);
		}
	});	

	$('#street_address').on('blur', function(event) {
		$('#updateMap').trigger('click');
	});

	$('#street_address').on('focus', function(event) {
		$('#addLocation').hide();
	});

	$('#updateMap').on('click', function(event) {
		event.preventDefault();

		// Implementing google map api
		var addr = $('#street_address').val()+', '+$('#city').val()+', '+$('#state').val()+', '+$('#zip_code').val()+', '+$('#country').val();
		var address = replaceAll(addr, ' ', '+');
		var pin = $('#zip_code').val();

		$.post('/ajax/load/gmap', {
			address: address, 
			pin: pin
		}, function(data,status) {
			for (var i = 0; i < data['address_components'].length; i++) {
				if(data['address_components'][i].types == 'locality,political') {
					// City
					$("#city").attr('value', data['address_components'][i].short_name).val(data['address_components'][i].short_name);
				}
				if(data['address_components'][i].types == 'administrative_area_level_1,political') {
					// state
					$("#state").attr('value', data['address_components'][i].short_name).val(data['address_components'][i].short_name);
				}
				if(data['address_components'][i].types == 'country,political') {
					// country
					$("#country").attr('value', data['address_components'][i].short_name).val(data['address_components'][i].short_name);
				}
			}
			$("#lat").attr('value', data['geometry']['location'].lat).val(data['geometry']['location'].lat);
			$("#lng").attr('value', data['geometry']['location'].lng).val(data['geometry']['location'].lng);
		});

		googleMap($("#lat").val(), $("#lng").val(), address, pin);

		$('#addLocation').show();
		// $(this).attr('disabled', 'disabled');
	});
});
function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}
function googleMap(lat, lng, address, pin) {
	var geocoder;
	var map;
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(lat,lng);
	var myOptions = {
		zoom: 17,
		center: latlng,
		mapTypeControl: true,
		mapTypeControlOptions: {
		  	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		},
		navigationControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("gMap"), myOptions);
	if (geocoder) {
		geocoder.geocode({
	  		'address': address
		}, function(results, status) {
	  		if (status == google.maps.GeocoderStatus.OK) {
	    		if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
					map.setCenter(results[0].geometry.location);

					var infowindow = new google.maps.InfoWindow({
						content: address,
						size: new google.maps.Size(150, 50)
					});

					var marker = new google.maps.Marker({
						position: results[0].geometry.location,
						map: map,
						title: address,
						icon: '{{ asset('/img/map-marker.png') }}',
						draggable: true,
        				animation: google.maps.Animation.DROP
					});
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map, marker);
					});

					google.maps.event.addListener(marker, "dragend", function (e) {
	                    var lat, lng, address;
	                    geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
	                        if (status == google.maps.GeocoderStatus.OK) {
	                            // lat = marker.getPosition().lat();
	                            // lng = marker.getPosition().lng();
	                            // address = results[0].formatted_address;
	                            $('#lat').attr('value', marker.getPosition().lat()).val(marker.getPosition().lat());
	                            $('#lng').attr('value', marker.getPosition().lng()).val(marker.getPosition().lng());

	                            // alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);

	                            addressComponent = results[0].address_components;
                            	var street_addressX = '';
	                            for (var i = 0; i < addressComponent.length; i++) {
									if(addressComponent[i].types == 'street_number') {
										street_addressX += addressComponent[i].long_name+' ';
									}
									if(addressComponent[i].types == 'route') {
										street_addressX += addressComponent[i].long_name;
									}
								}

								// $('#street_address').attr('value', street_addressX);
								$('#street_address').val(street_addressX).attr('value', street_addressX);

	                        }
	                    });
	                });

	    		} else {
	      			alert("No results found");
	    		}
	  		} else {
	    		alert("Geocode was not successful for the following reason: " + status);
	  		}
		});
	}
}
</script>
@endsection