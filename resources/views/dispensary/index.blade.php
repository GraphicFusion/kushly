{{-- Dispensary Admin Home --}}

@extends('layouts.dispensary-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('dispensary/home'); ?>>
			<a href="{{ url('/dispensary/home') }}">Product Inventory</a>
		</li>
		<li <?php echo navActive('dispensary/product/create'); ?>>
			<a href="{{ url('/dispensary/product/create') }}">Add New Product</a>
		</li>
	</ul>
</nav>	

<div class="bg-white">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<a href="{{ url('/dispensary/product/create') }}" class="btn btn-primary btn-disp">Add New</a>
				</div>
				<div class="col-lg-6 col-sm-6 ">
					<div class="form-search dtb btn-disp-input">
						<div class="form-group">
					        <input type="text" class="form-control" id="search_dtable" placeholder="Search for...">
					        <button type="button" class="btn btn-default">
					        	<?php 
					                echo embed_svg(
					                    $class = '', 
					                    $svgurl = 'img/search-icon.svg', 
					                    $height = '20px', 
					                    $width = '20px', 
					                    $link = ''
					                ); 
					            ?>
					        </button>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
			
				<table id="dTablesProducts" class="table table-hover">
					<thead>
						<tr>
							<td>Product</td>
							<td>Type</td>
							<td>Strain</td>
							<td>Inventory</td>
							<td>Status</td>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($products as $key => $product) :
								$pImage = App\Product::withTrashed()->find($product['id'])->picture->first();
								$stock = 0;
								$variant = App\Product::withTrashed()->find($product['id'])->prices;
								foreach ($variant as $key => $v) {
									if( is_numeric( $v->stock ) ){
										$stock += $v->stock;
									}
								}
						?>
							<tr class="table_row" data-id="{{ $product['id'] }}">
								<td>
									@if(!empty($pImage['path']))
										<img src="{{ asset('product-images/'.$pImage['path']) }}" width="120" height="64" alt="">
									@else
										<img src="{{ asset('img/product-thumb.jpg') }}" width="120" height="64" alt="">
									@endif
									{{ $product['product_name'] }}
								</td>
								<td>{{ (empty($product['type'])) ? '-' : title_case($product['type']) }}</td>
								<td>{{ (empty($product['strain_type'])) ? '-' : title_case($product['strain_type']) }}</td>
								<td>{{ $stock }} in Stock for {{ sizeof($variant) }} variants</td>
								<td>
									@if(App\Product::withTrashed()->find($product->id)->trashed())
										Trashed
									@else
										@if($product->active == 1)
											Published
										@elseif($product->active == 0)
											Draft
										@endif
									@endif
								</td>
							</tr>
						<?php
							endforeach;		
						?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>

@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
$(document).ready(function($) {
	var oTable = $('#dTablesProducts').DataTable({
		"sDom": '<"top">rt<"bottom"p><"clear">',
		"stateSave": true,
		"lengthMenu": [ 10, 25, 50, 75, 100 ],
	    "iDisplayLength": 10,
	    "bSort" : false,
	    // "aaSorting": [[0, 'asc']],
	    "destroy": true,
		"language": {
		    "oPaginate": {
	          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
	          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
	        }
	    }
  	});

	$('#search_dtable').keyup(function(){
        oTable.search($(this).val()).draw();
    });

	$('#dTablesProducts tbody').on( 'click', 'tr.table_row', function (e) {
		e.preventDefault();
		var $id = $(this).data('id');
		var href = "{!! url('/dispensary/product/"+$id+"') !!}";
		window.location.href = href;
	});

});

</script>
@endsection