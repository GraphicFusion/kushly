{{-- Dispensary Admin Profile --}}

@extends('layouts.dispensary-admin')

@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('dispensary/manage-locations'); ?>>
			<a href="{{ url('/dispensary/manage-locations') }}">Manage Locations</a>
		</li>
	</ul>
</nav>	
<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h5>Locations</h5>
				</div>
			</div>
		</div>		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<table id="dTables" class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Street Address</th>
							<th>City</th>
							<th>State</th>
							<th>Pincode</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($locations as $key => $location)
							<tr>
								<td>{!! $location->id !!}</td>
								<td>{!! $location->street_address !!}</td>
								<td>{!! $location->city !!}</td>
								<td>{!! $location->state !!}</td>
								<td>{!! $location->zip_code !!}</td>
								<td>
									{{-- <div class='btn-group btn-action' role='group'> --}}
										{{-- <button class='btn btn-danger po'><i class='glyphicon glyphicon-remove'></i></button> --}}
										<a href='{{ url('/dispensary/manage-locations/'.$location->id) }}' class='text-warning'><i class='glyphicon glyphicon-edit'></i> Edit</a>
									{{-- </div> --}}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
$(document).ready(function($) {
	var oTable = $('#dTables').DataTable({
		"columnDefs": [ {
			"targets": -1,
			"orderable": false
		} ]
	});
});
</script>
@endsection