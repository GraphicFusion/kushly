{{-- Dispensary Admin --}}

@extends('layouts.dispensary-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('dispensary/home'); ?>>
			<a href="{{ url('/dispensary/home') }}">Product Inventory</a>
		</li>
		<li <?php echo navActive('product/create'); ?>>
			<a href="{{ url('/dispensary/product/create') }}">Add New Product</a>
		</li>
	</ul>
</nav>

<div class="bg-white">

	<form action="<?php echo url('/dispensary/product/'.$product->id); ?>" method="post" class="form editProduct" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="head">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6">
						<ul class="list-inline">
							<li>
								@if($product->active == 1)
									<a title="Hide Product" href="javascript:void(0)" data-product_id="{{ $product->id }}" id="deactiveProduct">Hide</a>
								@elseif($product->active == 0)
									<a title="Publish Product" href="javascript:void(0)" data-product_id="{{ $product->id }}" id="activeProduct">Show</a>
								@endif
							</li>
							<li>
								@if(App\Product::withTrashed()->find($product->id)->trashed())
									<a title="Restore Product" href="javascript:void(0)" data-product_id="{{ $product->id }}" id="restoreProduct">Restore</a>
								@else
									<a title="Add Product in Trash" href="javascript:void(0)" data-product_id="{{ $product->id }}" id="trashProduct">Delete</a>
								@endif
							</li>
							<li>
								<a href="">Duplicate</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-6">
						@if(App\Product::withTrashed()->find($product->id)->trashed())
							<button type="submit" class="btn btn-primary pull-right" disabled="disabled">Save</button>
						@else
							<button type="submit" class="btn btn-primary pull-right btnSubmit">Save</button>
						@endif
					</div>
				</div>
			</div>		
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					<?php
						// check product is marijuana, if yes the remove the hidden class.
						function isMarijuana($product) {
							if($product->is_marijuana == 1) {
								echo '';
							} elseif($product->is_marijuana == 0) {
								echo 'is_m';
							}
						}
					?>
					<div class="form-group">
						<label for="is_marijuana">Is Marijuana Product</label>

					    <label class="radio-inline">
							<input type="radio" name="is_marijuana" value="1" <?php echo ($product->is_marijuana == 1) ? 'checked="checked"' : ''; ?>> Yes
						</label>
						<label class="radio-inline">
							<input type="radio" name="is_marijuana" value="0" <?php echo ($product->is_marijuana == 0) ? 'checked="checked"' : ''; ?>> No
						</label>
					</div>

					<div class="form-group {{ $errors->has('product_name') ? 'has-error' :'' }}">
						<label for="product_name">Product Name</label>
						<input type="text" name="product_name" id="product_name" class="form-control" value="{{ $product->product_name }}" />
					    {!! $errors->first('product_name','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('description') ? 'has-error' :'' }}">
						<label for="description">Description</label>
						<textarea name="description" id="description" class="form-control">{{ $product->description }}</textarea>
					    {!! $errors->first('description','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group <?php isMarijuana($product); ?> {{ $errors->has('type') ? 'has-error' :'' }}">
					    <label for="type">Type</label>
					    <select class="styleDropdown form-control" name="type" id="type">
					    	<?php
					    		$types = [
					    			'flowers' => 'Flowers',
					    			'pre-roll' => 'Pre-Roll',
					    			'edibles' => 'Edibles',
					    			'concentrates' => 'Concentrates',
					    			'topicals' => 'Topicals',
					    			'vapes' => 'Vapes',
					    			'hash' => 'hash',
					    		];
					    		foreach ($types as $key => $type) {
					    			if($key == $product->type) {
				    					echo '<option selected="selected" value="'.$key.'">'.$type.'</option>';
					    			} else {
						    			echo '<option value="'.$key.'">'.$type.'</option>';
					    			}
					    		}
					    	?>
					    </select>
					    {!! $errors->first('type','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group <?php isMarijuana($product); ?> {{ $errors->has('strain_type') ? 'has-error' :'' }}">
					    <label for="strain_type">Strain Type</label>
					    <select class="styleDropdown form-control" name="strain_type" id="strain_type">
					    	<?php
					    		$types = [
					    			'sativa' => 'Sativa',
					    			'indica' => 'Indica',
					    			'hybrid' => 'Hybrid',
					    			'high-cbd' => 'High CBD'
					    		];
					    		foreach ($types as $key => $type) {
					    			if($key == $product->strain_type) {
				    					echo '<option selected="selected" value="'.$key.'">'.$type.'</option>';
					    			} else {
						    			echo '<option value="'.$key.'">'.$type.'</option>';
					    			}
					    		}
					    	?>
					    </select>
					    {!! $errors->first('strain_type','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group <?php isMarijuana($product); ?> {{ $errors->has('thc') ? 'has-error' :'' }}">
						<label for="thc">THC %</label>
					    <input class="form-control" name="thc" type="text" value="{{ $product->thc }}" id="thc">
					    {!! $errors->first('thc','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group <?php isMarijuana($product); ?> {{ $errors->has('cbd') ? 'has-error' :'' }}">
						<label for="cbd">CBD %</label>
					    <input class="form-control" name="cbd" type="text" value="{{ $product->cbd }}" id="cbd">
						{!! $errors->first('cbd','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group <?php isMarijuana($product); ?> {{ $errors->has('cbn') ? 'has-error' :'' }}">
						<label for="cbn">CBN %</label>
					    <input class="form-control" name="cbn" type="text" value="{{ $product->cbn }}" id="cbn">
					    {!! $errors->first('cbn','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('group_id') ? 'has-error' :'' }}">
					    <label for="group_id">Dispensary</label>
					    <select class="styleDropdown form-control" name="group_id" id="group_id" onchange="validateGroup(this.value)">
						<?php
							$set = '';
							$gID = json_encode($product->group_id);
							$currentUser = App\User::find(Sentinel::getUser()->id);
							$currentUser->groups()->get();
							foreach ($currentUser->groups as $key => $gp) {
								if($gp->id == $product->group_id) {
									$set = 'selected=selected';
								} else {
									$set = '';
								}
								echo '<option '.$set.' value="'.$gp->id.'">'.$gp->name.'</option>';
							}
						?>
					    </select>
					    {!! $errors->first('group_id','<span class="help-block">:message</span>') !!}
					</div>

					<?php
						$arrError = [];
						$count = count($errors->get('quantity_per_unit.*'));
						if($count>0) {
							$ctr = 0;
							foreach ($errors->get('quantity_per_unit.*') as $error):
				                foreach ($error as $key => $err) {
				                 	 $arrError[] = '['.$ctr.'] '.$err;
				                 	 $ctr++;
				                }
				            endforeach;
			            }
						$count = count($errors->get('stock.*'));
						if($count>0) {
							$ctr = 0;
							foreach ($errors->get('stock.*') as $key => $error):
				                foreach ($error as $key => $err) {
									$arrError[] = '['.$ctr.'] '.$err;
									$ctr++;
			                 	}
				            endforeach;
			            }
						$count = count($errors->get('unit_price.*'));
						if($count>0) {
							$ctr = 0;
							foreach ($errors->get('unit_price.*') as $key => $error):
				                foreach ($error as $key => $err) {
				                 	$arrError[] = '['.$ctr.'] '.$err;
				                 	$ctr++;
				                 }
				            endforeach;
			            }

			            if(sizeof($arrError) > 0) {
				            echo '<div class="error-box"><p>All variant errors with index:</p><ul>';
		            		foreach ($arrError as $key => $e) {
            					echo '<li>'.$e.'</li>';
		            		}
				            echo '</ul></div>';
			            }
					?>

					<div class="form-group">
					    {!! Form::label('variants', 'Variants') !!}
					    <div class="fieldsWrapper">
							<?php
								$stock = '';
								$variant = App\Product::withTrashed()->find($product->id)->prices;
								foreach ($variant as $key => $v) :
							?>
								<div class="fields">
									<input type="hidden" name="variant_id[]" value="{{ $v->id }}">
									<div class="input-group">
										<span class="input-group-addon">Size:</span>
								      	<input class="form-control" name="quantity_per_unit[]" type="text" value="{{ $v->quantity_per_unit }}">
								    </div>
								    <div class="input-group">
										<span class="input-group-addon">Inventory:</span>
										<input class="form-control" name="stock[]" type="text" value="{{ $v->stock }}">
								    </div>
								    <div class="input-group">
										<span class="input-group-addon">Price:</span>
										<input class="form-control" name="unit_price[]" type="text" value="{{ $v->unit_price }}">
								    </div>
								</div>
							<?php
								endforeach;
							?>
					    </div>
						<div class="addVariant">
							<svg version="1.1" id="Layer_1" class="plus-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23.6 23.6" style="enable-background:new 0 0 23.6 23.6;" xml:space="preserve">
								<g>
									<g>
										<path class="st0" d="M11.8,23.3c-6.4,0-11.5-5.2-11.5-11.5S5.4,0.3,11.8,0.3s11.5,5.2,11.5,11.5S18.2,23.3,11.8,23.3z M11.8,1.3
											C6,1.3,1.3,6,1.3,11.8S6,22.3,11.8,22.3s10.5-4.7,10.5-10.5S17.6,1.3,11.8,1.3z"/>
									</g>
									<g>
										<path id="XMLID_2_" class="st0" d="M11.8,18.3c-0.3,0-0.5-0.2-0.5-0.5v-12c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5v12
											C12.3,18.1,12.1,18.3,11.8,18.3z"/>
									</g>
									<g>
										<path id="XMLID_1_" class="st0" d="M17.8,12.3h-12c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5h12c0.3,0,0.5,0.2,0.5,0.5
											S18.1,12.3,17.8,12.3z"/>
									</g>
								</g>
							</svg>

							Add Variant
						</div>
					</div>

				</div>
				<div class="col-lg-5 col-lg-offset-2">
					<ul class="list-unstyled image_preview" id="sortable">

						<?php
							$pImage = App\Product::withTrashed()->find($product->id)->picture;
							foreach ($pImage as $key => $pic) :
								if($pic['featured'] == 1) { $actv='active'; } else { $actv=''; }
						?>
							 <li id="{{ $pic->sortorder }}">
								<div class="thumbnail">
									<img src="{{ asset('product-images/'.$pic->path) }}" alt="">
									<input type="hidden" id="imgID" value="{{ $pic->id }}" />
									<input type="hidden" id="id" value="{{ $product->id }}">
									<div class="caption">
										<ul class="list-inline">
											<li><a class="close_icon" href="#x"></a></li>
											<li><a class="star_icon {{ $actv }}" href="#x"></a></li>
										</ul>
									</div>
								</div>
							</li>
						<?php
							endforeach;
						?>	
						<span class="replacewithme"></span>
					</ul>
					<div class="addPhoto customaddimg">
						<svg version="1.1" id="Layer_1" class="plus-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23.6 23.6" style="enable-background:new 0 0 23.6 23.6;" xml:space="preserve">
							<g>
								<g>
									<path class="st0" d="M11.8,23.3c-6.4,0-11.5-5.2-11.5-11.5S5.4,0.3,11.8,0.3s11.5,5.2,11.5,11.5S18.2,23.3,11.8,23.3z M11.8,1.3 C6,1.3,1.3,6,1.3,11.8S6,22.3,11.8,22.3s10.5-4.7,10.5-10.5S17.6,1.3,11.8,1.3z"/>
								</g>
								<g>
									<path id="XMLID_2_" class="st0" d="M11.8,18.3c-0.3,0-0.5-0.2-0.5-0.5v-12c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5v12 C12.3,18.1,12.1,18.3,11.8,18.3z"/>
								</g>
								<g>
									<path id="XMLID_1_" class="st0" d="M17.8,12.3h-12c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5h12c0.3,0,0.5,0.2,0.5,0.5 S18.1,12.3,17.8,12.3z"/>
								</g>
							</g>
						</svg>
						Add Photo
					</div>					
				</div>
			</div>
		</div>
		<div class="form-footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6">
						<ul class="list-inline">
							<li>
								@if($product->active == 1)
									<a title="Hide Product" href="javascript:void(0)" data-product_id="{{ $product->id }}" id="deactiveProduct">Hide</a>
								@elseif($product->active == 0)
									<a title="Publish Product" href="javascript:void(0)" data-product_id="{{ $product->id }}" id="activeProduct">Show</a>
								@endif
							</li>
							<li>
								@if(App\Product::withTrashed()->find($product->id)->trashed())
									<a title="Restore Product" href="javascript:void(0)" data-product_id="{{ $product->id }}" id="restoreProduct">Restore</a>
								@else
									<a title="Add Product in Trash" href="javascript:void(0)" data-product_id="{{ $product->id }}" id="trashProduct">Delete</a>
								@endif
							</li>
							<li><a href="">Duplicate</a></li>
						</ul>
					</div>
					<div class="col-lg-6 text-right">
						@if(App\Product::withTrashed()->find($product->id)->trashed())
							<button type="submit" class="btn btn-primary pull-right" disabled="disabled">Save</button>
						@else
							<button type="submit" class="btn btn-primary pull-right btnSubmit">Save</button>
						@endif
					</div>
				</div>
			</div>	
		</div>

	</form>

</div>

@endsection


@section('customCss')
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2-bootstrap.min.css') }}">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<style>
.uploadme {
	cursor: pointer;
}
.fileget{
	display: none !important;
}
.visibilty{
	display: none;
}
</style>
@endsection

@section('customJs')
<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">

function validateGroup(gid) {
	var currentGroupSession = '{{ Session::get('activeGroup') }}';
	if(gid != currentGroupSession) {
		$('#group_id').parent('.form-group').addClass('has-error');
		$('#group_id').parent('.form-group').find('.help-block').remove();
		$('#group_id').parent('.form-group').append('<span class="help-block">You can not add product for other dispensary.</span>');
		$('.btnSubmit').prop('disabled', 'disabled');
		return false;
	} else {
		$('#group_id').parent('.form-group').removeClass('has-error');
		$('#group_id').parent('.form-group').find('.help-block').remove();
		$('.btnSubmit').removeAttr('disabled');
	}
}

$(document).ready(function($) {

	// Customize select box via select2 js adding bootstrap stheme
	$.fn.select2.defaults.set( "theme", "bootstrap" );
	// Customize select box via select2 js
	$(".js-select-ws").select2({
  		allowClear: true
	});

	$('.addVariant').click(function(event) {
		var clone = $(this).parent('.form-group').find('.fieldsWrapper .fields:first').clone();
		clone.find('input').val('');
		$(this).parent('.form-group').find('.fieldsWrapper').append(clone);
	});

	$('.is_m').hide();
	$('input[name="is_marijuana"]').change(function(event) {
		event.preventDefault();
		var currentOption = $(this).val();
		if(currentOption == 1){
			$('.is_m').slideDown('slow');
		}
		if(currentOption == 0) {
			$('.is_m').slideUp('slow');
		}
	});

    $( "#sortable" ).sortable({ axis: 'y' });
    $( "#sortable" ).disableSelection();

    $('#deactiveProduct').click(function(){
    	$.ajax({
    		url: '{{ url('dispensary/product/deactive') }}',
    		type: 'POST',
    		data: {pid: $(this).data("product_id")},
    	})
    	.done(function(data) {
    		if(data.message == 'success') {
	    		location.reload();
    		}
    	});    	
    });

    $('#activeProduct').click(function(){
    	$.ajax({
    		url: '{{ url('dispensary/product/active') }}',
    		type: 'POST',
    		data: {pid: $(this).data("product_id")},
    	})
    	.done(function(data) {
    		if(data.message == 'success') {
	    		location.reload();
    		}
    	});    	
    });

	$('#trashProduct').click(function(){
    	$.ajax({
    		url: '{{ url('dispensary/product/trash') }}',
    		type: 'POST',
    		data: {pid: $(this).data("product_id")},
    	})
    	.done(function(data) {
    		if(data.message == 'success') {
	    		location.reload();
    		}
    	});    	
    });

    $('#restoreProduct').click(function(){
    	$.ajax({
    		url: '{{ url('dispensary/product/restore') }}',
    		type: 'POST',
    		data: {pid: $(this).data("product_id")},
    	})
    	.done(function(data) {
    		if(data.message == 'success') {
	    		location.reload();
    		}
    	});    	
    });
    
	var gid = <?php echo $gID; ?>;
	$("#dispensary").select2().val(gid).trigger('change');
});


$(document).on('change', '.fileget', function(event){
	$(this).parent('.thumbnail').children('img').attr('src',URL.createObjectURL(event.target.files[0]));
	$(this).parent('.thumbnail').children('.caption.visibilty').css('display', 'block');
	$(this).hide();
});

$(document).on('click', '.customaddimg', function(){
	$(".replacewithme").replaceWith('<li><div class="thumbnail"><img class="uploadme" src="{{ asset('img/upload.png') }}" alt=""><input name="image_id[]" type="hidden" value=""><input class="fileget" type="file" name="path[]" /><div class="caption visibilty"><ul class="list-inline"><li><a class="close_icon" href="#x"></a></li><li><a class="star_icon" href="#x"></a></li></ul></div></div></li><span class="replacewithme"></span>');
});

$(document).on('click', '.uploadme', function(){
	$(this).parent('.thumbnail').children('input').click();
});

$(document).on('click', '.close_icon', function(){
	var $thumbnail = $(this).parent('li').parent('ul').parent('.caption').parent('.thumbnail');
	var image_id = $thumbnail.find('#imgID').val();

	if(image_id) {
		$.ajax({
			url: '{{ url('dispensary/product/imagedelete') }}',
			type: 'POST',
			data: {imageId: image_id},
		})
		.done(function(data) {
			if(data.message == 'success') {
				$thumbnail.parent('li').remove();
			}
		});
	} else {
		$(this).parent('li').parent('ul').parent('.caption').parent('.thumbnail').parent('li').remove();
	}
});

$(document).on('click', '.star_icon', function(){
	var $thumbnail = $(this).parent('li').parent('ul').parent('.caption').parent('.thumbnail');
	var image_id = $thumbnail.find('#imgID').val();
	var id = $thumbnail.find('#id').val();
	if(image_id) {
		$.ajax({
			url: '{{ url('dispensary/product/image-featured') }}',
			type: 'POST',
			data: {imageId: image_id, id: id},
		})
		.done(function(data) {
			if(data.message == 'success') {
				location.reload();
			}
		});
	} else {
		alert('Currently, This image is not uploaded on server. Please complete the upload process, then you can set this image as featured image.');
	}
});

</script>
@endsection