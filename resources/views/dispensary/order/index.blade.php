{{-- Dispensary Admin --}}
@extends('layouts.dispensary-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('dispensary/orders'); ?>>
			<a href="{{ url('/dispensary/orders') }}">Orders</a>
		</li>
	</ul>
</nav>	

<div class="bg-white">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-3">
					<ul class="list-inline order-table-icons">
						<li>
							<a class="refreshRecords" href="javascript:void(0)"></a>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.2 17" style="enable-background:new 0 0 22.2 17;" xml:space="preserve">
								<g>
									<g>
										<path class="st0" d="M3.1,11.4C3.1,11.4,3.1,11.4,3.1,11.4c-0.2,0-0.3-0.1-0.4-0.2L0.1,7.2C-0.1,7,0,6.7,0.2,6.5
											c0.2-0.2,0.5-0.1,0.7,0.1L3.2,10l2.6-3.1c0.2-0.2,0.5-0.2,0.7-0.1C6.7,7,6.7,7.4,6.5,7.6l-3,3.6C3.4,11.3,3.3,11.4,3.1,11.4z"/>
									</g>
									<g>
										<path class="st0" d="M21.7,11.5c-0.2,0-0.3-0.1-0.4-0.2l-2.3-3.4L16.5,11c-0.2,0.2-0.5,0.2-0.7,0.1c-0.2-0.2-0.2-0.5-0.1-0.7
											l3-3.6c0.1-0.1,0.3-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2l2.6,3.9c0.2,0.2,0.1,0.5-0.1,0.7C21.9,11.5,21.8,11.5,21.7,11.5z"/>
									</g>
									<g>
										<path class="st0" d="M11.1,17c-2.2,0-4.3-0.9-6-2.5c-0.2-0.2-0.2-0.5,0-0.7c0.2-0.2,0.5-0.2,0.7,0c1.9,1.8,4.5,2.6,7,2
											c3.8-0.9,6.5-4.8,5.7-8.6c-0.1-0.3,0.1-0.5,0.4-0.6c0.3-0.1,0.5,0.1,0.6,0.4c0.8,4.3-2.1,8.7-6.5,9.7C12.4,16.9,11.8,17,11.1,17z"
											/>
									</g>
									<g>
										<path class="st0" d="M3.1,11.3c-0.2,0-0.4-0.1-0.5-0.4C2,8.7,2.4,6.4,3.6,4.4c1.2-2.1,3.3-3.6,5.6-4.1c3.1-0.7,6.2,0.3,8.3,2.6
											c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0c-1.8-2.1-4.6-3-7.3-2.3c-2,0.5-3.8,1.8-5,3.7c-1.1,1.8-1.4,3.9-0.8,5.8
											c0.1,0.3-0.1,0.5-0.4,0.6C3.2,11.3,3.2,11.3,3.1,11.3z"/>
									</g>
								</g>
							</svg>
							Refresh
						</li>
					</ul>
				</div>
				<div class="col-md-5 col-sm-4">
					<div class="btn-disp-input2 form-search dtb">
						<div class="form-group">
					        <input type="text" class="form-control" id="search_dtable" placeholder="Search for...">
					        <button type="button" class="btn btn-default">
					        	<?php 
					                echo embed_svg(
					                    $class = '', 
					                    $svgurl = 'img/search-icon.svg', 
					                    $height = '20px', 
					                    $width = '20px', 
					                    $link = ''
					                ); 
					            ?>
					        </button>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-5">
					<input type="hidden" name="min" id="min" value="">
					<input type="hidden" name="max" id="max" value="">
					<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px; border: 1px solid #ccc; width: 100%">
					    <i class="glyphicon glyphicon-calendar"></i>&nbsp;
					    <span></span>&nbsp;<b class="caret"></b>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
			
				<table id="dTableOrder" class="table table-hover">
					<thead>
						<tr>
							<td>Order #</td>
							<td>Order Date</td>
							<td>Order Time</td>
							<td>Customer</td>
							<td>Purchase</td>
							<td>Status</td>
						</tr>
					</thead>
					{{-- Ajax Content Load --}}
				</table>

			</div>
		</div>
	</div>
</div>
@endsection
@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection

@section('customJs')
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var iFini = document.getElementById('min').value;
        var iFfin = document.getElementById('max').value;
        var iStartDateCol = 1;
        var iEndDateCol = 1;
 
        iFini = iFini.substring(6,10) + iFini.substring(0,2) + iFini.substring(3,5);
        iFfin = iFfin.substring(6,10) + iFfin.substring(0,2) + iFfin.substring(3,5);
 
        var datofini = aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(0,2) + aData[iStartDateCol].substring(3,5);
        var datoffin = aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(0,2) + aData[iEndDateCol].substring(3,5);

        // alert(iFini + '<=' + datofini + '&&' + iFfin + '>=' + datoffin);

        if ( iFini === "" && iFfin === "" ) 
        {
            return true;
        }
        else if ( iFini <= datofini && iFfin === "")
        {
            return true;
        }
        else if ( iFfin >= datoffin && iFini === "")
        {
            return true;
        }
        else if (iFini <= datofini && iFfin >= datoffin)
        {
            return true;
        }
        return false;
    }
);
$(document).ready(function($) {

	var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        $('#min').attr('value', start.format('MM/DD/YYYY')).change();
        $('#max').attr('value', end.format('MM/DD/YYYY')).change();
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);

	var oTable = $('#dTableOrder').DataTable({
		"ajax": "{{ url('/dispensary/orders/json') }}",
		columns: [
            { data: 'id' },
			{ data: 'order_date' },
			{ data: 'order_time' },
            { data: 'customer_name' },
            {
				data: 'null', 
				defaultContent:"<div class='order_value'></div>" 
            },
            { data: 'order_status' },
        ],
		"sDom": '<"top">rt<"bottom"p><"clear">',
		"stateSave": true,
		"lengthMenu": [ 10, 25, 50, 75, 100 ],
	    "iDisplayLength": 10,
	    "bSort" : false,
	    // "aaSorting": [[0, 'asc']],
	    "destroy": true,
		"language": {
		    "oPaginate": {
	          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
	          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
	        }
	    },
	    "createdRow": function( row, data, dataIndex ) {
	    	$(row).attr('id', 'row-'+data['id']);

	    	if(data['order_value']) {
				$(row).find('.order_value').text('$'+data['order_value']);
	    	}
	  	}
  	});

	$('#search_dtable').keyup(function(){
        oTable.search($(this).val()).draw();
    });

	$('#dTableOrder tbody').on( 'click', 'tr', function (e) {
		e.preventDefault();
		var tblData = oTable.row(this).data();
		var orderid = tblData['id'];
		var href = "{!! url('/dispensary/order/"+orderid+"') !!}";
		window.location.href = href;
	});

	$('#min').on('change', function() { 
		oTable.draw(); 
	});
	$('#max').on('change', function() { 
		oTable.draw(); 
	});
});

$(document).on("click", '.refreshRecords', function (e) {
	$('#dTableOrder').DataTable().ajax.reload(null, true);
});

</script>
@endsection