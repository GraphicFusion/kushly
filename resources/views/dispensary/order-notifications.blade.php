@php
	$groupID = Session::get('activeGroup');
    $OP = App\OrderProduct::select('order_id', 'product_id')
        ->groupBy('order_id') 
        ->get();
    $ordersID = [];
    foreach ($OP as $key => $val) {
        $group = App\Product::select('group_id')->where('id',$val['product_id'])->first();
        if($group['group_id'] == $groupID) {
            $ordersID[] = $val['order_id'];
        }
    }
    // dd($ordersID);

    $orders = App\Order::select(
            'id', 
            DB::raw('DATE_FORMAT(timestamp_order_made, "%m/%d/%Y") as order_date'),
            DB::raw('DATE_FORMAT(timestamp_order_made, "%h:%i %p") as order_time')
        )
        ->where('active', 1)
        ->whereNull('timestamp_order_accepted_by_dispensary')
        ->whereIn('id', $ordersID)
        ->orderBy('id','desc')
        ->get();

	// pr($orders);

    if(sizeof($orders) > 0) {
		echo '<div class="alert alert-info text-center" role="alert">';
		echo '<h5>'.sizeof($orders).' Orders are pending to accept. <a href="'.url('/dispensary/orders').'">Click Here</a> to see your orders.</h5>';
		// echo '<ul class="list-inline">';
	 //    foreach ($orders as $key => $order) {
		// 	echo '<li><span class="badge">'.$order->id.' dt. '.$order->order_date.'</span></li>';
	 //    }
  //       echo '</ul>';
        echo '</div>';
    }

@endphp


