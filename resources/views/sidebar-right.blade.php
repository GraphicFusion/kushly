<nav id="myNavmenu" class="navmenu navmenu-inverse navmenu-fixed-right offcanvas" role="navigation">
    <a href="javascript:void(0)" class="navbar-rside btn-nav-close" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas=".main-app" title="Other Links">&times;</a>

    {{-- <a class="navmenu-brand" href="#">Brand</a> --}}
    <ul class="nav navmenu-nav">
        <?php $pages = App\Pages::get(); ?>
        @foreach ($pages as $key => $page)
            <li <?php echo navActive($page->slug); ?>>
				<?php if ($page->page_title == 'voidBecome a Driver' ){ $slug = "/blog/become-a-driver/"; } else{ $slug = '/page/'.$page->slug; } ?>
                <a href="{{ url($slug) }}">{{ $page->page_title }}</a>
            </li>
        @endforeach
        <li>
            <a href="{{ url('/blog') }}">Blog</a>
        </li>
		<li>
			<a href="{{ url('/products') }}">Products</a>
		</li>
    </ul>
    
    @if(Sentinel::check())
        <ul class="nav navmenu-nav user-role" id="activeRole">
            @php
                $active = '';
                $roles = Sentinel::findById(getUserId())->roles;
                $ajaxURL = makeURL('/ajax/active-role');
                foreach ($roles as $role) {
                    // make li active
                    if(getActiveRole() == $role->slug){
                        $active = 'class="active"';
                    } else {
                        $active = '';
                    }
                    
                    switch ($role->slug) {
                        case 'super-admin':
                            $redirectURL = makeURL('/super');
                            echo '<li '.$active.'><a href="javascript:void(0)" data-aurl="'.$ajaxURL.'" data-url="'.$redirectURL.'" data-role="'.$role->slug.'">'.$role->name.'</a></li>';
                            break;
                        case 'dispensary-admin':
                            
                            $redirectURL = makeURL('/dispensary/home');
                            $user = Sentinel::findById(getUserId());
                            $assigned_groups = $user->groups;
                            if(count($assigned_groups) == 0) {
                                echo '<p style="color:red;margin:0;">No dispensary allocated.</p>';
                            } else {
                                $defaultActiveGroup = '';
                                foreach ($assigned_groups as $key => $group) {
                                    if($key == 0) {
                                        $defaultActiveGroup = $group->id;
                                    }
                                }
                                echo '<li '.$active.'><a href="javascript:void(0)"  data-aurl="'.$ajaxURL.'" data-url="'.$redirectURL.'" data-role="'.$role->slug.'" data-group-id="'.$defaultActiveGroup.'">'.$role->name.'</a>';
                                echo '<ul class="list-unstyled listAssignedGroups">';
                                foreach ($assigned_groups as $key => $group) {
                                    if(Session::get('activeGroup') == $group->id) {
                                        echo '<li class="active"><a href="javascript:void(0)" data-url="'.$redirectURL.'" data-aurl="'.$ajaxURL.'" data-role="'.$role->slug.'" data-group-id="'.$group->id.'">'.title_case($group->name).'</a></li>';
                                    } else {
                                        echo '<li><a href="javascript:void(0)" data-aurl="'.$ajaxURL.'" data-url="'.$redirectURL.'" data-role="'.$role->slug.'" data-group-id="'.$group->id.'">'.title_case($group->name).'</a></li>';
                                    }
                                }
                                echo '</ul></li>';
                            }

                            break;
                        case 'driver':
                            $redirectURL = makeURL('/driver');
                            echo '<li '.$active.'><a href="javascript:void(0)" data-aurl="'.$ajaxURL.'" data-url="'.$redirectURL.'" data-role="'.$role->slug.'">'.$role->name.'</a></li>';
                            break;
                        case 'customer':
                            $redirectURL = makeURL('/products');
                            echo '<li '.$active.'><a href="javascript:void(0)" data-aurl="'.$ajaxURL.'" data-url="'.$redirectURL.'" data-role="'.$role->slug.'">'.$role->name.'</a></li>';
                            break;
                    }
                    
                }
            @endphp
        </ul>
    @endif

    <ul class="nav navmenu-nav">
        @if(checkRole('super-admin'))
            <li class="visible-sm visible-xs">
                <a href="{{ url('/super/drivers') }}" title="Drivers">Drivers</a>
            </li>
            <li class="visible-sm visible-xs">
                <a href="{{ url('/super/customers') }}" title="Customers">Customers</a>
            </li>

            <li <?php echo navActive('super/users'); ?>>
                <a href="{{ url('/super/users') }}">Manage Users</a>
            </li>

            <li <?php echo navActive('super/verifications'); ?>>
                <a href="{{ url('/super/verifications') }}">Manage Card Verifications</a>
            </li>

            <!--li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Users <b class="caret"></b></a>
                <ul class="dropdown-menu navmenu-nav" role="menu">
                    <li <?php echo navActive('super/users'); ?>><a href="{{ url('/super/users') }}">Users</a></li>
                    <li <?php echo navActive('super/roles'); ?>><a href="{{ url('/super/roles') }}">Roles</a></li>
                </ul>
            </li-->
            <li <?php echo navActive('super/manage-pages'); ?>>
                <a href="{{ url('/super/manage-pages') }}" title="Manage Pages">Manage Pages</a>
            </li>
            <li <?php echo navActive('super/locations'); ?>>
                <a href="{{ url('/super/locations') }}" title="Manage Locations">Manage Locations</a>
            </li>
            <li <?php echo navActive('super/reports/allocated-drivers'); ?>>
                <a href="{{ url('/super/reports/allocated-drivers') }}">Allocated Drivers</a>
            </li>
        @endif

        @if(checkRole('dispensary-admin'))
            <li <?php echo navActive('dispensary/manage-locations'); ?>>
                <a href="{{ url('/dispensary/manage-locations') }}" title="Manage Locations">Manage Locations</a>
            </li>
        @endif

        @php
            if(checkRole('super-admin')){
                $url = '/super/logout';
            } else {
                $url = '/logout';
            }
        @endphp

        @if(Sentinel::Check())
            <li>
                <form action="{{ url($url) }}" method="post" id="formLogout">
                    {{ csrf_field() }}
                    <a href="#" onclick="document.getElementById('formLogout').submit()">Sign-Out</a>
                </form>
            </li>
        @else
            <li><a href="{{ url('/signin') }}">Sign-In</a></li>
            <li><a href="{{ url('/signup') }}">Sign-Up</a></li>
        @endif

    </ul>
</nav>