{{-- Response of filter ajax call --}}
@if(Session::has('addToCart'))
<?php
$sessionValues = array_values(Session::get('addToCart'));
$procutIds= array_column($sessionValues,'product_id');
?>
@else
<?php $procutIds =[];?>
@endif
@if(sizeof($products) == 0)
	<h3 class="text-center text-danger" style="width:100%;">We currently have no products available in your selected area. Take a look at what is offered in other areas. </h3>
@else
	@foreach ($products as $key => $product)

<style>
    .box
    {
    	margin:0px 2px;
    }
	.box_list
	{
		background-color: #ddd;
		width:40px;
		height: 50px;
		margin:0 auto;
	}
	.cash
	{
		color:#21DB00;
	}
	.box h4 {
	    font-family: 'Lato', sans-serif;
	    font-weight: 400;
	    font-size: 18px;
	    text-align: center;
	    line-height: 2.5;
	    color: #fff;
	}
	.box_list:after
	{
		content: '';
		position: absolute;
		border-right: 10px solid transparent;
		border-left: 10px solid transparent;
		border-top: 10px solid #ddd;
		left:18px;
		top: 60px;
	}
	.add_button
	{
		width:100%;
		font-size: 18px;
	}
	.add_button a 
	{
		color:#fff !important;
	}
	.box_list_active
	{
		background-color: #333;
		width:40px;
		height: 50px;
		margin:0 auto;

	}
	.box_list_active:after
	{
		content: '';
		position: absolute;
		border-right: 10px solid transparent;
		border-left: 10px solid transparent;
		border-top: 10px solid #333;
		left:18px;
		top: 60px;
	}
	.products-grid .error .alert 
	{		  
	    border-radius: 4px;
	    font-size: 12px;
	    margin: 5px auto 0px;
	}
	.product .caption h3 {
	    font-family: 'Fjalla One', sans-serif;
	    font-weight: 400;
	    font-size: 22px;
	    text-align: center;
	    text-transform: uppercase;
	    margin: 10px 0 15px;
	    letter-spacing: -1px;
	    height: 55px;
	}
</style>


<!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6" id="p-34"> -->
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="{{$product->id}}">
	<div class="thumbnail product">
		<a href="javascript:void()" class="loadProductModalSlider" data-pid="{{$product->id}}">
			<?php
				$proImg = App\ProductImages::select('id', 'path')
                    ->where('product_id', $product->id)
                    ->where('featured', 1)                
                    ->first();
            	$featuredImage = (!empty($proImg['path'])) ? url('product-images/'.$proImg['path']) : url('img/product-thumb.jpg');
            	$proPrice = App\Price::where('product_id',$product->id)->first();
            	if(count($proPrice)){
            		$unit_price = '$ '.$proPrice->unit_price;
            		$unit_pricee = $proPrice->unit_price;
            		$variant_id = $proPrice->id;
            		$quantity_per_unit = $proPrice->quantity_per_unit;
            	}else{
            		$unit_price = '$ 0';
            		$unit_pricee = 0;
            		$variant_id = '';
            		$quantity_per_unit = 0;
            	}
			?>
			<img src="{{ $featuredImage }}" alt="">
		</a>
		<div class="caption clearfix">


			<span class="category">
				<?php
					if(!empty($product->type) && !empty($product->strain_type)){
						echo $product->type.'/'.$product->strain_type;
					} elseif (!empty($product->type) && empty($product->strain_type)) {
						echo $product->type;
					} elseif (empty($product->type) && !empty($product->strain_type)) {
						echo $product->strain_type;
					}
				?>				
			</span>
			<h3>{{ $product->product_name }}</h3>
			<?php 
			$varPrice ='0'; $varId='';
			//print_r($sessionValues);
					if(in_array($product->id, $procutIds)) { 
		                $arraySearch = array_search($product->id, array_column($sessionValues, 'product_id'));
		                //echo $arraySearch;
		                
		                $varId = $sessionValues[$arraySearch]['variant_id'];
		                $varPrice = $sessionValues[$arraySearch]['unit_price'];
		            	
		                
		                
					}
			?>

			<?php
						$set = '';
						$cartVariants = [];
						if(Session::get('addToCart')) {
							$cartVariant = Session::get('addToCart');
							foreach ($cartVariant as $key => $cv) {
							 	$cartVariants[] = $cv['variant_id'];
							}
						}
					
						$proPrice = App\Price::select('id','product_id','quantity_per_unit','unit_price','stock')
		                    ->where('product_id', $product->id)
		                    ->get();
		                    $count = 0;
		                    $proPrices = $proPrice->toArray();

		                 
				?>
				

			
			<h4 class="text-center"><span id="productPriceByVarient_{{$product->id}}">$ {{$varPrice}}</span></h4>		
			<div class="row data_row" id="product-opt_<?php echo $product->id; ?>">
				
					@foreach($proPrice as $key => $varient)
							<?php
								if(in_array($varient->id, $cartVariants)) {
									$set="box_list_active";		
								} else {
									$set="";
								}
								if($count==0){
									$class ='box_list_active';
								}else{
									$class = '';
								}
							?>

						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 box" data-product="{{$varient->product_id}}" data-varient="{{$varient->id}}" date-price="{{$varient->id}}">
							<div data-varient="{{$varient->id}}" class="box_list {{$set}} box_list_{{$varient->product_id}} box_list_{{$varient->id}}">
								<h4>{{$varient->quantity_per_unit}}</h4>
							</div>
						</div>
						<?php $count++; ?>
					@endforeach
			</div>

			<ul class="list-inline clearfix">	
				<li>
					<form action="{{ url('addtocart') }}" method="post" id="addtocart_<?php echo $product->id; ?>">
			                    {{ csrf_field() }}
	                    		<input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}"/>
								<input type="hidden" id="unit_price" name="unit_price" value="{{$unit_pricee}}" />
								<input type="hidden" id="variant_id" name="variant_id" value="{{$variant_id}}" />
								<input type="hidden" id="quantity_per_unit" name="quantity_per_unit" value="{{$quantity_per_unit}}" />
								<input type="hidden" id="quantity" name="quantity" value="1" />
								<input type="hidden" id="user_id" name="user_id" value="<?php echo getUserId(); ?>">
								<input type="hidden" id="group_id" name="group_id" value="{{ $product->group_id }}">
					</form>
				</li>
			</ul>	

								<?php
									$carts = Session::get('addToCart');
									if(Session::has('addToCart')) {
										$pr = array();
										foreach ($carts as $key => $cart) {
											$pr[] = $cart['product_id'];
										}
										if(in_array($product->id, $pr)) {
											echo '<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a title="View Cart" class="viewCart" id="viewCart" href="javascript:void(0)">
							<button class="btn btn-primary add_button"><i class="glyphicon glyphicon-eye-open"></i></button></a>
						</div>	 
					</div>';
										} else {
											echo '<div class="row changeIcon">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a href="javascript:void(0)" data-productId='."{$product->id}".' id="add2cart" title="Add to cart">
					<button class="btn btn-primary add_button"><i class="glyphicon glyphicon-plus"></i></button></a>
				</div>	 
			</div>';
										}
									}  else {
										echo '<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a href="javascript:void(0)"  data-productId='."{$product->id}".' id="add2cart" title="Add to cart">
					<button class="btn btn-primary add_button"><i class="glyphicon glyphicon-plus"></i></button></a>
				</div>	 
			</div>';
									}
								?>
			<div class="error"></div>
		</div>
	</div>
</div>
	@endforeach
@endif
<?php 
// use pipe symbol in ajax handler
echo '|'.$count; 
?>