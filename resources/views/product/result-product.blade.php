@extends('layouts.master')
{{-- Response of filter ajax call --}}
@section('content')
@foreach ($products as $key => $product)
	<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6" id="p-{{ $product->id }}">
		<div class="thumbnail product">
			<a href="javascript:void()" class="loadProductModalSlider" data-pid="{{ $product->id }}">
				<?php
					$proImg = App\ProductImages::select('id', 'path')
	                    ->where('product_id', $product->id)
	                    ->where('featured', 1)                
	                    ->first();
                	$featuredImage = (!empty($proImg['path'])) ? url('product-images/'.$proImg['path']) : url('img/product-thumb.jpg');
				?>
		        <img src="{{ $featuredImage }}" alt="">
			</a>
			<div class="caption clearfix">
				<span class="category">
					<?php
						if(!empty($product->type) && !empty($product->strain_type)){
							echo $product->type.'/'.$product->strain_type;
						} elseif (!empty($product->type) && empty($product->strain_type)) {
							echo $product->type;
						} elseif (empty($product->type) && !empty($product->strain_type)) {
							echo $product->strain_type;
						}
					?>
				</span>
				<h3>{{ $product->product_name }}</h3>
				<ul class="list-inline clearfix">
					<li>
						<?php
							$set = '';
							$cartVariants = [];
							if(Session::get('addToCart')) {
								$cartVariant = Session::get('addToCart');
								foreach ($cartVariant as $key => $cv) {
								 	$cartVariants[] = $cv['variant_id'];
								}
							}
						?>
						<select name="product-opt" id="product-opt_<?php echo $product->id; ?>" class="form-control product-opt-x" onchange="process(this.value, this.id);">
							<option value="">Select Variant</option>
							@foreach($product->prices as $key => $varient)
								<?php
									if(in_array($varient->id, $cartVariants)) {
										$set="selected='selected'";		
									} else {
										$set="";
									}
								?>
								<option {{ $set }} value="{{ $varient->id.'-'.$varient->unit_price.'-'.$varient->quantity_per_unit }}">
									{{ $varient->unit_price.'('.$varient->quantity_per_unit.')' }}
								</option>
							@endforeach
						</select>
					</li>
					<li>
						<form action="{{ url('addtocart') }}" method="post" id="addtocart_<?php echo $product->id; ?>">
		                    {{ csrf_field() }}
							<input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}"/>
							<input type="hidden" id="unit_price" name="unit_price" value="" />
							<input type="hidden" id="variant_id" name="variant_id" value="" />
							<input type="hidden" id="quantity_per_unit" name="quantity_per_unit" value="" />
							<input type="hidden" id="quantity" name="quantity" value="1" />
							<input type="hidden" id="user_id" name="user_id" value="<?php echo getUserId(); ?>">
							<input type="hidden" id="group_id" name="group_id" value="{{ $product->group_id }}">
							<?php
								$carts = Session::get('addToCart');
								if(Session::has('addToCart')) {
									$pr = array();
									foreach ($carts as $key => $cart) {
										$pr[] = $cart['product_id'];
									}
									if(in_array($product->id, $pr)) {
										echo '<div class="icon"><a title="View Cart" class="viewCart" id="viewCart" href="javascript:void(0)"><i class="glyphicon glyphicon-eye-open"></i></a></div>';
									} else {
										echo '<div class="icon"><a href="javascript:void(0)" id="add2cart" title="Add to cart"></a><svg class="embed"><use xlink:href="#plusIcon"/></svg></div>';
									}
								}  else {
									echo '<div class="icon"><a href="javascript:void(0)" id="add2cart" title="Add to cart"></a><svg class="embed"><use xlink:href="#plusIcon"/></svg></div>';
								}
							?>
		                </form>
					</li>
				</ul>
				<div class="error"></div>
			</div>
		</div>
	</div>
@endforeach

<?php 
// use pipe symbol in ajax handler
echo '|'.$count; 
?>
@endsection
@section('customCss')
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2-bootstrap.min.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('lib/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={!! \Config::get('app.gmap') !!}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>

<script type="text/javascript">

var geocoder= new google.maps.Geocoder();
window.onload = function() {
	var startPos;
	var geoOptions = {
		enableHighAccuracy: true,
		timeout: 10 * 1000
	};
	var geoSuccess = function(position) {
		startPos = position;
		document.getElementById('startLat').value = startPos.coords.latitude;
		document.getElementById('startLon').value = startPos.coords.longitude;

		codeLatLng(startPos.coords.latitude, startPos.coords.longitude);
	};
	var geoError = function(error) {
		console.log('Error occurred. Error code: ' + error.code);
		// error.code can be:
		//   0: unknown error
		//   1: permission denied
		//   2: position unavailable (error response from location provider)
		//   3: timed out
	};
  	navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);

	var codeLatLng = function(lat, lng) {
	    var latlng = new google.maps.LatLng(lat, lng);
	    geocoder.geocode({'latLng': latlng}, function(results, status) {
	      	if(status == google.maps.GeocoderStatus.OK) {
	          	if(results[1]) {
	              	//formatted address
	              	var city = results[0].address_components[4].short_name;
	              	if(city != 'Las Vegas'){
	              		document.getElementById('location_error').innerHTML = '<div class="alert alert-danger">Currently our service is avaliable only in <b>Las Vegas, NV</b></div>';
	              	}
	              	// alert(city);
	          	} else {
	            	alert("No results found");
	          	}
	      	} else {
	          	alert("Geocoder failed due to: " + status);
	      	}
	    });
	}
};

// FilterNav Sidebar
/* Set the width of the side navigation to 250px */
function openFilterNav() {
    document.getElementById("filterNav").style.marginLeft = "0";
}

/* Set the width of the side navigation to 0 */
function closeFilterNav() {
    document.getElementById("filterNav").style.marginLeft = "-250px";
}

$(document).ready(function() {
	// Customize select box via select2 js adding bootstrap stheme
	$.fn.select2.defaults.set( "theme", "bootstrap" );
	// Customize select box via select2 js
	$(".js-select-ws").select2({
	  	placeholder: function(){
	        $(this).data('placeholder');
	    },
  		allowClear: true
	});

	$(".js-select-wos").select2({
		minimumResultsForSearch: -1
	});

	// Add nicescroll to filter Nav bar	
	$(".filterOptions").niceScroll({
		cursorcolor:"#21DB00",
		cursorborder: "1px solid #21DB00",
    	cursorborderradius: "0px",
    	cursordragontouch: true,
    	touchbehavior: true,
		preventmultitouchscrolling: false
	});

    $.ajaxSetup({
	  	headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  	}
	});

    // Ajax call for load more product on home page
    $(document).on('click', '#loadMoreProduct',  function(e){

    	e.preventDefault(); 
		var dataX = {
			nop: $(this).attr('data-nop'),
			limit: $(this).attr('data-limit'),
			offset: $(this).attr('data-offset'),
			groupID: $('#groups').val(),
			options: $('#options').val(),
			viewAll: $('#viewAll').val(),
			q: $('#form-search').find('#q').val()
		}
		callProductFilter('{{ url('/ajax/load-more-product') }}', dataX, 'append');

    })

    // Filter on view all click
	$('.checkbox-x input.viewall').click(function(event) {
		if($(this).is(':checked')){ 
			$('#viewAll').val($(this).val());
			$('#options').val('');
			$('.list-unstyled li .checkbox-x input.opt').prop('checked', false);
		} else {
			$('#viewAll').val('');
		}

		var dataX = {
			groupID: $('#groups').val(),
			options: $('#options').val(),
			viewAll: $('#viewAll').val(),
			nop: $('#loadMoreProduct').attr('data-nop'),
			limit: $('#loadMoreProduct').attr('data-limit'),
			offset: $('#loadMoreProduct').attr('data-offset')
		}
		callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
	});

	// Filter on category options
	$('.checkbox-x input.opt').click(function(event) {
		
		var getcheckvari = "";
		$('.options input.viewall').prop('checked', false);

		$(".list-unstyled li").each(function(){
			var getcheck = $(this).children('.checkbox-x').children('.opt');
			if($(getcheck).is(":checked")){
				getcheckvari += $(this).children('.checkbox-x').children('.opt').val()+','; 
			} 
		});
		getcheckvari = getcheckvari.slice(0,-1);

		$('#options').prop('value', getcheckvari);

		$('#viewAll').val('');
		var dataX = {
			groupID: $('#groups').val(),
			options: getcheckvari,
			viewAll: $('#viewAll').val(),
			nop: $('#loadMoreProduct').attr('data-nop'),
			limit: $('#loadMoreProduct').attr('data-limit'),
			offset: $('#loadMoreProduct').attr('data-offset')
		}
		callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
    });

	// ON PAGE LOAD 
    var dataX = {
		groupID: $('#groups').val(),
		options: $('#options').val(),
		viewAll: $('#viewAll').val(),
		nop: $('#loadMoreProduct').attr('data-nop'),
		limit: $('#loadMoreProduct').attr('data-limit'),
		offset: $('#loadMoreProduct').attr('data-offset')
	}
	callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace-load');

	// $('#form-search input#q').keyup(function(event) {
	// 	if( this.value.length < 3 ) return;
	//    	/* Ajax call to found the search result. */
	//    	var dataX = {
	// 		groupID: $('#groups').val(),
	// 		options: $('#options').val(),
	// 		viewAll: $('#viewAll').val(),
	// 		q: this.value
	// 	}
	// 	callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
	// });

	// Form submit search
	$('#form-search').submit(function(event) {
		event.preventDefault();
		
		$('#loadMoreProduct').addClass('disabled').text('No More Products Available');
		$('#loadMoreProduct').unbind("click");

	   	/* Ajax call to found the search result. */
	   	var dataX = {
			groupID: $('#groups').val(),
			options: $('#options').val(),
			viewAll: $('#viewAll').val(),
			// limit: $('#loadMoreProduct').data('nop'),
			// offset: $('#loadMoreProduct').data('offset'),
			q: $(this).find('#q').val()
		}
		callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
	});

});

 $("#locations").select2({selectOnClose: true}).on("select2:selecting", function(evt) {
 	var selected_location = evt.params.args.data.id;
	var dataX = {
		groupID: $('#groups').val(),
		options: $('#options').val(),
		viewAll: $('#viewAll').val(),
		location: selected_location
	}
	callProductFilter('{{ url('/ajax/product/filter/location') }}', dataX, 'replace');
});

/**
 * Process variant selection
 **/
function process(x, id) {
	var fields = x.split("-");
	var formID = $('#'+id).parent('li').parent('ul').find('form').attr('id');
	$('#'+formID+' input[name="variant_id"]').val(fields[0]);
	$('#'+formID+' input[name="unit_price"]').val(fields[1]);
	$('#'+formID+' input[name="quantity_per_unit"]').val(fields[2]);
}

// Ajax call for modal popup with carousel slider
$(document).on('click', '.loadProductModalSlider', function(e){
    e.preventDefault(); 
    var id = $(this).data('pid');
    $.ajax({
        url: '{{ url('/ajax/load-product-modal') }}',
        type: "POST",
        data: 'id='+id,
        success: function(data){
            // console.log(data);
            $('#loadCarousel').html(data);
            $('#productModal').modal('show');
        },
        error: function(xhr, status, error){
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
});

// Ajax load view cart.
$(document).on('click', '.viewCart', function(e){
    e.preventDefault(); 
    $.ajax({
		url: '{{ url('/ajax/load-view-cart') }}',
		type: 'GET',
	}).done(function(data) {
		$('#viewCartProduct').html(data);
		$('#productModal').modal('hide');
        $('#viewCartModal').modal('show');
        $('[data-toggle="tooltip"]').tooltip();
	});
});

/**
 * Remove from cart
 **/
$(document).on('click', '.removeFromCart', function(e){
	e.preventDefault();
	var $this = $(this);
	$.ajax({
		url: '{{ url('/ajax/remove-product-from-cart') }}',
		type: 'GET',
		data: {key: $this.data('key')},
	}).done(function(data) {
		$('.cart-counter').text(data.count);
		$('#viewCartModal').modal('hide');
		window.location.reload();
    });
});

/**
 * Filter on change dispensary
 **/
$(document).on('change', 'select#dispensary', function(e){
	$('#groups').val($(this).val());
	var dataX = {
		groupID: $('#groups').val(),
		options: $('#options').val(),
		viewAll: $('#viewAll').val()
	}
	callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
});

/**
 * Product filter ajax
 **/
var getcount = "";
var totalclick = "";
var alreadyclick = 1;
var secondcount = 0;

function callProductFilter(url, dataX, method) {

	$.ajax({
		url: url,
		type: 'POST',
		data: dataX,
	}).done(function(data) {
		var dataResponse = data.split('|');

		/*jugard code start*/
		if(getcount == ""){
			getcount = dataResponse[1];
			totalclick = Math.ceil(getcount/8);

		}
		/*jugard code start*/
	
		var limit = $('#loadMoreProduct').attr('data-nop');
		var tp = Math.ceil(dataResponse[1] / limit);
		$("#loadMoreProduct").attr('data-total-pages',tp); 

		var offset = $('#loadMoreProduct').attr('data-offset');

		// Update limit as per offset
		$("#loadMoreProduct").attr('data-limit', (parseInt(offset)));

		// Increase offset as per click
		$("#loadMoreProduct").attr('data-offset', (parseInt(offset) + parseInt(limit)));

		
		if(method == 'append') {

			$('#listProducts').append(dataResponse[0]);
			// Disable load more when reached to page limit
			alreadyclick++;
			if(totalclick == alreadyclick){
				$('#loadMoreProduct').addClass('disabled').text('No More Products Available');
				$('#loadMoreProduct').attr('disabled','disabled');
			}else{
				$('#loadMoreProduct').removeAttr('disabled');
			}


		} else if(method == 'replace') {

			if(dataResponse[1] <= 8){
				$('#loadMoreProduct').addClass('disabled').text('No More Products Available');
				$('#loadMoreProduct').attr('disabled','disabled');
			}else{
				$('#loadMoreProduct').removeClass('disabled').text('LOAD MORE').bind('click');
				$('#loadMoreProduct').removeAttr('disabled');
			}
			$('#loadMoreProduct').attr('data-nop', 8);
		 	$('#loadMoreProduct').attr('data-offset', 8);
		 	getcount = dataResponse[1];
			totalclick = Math.ceil(getcount/8);
			$('#listProducts').html(dataResponse[0]);
			alreadyclick = 1;

		} else if(method == 'replace-load') {

			if(dataResponse[1] <= 8){
				$('#loadMoreProduct').addClass('disabled').text('No More Products Available');
				$('#loadMoreProduct').unbind('click');
			}else{
				$('#loadMoreProduct').removeAttr('disabled');
			}

			getcount = dataResponse[1];
			totalclick = Math.ceil(getcount/8);
			$('#listProducts').html(dataResponse[0]);
		}

		$(".js-select-wos").select2({
			minimumResultsForSearch: -1
		});
	});
}

/**
 * Add to cart
 */
$(document).on('click', '#add2cart', function(){
	var formID = '#'+$(this).parent('.icon').parent('form').attr('id');
	var variant = $(formID).parent('li').parent('ul').find('select').val();
	var iconWrapper = $(this).parent('.icon');
    
	if(!variant) {
		$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('<div class="alert alert-danger">Please Select Variant.</div>');
	} else {
		$.ajax({
			url: '{{ url('addtocart') }}',
			type: 'POST',
			data: {
				product_id: $(formID+' #product_id').val(),
				variant_id: $(formID+' #variant_id').val(),
				unit_price: $(formID+' #unit_price').val(),
				quantity_per_unit: $(formID+' #quantity_per_unit').val(),
				quantity: $(formID+' #quantity').val(),
				user_id: $(formID+' #user_id').val(),
				group_id: $(formID+' #group_id').val(),
			},
		})
		.done(function(data) {
			if(data.message == 'success') {
				$('.cart-counter').text(data.count);
				$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('');
				$(iconWrapper).html('<div class="icon"><a title="View Cart" class="viewCart" id="viewCart" href="#"><i class="glyphicon glyphicon-eye-open"></i></a></div>');
			}
			if(data.error == 'danger') {
				$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('<div class="alert alert-danger">'+data.message+'</div>');
			}
		});
	}
});

/**
 * Add to cart on slider
 */
$(document).on('click', '#add2cartOnSlider', function(){
	var formID = '#'+$(this).parent('.icon').parent('form').attr('id');
	var variant = $(formID).parent('li').parent('ul').find('select').val();
	var iconWrapper = $(this).parent('.icon');

	if(!variant) {
		$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('<div class="alert alert-danger">Please Select Variant.</div>');
	} else {
		$.ajax({
			url: '{{ url('addtocart') }}',
			type: 'POST',
			data: {
				product_id: $(formID+' #product_id').val(),
				variant_id: $(formID+' #variant_id').val(),
				unit_price: $(formID+' #unit_price').val(),
				quantity_per_unit: $(formID+' #quantity_per_unit').val(),
				quantity: $(formID+' #quantity').val(),
				user_id: $(formID+' #user_id').val(),
				group_id: $(formID+' #group_id').val(),
			},
		})
		.done(function(data) {
			if(data.message == 'success') {
				$('.cart-counter').text(data.count);
				$(iconWrapper).html('<div class="icon"><a title="View Cart" class="viewCart btn btn-primary-outline" id="viewCart" href="#">View Cart</a></div>');
			}
			if(data.error == 'danger') {
				$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('<div class="alert alert-danger">'+data.message+'</div>');
			}
		});
	}
});

$(window).on('load', function() {
	var loc = '{{ Session::get('search_location') }}';
	if(loc) {
		alert(loc);
	}
});

/**
 * Add to cart session update
 */
function updateSession(val, product_id) {
	var fields = val.split("-");
	$.ajax({
		url: '{{ url('addtocart/update') }}',
		type: 'POST',
		data: {
			product_id: product_id,
			variant_id: fields[0],
			unit_price: fields[1],
			quantity_per_unit: fields[2]
		}
	})
	.done(function(data) {
		$('#viewCartProduct').html(data);
	});
}

</script>
@endsection