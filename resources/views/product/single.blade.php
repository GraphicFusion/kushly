{{-- Single Product --}}
@extends('layouts.master')

@section('content')
<?php
$set = '';
$cartVariants = [];
$cartProductIDs = [];
$nocp = sizeof(Session::get('addToCart'));

if(Session::get('addToCart')) {
	$cartVariant = Session::get('addToCart');
	foreach ($cartVariant as $key => $cv) {
	 	$cartVariants[] = $cv['variant_id'];
	 	$cartProductIDs[] = $cv['product_id'];
	}
}
?>
<div class="single-product">
	<div class="error text-center"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 visible-xs visible-sm ">
				<div class="title">
					<span>{{ $product->type }}</span>
					{{ $product->product_name }}
				</div>
				<div class="row">
					<div class="col-sm-8 col-xs-12">
						<select name="product-opt" id="down_product-opt_<?php echo $product->id; ?>" class="form-control product-opt-x" onchange="process(this.value, 'addtocartDown');">
							<option value="">Select Variant</option>
							@foreach($product->prices as $key => $varient)
								<?php
									if(in_array($varient->id, $cartVariants)) {
										$set="selected=selected";		
									} else {
										$set="";
									}
								?>
								<option {{ $set }} value="{{ $varient->id.'-'.$varient->unit_price.'-'.$varient->quantity_per_unit }}">
									{{ $varient->unit_price.'('.$varient->quantity_per_unit.')' }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-4 col-xs-12 btn-center">
						
						<form action="{{ url('addtocart') }}" method="post" id="addtocartDown">
		                    {{ csrf_field() }}
							<input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}"/>
							<input type="hidden" id="unit_price" name="unit_price" value="" />
							<input type="hidden" id="variant_id" name="variant_id" value="" />
							<input type="hidden" id="quantity_per_unit" name="quantity_per_unit" value="" />
							<input type="hidden" id="quantity" name="quantity" value="1" />
							<input type="hidden" id="user_id" name="user_id" value="<?php echo getUserId(); ?>">
							<input type="hidden" id="group_id" name="group_id" value="{{ $product->group_id }}">
							<?php
								if(in_array($product->id, $cartProductIDs)) {
									echo '<div class="icon"><a title="View Cart" class="viewCart btn btn-primary-outline" id="viewCart" href="#">View Cart</a></div>';
								} else {
									echo '<div class="icon"><a class="btn btn-primary" href="#" id="add2cartDOWN" data-nocp="'.$nocp.'" title="Add to cart">Add to Cart</a></div>';
								}
							?>
		                </form>

					</div>					
				</div>
			</div>
			<div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 xs-pdlr0 sm-pdlr0">
				<?php
					$countImages = App\ProductImages::select('id','path','sortorder','featured')->where('product_id', $product->id)->count();
				?>
				@if($countImages > 1)
					<div id="carousel-product-image" class="carousel slide" data-ride="carousel">
					  	<div class="carousel-inner" role="listbox">
							<?php
								$images = App\ProductImages::select('id','path','sortorder','featured')->where('product_id', $product->id)->get();
								foreach ($images as $key => $image) {
									// pr($image);
									if($image->featured == 1) {
										$act = 'active';
									} else {
										$act = '';
									}
									echo '<div class="item '.$act.'"><img src="'.asset('/product-images/'.$image->path).'" alt="Product"></div>';
								}
							?>
					  	</div>
					  	<a class="left carousel-control" href="#carousel-product-image" role="button" data-slide="prev"><img src="{{ asset('/img/arrow-left.png') }}" alt="Previous Product"></a>
					  	<a class="right carousel-control" href="#carousel-product-image" role="button" data-slide="next"><img src="{{ asset('/img/arrow-right.png') }}" alt="Next Product"></a>
					</div>
				@elseif($countImages == 1)
					<?php
						$image = App\ProductImages::select('id','path','sortorder','featured')->where('product_id', $product->id)->get()->first();
					?>
					<div class="thumbnail">
						<img src="{{ asset('/product-images/'.$image->path) }}" alt="Product Thumb">
					</div>
				@else
					<div class="thumbnail">
						<img src="{{ asset('/img/product-thumb-lg.jpg') }}" alt="Product Thumb">
					</div>
				@endif
			</div>
			<div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6">
				<div class="visible-lg visible-md">
					<div class="title">
						<span>{{ $product->type }}</span>
						{{ $product->product_name }}
					</div>
					<div class="row">
						<div class="col-sm-8 col-xs-12">
							<select name="product-opt" id="up_product-opt_<?php echo $product->id; ?>" class="form-control product-opt-x" onchange="process(this.value, 'addtocartUP');">
								<option value="">Select Variant</option>
								@foreach($product->prices as $key => $varient)
									<?php
										if(in_array($varient->id, $cartVariants)) {
											$set="selected=selected";		
										} else {
											$set="";
										}
									?>
									<option {{ $set }} value="{{ $varient->id.'-'.$varient->unit_price.'-'.$varient->quantity_per_unit }}">
										{{ $varient->unit_price.'('.$varient->quantity_per_unit.')' }}
									</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-4 col-xs-12 btn-center">

							<form action="{{ url('addtocart') }}" method="post" id="addtocartUP">
			                    {{ csrf_field() }}
								<input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}"/>
								<input type="hidden" id="unit_price" name="unit_price" value="" />
								<input type="hidden" id="variant_id" name="variant_id" value="" />
								<input type="hidden" id="quantity_per_unit" name="quantity_per_unit" value="" />
								<input type="hidden" id="quantity" name="quantity" value="1" />
								<input type="hidden" id="user_id" name="user_id" value="<?php echo getUserId(); ?>">
								<input type="hidden" id="group_id" name="group_id" value="{{ $product->group_id }}">

								<?php
									if(in_array($product->id, $cartProductIDs)) {
										echo '<div class="icon"><a title="View Cart" class="viewCart btn btn-primary-outline" id="viewCart" href="#">View Cart</a></div>';
									} else {
										echo '<div class="icon"><a class="btn btn-primary" href="#" id="add2cartUP" data-nocp="'.$nocp.'" title="Add to cart">Add to Cart</a></div>';
									}
								?>
			                </form>

						</div>					
					</div>
				</div>

				@if($product->is_marijuana == 1)
					<ul class="list-inline list-stat">
						<li><span>THC</span>{{ $product->thc }}%</li>
						<li><span>CBD</span>{{ $product->cbd }}%</li>
						<li><span>CBN</span>{{ $product->cbn }}%</li>
					</ul>
				@endif

				<?php
					$groupID = $product->group_id;
					$group_logo = App\Groups::select('id', 'logo')->where('id', $groupID)->get()->first();
					if(!empty($group_logo['logo'])) {
						echo '<div class="dispensary-logo"><img src="'.url('groups-logo/'.$group_logo['logo']).'" width="120px" height="31px" alt="Group Logo"></div>';
					}
				?>

				<p style="margin-top: 20px;">
					{{ $product->description }}
				</p>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="viewCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" id="viewCartProduct">
  	</div>
  </div>
</div>
@endsection


@section('customJs')
<script type="text/javascript">
$(document).on('click', '.viewCart', function(){
	$.ajax({
		url: '{{ url('/ajax/load-view-cart') }}',
		type: 'GET',
	}).done(function(data) {
		$('#viewCartProduct').html(data);
        $('#viewCartModal').modal('show');
	});
});

$(document).on('click', '#add2cartUP', function(){
	var formID = '#'+$(this).parent('.icon').parent('form').attr('id');
	var variant = $(formID).parent('div').parent('div').find('select').val();
	var iconWrapper = $(this).parent('.icon');

	if(!variant) {
		$('.single-product .error').html('<div class="alert alert-danger">Please Select Variant.</div>');
	} else {
		$.ajax({
			url: '{{ url('addtocart') }}',
			type: 'POST',
			data: {
				product_id: $(formID+' #product_id').val(),
				variant_id: $(formID+' #variant_id').val(),
				unit_price: $(formID+' #unit_price').val(),
				quantity_per_unit: $(formID+' #quantity_per_unit').val(),
				quantity: $(formID+' #quantity').val(),
				user_id: $(formID+' #user_id').val(),
				group_id: $(formID+' #group_id').val(),
			},
		})
		.done(function(data) {
			if(data.message == 'success') {
				$('.cart-counter').text(data.count);
				$('.single-product .error').html('');
				$(iconWrapper).html('<div class="icon"><a title="View Cart" class="viewCart btn btn-primary-outline" id="viewCart" href="#">View Cart</a></div>');
			}
			if(data.error == 'danger') {
				$('.single-product .error').html('<div class="alert alert-danger">'+data.message+'</div>');
			}
		});
	}
});

$(document).on('click', '#add2cartDOWN', function(){
	var formID = '#'+$(this).parent('.icon').parent('form').attr('id');
	var variant = $(formID).parent('div').parent('div').find('select').val();
	var iconWrapper = $(this).parent('.icon');

	if(!variant) {
		$('.single-product .error').html('<div class="alert alert-danger">Please Select Variant.</div>');
	} else {
		$.ajax({
			url: '{{ url('addtocart') }}',
			type: 'POST',
			data: {
				product_id: $(formID+' #product_id').val(),
				variant_id: $(formID+' #variant_id').val(),
				unit_price: $(formID+' #unit_price').val(),
				quantity_per_unit: $(formID+' #quantity_per_unit').val(),
				quantity: $(formID+' #quantity').val(),
				user_id: $(formID+' #user_id').val(),
				group_id: $(formID+' #group_id').val(),
			},
		})
		.done(function(data) {
			if(data.message == 'success') {
				$('.cart-counter').text(data.count);
				$('.single-product .error').html('');
				$(iconWrapper).html('<div class="icon"><a title="View Cart" class="viewCart btn btn-primary-outline" id="viewCart" href="#">View Cart</a></div>');
			}
			if(data.error == 'danger') {
				$('.single-product .error').html('<div class="alert alert-danger">'+data.message+'</div>');
			}
		});
	}
});


/**
 * Process variant selection
 **/
function process(x, id) {
	var fields = x.split("-");
	$('#'+id+' #variant_id').val(fields[0]);
	$('#'+id+' #unit_price').val(fields[1]);
	$('#'+id+' #quantity_per_unit').val(fields[2]);
}

	
$(document).on('click', '.removeFromCart', function(e){
	e.preventDefault();
	var $this = $(this);
	$.ajax({
		url: '{{ url('/ajax/remove-product-from-cart') }}',
		type: 'GET',
		data: {key: $this.data('key')},
	}).done(function(data) {
		$('.cart-counter').text(data.count);
		$('#viewCartModal').modal('hide');
		window.location.reload();
    });
});

</script>
@endsection