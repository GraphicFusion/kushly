{{-- Login Form --}}
@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-content">
					<div class="page-header">
						<h3>Search Result</h3>
					</div>
					<?php
						if(count($data) > 0 ) {
							foreach ($data as $key => $v) {
								echo $v->product_name.'<br/>';
							}
						} else {
							echo '<h4 class="text-center">No record found.</h4>';
						}
					?>
				</div>
			</div>
		</div>
	</div>
@endsection