@extends('layouts.cart')

@section('customCss')
<style>
    body { margin-top: 0; }
</style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="alert alert-danger">
                    <h2>{!! $msg !!}</h2>
                </div>
                <p>Please contact support to solve this issue.</p>
            </div>
        </div>
    </div>
@endsection