@extends('layouts.error')
@section('content')
    <div class="container">
        <div class="content">
            <h3>Email sending failed due to host connection could not be established.</h3>
            <a class="btnBack" href="#x" onclick="return history.back()">Back</a>
        </div>
    </div>
@endsection
@section('customCss')
<style>
    body {
        margin-top:0 !important;
    }
    .content{text-align: center;padding-top: 7%;}
</style>
@endsection