{{-- Customer Sign Up Form --}}
@extends('layouts.master')

@section('content')
<style>
.progress {
	margin-top: 5px;
    height: 20px;
    margin-bottom: 20px;
    overflow: hidden;
    background-color: #f5f5f5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
    box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}
</style>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

				<div class="signup-from">
					<h3>Sign Up</h3>
					{!! Form::open(['url' => 'signup', 'method' => 'post', 'class' => 'form', 'id' => 'signupfrom','files'=>'true','enctype'=>'multipart/form-data']) !!}
						<div class="form-group {{ $errors->has('first_name') ? 'has-error' :'' }}">
						    {!! Form::label('first_name', 'First Name') !!}
						    {!! Form::text('first_name', '', ['class' => 'form-control']) !!}
						    {!! $errors->first('first_name','<span class="help-block">:message</span>') !!}
						</div>
						<div class="form-group {{ $errors->has('last_name') ? 'has-error' :'' }}">
						    {!! Form::label('last_name', 'Last Name') !!}
						    {!! Form::text('last_name', '', ['class' => 'form-control']) !!}
						    {!! $errors->first('last_name','<span class="help-block">:message</span>') !!}
						</div>
						<div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
						    {!! Form::label('email', 'E-Mail Address') !!}
						    {!! Form::text('email', '', ['class' => 'form-control']) !!}
						    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
						</div>
						<div class="form-group {{ $errors->has('password') ? 'has-error' :'' }}">
						    {!! Form::label('password', 'Password') !!}
						    {!! Form::password('password', ['class' => 'form-control']) !!}
						    {!! $errors->first('password','<span class="help-block">:message</span>') !!}
						</div>
						<div class="form-group {{ $errors->has('re_password') ? 'has-error' :'' }}">
						    {!! Form::label('re_password', 'Confirm Password') !!}
						    {!! Form::password('re_password', ['class' => 'form-control']) !!}
						    {!! $errors->first('re_password','<span class="help-block">:message</span>') !!}
						</div>
						<div class="form-group {{ $errors->has('phone_no') ? 'has-error' :'' }}">
						    {!! Form::label('phone_no', 'Phone No') !!}
						    {!! Form::text('phone_no', '', ['class' => 'form-control']) !!}
						    {!! $errors->first('phone_no','<span class="help-block">:message</span>') !!}
						</div>
						<div class="form-group {{ $errors->has('birthday') ? 'has-error' :'' }}">
						    {!! Form::label('birthday', 'Date of Birth') !!}
						    {!! Form::text('birthday', '', ['class' => 'form-control ll-skin-melon', 'id'=>'datepicker']) !!}
						    {!! $errors->first('birthday','<span class="help-block">:message</span>') !!}
						</div>						

						<div class="form-group {{ $errors->has('stateIdFront') ? 'has-error' :'' }}">
						    {!! Form::label('stateIdFront', 'Upload License / State Id-Front') !!}
						    <input type="file" name="files" id="stateIdFront" class="form-control">
						    <div class="progress progress-striped active" id="stateIdFrontbox" style="display:none;">
										<div class="progress-bar progress-bar-warning" id="stateIdFrontboxprogress"></div>
									</div>
							{!!Form::hidden('stateIdFronthidden','',['id'=>'stateIdFronthidden'])!!}

						</div>

						<div class="form-group {{ $errors->has('stateIdBack') ? 'has-error' :'' }}">
						    {!! Form::label('stateIdBack', 'Upload License / State Id-Back') !!}
						    
						    <input type="file" name="files" id="stateIdBack" class="form-control">
						     <div class="progress progress-striped active" id="stateIdBackbox" style="display:none;">
										<div class="progress-bar progress-bar-warning" id="stateIdBackboxprogress"></div>
									</div>
							{!!Form::hidden('stateIdBackhidden','',['id'=>'stateIdBackhidden'])!!}
						</div>

						<div class="form-group {{ $errors->has('medicalCardFront') ? 'has-error' :'' }}">
						    {!! Form::label('medicalCardFront', 'Upload Medical Card-Front') !!}
						     <input type="file" name="files" id="medicalCardFront" class="form-control">
						     <div class="progress progress-striped active" id="medicalCardFrontbox" style="display:none;">
										<div class="progress-bar progress-bar-warning" id="medicalCardFrontboxprogress"></div>
									</div>
							{!!Form::hidden('medicalCardFronthidden','',['id'=>'medicalCardFronthidden'])!!}
						</div>

						<div class="form-group {{ $errors->has('medicalCardBack') ? 'has-error' :'' }}">
						    {!! Form::label('medicalCardBack', 'Upload Medical Card-Back') !!}
						    <input type="file" name="files" id="medicalCardBack" class="form-control">
						     <div class="progress progress-striped active" id="medicalCardBackbox" style="display:none;">
										<div class="progress-bar progress-bar-warning" id="medicalCardBackboxprogress"></div>
									</div>
							{!!Form::hidden('medicalCardBackhidden','',['id'=>'medicalCardBackhidden'])!!}
						</div>


						{!! Form::hidden('api_token', str_random(60)) !!}
					    {!! Form::submit('Sign Up', ['class'=>'btn btn-primary-outline','id'=>'signupbutton']) !!}
					{!! Form::close() !!}
				</div>

			</div>
		</div>
	</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection

@section('customJs')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
{!!Html::script('js/fileupload.js')!!}
<script type="text/javascript">
	$(document).ready(function(){
		$('#signupbutton').show();
		$('#datepicker').datepicker({
	      	changeMonth: true,
	      	changeYear: true,
	      	yearRange: "1940:c",
	      	beforeShow: function(input, inst) {
				$('#ui-datepicker-div').wrap("<div class='datepicker ll-skin-melon hasDatepicker'></div>");
			},
			afterClose: function(input, inst) {
				$('#ui-datepicker-div').unwrap();
			}
	    });
	});

	 'use strict';       
$('#stateIdBack').fileupload({
    url: '/signup/customerfileupdate',
    headers: { 'X-XSRF-Token': $('meta[name="_token"]').attr('content'), },
    dataType: 'json',
    formData: { path:'' },
    done: function (e, data) {
    	$('#signupbutton').hide();
    	console.log(data);
    	$('.sb_emsg').remove();
    	var iferror = data.result.files[0].error;
    	console.log(iferror);
    	if(iferror != undefined){
    		//alert(iferror); return false;
    		$('#stateIdBack').after('<em style="color:red;" class="sb_emsg">'+iferror+'</em>');
    		return false;
    	}
        var baseurl = document.location.origin;
        var src = baseurl+'/'+data.result.files[0].url;
        $('#stateIdBackhidden').val(src);
        $('#stateIdBack').after('<a target="_blank" style="color:green;" href="'+src+'" class="sb_emsg">'+data.files[0].name+'</a>');
       console.log(src);
        $('#signupbutton').show();
    },
    progressall: function (e, data) {
    	$('#signupbutton').hide();
        $('#stateIdBackbox').show();
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#stateIdBackboxprogress').css(
            'width',
            progress + '%'
        ).text(progress+'%');
        if(progress == 100){ 
        	closeprogress();  
      }
    },error: function(e){
    	$('.sb_emsg').remove();
      console.log(e.responseText);
    }
}).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');


$('#stateIdFront').fileupload({
    url: '/signup/customerfileupdate',
    headers: { 'X-XSRF-Token': $('meta[name="_token"]').attr('content'), },
    dataType: 'json',
    formData: { path:'' },
    done: function (e, data) {
    	$('#signupbutton').hide();
    	console.log(data);
    	$('.sf_emsg').remove();
    	var iferror = data.result.files[0].error;
    	console.log(iferror);
    	if(iferror != undefined){
    		//alert(iferror); return false;
    		$('#stateIdFront').after('<em style="color:red;" class="sf_emsg">'+iferror+'</em>');
    		return false;
    	}
        var baseurl = document.location.origin;
        var src = baseurl+'/'+data.result.files[0].url;
        $('#stateIdFronthidden').val(src);
        $('#stateIdFront').after('<a target="_blank" style="color:green;" href="'+src+'" class="sf_emsg">'+data.files[0].name+'</a>');
       console.log(src);
       $('#signupbutton').show();
    },
    progressall: function (e, data) {
    	$('#signupbutton').hide();
        $('#stateIdFrontbox').show();
        $('.signupsubmit').hide();
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#stateIdFrontboxprogress').css(
            'width',
            progress + '%'
        ).text(progress+'%');
        if(progress == 100){ closeprogress();  }
    },error: function(e){
    	$('.sf_emsg').remove();
      console.log(e.responseText);
    }
}).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');


$('#medicalCardFront').fileupload({
    url: '/signup/customerfileupdate',
    headers: { 'X-XSRF-Token': $('meta[name="_token"]').attr('content'), },
    dataType: 'json',
    formData: { path:'' },
    done: function (e, data) {
    	$('#signupbutton').hide();
    	console.log(data);
    	$('.mcf_emsg').remove();
    	var iferror = data.result.files[0].error;
    	console.log(iferror);
    	if(iferror != undefined){
    		//alert(iferror); return false;
    		$('#medicalCardFront').after('<em style="color:red;" class="mcf_emsg">'+iferror+'</em>');
    		return false;
    	}
        var baseurl = document.location.origin;
        var src = baseurl+'/'+data.result.files[0].url;
        $('#medicalCardFronthidden').val(src);
        $('#medicalCardFront').after('<a target="_blank" style="color:green;" href="'+src+'" class="mcf_emsg">'+data.files[0].name+'</a>');
       console.log(src);
       $('#signupbutton').show();
    },
    progressall: function (e, data) {
    	$('#signupbutton').hide();
        $('#medicalCardFrontbox').show();
        $('.signupsubmit').hide();
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#medicalCardFrontboxprogress').css(
            'width',
            progress + '%'
        ).text(progress+'%');
        if(progress == 100){ closeprogress();  }
    },error: function(e){
    	$('.mcf_emsg').remove();
      console.log(e.responseText);
    }
}).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');


$('#medicalCardBack').fileupload({
    url: '/signup/customerfileupdate',
    headers: { 'X-XSRF-Token': $('meta[name="_token"]').attr('content'), },
    dataType: 'json',
    formData: { path:'' },
    done: function (e, data) {
    	$('#signupbutton').hide();
    	console.log(data);
    	$('.mcb_emsg').remove();
    	var iferror = data.result.files[0].error;
    	console.log(iferror);
    	if(iferror != undefined){
    		//alert(iferror); return false;
    		$('#medicalCardBack').after('<em style="color:red;" class="mcb_emsg">'+iferror+'</em>');
    		return false;
    	}
        var baseurl = document.location.origin;
        var src = baseurl+'/'+data.result.files[0].url;
        $('#medicalCardBackhidden').val(src);
        $('#medicalCardBack').after('<a target="_blank" style="color:green;" href="'+src+'" class="mcb_emsg">'+data.files[0].name+'</a>');
       console.log(src);
       $('#signupbutton').show();
    },
    progressall: function (e, data) {
    	$('#signupbutton').hide();
        $('#medicalCardBackbox').show();
        $('.signupsubmit').hide();
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#medicalCardBackboxprogress').css(
            'width',
            progress + '%'
        ).text(progress+'%');
        if(progress == 100){ closeprogress();  }
    },error: function(e){
    	$('.mcb_emsg').remove();
      console.log(e.responseText);
    }
}).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');

function closeprogress(){
	$('.signupsubmit').show();
	setTimeout(function(){ $('.progress').next().css('width','0%').text(''); $('.progress').hide(); }, 1000);
}
</script>
@endsection