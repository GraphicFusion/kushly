<nav class="navbar navbar-default navbar-xs visible-xs">

    <div class="container-fluid">
        <div class="navbar-header">
            <?php 
                echo embed_svg(
                    $class = 'navbar-brand', 
                    $svgurl = 'img/logo.svg', 
                    $height = '42px', 
                    $width = '113px', 
                    $link = makeURL('/')
                ); 
            ?>
            
            @if(Sentinel::Check())
                <form action="{{ url('/logout') }}" method="post" id="formLogout">
                    {{ csrf_field() }}
                    <a class="btn btn-primary navbar-btn" href="#" onclick="document.getElementById('formLogout').submit()">Sign Out</a>
                </form>
            @else
                <a class="btn btn-primary navbar-btn" href="{{ url('/signup') }}">Sign Up</a>
            @endif
        </div>

        <div class="navbar-mobile">

            <ul class="nav navbar-nav navbar-right main-nav">

                <li <?php echo navActive('products'); ?>>
                    <a href="{{ url('/products') }}" title="Products"></a>
                    <svg version="1.1" id="svg-home" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23.1 24" style="enable-background:new 0 0 23.1 24;" xml:space="preserve">
                    <path class="st0" d="M22.6,16.7C20.8,15.4,18,15,15.8,15c2.2-1.4,5.6-4.1,7.3-8c-4.3,0.8-7.7,3.7-9.6,6C15,8.3,12.8,2.2,11.5,0
                        c-1.2,2.2-3.4,8.3-1.9,13C7.7,10.7,4.3,7.8,0,7c1.7,4,5.1,6.6,7.3,8c-2.3,0-5,0.4-6.9,1.7c1.5,0.9,4.4,1.3,6.8,1.2
                        c-0.9,1.3-1.2,2.5-1.2,2.5s3.1,0,5.1-3c-0.1,1.9,0.1,4.6,0.9,6.5l1.2-0.7c-1.2-1.5-1.3-3.4-1.4-5.9c2,3,5.1,3,5.1,3
                        s-0.3-1.3-1.2-2.5C18.2,18.1,21.1,17.6,22.6,16.7z"/>
                    </svg>
                </li>

                <li <?php echo navActive('#x'); ?>>
                    <a href="{{ url('#') }}"></a>
                    <svg version="1.1" id="svg-marker" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.8 24" style="enable-background:new 0 0 22.8 24;" xml:space="preserve">
                    <path class="st0" d="M4.9,0C2.2,0,0,2.2,0,4.9c0,2.5,4,7.4,4.5,8C4.6,12.9,4.7,13,4.8,13c-0.2,0-0.4,0.2-0.4,0.5v1
                        c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5v-1c0-0.2-0.2-0.4-0.4-0.5c0.1,0,0.2-0.1,0.3-0.2c0.5-0.6,4.5-5.5,4.5-8C9.8,2.2,7.6,0,4.9,0z M4.9,11.8C3.5,10,1,6.5,1,4.9C1,2.7,2.7,1,4.9,1C7,1,8.8,2.7,8.8,4.9C8.8,6.5,6.2,10,4.9,11.8z M4.9,2.7c-1.2,0-2.1,1-2.1,2.1 c0,1.2,1,2.1,2.1,2.1C6.1,7,7,6.1,7,4.9C7,3.7,6.1,2.7,4.9,2.7z M4.9,6C4.2,6,3.7,5.5,3.7,4.9c0-0.6,0.5-1.1,1.1-1.1
                        C5.5,3.7,6,4.2,6,4.9C6,5.5,5.5,6,4.9,6z M17.9,11c-2.7,0-4.9,2.2-4.9,4.9c0,2.5,4,7.4,4.5,8c0.1,0.1,0.2,0.2,0.4,0.2
                        s0.3-0.1,0.4-0.2c0.5-0.6,4.5-5.5,4.5-8C22.8,13.2,20.6,11,17.9,11z M17.9,22.8C16.5,21,14,17.5,14,15.9c0-2.1,1.7-3.9,3.9-3.9
                        c2.1,0,3.9,1.7,3.9,3.9C21.8,17.5,19.2,21,17.9,22.8z M17.9,13.7c-1.2,0-2.1,1-2.1,2.1c0,1.2,1,2.1,2.1,2.1c1.2,0,2.1-1,2.1-2.1
                        C20,14.7,19.1,13.7,17.9,13.7z M17.9,17c-0.6,0-1.1-0.5-1.1-1.1c0-0.6,0.5-1.1,1.1-1.1c0.6,0,1.1,0.5,1.1,1.1
                        C19,16.5,18.5,17,17.9,17z M5.9,23H5.4v-0.5c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5v1c0,0.3,0.2,0.5,0.5,0.5h1
                        c0.3,0,0.5-0.2,0.5-0.5S6.2,23,5.9,23z M14.9,23h-1c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5h1c0.3,0,0.5-0.2,0.5-0.5
                        S15.2,23,14.9,23z M11.9,23h-1c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5h1c0.3,0,0.5-0.2,0.5-0.5S12.2,23,11.9,23z M8.9,23h-1
                        c-0.3,0-0.5,0.2-0.5,0.5S7.6,24,7.9,24h1c0.3,0,0.5-0.2,0.5-0.5S9.2,23,8.9,23z M4.9,18c0.3,0,0.5-0.2,0.5-0.5v-1
                        c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5v1C4.4,17.8,4.6,18,4.9,18z M4.4,20.5c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5v-1
                        c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5V20.5z"/>
                    </svg>
                </li>
                 <li <?php echo navActive('cart'); ?>>
                    <a href="{{ url('/cart') }}"></a>

                    <span class="cart-counter">
                        <?php echo sizeof(Session::get('addToCart')); ?>
                    </span>
                    <svg version="1.1" id="svg-cart" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 20" style="enable-background:new 0 0 24 20;" xml:space="preserve">
                    <path class="st0" d="M23.5,0h-2.6c-0.5,0-0.9,0.4-1,0.9L18.1,14H3.5C3.2,14,3,13.8,3,13.5S3.2,13,3.5,13h12c0.8,0,1.5-0.7,1.5-1.5
                        S16.3,10,15.5,10h-13C2.2,10,2,9.8,2,9.5S2.2,9,2.5,9H16c0.8,0,1.5-0.7,1.5-1.5S16.8,6,16,6H1.5C1.2,6,1,5.8,1,5.5S1.2,5,1.5,5h16
                        C17.8,5,18,4.8,18,4.5S17.8,4,17.5,4h-16C0.7,4,0,4.7,0,5.5S0.7,7,1.5,7H16c0.3,0,0.5,0.2,0.5,0.5S16.3,8,16,8H2.5
                        C1.7,8,1,8.7,1,9.5S1.7,11,2.5,11h13c0.3,0,0.5,0.2,0.5,0.5S15.8,12,15.5,12h-12C2.7,12,2,12.7,2,13.5S2.7,15,3.5,15h14.6
                        c0.5,0,0.9-0.4,1-0.9L20.9,1h2.6C23.8,1,24,0.8,24,0.5S23.8,0,23.5,0z M6,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S7.1,16,6,16z
                         M6,19c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S6.6,19,6,19z M17,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S18.1,16,17,16z M17,19
                        c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S17.6,19,17,19z"/>
                    </svg>
                </li>
            
                <li <?php echo navActive('customer/profile'); ?>>
                    <a href="{{ url('/customer/profile') }}"></a>
                    <svg version="1.1" id="svg-profile" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 12 24" style="enable-background:new 0 0 12 24;" xml:space="preserve">
                    <path class="st0" d="M11.5,9h-11C0.2,9,0,9.2,0,9.5c0,4.8,2.1,6.9,3.5,7.8v6.2C3.5,23.8,3.7,24,4,24h4c0.3,0,0.5-0.2,0.5-0.5v-6.2
                        c1.4-0.9,3.5-3,3.5-7.8C12,9.2,11.8,9,11.5,9z M7.8,16.6c-0.2,0.1-0.3,0.3-0.3,0.4v6h-3v-6c0-0.2-0.1-0.4-0.3-0.4
                        c-2-1.1-3.1-3.4-3.2-6.6h10C10.9,13.2,9.7,15.5,7.8,16.6z M6,8c2.2,0,4-1.8,4-4S8.2,0,6,0S2,1.8,2,4S3.8,8,6,8z M6,1
                        c1.7,0,3,1.3,3,3S7.7,7,6,7S3,5.7,3,4S4.3,1,6,1z"/>
                    </svg>
                </li>
        
                <li <?php echo navActive('#x'); ?>>
                    <a href="javascript:void(0)" class="navbar-rside navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas=".main-app" title="Other Links"></a>

                    <svg version="1.1" id="svg-bar" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 13" style="enable-background:new 0 0 16 13;" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,1h-15C0.2,1,0,0.8,0,0.5S0.2,0,0.5,0h15C15.7,0,16,0.2,16,0.5S15.7,1,15.5,1z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,5h-15C0.2,5,0,4.8,0,4.5S0.2,4,0.5,4h15C15.7,4,16,4.2,16,4.5S15.7,5,15.5,5z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,9h-15C0.2,9,0,8.8,0,8.5S0.2,8,0.5,8h15C15.7,8,16,8.2,16,8.5S15.7,9,15.5,9z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,13h-15C0.2,13,0,12.8,0,12.5S0.2,12,0.5,12h15c0.3,0,0.5,0.2,0.5,0.5S15.7,13,15.5,13z"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                </li>
            </ul>

        </div>
    </div>
</nav>

<nav class="navbar navbar-default navbar-fixed-top navbar-all hidden-xs">

    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php 
                echo embed_svg(
                    $class = 'navbar-brand', 
                    $svgurl = 'img/logo.svg', 
                    $height = '42px', 
                    $width = '113px', 
                    $link = makeURL('/')
                ); 
            ?>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">

			<ul class="nav navbar-nav navbar-right main-nav">
                <li <?php echo navActive('/'); ?>>
                    <a href="{{ url('/') }}" title="Products"></a>
                    <svg version="1.1" id="svg-home" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23.1 24" style="enable-background:new 0 0 23.1 24;" xml:space="preserve">
                    <path class="st0" d="M22.6,16.7C20.8,15.4,18,15,15.8,15c2.2-1.4,5.6-4.1,7.3-8c-4.3,0.8-7.7,3.7-9.6,6C15,8.3,12.8,2.2,11.5,0
                        c-1.2,2.2-3.4,8.3-1.9,13C7.7,10.7,4.3,7.8,0,7c1.7,4,5.1,6.6,7.3,8c-2.3,0-5,0.4-6.9,1.7c1.5,0.9,4.4,1.3,6.8,1.2
                        c-0.9,1.3-1.2,2.5-1.2,2.5s3.1,0,5.1-3c-0.1,1.9,0.1,4.6,0.9,6.5l1.2-0.7c-1.2-1.5-1.3-3.4-1.4-5.9c2,3,5.1,3,5.1,3
                        s-0.3-1.3-1.2-2.5C18.2,18.1,21.1,17.6,22.6,16.7z"/>
                    </svg>
                </li>

                <li id="locationInfo" <?php echo navActive('#x'); ?>>
                    <a href="javascript:void(0)"></a>
                    
                    <ul id="locationPopover">
                        <div class="inner-ul">
                            @if(Sentinel::check())
                                @php
                                    $currentUser = getUserId();
                                    $orders = App\Order::select('id','status')->where('status','!=',6)->where('user_id', $currentUser)->get();
                                @endphp
                                
                                @if(sizeof($orders) == 0)
                                    <li>
                                        <div class="info">
                                            <p>No order placed.</p>
                                        </div>
                                    </li>
                                @else
                                    @foreach($orders as $key => $order)
                                        @php
                                            $od = $order->orderDriver()->first();
                                        @endphp
                                        <li>
                                            <div class="info">
                                                
                                                <p>
                                                    <a href="#x">Order is: {!! $order->id !!}</a>
                                                    <?php
                                                        $status = '';
                                                        $info = '';
                                                        if($order->status == 1) { $status = 'is Placed'; }
                                                        if($order->status == 2) { $status = 'Allocated'; }
                                                        if($order->status == 3) { $status = 'Rejected'; }
                                                        if($order->status == 4) { $status = 'Failure'; }
                                                        if($order->status == 5) { $status = 'Accepted'; $info = 'Estimated delivery time is '.$od->cron_estimate_time; }
                                                        if($order->status == 6) { $status = 'Delivered'; }
                                                        if($order->status == 7) { $status = 'Accepted by dispensary'; }
                                                        if($order->status == 8) { $status = 'Pickup at dispensary'; $info = 'Estimated delivery time is '.$od['cron_estimate_time']; }
                                                        echo $status.'<br/>'.$info;
                                                    ?>
                                                </p>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            @else
                                <li>
                                    <div class="info">
                                        <p>Login to view order list.</p>
                                    </div>
                                </li>
                            @endif
                        </div>
                    </ul>

                    <svg version="1.1" id="svg-marker" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.8 24" style="enable-background:new 0 0 22.8 24;" xml:space="preserve">
                    <path class="st0" d="M4.9,0C2.2,0,0,2.2,0,4.9c0,2.5,4,7.4,4.5,8C4.6,12.9,4.7,13,4.8,13c-0.2,0-0.4,0.2-0.4,0.5v1
                        c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5v-1c0-0.2-0.2-0.4-0.4-0.5c0.1,0,0.2-0.1,0.3-0.2c0.5-0.6,4.5-5.5,4.5-8C9.8,2.2,7.6,0,4.9,0
                        z M4.9,11.8C3.5,10,1,6.5,1,4.9C1,2.7,2.7,1,4.9,1C7,1,8.8,2.7,8.8,4.9C8.8,6.5,6.2,10,4.9,11.8z M4.9,2.7c-1.2,0-2.1,1-2.1,2.1
                        c0,1.2,1,2.1,2.1,2.1C6.1,7,7,6.1,7,4.9C7,3.7,6.1,2.7,4.9,2.7z M4.9,6C4.2,6,3.7,5.5,3.7,4.9c0-0.6,0.5-1.1,1.1-1.1
                        C5.5,3.7,6,4.2,6,4.9C6,5.5,5.5,6,4.9,6z M17.9,11c-2.7,0-4.9,2.2-4.9,4.9c0,2.5,4,7.4,4.5,8c0.1,0.1,0.2,0.2,0.4,0.2
                        s0.3-0.1,0.4-0.2c0.5-0.6,4.5-5.5,4.5-8C22.8,13.2,20.6,11,17.9,11z M17.9,22.8C16.5,21,14,17.5,14,15.9c0-2.1,1.7-3.9,3.9-3.9
                        c2.1,0,3.9,1.7,3.9,3.9C21.8,17.5,19.2,21,17.9,22.8z M17.9,13.7c-1.2,0-2.1,1-2.1,2.1c0,1.2,1,2.1,2.1,2.1c1.2,0,2.1-1,2.1-2.1
                        C20,14.7,19.1,13.7,17.9,13.7z M17.9,17c-0.6,0-1.1-0.5-1.1-1.1c0-0.6,0.5-1.1,1.1-1.1c0.6,0,1.1,0.5,1.1,1.1
                        C19,16.5,18.5,17,17.9,17z M5.9,23H5.4v-0.5c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5v1c0,0.3,0.2,0.5,0.5,0.5h1
                        c0.3,0,0.5-0.2,0.5-0.5S6.2,23,5.9,23z M14.9,23h-1c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5h1c0.3,0,0.5-0.2,0.5-0.5
                        S15.2,23,14.9,23z M11.9,23h-1c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5h1c0.3,0,0.5-0.2,0.5-0.5S12.2,23,11.9,23z M8.9,23h-1
                        c-0.3,0-0.5,0.2-0.5,0.5S7.6,24,7.9,24h1c0.3,0,0.5-0.2,0.5-0.5S9.2,23,8.9,23z M4.9,18c0.3,0,0.5-0.2,0.5-0.5v-1
                        c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5v1C4.4,17.8,4.6,18,4.9,18z M4.4,20.5c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5v-1
                        c0-0.3-0.2-0.5-0.5-0.5s-0.5,0.2-0.5,0.5V20.5z"/>
                    </svg>
                </li>
                <li <?php echo navActive('cart'); ?>>
                    <a href="{{ url('/cart') }}"></a>

                    <span class="cart-counter">
                        <?php echo sizeof(Session::get('addToCart')); ?>
                    </span>

                    <svg version="1.1" id="svg-cart" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 20" style="enable-background:new 0 0 24 20;" xml:space="preserve">
                    <path class="st0" d="M23.5,0h-2.6c-0.5,0-0.9,0.4-1,0.9L18.1,14H3.5C3.2,14,3,13.8,3,13.5S3.2,13,3.5,13h12c0.8,0,1.5-0.7,1.5-1.5
                        S16.3,10,15.5,10h-13C2.2,10,2,9.8,2,9.5S2.2,9,2.5,9H16c0.8,0,1.5-0.7,1.5-1.5S16.8,6,16,6H1.5C1.2,6,1,5.8,1,5.5S1.2,5,1.5,5h16
                        C17.8,5,18,4.8,18,4.5S17.8,4,17.5,4h-16C0.7,4,0,4.7,0,5.5S0.7,7,1.5,7H16c0.3,0,0.5,0.2,0.5,0.5S16.3,8,16,8H2.5
                        C1.7,8,1,8.7,1,9.5S1.7,11,2.5,11h13c0.3,0,0.5,0.2,0.5,0.5S15.8,12,15.5,12h-12C2.7,12,2,12.7,2,13.5S2.7,15,3.5,15h14.6
                        c0.5,0,0.9-0.4,1-0.9L20.9,1h2.6C23.8,1,24,0.8,24,0.5S23.8,0,23.5,0z M6,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S7.1,16,6,16z
                         M6,19c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S6.6,19,6,19z M17,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S18.1,16,17,16z M17,19
                        c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S17.6,19,17,19z"/>
                    </svg>
                </li>
            
                
                <li <?php echo navActive('customer/profile'); ?>>
                    <a href="{{ url('/customer/profile') }}"></a>
                    <svg version="1.1" id="svg-profile" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 12 24" style="enable-background:new 0 0 12 24;" xml:space="preserve">
                    <path class="st0" d="M11.5,9h-11C0.2,9,0,9.2,0,9.5c0,4.8,2.1,6.9,3.5,7.8v6.2C3.5,23.8,3.7,24,4,24h4c0.3,0,0.5-0.2,0.5-0.5v-6.2
                        c1.4-0.9,3.5-3,3.5-7.8C12,9.2,11.8,9,11.5,9z M7.8,16.6c-0.2,0.1-0.3,0.3-0.3,0.4v6h-3v-6c0-0.2-0.1-0.4-0.3-0.4
                        c-2-1.1-3.1-3.4-3.2-6.6h10C10.9,13.2,9.7,15.5,7.8,16.6z M6,8c2.2,0,4-1.8,4-4S8.2,0,6,0S2,1.8,2,4S3.8,8,6,8z M6,1
                        c1.7,0,3,1.3,3,3S7.7,7,6,7S3,5.7,3,4S4.3,1,6,1z"/>
                    </svg>
                </li>
        
                <li <?php echo navActive('#x'); ?>>
                    <a href="javascript:void(0)" class="navbar-rside navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas=".main-app" title="Other Links"></a>

                    <svg version="1.1" id="svg-bar" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 13" style="enable-background:new 0 0 16 13;" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,1h-15C0.2,1,0,0.8,0,0.5S0.2,0,0.5,0h15C15.7,0,16,0.2,16,0.5S15.7,1,15.5,1z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,5h-15C0.2,5,0,4.8,0,4.5S0.2,4,0.5,4h15C15.7,4,16,4.2,16,4.5S15.7,5,15.5,5z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,9h-15C0.2,9,0,8.8,0,8.5S0.2,8,0.5,8h15C15.7,8,16,8.2,16,8.5S15.7,9,15.5,9z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,13h-15C0.2,13,0,12.8,0,12.5S0.2,12,0.5,12h15c0.3,0,0.5,0.2,0.5,0.5S15.7,13,15.5,13z"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                </li>

            </ul>

            @if(Sentinel::check())
                <p class="navbar-text navbar-right">Hello, <a href="{{ url('/customer/profile') }}" class="navbar-link">{{ Sentinel::getUser()->first_name }}</a></p>
            @endif
        </div>
    </div>
</nav>