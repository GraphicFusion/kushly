<nav class="navbar navbar-default  navbar-driver navbar-xs visible-xs">
    <div class="container-fluid">
        <div class="navbar-header">
            <?php 
                echo embed_svg(
                    $class = 'navbar-brand', 
                    $svgurl = 'img/logo.svg', 
                    $height = '42px', 
                    $width = '113px', 
                    $link = makeURL('/')
                ); 
            ?>
            @if(Sentinel::Check())
                <form action="{{ url('/logout') }}" method="post" id="formLogout">
                    {{ csrf_field() }}
                    <a class="btn btn-primary navbar-btn" href="#" onclick="document.getElementById('formLogout').submit()">Sign Out</a>
                </form>
            @else
                <a class="btn btn-primary navbar-btn" href="{{ url('/signup') }}">Sign Up</a>
            @endif
        </div>

        <div class="navbar-mobile">

            <ul class="nav navbar-nav navbar-right main-nav">

                <li <?php echo navActive('driver'); ?>>
                    <a href="{{ url('/driver') }}" title="Deliveries"></a>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24.6 21.5" style="enable-background:new 0 0 24.6 21.5;" xml:space="preserve">
                    <g>
                        <path class="st0" d="M24.2,15.5l-2.5-4c-0.1-0.1-0.3-0.2-0.4-0.2h-4V8.7c0-0.3-0.2-0.5-0.5-0.5h-5.5c-0.3,0-0.5,0.2-0.5,0.5
                            c0,0.3,0.2,0.5,0.5,0.5h5v9l-4.8,0c-0.3-0.6-1-1-1.7-1c-0.7,0-1.4,0.4-1.7,1H6.3v-7c0-0.3-0.2-0.5-0.5-0.5c-0.3,0-0.5,0.2-0.5,0.5
                            v7.5c0,0.3,0.2,0.5,0.5,0.5h2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2l4.7,0c0.1,0,0.2,0.1,0.2,0.1c0.1,0,0.2,0,0.3-0.1h0.8c0,0,0,0,0,0
                            c0,1.1,0.9,2,2,2s2-0.9,2-2c0,0,0,0,0,0h2c0.3,0,0.5-0.2,0.5-0.5v-3C24.3,15.6,24.2,15.6,24.2,15.5z M21,12.2l1.9,3h-5.6v-3H21z
                             M9.8,20.2c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S10.3,20.2,9.8,20.2z M19.8,20.2c-0.6,0-1-0.4-1-1c0-0.1,0-0.1,0-0.2
                            c0.1-0.1,0.1-0.2,0.1-0.3c0,0,0,0,0,0c0.2-0.3,0.5-0.5,0.8-0.5c0.6,0,1,0.4,1,1S20.3,20.2,19.8,20.2z M21.5,18.2
                            c-0.4-0.6-1-1-1.7-1c-0.7,0-1.4,0.4-1.7,1h-0.8v-2h6v2H21.5z"/>
                        <path class="st0" d="M5.3,10.2c2.7,0,5-2.3,5-5c0-2.7-2.3-5-5-5s-5,2.3-5,5C0.3,8,2.6,10.2,5.3,10.2z M5.3,1.2c2.2,0,4,1.8,4,4
                            c0,2.2-1.8,4-4,4s-4-1.8-4-4C1.3,3.1,3.1,1.2,5.3,1.2z"/>
                        <path id="XMLID_4_" class="st0" d="M4.8,6.2h2c0.3,0,0.5-0.2,0.5-0.5c0-0.3-0.2-0.5-0.5-0.5H5.3V2.7c0-0.3-0.2-0.5-0.5-0.5
                            c-0.3,0-0.5,0.2-0.5,0.5v3C4.3,6,4.5,6.2,4.8,6.2z"/>
                        <path id="XMLID_3_" class="st0" d="M3.8,13.2l-3,0c0,0,0,0,0,0c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5l3,0c0,0,0,0,0,0
                            c0.3,0,0.5-0.2,0.5-0.5C4.3,13.5,4.1,13.2,3.8,13.2z"/>
                        <path id="XMLID_2_" class="st0" d="M3.8,15.2l-1.5,0c0,0,0,0,0,0c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5l1.5,0c0,0,0,0,0,0
                            c0.3,0,0.5-0.2,0.5-0.5C4.3,15.5,4.1,15.2,3.8,15.2z"/>
                        <path id="XMLID_1_" class="st0" d="M3.8,17.2l-0.5,0c0,0,0,0,0,0c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5l0.5,0c0,0,0,0,0,0
                            c0.3,0,0.5-0.2,0.5-0.5C4.3,17.5,4.1,17.2,3.8,17.2z"/>
                    </g>
                    </svg>
                </li>

                <!--li <?php echo navActive('driver/map-view'); ?>>
                    <a href="{{ url('/driver/map-view') }}" title="Map View"></a>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.5 24.5" style="enable-background:new 0 0 21.5 24.5;" xml:space="preserve">
                    <g>
                        <path class="st0" d="M10.8,8.2c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4S8.4,4.5,8.4,5.8C8.4,7.1,9.5,8.2,10.8,8.2z M10.8,4.4
                            c0.8,0,1.4,0.6,1.4,1.4s-0.6,1.4-1.4,1.4c-0.8,0-1.4-0.6-1.4-1.4S10,4.4,10.8,4.4z"/>
                        <path class="st0" d="M19.3,4.7c0-0.3-0.2-0.5-0.5-0.5h-2.6c-0.7-2.3-2.8-4-5.4-4c-2.6,0-4.7,1.7-5.4,4H2.7c-0.3,0-0.5,0.2-0.5,0.5
                            l-2,19.2c0,0.1,0,0.3,0.1,0.4c0.1,0.1,0.2,0.2,0.4,0.2h20.2c0.1,0,0.3-0.1,0.4-0.2c0.1-0.1,0.1-0.2,0.1-0.4L19.3,4.7z M6.6,17.3
                            L7,11.3h0.6c1.3,2,2.7,3.7,2.8,3.8c0.1,0.1,0.2,0.2,0.4,0.2c0.1,0,0.3-0.1,0.4-0.2c0.1-0.1,1.5-1.8,2.8-3.8h0.6l0.4,6.1H6.6z
                             M15.1,18.3l0.3,5H6.2l0.3-5H15.1z M2.5,11.3H6l-0.4,6.1H1.8L2.5,11.3z M15.6,11.3H19l0.6,6.1H16L15.6,11.3z M18.3,5.2l0.5,5h-3.3
                            l-0.1-1.4c0.6-1.1,1-2.2,1-3c0-0.2,0-0.4,0-0.6H18.3z M10.8,1.2c2.6,0,4.6,2.1,4.6,4.6c0,2-3,6.2-4.6,8.2c-1.6-2-4.6-6.2-4.6-8.2
                            C6.2,3.3,8.3,1.2,10.8,1.2z M3.1,5.2h2.1c0,0.2,0,0.4,0,0.6c0,0.8,0.4,1.9,1,3l-0.1,1.4H2.6L3.1,5.2z M1.7,18.3h3.8l-0.3,5h-4
                            L1.7,18.3z M16.4,23.4l-0.3-5h3.6l0.5,5H16.4z"/>
                    </g>
                    </svg>
                </li-->
                <li <?php echo navActive('driver/profile'); ?>>
                    <a href="{{ url('/driver/profile') }}" title="Driver Account"></a>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24.5 24.5" style="enable-background:new 0 0 24.5 24.5;" xml:space="preserve">
                    <path class="st0" d="M12.2,3.2c-5,0-9,4-9,9s4,9,9,9s9-4,9-9S17.2,3.2,12.2,3.2z M12.2,4.2c3.4,0,6.2,2.1,7.4,5
                        c-4.8-1.3-10-1.3-14.8,0C6,6.3,8.9,4.2,12.2,4.2z M10.2,20c-3.1-0.8-5.5-3.5-5.9-6.7c3.3,0,5.9,2.7,5.9,6
                        C10.2,19.5,10.2,19.7,10.2,20z M12.2,20.2c-0.4,0-0.7,0-1.1-0.1c0-0.3,0.1-0.6,0.1-0.9c0-3.9-3.1-7-7-7c0-0.6,0.1-1.3,0.2-1.9
                        c5-1.5,10.5-1.5,15.5,0c0.1,0.6,0.2,1.2,0.2,1.9c-3.9,0-7,3.1-7,7c0,0.3,0,0.6,0.1,0.9C12.9,20.2,12.6,20.2,12.2,20.2z M14.3,20
                        c0-0.2,0-0.5,0-0.7c0-3.3,2.7-6,5.9-6C19.7,16.5,17.4,19.1,14.3,20z M14.2,12.2c0-1.1-0.9-2-2-2s-2,0.9-2,2s0.9,2,2,2
                        S14.2,13.3,14.2,12.2z M12.2,13.2c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S12.8,13.2,12.2,13.2z M12.2,0.2c-6.6,0-12,5.4-12,12
                        s5.4,12,12,12s12-5.4,12-12S18.8,0.2,12.2,0.2z M12.2,23.2c-6.1,0-11-4.9-11-11s4.9-11,11-11s11,4.9,11,11S18.3,23.2,12.2,23.2z"/>
                    </svg>
                </li>
            
                <li <?php echo navActive('#x'); ?>>
                    <a href="javascript:void(0)" class="navbar-rside navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas=".main-app" title="Other Links"></a>

                    <svg version="1.1" id="svg-bar" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 13" style="enable-background:new 0 0 16 13;" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,1h-15C0.2,1,0,0.8,0,0.5S0.2,0,0.5,0h15C15.7,0,16,0.2,16,0.5S15.7,1,15.5,1z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,5h-15C0.2,5,0,4.8,0,4.5S0.2,4,0.5,4h15C15.7,4,16,4.2,16,4.5S15.7,5,15.5,5z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,9h-15C0.2,9,0,8.8,0,8.5S0.2,8,0.5,8h15C15.7,8,16,8.2,16,8.5S15.7,9,15.5,9z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,13h-15C0.2,13,0,12.8,0,12.5S0.2,12,0.5,12h15c0.3,0,0.5,0.2,0.5,0.5S15.7,13,15.5,13z"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                </li>

            </ul>

        </div>
    </div>
</nav>

<nav class="navbar navbar-default navbar-fixed-top navbar-all hidden-xs">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php 
                echo embed_svg(
                    $class = 'navbar-brand', 
                    $svgurl = 'img/logo.svg', 
                    $height = '42px', 
                    $width = '113px', 
                    $link = makeURL('/')
                ); 
            ?>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">

			<ul class="nav navbar-nav navbar-right main-nav">
                <li <?php echo navActive('driver'); ?>>
                    <a href="{{ url('/driver') }}" title="Deliveries"></a>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24.6 21.5" style="enable-background:new 0 0 24.6 21.5;" xml:space="preserve">
                    <g>
                        <path class="st0" d="M24.2,15.5l-2.5-4c-0.1-0.1-0.3-0.2-0.4-0.2h-4V8.7c0-0.3-0.2-0.5-0.5-0.5h-5.5c-0.3,0-0.5,0.2-0.5,0.5
                            c0,0.3,0.2,0.5,0.5,0.5h5v9l-4.8,0c-0.3-0.6-1-1-1.7-1c-0.7,0-1.4,0.4-1.7,1H6.3v-7c0-0.3-0.2-0.5-0.5-0.5c-0.3,0-0.5,0.2-0.5,0.5
                            v7.5c0,0.3,0.2,0.5,0.5,0.5h2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2l4.7,0c0.1,0,0.2,0.1,0.2,0.1c0.1,0,0.2,0,0.3-0.1h0.8c0,0,0,0,0,0
                            c0,1.1,0.9,2,2,2s2-0.9,2-2c0,0,0,0,0,0h2c0.3,0,0.5-0.2,0.5-0.5v-3C24.3,15.6,24.2,15.6,24.2,15.5z M21,12.2l1.9,3h-5.6v-3H21z
                             M9.8,20.2c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S10.3,20.2,9.8,20.2z M19.8,20.2c-0.6,0-1-0.4-1-1c0-0.1,0-0.1,0-0.2
                            c0.1-0.1,0.1-0.2,0.1-0.3c0,0,0,0,0,0c0.2-0.3,0.5-0.5,0.8-0.5c0.6,0,1,0.4,1,1S20.3,20.2,19.8,20.2z M21.5,18.2
                            c-0.4-0.6-1-1-1.7-1c-0.7,0-1.4,0.4-1.7,1h-0.8v-2h6v2H21.5z"/>
                        <path class="st0" d="M5.3,10.2c2.7,0,5-2.3,5-5c0-2.7-2.3-5-5-5s-5,2.3-5,5C0.3,8,2.6,10.2,5.3,10.2z M5.3,1.2c2.2,0,4,1.8,4,4
                            c0,2.2-1.8,4-4,4s-4-1.8-4-4C1.3,3.1,3.1,1.2,5.3,1.2z"/>
                        <path id="XMLID_4_" class="st0" d="M4.8,6.2h2c0.3,0,0.5-0.2,0.5-0.5c0-0.3-0.2-0.5-0.5-0.5H5.3V2.7c0-0.3-0.2-0.5-0.5-0.5
                            c-0.3,0-0.5,0.2-0.5,0.5v3C4.3,6,4.5,6.2,4.8,6.2z"/>
                        <path id="XMLID_3_" class="st0" d="M3.8,13.2l-3,0c0,0,0,0,0,0c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5l3,0c0,0,0,0,0,0
                            c0.3,0,0.5-0.2,0.5-0.5C4.3,13.5,4.1,13.2,3.8,13.2z"/>
                        <path id="XMLID_2_" class="st0" d="M3.8,15.2l-1.5,0c0,0,0,0,0,0c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5l1.5,0c0,0,0,0,0,0
                            c0.3,0,0.5-0.2,0.5-0.5C4.3,15.5,4.1,15.2,3.8,15.2z"/>
                        <path id="XMLID_1_" class="st0" d="M3.8,17.2l-0.5,0c0,0,0,0,0,0c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5l0.5,0c0,0,0,0,0,0
                            c0.3,0,0.5-0.2,0.5-0.5C4.3,17.5,4.1,17.2,3.8,17.2z"/>
                    </g>
                    </svg>
                </li>

                <!--li <?php echo navActive('driver/map-view'); ?>>
                    <a href="{{ url('/driver/map-view') }}" title="Map View"></a>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.5 24.5" style="enable-background:new 0 0 21.5 24.5;" xml:space="preserve">
                    <g>
                        <path class="st0" d="M10.8,8.2c1.3,0,2.4-1.1,2.4-2.4c0-1.3-1.1-2.4-2.4-2.4S8.4,4.5,8.4,5.8C8.4,7.1,9.5,8.2,10.8,8.2z M10.8,4.4
                            c0.8,0,1.4,0.6,1.4,1.4s-0.6,1.4-1.4,1.4c-0.8,0-1.4-0.6-1.4-1.4S10,4.4,10.8,4.4z"/>
                        <path class="st0" d="M19.3,4.7c0-0.3-0.2-0.5-0.5-0.5h-2.6c-0.7-2.3-2.8-4-5.4-4c-2.6,0-4.7,1.7-5.4,4H2.7c-0.3,0-0.5,0.2-0.5,0.5
                            l-2,19.2c0,0.1,0,0.3,0.1,0.4c0.1,0.1,0.2,0.2,0.4,0.2h20.2c0.1,0,0.3-0.1,0.4-0.2c0.1-0.1,0.1-0.2,0.1-0.4L19.3,4.7z M6.6,17.3
                            L7,11.3h0.6c1.3,2,2.7,3.7,2.8,3.8c0.1,0.1,0.2,0.2,0.4,0.2c0.1,0,0.3-0.1,0.4-0.2c0.1-0.1,1.5-1.8,2.8-3.8h0.6l0.4,6.1H6.6z
                             M15.1,18.3l0.3,5H6.2l0.3-5H15.1z M2.5,11.3H6l-0.4,6.1H1.8L2.5,11.3z M15.6,11.3H19l0.6,6.1H16L15.6,11.3z M18.3,5.2l0.5,5h-3.3
                            l-0.1-1.4c0.6-1.1,1-2.2,1-3c0-0.2,0-0.4,0-0.6H18.3z M10.8,1.2c2.6,0,4.6,2.1,4.6,4.6c0,2-3,6.2-4.6,8.2c-1.6-2-4.6-6.2-4.6-8.2
                            C6.2,3.3,8.3,1.2,10.8,1.2z M3.1,5.2h2.1c0,0.2,0,0.4,0,0.6c0,0.8,0.4,1.9,1,3l-0.1,1.4H2.6L3.1,5.2z M1.7,18.3h3.8l-0.3,5h-4
                            L1.7,18.3z M16.4,23.4l-0.3-5h3.6l0.5,5H16.4z"/>
                    </g>
                    </svg>
                </li-->
                <li <?php echo navActive('driver/profile'); ?>>
                    <a href="{{ url('/driver/profile') }}" title="Driver Account"></a>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24.5 24.5" style="enable-background:new 0 0 24.5 24.5;" xml:space="preserve">
                    <path class="st0" d="M12.2,3.2c-5,0-9,4-9,9s4,9,9,9s9-4,9-9S17.2,3.2,12.2,3.2z M12.2,4.2c3.4,0,6.2,2.1,7.4,5
                        c-4.8-1.3-10-1.3-14.8,0C6,6.3,8.9,4.2,12.2,4.2z M10.2,20c-3.1-0.8-5.5-3.5-5.9-6.7c3.3,0,5.9,2.7,5.9,6
                        C10.2,19.5,10.2,19.7,10.2,20z M12.2,20.2c-0.4,0-0.7,0-1.1-0.1c0-0.3,0.1-0.6,0.1-0.9c0-3.9-3.1-7-7-7c0-0.6,0.1-1.3,0.2-1.9
                        c5-1.5,10.5-1.5,15.5,0c0.1,0.6,0.2,1.2,0.2,1.9c-3.9,0-7,3.1-7,7c0,0.3,0,0.6,0.1,0.9C12.9,20.2,12.6,20.2,12.2,20.2z M14.3,20
                        c0-0.2,0-0.5,0-0.7c0-3.3,2.7-6,5.9-6C19.7,16.5,17.4,19.1,14.3,20z M14.2,12.2c0-1.1-0.9-2-2-2s-2,0.9-2,2s0.9,2,2,2
                        S14.2,13.3,14.2,12.2z M12.2,13.2c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S12.8,13.2,12.2,13.2z M12.2,0.2c-6.6,0-12,5.4-12,12
                        s5.4,12,12,12s12-5.4,12-12S18.8,0.2,12.2,0.2z M12.2,23.2c-6.1,0-11-4.9-11-11s4.9-11,11-11s11,4.9,11,11S18.3,23.2,12.2,23.2z"/>
                    </svg>
                </li>
            
                <li <?php echo navActive('#x'); ?>>
                    <a href="javascript:void(0)" class="navbar-rside navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas=".main-app" title="Other Links"></a>

                    <svg version="1.1" id="svg-bar" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 13" style="enable-background:new 0 0 16 13;" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,1h-15C0.2,1,0,0.8,0,0.5S0.2,0,0.5,0h15C15.7,0,16,0.2,16,0.5S15.7,1,15.5,1z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,5h-15C0.2,5,0,4.8,0,4.5S0.2,4,0.5,4h15C15.7,4,16,4.2,16,4.5S15.7,5,15.5,5z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,9h-15C0.2,9,0,8.8,0,8.5S0.2,8,0.5,8h15C15.7,8,16,8.2,16,8.5S15.7,9,15.5,9z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path class="st0" d="M15.5,13h-15C0.2,13,0,12.8,0,12.5S0.2,12,0.5,12h15c0.3,0,0.5,0.2,0.5,0.5S15.7,13,15.5,13z"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                </li>

            </ul>
        </div>
    </div>
</nav>