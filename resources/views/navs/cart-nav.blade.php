<nav class="navbar navbar-default navbar-cart">
    <div class="container-fluid">
        <div class="navbar-header">
            <?php 
                echo embed_svg(
                    $class = 'navbar-brand', 
                    $svgurl = 'img/logo.svg', 
                    $height = '42px', 
                    $width = '113px', 
                    $link = makeURL('/')
                ); 
            ?>
        </div>
   </div>
</nav>