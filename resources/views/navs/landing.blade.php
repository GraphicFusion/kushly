<nav class="navbar navbar-default navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-header">
            <?php 
                echo embed_svg(
                    $class = 'navbar-brand', 
                    $svgurl = 'img/logo.svg', 
                    $height = '42px', 
                    $width = '113px', 
                    $link = makeURL('/')
                ); 
            ?>
        </div>
        
        <ul class="nav navbar-nav navbar-right main-nav landing-nav">
            <li><a href="{{ url('/signin') }}" class="btn btn-primary-outline">Sign-In</a></li>
            <li><a href="{{ url('/signup') }}" class="btn btn-primary-outline">Sign-Up</a></li>
            <li>
                <a href="javascript:void(0)" class="navbar-rside navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas=".main-app" title="Other Links"></a>

                <svg version="1.1" id="svg-bar" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 13" style="enable-background:new 0 0 16 13;" xml:space="preserve">
                    <g>
                        <g>
                            <g>
                                <path class="st0" d="M15.5,1h-15C0.2,1,0,0.8,0,0.5S0.2,0,0.5,0h15C15.7,0,16,0.2,16,0.5S15.7,1,15.5,1z"/>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M15.5,5h-15C0.2,5,0,4.8,0,4.5S0.2,4,0.5,4h15C15.7,4,16,4.2,16,4.5S15.7,5,15.5,5z"/>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M15.5,9h-15C0.2,9,0,8.8,0,8.5S0.2,8,0.5,8h15C15.7,8,16,8.2,16,8.5S15.7,9,15.5,9z"/>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path class="st0" d="M15.5,13h-15C0.2,13,0,12.8,0,12.5S0.2,12,0.5,12h15c0.3,0,0.5,0.2,0.5,0.5S15.7,13,15.5,13z"/>
                            </g>
                        </g>
                    </g>
                </svg>
            </li>
        </ul>
    </div>
</nav>