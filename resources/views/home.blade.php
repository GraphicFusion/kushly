{{-- Main Page --}}
@extends('layouts.master')

@section('content')

{{-- <div class="loginerror"></div> --}}
<div id="location_error"></div>

<svg class="defs">
  <symbol id="plusIcon" viewBox="0 0 23.6 23.6">
    <desc>Product Plus Icon</desc>
	<g>
		<g>
			<path class="st0" d="M11.8,23.3c-6.4,0-11.5-5.2-11.5-11.5S5.4,0.3,11.8,0.3s11.5,5.2,11.5,11.5S18.2,23.3,11.8,23.3z M11.8,1.3
				C6,1.3,1.3,6,1.3,11.8S6,22.3,11.8,22.3s10.5-4.7,10.5-10.5S17.6,1.3,11.8,1.3z"/>
		</g>
		<g>
			<path id="XMLID_2_" class="st0" d="M11.8,18.3c-0.3,0-0.5-0.2-0.5-0.5v-12c0-0.3,0.2-0.5,0.5-0.5s0.5,0.2,0.5,0.5v12
				C12.3,18.1,12.1,18.3,11.8,18.3z"/>
		</g>
		<g>
			<path id="XMLID_1_" class="st0" d="M17.8,12.3h-12c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5h12c0.3,0,0.5,0.2,0.5,0.5
				S18.1,12.3,17.8,12.3z"/>
		</g>
	</g>
  </symbol>
</svg>

	{{-- Sidebar --}}
	@include('sidebar')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            	
            	<div class="row filters">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
						<div class="filterResults">
							<a href="javascript:void(0)" onclick="openFilterNav()"></a>
							<?php 
				                echo embed_svg(
				                    $class = 'filter-icon pull-left', 
				                    $svgurl = 'img/filter-icon.svg', 
				                    $height = '24px', 
				                    $width = '24px', 
				                    $link = ''
				                ); 
				            ?>
							Filter Results
						</div>
					</div>			
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
{{-- 						<ul class="list-inline p-range">
							<li class="active"><a href="javascript:void(0)" data-range="high">High $</a></li>
							<li><a href="javascript:void(0)" data-range="low">Low $</a></li>
						</ul>
						<input type="hidden" id="range" value="high" /> --}}
					</div>
					<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
						<select class="js-select-ws" id="locations" name="locations" data-placeholder="Select Location">
							<option value="Phoenix">Phoenix</option>
						</select>
 					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						@include('searchform')
					</div>
            	</div>

            </div>
        </div>

        <div class="row products-grid">
			
			<div id="listProducts" class="is-flex"></div>
			
			<div class="text-center">
				<?php
					$countProducts = App\Product::count();
					$no_of_products_per_page = 8;
					$offset = 0;
					$limit = $no_of_products_per_page;
					$total_pages = ceil($countProducts / $no_of_products_per_page);
				?>
				<button 
					type="button" 
					id="loadMoreProduct" 
					class="btn btn-primary-outline" 
					data-nop="{{ $no_of_products_per_page }}" 
					data-limit="{{ $limit }}"
					data-offset="{{ $offset }}" 
					data-total-pages="{{ $total_pages }}">
						Load More
				</button>
			</div>
        </div>

    </div>

	{{-- Ajax Modal Wrapper to load products --}}
	<div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-body" id="loadCarousel">
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="viewCartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content" id="viewCartProduct">
	  	</div>
	  </div>
	</div>

	{{-- <div id="dvMap" style="display:block;"></div> --}}
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2-bootstrap.min.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('lib/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={!! \Config::get('app.gmap') !!}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>

<script type="text/javascript">

// var geocoder= new google.maps.Geocoder();
// window.onload = function() {
// 	var startPos;
// 	var geoOptions = {
// 		enableHighAccuracy: true,
// 		timeout: 10 * 1000
// 	};
// 	var geoSuccess = function(position) {
// 		startPos = position;
// 		document.getElementById('startLat').value = startPos.coords.latitude;
// 		document.getElementById('startLon').value = startPos.coords.longitude;

// 		codeLatLng(startPos.coords.latitude, startPos.coords.longitude);
// 	};
// 	var geoError = function(error) {
// 		console.log('Error occurred. Error code: ' + error.code);
// 		// error.code can be:
// 		//   0: unknown error
// 		//   1: permission denied
// 		//   2: position unavailable (error response from location provider)
// 		//   3: timed out
// 	};
//   	navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);

// 	var codeLatLng = function(lat, lng) {
// 	    var latlng = new google.maps.LatLng(lat, lng);
// 	    geocoder.geocode({'latLng': latlng}, function(results, status) {
// 	      	if(status == google.maps.GeocoderStatus.OK) {
// 	          	if(results[1]) {
// 	              	//formatted address
// 	              	var city = results[0].address_components[4].short_name;
// 	              	if(city != 'Las Vegas'){
// 	              		document.getElementById('location_error').innerHTML = '<div class="alert alert-danger">Currently our service is avaliable only in <b>Las Vegas, NV</b></div>';
// 	              	}
// 	              	// alert(city);
// 	          	} else {
// 	            	alert("No results found");
// 	          	}
// 	      	} else {
// 	          	alert("Geocoder failed due to: " + status);
// 	      	}
// 	    });
// 	}
// };

// FilterNav Sidebar
/* Set the width of the side navigation to 250px */
function openFilterNav() {
    document.getElementById("filterNav").style.marginLeft = "0";
}

/* Set the width of the side navigation to 0 */
function closeFilterNav() {
    document.getElementById("filterNav").style.marginLeft = "-250px";
}

$(document).ready(function() {
	// Customize select box via select2 js adding bootstrap stheme
	$.fn.select2.defaults.set( "theme", "bootstrap" );
	// Customize select box via select2 js
	$(".js-select-ws").select2({
	  	placeholder: function(){
	        $(this).data('placeholder');
	    },
  		allowClear: true
	});

	$(".js-select-wos").select2({
		minimumResultsForSearch: -1
	});

	// Add nicescroll to filter Nav bar	
	$(".filterOptions").niceScroll({
		cursorcolor:"#21DB00",
		cursorborder: "1px solid #21DB00",
    	cursorborderradius: "0px",
    	cursordragontouch: true,
    	touchbehavior: true,
		preventmultitouchscrolling: false
	});

    $.ajaxSetup({
	  	headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  	}
	});

    // Ajax call for load more product on home page
    $(document).on('click', '#loadMoreProduct',  function(e){
    	e.preventDefault(); 
		var dataX = {
			nop: $(this).attr('data-nop'),
			limit: $(this).attr('data-limit'),
			offset: $(this).attr('data-offset'),
			groupID: $('#groups').val(),
			options: $('#options').val(),
			viewAll: $('#viewAll').val(),
			q: $('#form-search').find('#q').val()
		}
		callProductFilter('{{ url('/ajax/load-more-product') }}', dataX, 'append');
    });

    // Filter on view all click
	$('.checkbox-x input.viewall').click(function(event) {
		if($(this).is(':checked')){ 
			$('#viewAll').val($(this).val());
			$('#options').val('');
			$('.list-unstyled li .checkbox-x input.opt').prop('checked', false);
		} else {
			$('#viewAll').val('');
		}
		var dataX = {
			groupID: $('#groups').val(),
			options: $('#options').val(),
			viewAll: $('#viewAll').val(),
			nop: $('#loadMoreProduct').attr('data-nop'),
			limit: $('#loadMoreProduct').attr('data-limit'),
			offset: $('#loadMoreProduct').attr('data-offset')
		}
		callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
	});

	// Filter on category options
	$('.checkbox-x input.opt').click(function(event) {
		
		var getcheckvari = "";
		$('.options input.viewall').prop('checked', false);

		$(".list-unstyled li").each(function(){
			var getcheck = $(this).children('.checkbox-x').children('.opt');
			if($(getcheck).is(":checked")){
				getcheckvari += $(this).children('.checkbox-x').children('.opt').val()+','; 
			} 
		});
		getcheckvari = getcheckvari.slice(0,-1);

		$('#options').prop('value', getcheckvari);

		$('#viewAll').val('');
		var dataX = {
			groupID: $('#groups').val(),
			options: getcheckvari,
			viewAll: $('#viewAll').val(),
			nop: $('#loadMoreProduct').attr('data-nop'),
			limit: $('#loadMoreProduct').attr('data-limit'),
			offset: $('#loadMoreProduct').attr('data-offset')
		}
		callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
    });

	// ON PAGE LOAD 
    var dataX = {
		groupID: $('#groups').val(),
		options: $('#options').val(),
		viewAll: $('#viewAll').val(),
		nop: $('#loadMoreProduct').attr('data-nop'),
		limit: $('#loadMoreProduct').attr('data-limit'),
		offset: $('#loadMoreProduct').attr('data-offset'),
		location: $("#locations").val()
	}
	callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace-load');

	// $('#form-search input#q').keyup(function(event) {
	// 	if( this.value.length < 3 ) return;
	//    	/* Ajax call to found the search result. */
	//    	var dataX = {
	// 		groupID: $('#groups').val(),
	// 		options: $('#options').val(),
	// 		viewAll: $('#viewAll').val(),
	// 		q: this.value
	// 	}
	// 	callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
	// });

	// Form submit search
	$('#form-search').submit(function(event) {
		event.preventDefault();
		
		$('#loadMoreProduct').addClass('disabled').text('No More Products Available');
		$('#loadMoreProduct').unbind("click");

	   	/* Ajax call to found the search result. */
	   	var dataX = {
			groupID: $('#groups').val(),
			options: $('#options').val(),
			viewAll: $('#viewAll').val(),
			// limit: $('#loadMoreProduct').data('nop'),
			// offset: $('#loadMoreProduct').data('offset'),
			q: $(this).find('#q').val()
		}
		callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
	});

});

/**
 * Process variant selection
 **/
function process(x, id) {
	var fields = x.split("-");
	var formID = $('#'+id).parent('li').parent('ul').find('form').attr('id');
	alert(formID);
	$('#'+formID+' input[name="variant_id"]').val(fields[0]);
	$('#'+formID+' input[name="unit_price"]').val(fields[1]);
	$('#'+formID+' input[name="quantity_per_unit"]').val(fields[2]);
}

// Ajax call for modal popup with carousel slider
$(document).on('click', '.loadProductModalSlider', function(e){
    e.preventDefault(); 
    var id = $(this).data('pid');
    $.ajax({
        url: '{{ url('/ajax/load-product-modal') }}',
        type: "POST",
        data: 'id='+id,
        success: function(data){
            // console.log(data);
            $('#loadCarousel').html(data);
            $('#productModal').modal('show');
        },
        error: function(xhr, status, error){
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
});

// Ajax load view cart.
$(document).on('click', '.viewCart', function(e){
    e.preventDefault(); 
    $.ajax({
		url: '{{ url('/ajax/load-view-cart') }}',
		type: 'GET',
	}).done(function(data) {
		$('#viewCartProduct').html(data);
		$('#productModal').modal('hide');
        $('#viewCartModal').modal('show');
        $('[data-toggle="tooltip"]').tooltip();
	});
});

/**
 * Remove from cart
 **/
$(document).on('click', '.removeFromCart', function(e){
	e.preventDefault();
	var $this = $(this);
	$.ajax({
		url: '{{ url('/ajax/remove-product-from-cart') }}',
		type: 'GET',
		data: {key: $this.data('key')},
	}).done(function(data) {
		$('.cart-counter').text(data.count);
		$('#viewCartModal').modal('hide');
		window.location.reload();
    });
});

/**
 * Filter on change dispensary
 **/
$(document).on('change', 'select#dispensary', function(e){
	$('#groups').val($(this).val());
	var dataX = {
		groupID: $('#groups').val(),
		options: $('#options').val(),
		viewAll: $('#viewAll').val()
	}
	callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
});

$("#locations").select2({selectOnClose: true}).on("select2:selecting", function(evt) {
 	var selected_location = evt.params.args.data.id;
 	var dataX = {
		groupID: $('#groups').val(),
		options: $('#options').val(),
		viewAll: $('#viewAll').val(),
		nop: $('#loadMoreProduct').attr('data-nop'),
		limit: $('#loadMoreProduct').attr('data-limit'),
		offset: $('#loadMoreProduct').attr('data-offset'),
		location: selected_location
	}
	callProductFilter('{{ url('/ajax/product/filter') }}', dataX, 'replace');
});

$(window).on('load', function() {
	var loc = '{{ Session::get('search_location') }}';
	if(loc) {
		$('#locations').val(loc).trigger('change');
	}
});



/**
 * Product filter ajax
 **/
var getcount = "";
var totalclick = "";
var alreadyclick = 1;
var secondcount = 0;

function callProductFilter(url, dataX, method) {

	$.ajax({
		url: url,
		type: 'POST',
		data: dataX,
	}).done(function(data) {
		var dataResponse = data.split('|');

		if(getcount == ""){
			getcount = dataResponse[1];
			totalclick = Math.ceil(getcount/8);
		}
	
		var limit = $('#loadMoreProduct').attr('data-nop');
		var tp = Math.ceil(dataResponse[1] / limit);
		$("#loadMoreProduct").attr('data-total-pages',tp); 

		var offset = $('#loadMoreProduct').attr('data-offset');

		// Update limit as per offset
		$("#loadMoreProduct").attr('data-limit', (parseInt(offset)));

		// Increase offset as per click
		$("#loadMoreProduct").attr('data-offset', (parseInt(offset) + parseInt(limit)));

		
		if(method == 'append') {

			$('#listProducts').append(dataResponse[0]);
			// Disable load more when reached to page limit
			alreadyclick++;
			if(totalclick == alreadyclick){
				$('#loadMoreProduct').addClass('disabled').text('No More Products Available');
				$('#loadMoreProduct').attr('disabled','disabled');
			}else{
				$('#loadMoreProduct').removeAttr('disabled');
			}


		} else if(method == 'replace') {

			if(dataResponse[1] <= 8){
				$('#loadMoreProduct').addClass('disabled').text('No More Products Available');
				$('#loadMoreProduct').attr('disabled','disabled');
			}else{
				$('#loadMoreProduct').removeClass('disabled').text('LOAD MORE').bind('click');
				$('#loadMoreProduct').removeAttr('disabled');
			}
			$('#loadMoreProduct').attr('data-nop', 8);
		 	$('#loadMoreProduct').attr('data-offset', 8);
		 	getcount = dataResponse[1];
			totalclick = Math.ceil(getcount/8);
			$('#listProducts').html(dataResponse[0]);
			alreadyclick = 1;

		} else if(method == 'replace-load') {

			if(dataResponse[1] <= 8){
				$('#loadMoreProduct').addClass('disabled').text('No More Products Available');
				$('#loadMoreProduct').unbind('click');
			}else{
				$('#loadMoreProduct').removeAttr('disabled');
			}

			getcount = dataResponse[1];
			totalclick = Math.ceil(getcount/8);
			$('#listProducts').html(dataResponse[0]);
		}

		$(".js-select-wos").select2({
			minimumResultsForSearch: -1
		});
	});
}

/**
 * Add to cart
 */
$(document).on('click', '#add2cart', function(){
	// var formID = '#'+$(this).parent('.icon').parent('form').attr('id');
	// var variant = $(formID).parent('li').parent('ul').find('select').val();
	// var iconWrapper = $(this).parent('.icon');

	var formID = '#'+$(this).parent().parent().parent().parent().find('form').attr('id');
	var variant = $(this).parents('.caption').find('.box_list_active').attr('data-varient');
	var iconWrapper = $(this).parents('.changeIcon');
	//alert(iconWrapper); return false;

	if(!variant) {
		$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('<div class="alert alert-danger">Please Select Variant.</div>');
	} else {
		//console.log(formID); return false;
		$.ajax({
			url: '{{ url('addtocart') }}',
			type: 'POST',
			data: {
				product_id: $(formID+' #product_id').val(),
				variant_id: $(formID+' #variant_id').val(),
				unit_price: $(formID+' #unit_price').val(),
				quantity_per_unit: $(formID+' #quantity_per_unit').val(),
				quantity: $(formID+' #quantity').val(),
				user_id: $(formID+' #user_id').val(),
				group_id: $(formID+' #group_id').val(),
			},
		})
		.done(function(data) {
			if(data.message == 'success') {
				$('.cart-counter').text(data.count);
				//$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('');
				$(iconWrapper).html('<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a title="View Cart" class="viewCart" id="viewCart" href="javascript:void(0)"></a><button class="btn btn-primary add_button"><a title="View Cart" class="viewCart" id="viewCart" href="javascript:void(0)"><i class="glyphicon glyphicon-eye-open"></i></button></a></div>');
			}
			if(data.error == 'danger') {
				$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('<div class="alert alert-danger">'+data.message+'</div>');
			}
		});
	}
});

/**
 * Add to cart on slider
 */
$(document).on('click', '#add2cartOnSlider', function(){
	var formID = '#'+$(this).parent('.icon').parent('form').attr('id');
	var variant = $(formID).parent('li').parent('ul').find('select').val();
	var iconWrapper = $(this).parent('.icon');

	if(!variant) {
		$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('<div class="alert alert-danger">Please Select Variant.</div>');
	} else {
		$.ajax({
			url: '{{ url('addtocart') }}',
			type: 'POST',
			data: {
				product_id: $(formID+' #product_id').val(),
				variant_id: $(formID+' #variant_id').val(),
				unit_price: $(formID+' #unit_price').val(),
				quantity_per_unit: $(formID+' #quantity_per_unit').val(),
				quantity: $(formID+' #quantity').val(),
				user_id: $(formID+' #user_id').val(),
				group_id: $(formID+' #group_id').val(),
			},
		})
		.done(function(data) {
			if(data.message == 'success') {
				$('.cart-counter').text(data.count);
				$(iconWrapper).html('<div class="icon"><a title="View Cart" class="viewCart btn btn-primary-outline" id="viewCart" href="#">View Cart</a></div>');
			}
			if(data.error == 'danger') {
				$(formID).parent('li').parent('ul').parent('.caption').find('.error').html('<div class="alert alert-danger">'+data.message+'</div>');
			}
		});
	}
});

/**
 * Add to cart session update
 */
function updateSession(val, product_id) {
	var fields = val.split("-");
	$.ajax({
		url: '{{ url('addtocart/update') }}',
		type: 'POST',
		data: {
			product_id: product_id,
			variant_id: fields[0],
			unit_price: fields[1],
			quantity_per_unit: fields[2]
		}
	})
	.done(function(data) {
		$('#viewCartProduct').html(data);
	});
}

$(document).ready(function()
{
	$(document).on('click','.box',function(){
	var varientId = $(this).attr('data-varient');
	var productId = $(this).attr('data-product');
	var current = $(this);
		$('.box_list_'+productId).removeClass('box_list_active');
		$(this).find('.box_list_'+varientId).addClass("box_list_active");
		var varientId = $(this).attr('date-price');
		$.ajax({
		url: '{{ url('product/updatePriceByVariant') }}',
		type: 'POST',
		data: {
			varientId: varientId
		}
	})
	.done(function(data) {
		console.log(data);
		processss(data.q_per_unit,varientId,data.price,current);
		$('#productPriceByVarient_'+data.productId).text('$ '+data.price);
		$('#addtocart_'+productId);

	});
	});

	// $(document).on('click','.updatePriceByVariant',function(){
	// 	alert('test');
	// });	

});

$(document).on('click','.box1',function(){
	var varientId = $(this).attr('data-varient');
	var varientAmount = $(this).attr('data-price');
	var varientUnit = $(this).attr('data-unit');
	var productId = $(this).attr('data-product');
	//alert(varientId+'---'+varientAmount+'---'+varientUnit+'---'+productId); return false;
	$.ajax({
		url: '{{ url('addtocart/update') }}',
		type: 'POST',
		data: {
			product_id: productId,
			variant_id: varientId,
			unit_price: varientAmount,
			quantity_per_unit:varientUnit
		}
	})
	.done(function(data) {
		$('#viewCartProduct').html(data);
	});
});



function processss(perUnit,varientId,unitprice,current)
{
	//alert(perUnit+'------'+varientId+'-----'+unitprice);
	var formID = current.parents('.data_row').next().find('form').attr('id');
	//alert(formID);
	$('#'+formID+' input[name="variant_id"]').val(varientId);
	$('#'+formID+' input[name="unit_price"]').val(unitprice);
	$('#'+formID+' input[name="quantity_per_unit"]').val(perUnit);
}


</script>
@endsection