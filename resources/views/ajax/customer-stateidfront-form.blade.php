{{-- Customer state id front update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCustomerStateIdFront','files'=>'true','enctype'=>'multipart/form-data']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'stateidfront') !!}
    {!!Form::hidden('stateIdFront','',['id'=>'heddinStateId'])!!}
	<div class="form-group">
	    {!! Form::label('stateIdFront', 'State Id Front') !!}
	    {!! Form::file('files', ['class' => 'form-control stateIdFrontFile','required' => 'true','id'=>'stateIdFront']) !!}
	</div>

	{!! Form::button('&check;', ['id'=>'updateStateIdFront', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelStateIdFront', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}