{{-- Customer credit card update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateBankInfoFrom']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'bankinfo') !!}
    <div class="form-group">
	    {!! Form::label('bank', 'Bank Name') !!}
	    {!! Form::text('bank', $data['bank'], ['class' => 'form-control']) !!}
	</div>
	<div class="form-group">
	    {!! Form::label('routingno', 'Routing No') !!}
	    {!! Form::text('routingno', $data['routingno'], ['class' => 'form-control ll-skin-melon', 'id'=>'datepicker','required' => 'true']) !!}
	</div>
	<div class="form-group">
	    {!! Form::label('accountno', 'Account No') !!}
	    {!! Form::text('accountno', $data['accountno'], ['class' => 'form-control']) !!}
	</div>

	{!! Form::button('&check;', ['id'=>'updateBankInfo', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelBankInfo', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}