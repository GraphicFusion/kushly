{{-- Customer name update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCustomerName']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'name') !!}
    <div class="form-group">
	    {!! Form::label('first_name', 'First Name') !!}
	    {!! Form::text('first_name', $data['first_name'], ['class' => 'form-control validate']) !!}
	</div>
	<div class="form-group">
	    {!! Form::label('last_name', 'Last Name') !!}
	    {!! Form::text('last_name', $data['last_name'], ['class' => 'form-control validate']) !!}
	</div>

	{!! Form::button('&check;', ['id'=>'updateName', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelName', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}