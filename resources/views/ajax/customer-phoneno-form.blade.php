{{-- Customer phone no update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCustomerPhone']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'phone') !!}
	<div class="form-group">
	    {!! Form::label('phone_no', 'Phone No') !!}
	    {!! Form::text('phone_no', $data['phone_no'], ['class' => 'form-control numbersOnly', 'required' => 'true']) !!}
	</div>

	{!! Form::button('&check;', ['id'=>'updatePhone', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelPhone', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}