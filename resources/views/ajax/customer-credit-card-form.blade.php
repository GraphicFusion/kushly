{{-- Customer credit card update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCCInfo']) !!}
  {!! Form::hidden('id', $data['id']) !!}
  {!! Form::hidden('action', 'ccinfo') !!}

  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('cardnumber', 'Card Number') !!}
        {!! Form::text('cardnumber', $data['cardnumber'], ['class' => 'form-control']) !!}
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('expdate', 'Exp.Date') !!}
        {!! Form::text('expdate', $data['expdate'], ['class' => 'form-control ll-skin-melon', 'id'=>'datepicker','required' => 'true']) !!}
      </div>    
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('cardcode', 'Card Code') !!}
        {!! Form::text('cardcode', $data['cardcode'], ['class' => 'form-control']) !!}
      </div>
    </div>
  </div>
	
	{!! Form::button('&check;', ['id'=>'updateInfo', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelInfo', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}

<script>
$(document).ready(function($) {
	$('#expdate').datepicker({
      	changeMonth: true,
      	changeYear: true,
      	dateFormat: 'mm/yy',
      	showButtonPanel: true,
      	yearRange: "c:+20",
      	onClose: function(dateText, inst) {  
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
            $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
		},
      	beforeShow: function(input, inst) {
			$('#ui-datepicker-div').wrap("<div class='datepicker ll-skin-melon hasDatepicker'></div>");
			if ((datestr = $(this).val()).length > 0) {
                year = datestr.substring(datestr.length-4, datestr.length);
                month = datestr.substring(0, 2);
                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                $(this).datepicker('setDate', new Date(year, month-1, 1));
            }
		},
		afterClose: function(input, inst) {
			$('#ui-datepicker-div').unwrap();
		}
    }).focus(function () {
    	$("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });    
	});
});
</script>