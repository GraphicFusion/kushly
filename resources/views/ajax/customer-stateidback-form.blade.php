{{-- Customer state id back update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCustomerStateIdBack','files'=>'true','enctype'=>'multipart/form-data']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'stateidback') !!}
    {!!Form::hidden('stateIdBack','',['id'=>'heddinStateId'])!!}
	<div class="form-group">
	    {!! Form::label('stateIdBack', 'State Id Back') !!}
	    {!! Form::file('files', ['class' => 'form-control stateIdBackFile','required' => 'true','id'=>'stateIdBack']) !!}
	</div>

	{!! Form::button('&check;', ['id'=>'updateStateIdBack', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelStateIdBack', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}

