{{-- Customer medicalcard back update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCustomerMedicalCardBack','files'=>'true','enctype'=>'multipart/form-data']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'medicalcardback') !!}
    {!!Form::hidden('medicalCardBack','',['id'=>'heddinStateId'])!!}
	<div class="form-group">
	    {!! Form::label('medicalCardBack', 'Medical Card Back') !!}
	    {!! Form::file('files', ['class' => 'form-control medicalCardBackFile','required' => 'true','id'=>'medicalCardBack']) !!}
	</div>

	{!! Form::button('&check;', ['id'=>'updateMedicalCardBack', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelMedicalCardBack', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}