{{-- Customer Medical Card front update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCustomerMedicalCardFront','files'=>'true','enctype'=>'multipart/form-data']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'medicalcardfront') !!}
    {!!Form::hidden('medicalCardFront','',['id'=>'heddinStateId'])!!}
	<div class="form-group">
	    {!! Form::label('medicalCardFront', 'Medical Card Front') !!}
	    {!! Form::file('files', ['class' => 'form-control medicalCardFrontFile','required' => 'true','id'=>'medicalCardFront']) !!}
	</div>

	{!! Form::button('&check;', ['id'=>'updateMedicalCardFront', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelMedicalCardFront', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}