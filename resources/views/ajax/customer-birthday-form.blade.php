{{-- Customer birthday update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCustomerBirthday']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'birthday') !!}
	<div class="form-group">
	    {!! Form::label('birthday', 'Date of Birth') !!}
	    {!! Form::text('birthday', $data['birthday'], ['class' => 'form-control ll-skin-melon', 'id'=>'datepicker','required' => 'true']) !!}
	</div>
	{!! Form::button('&check;', ['id'=>'updateDOB', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelDOB', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}

<script>
$(document).ready(function($) {
	$('#datepicker').datepicker({
      	changeMonth: true,
      	changeYear: true,
      	yearRange: "1940:c",
      	beforeShow: function(input, inst) {
			$('#ui-datepicker-div').wrap("<div class='datepicker ll-skin-melon hasDatepicker'></div>");
		},
		afterClose: function(input, inst) {
			$('#ui-datepicker-div').unwrap();
		}
    });
});
</script>