{{-- Customer email update form --}}
{!! Form::open(['url' => 'ajax/customer/profile/update/info', 'method' => 'post', 'class' => 'form-inline', 'id' => 'updateCustomerEmail']) !!}
    {!! Form::hidden('id', $data['id']) !!}
    {!! Form::hidden('action', 'email') !!}
	<div class="form-group">
	    {!! Form::label('email', 'Email Id') !!}
	    {!! Form::text('email', $data['email'], ['class' => 'form-control','required' => 'true']) !!}
	</div>

	{!! Form::button('&check;', ['id'=>'updateEmail', 'class'=>'btn btn-primary-outline']) !!}
	{!! Form::button('&times;', ['id'=>'cancelEmail', 'class'=>'btn btn-base-outline']) !!}
{!! Form::close() !!}