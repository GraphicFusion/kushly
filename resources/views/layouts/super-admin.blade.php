<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Kushly</title>
	
	{{-- Google Font --}}
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One|Roboto+Mono:400,500,700|Lato:300,400,700,900" rel="stylesheet"> 

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    @yield('customCss')
    <script src="{{ asset('/js/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>
<body>
    <div id="ajaxLoader"></div>
    <div id="overlay"></div>

    <div class="main-app">

        @include('sidebar-right')
        <div id="app">
            {{-- Navbar --}}
            @include('navs.super')
            
            <div class="content">
                @include('flash')
                @yield('content')
            </div>
        </div>
        {{-- Footer --}}
        <footer class="footer alt">
            <div class="bg-primary">
                <div class="info">
                    <?php 
                        echo embed_svg(
                            $class = 'footer-logo', 
                            $svgurl = 'img/logo.svg', 
                            $height = '42px', 
                            $width = '113px', 
                            $link = makeURL('/')
                        ); 
                    ?>
                </div>
            </div>
            <div class="bg-base">
                <div class="container">
                    <div class="row credits">
                        <div class="col-sm-8 col-xs-12">
                            <p class="text-left">&copy; 2017 Kushly. All Rights Reserved.  <a href="/page/privacy-policy">Privacy Policy</a>, <a href="/page/terms-and-conditions">Legal Terms</a>. Design by <a href="#x">Sonder</a></p>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p class="text-right"><a class="btop" href="javascript:void(0)">Back to top</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>
    @yield('customJs')
</body>
</html>
