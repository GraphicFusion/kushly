@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has($msg))
        <div class="alert alert-{{ $msg }} alert-dismissible text-center" role="alert">
            {!! Session::get($msg) !!}
        </div>
    @endif
@endforeach