<footer class="footer">
	<div class="col-xs-12">
		<p class="text-center">&copy; 2017 Kushly. All Rights Reserved.  <a href="/page/privacy-policy">Privacy Policy</a>, <a href="/page/terms-and-conditions">Legal Terms</a>. Design by <a href="#x">Sonder</a></p>
	</div>
</footer>