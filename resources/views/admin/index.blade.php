{{-- Super Admin --}}
@extends('layouts.super-admin')

@section('content')
<?php $groups =  App\Groups::select("name")->get();?>	
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super'); ?>>
			<a href="{{ url('/super') }}">Dashboard</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="container head">
		<div class="row">
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<h5>Order Status</h5>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
						<select class="form-control" id="sortChat" style="width: 30%;display: inline-block;margin-right: 10px;cursor: pointer;">
					<option value="7 days">Select Dispensary</option>
					<?php foreach ($groups as $value) { ?>
						<option value="<?php echo $value->name;?>"><?php echo $value->name;?></option>
				<?php } ?>
					
					
				</select>
				<input type="hidden" name="min" id="min" value="">
					<input type="hidden" name="max" id="max" value="">
						<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 2px 5px 5px; border: 1px solid #ccc;">
					    <i class="glyphicon glyphicon-calendar"></i>&nbsp;
					    <span></span>&nbsp;<b class="caret"></b>
					</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="loadLineChart">
					    	<canvas id="line-chart"></canvas>
					    </div>
					    <div id="lineChartLegend"></div> 
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<h5>Dispensary Activity</h5>
				<select class="form-control" id="sortpieChat" style="width: 72%;display: inline-block;margin: 10px 0 10px 0;cursor: pointer;">
					<option value="">Select Dispensary</option>
					<?php foreach ($groups as $value) { ?>
						<option value="<?php echo $value->name;?>"><?php echo $value->name;?></option>
				<?php } ?>
					
					
				</select>
				<canvas id="pie-chart"></canvas>
				<div id="pieChartLegend"></div>
			</div>
		</div>
	</div>
    <!--Datatables-->
    <div class="container head pd-bottom0">
		<div class="row">
		    <div class="col-sm-6">
			<h5>Order List</h5>
			</div>
			<div class="col-sm-6 text-right">
			<ul class="list-inline order-table-icons" style="display: inline-block;margin-right: 10px;">
								<li>
									<a class="refreshRecords" href="javascript:void(0)"></a>
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.2 17" style="enable-background:new 0 0 22.2 17;" xml:space="preserve">
										<g>
											<g>
												<path class="st0" d="M3.1,11.4C3.1,11.4,3.1,11.4,3.1,11.4c-0.2,0-0.3-0.1-0.4-0.2L0.1,7.2C-0.1,7,0,6.7,0.2,6.5
													c0.2-0.2,0.5-0.1,0.7,0.1L3.2,10l2.6-3.1c0.2-0.2,0.5-0.2,0.7-0.1C6.7,7,6.7,7.4,6.5,7.6l-3,3.6C3.4,11.3,3.3,11.4,3.1,11.4z"/>
											</g>
											<g>
												<path class="st0" d="M21.7,11.5c-0.2,0-0.3-0.1-0.4-0.2l-2.3-3.4L16.5,11c-0.2,0.2-0.5,0.2-0.7,0.1c-0.2-0.2-0.2-0.5-0.1-0.7
													l3-3.6c0.1-0.1,0.3-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2l2.6,3.9c0.2,0.2,0.1,0.5-0.1,0.7C21.9,11.5,21.8,11.5,21.7,11.5z"/>
											</g>
											<g>
												<path class="st0" d="M11.1,17c-2.2,0-4.3-0.9-6-2.5c-0.2-0.2-0.2-0.5,0-0.7c0.2-0.2,0.5-0.2,0.7,0c1.9,1.8,4.5,2.6,7,2
													c3.8-0.9,6.5-4.8,5.7-8.6c-0.1-0.3,0.1-0.5,0.4-0.6c0.3-0.1,0.5,0.1,0.6,0.4c0.8,4.3-2.1,8.7-6.5,9.7C12.4,16.9,11.8,17,11.1,17z"
													/>
											</g>
											<g>
												<path class="st0" d="M3.1,11.3c-0.2,0-0.4-0.1-0.5-0.4C2,8.7,2.4,6.4,3.6,4.4c1.2-2.1,3.3-3.6,5.6-4.1c3.1-0.7,6.2,0.3,8.3,2.6
													c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0c-1.8-2.1-4.6-3-7.3-2.3c-2,0.5-3.8,1.8-5,3.7c-1.1,1.8-1.4,3.9-0.8,5.8
													c0.1,0.3-0.1,0.5-0.4,0.6C3.2,11.3,3.2,11.3,3.1,11.3z"/>
											</g>
										</g>
									</svg>
									Refresh
								</li>
							</ul>
						
				<select class="form-control" id="sort" style="width: 30%;display: inline-block;margin-right: 10px;">
					<option>Select Dispensary</option>
					<?php foreach ($groups as $value) { ?>
						<option value="<?php echo $value->name;?>"><?php echo $value->name;?></option>
				<?php } ?>
					
					
				</select>
				
			 </div>
		   </div>
		</div>

		<div class="container">
		  <div class="row">
			 <div class="col-lg-12">
			
				<table id="dTableOrder" class="table table-hover">
					<thead>
						<tr>
						    <td>Order #</td>
							<td>Order Date</td>
							<td>Dispensary</td>
							<td>Customer</td>
							<td>Status</td>
							<td>Action</td>
						</tr>
					</thead>
					{{-- Ajax Content Load --}}
				</table>

			</div>
		</div>
	</div>
		   
    <!--Driver -->

    <div class="container head pd-bottom0">
		<div class="row">
		    <div class="col-sm-6">
			<h5>Driver List</h5>
			</div>
			<div class="col-sm-6 text-right">
			<ul class="list-inline order-table-icons" style="display: inline-block;margin-right: 10px;">
								<li>
									<a class="refreshRecordsD" href="javascript:void(0)"></a>
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22.2 17" style="enable-background:new 0 0 22.2 17;" xml:space="preserve">
										<g>
											<g>
												<path class="st0" d="M3.1,11.4C3.1,11.4,3.1,11.4,3.1,11.4c-0.2,0-0.3-0.1-0.4-0.2L0.1,7.2C-0.1,7,0,6.7,0.2,6.5
													c0.2-0.2,0.5-0.1,0.7,0.1L3.2,10l2.6-3.1c0.2-0.2,0.5-0.2,0.7-0.1C6.7,7,6.7,7.4,6.5,7.6l-3,3.6C3.4,11.3,3.3,11.4,3.1,11.4z"/>
											</g>
											<g>
												<path class="st0" d="M21.7,11.5c-0.2,0-0.3-0.1-0.4-0.2l-2.3-3.4L16.5,11c-0.2,0.2-0.5,0.2-0.7,0.1c-0.2-0.2-0.2-0.5-0.1-0.7
													l3-3.6c0.1-0.1,0.3-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2l2.6,3.9c0.2,0.2,0.1,0.5-0.1,0.7C21.9,11.5,21.8,11.5,21.7,11.5z"/>
											</g>
											<g>
												<path class="st0" d="M11.1,17c-2.2,0-4.3-0.9-6-2.5c-0.2-0.2-0.2-0.5,0-0.7c0.2-0.2,0.5-0.2,0.7,0c1.9,1.8,4.5,2.6,7,2
													c3.8-0.9,6.5-4.8,5.7-8.6c-0.1-0.3,0.1-0.5,0.4-0.6c0.3-0.1,0.5,0.1,0.6,0.4c0.8,4.3-2.1,8.7-6.5,9.7C12.4,16.9,11.8,17,11.1,17z"
													/>
											</g>
											<g>
												<path class="st0" d="M3.1,11.3c-0.2,0-0.4-0.1-0.5-0.4C2,8.7,2.4,6.4,3.6,4.4c1.2-2.1,3.3-3.6,5.6-4.1c3.1-0.7,6.2,0.3,8.3,2.6
													c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0c-1.8-2.1-4.6-3-7.3-2.3c-2,0.5-3.8,1.8-5,3.7c-1.1,1.8-1.4,3.9-0.8,5.8
													c0.1,0.3-0.1,0.5-0.4,0.6C3.2,11.3,3.2,11.3,3.1,11.3z"/>
											</g>
										</g>
									</svg>
									Refresh
								</li>
							</ul>
				   <?php $drivername =  App\OrderDriver::select('order_driver.*',
		    DB::raw('DATE_FORMAT(order_driver.created_at, "%m/%d/%Y") as order_date'),
		    DB::raw("CONCAT_WS(' ',users.first_name, users.last_name) AS driver_name")
    	)
    	->join('users', 'users.id','=','order_driver.driver_id')
    	->orderBy('order_driver.id','desc')
    	->groupBy('driver_name')
        ->get();


       
       
	    $role = Sentinel::findRoleBySlug('driver');
        $users = $role->users()->with('roles')->get();
        

        ?>
				   <select class="form-control" id="driver_sort" style="width: 30%;display: inline-block;margin-right: 10px;">
					<option>Select Driver</option>
					<?php foreach ($users as $usersdata) { ?>
							<option><?php echo  $usersdata->first_name." ".$usersdata->last_name ;?></option>
						<?php } ?>
				  </select>
			 </div>
		   </div>
		</div>

	<div class="container">
		  <div class="row">
			 <div class="col-lg-12">
			     <table id="dTableOrderD" class="table table-hover">
					<thead>
						<tr>
						    <td>Driver Name</td>
							<td>Order ID</td>
							<td>Date</td>
							<td>Status</td>
							<td>Action</td>
						</tr>
					</thead>
					{{-- Ajax Content Load --}}
				 </table>

			 </div>
		</div>
	</div>

			
	<!--Datatables-->
	</div>

	<!-- Modal Popup -->
	<?php 
	 $role = Sentinel::findRoleBySlug('driver');
        $users = $role->users()->with('roles')->get();
        

        ?>
	    <div class="modal fade" id="myModaldriver" role="dialog">
			    <div class="modal-dialog modal-sm">
			    
			      <!-- Modal content-->
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h5 class="modal-title">Allocate Driver</h5>
			        </div>
			        <div class="modal-body">
			          <p class="Rejecteduser"></p>
			          <p class="oderid"></p>
			          <select class="deriverlist2" style="display: none;">
						<?php foreach ($users as $usersdata) { ?>
							<option value="<?php echo $usersdata->id ;?>"><?php echo  $usersdata->first_name." ".$usersdata->last_name ;?></option>
						<?php } ?>
			          </select>
			          <select class="deriverlist" style="width: 80%;">
						<?php foreach ($users as $usersdata) { ?>
							<option value="<?php echo $usersdata->id ;?>"><?php echo  $usersdata->first_name." ".$usersdata->last_name ;?></option>
						<?php } ?>
			          </select>

			        </div>
			        <div class="modal-footer">
			          <button type="button" class="btn btn-primary-outline Updatedriver">Update</button>
			        </div>
			      </div>
			      
			    </div>
		 </div>


		  <div class="modal fade" id="allocatednewdriver" role="dialog">
			    <div class="modal-dialog modal-sm">
			    
			      <!-- Modal content-->
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h5 class="modal-title"></h5>
			        </div>
			        <div class="modal-body">
			          <p>New Driver Allocated Successfully.</p>
			         

			        </div>
			       
			      </div>
			      
			    </div>
		 </div>
		  
		
<!-- Modal Popup -->
@endsection

@section('customCss')
	<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	<link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('lib/select2/css/select2-bootstrap.min.css') }}">
	<style type="text/css">
		#lineChartLegend ul li{list-style-type: none;float: left;width: 25%;font-size: 11px;}
	    #pieChartLegend ul li{list-style-type: none;
	    float: left;
	    width: 50%;
	    font-size: 10px;}
		#lineChartLegend ul li span, #pieChartLegend ul li span{
		width: 25px;
	    height: 15px;
	    float: left;
	    margin-top: 3px;
	    margin-right: 4px;
		}
		.bdr-div{border: 1px solid #eeeeee;padding: 10px;}
		#dTableOrderD_filter, #dTableOrderD_length{display: none;}
		#dTableOrder tr{cursor: default!important;}
		#dTableOrder .view_order{cursor: pointer!important;}
		.pd-bottom0{padding-bottom: 0px!important;}
		
		
	</style>
@endsection

@section('customJs')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

	<script src="{{ asset('js/dataTables.js') }}"></script>
	<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
	<script src="{{ asset('lib/nicescroll/jquery.nicescroll.min.js') }}"></script>
	<script type="text/javascript">
	$(document).ready(function(){

	        
			/*Date Picker*/
			var start = moment().subtract(6, 'days');
		    var end = moment();

		    function cb(start, end) {
		        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
		        $('#min').attr('value', start.format('YYYY-MM-DD')).change();
		        $('#max').attr('value', end.format('YYYY-MM-DD')).change();
		       
		    }
		    var ic = 0;
		    $('#max').on('change', function() { 
		    	ic++;
		    	if(ic>1)
		    	{
		    		
		    	    loaddata("",$("#sortChat").val(), $('#min').val(),$('#max').val());
		        }
			});

		    $('#reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        ranges: {
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        },
		        "dateLimit": {
		                "month": 1
		            },
		    }, cb);
		    cb(start, end);
		    // Return with commas in between
			var numberWithCommas = function(x) {
				y = x.toFixed(0);
			console.log(y);
			    return y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			};
			 	// Line Chart
			$("#sortChat").on("change", function(){
				if($(this).val()=="7 days")
				{
			      loaddata($(this).val(),"","","");
				}

				else
				{
					loaddata("",$(this).val(), $('#min').val(),$('#max').val());
				}

			});

	        // Pie chart
	      loadsortpieChat("val");

	      function loadsortpieChat(val)
	      {
	      	$.ajax({

	            url: '{{ url('/super/piechartjson') }}',
	            type: "POST",
	            data : {data : val},
	            success: function(data){
	            	

	// PIE 
	if(data.data[0]['Delivered'] == "" && data.data[0]['Rejected'] == "" && data.data[0]['Failure'] == "" && data.data[0]['OrderPlaced']== "")
	{
	var dataPie = {
	        labels: ["Total Delivered","Total Rejected","Total Failure","Total Order Placed"],
	        datasets: [
	            {
	                data: [data.data[0]['Delivered'], data.data[0]['Rejected'],data.data[0]['Failure'],data.data[0]['OrderPlaced']],
	                backgroundColor: [
	                    "#3adc23",
	                    "#77b4db",
	                    "#e92020",
	                    "#f58220"
	                    
	                ]
	            }]
	    };
	    var ctxPie = document.getElementById("pie-chart").getContext("2d");
	    if(window.barr != undefined)
	     	window.barr.destroy();
	    
	    window.barr = new Chart(ctxPie,{
	        type: 'pie',
	        data: "",
	        options: {
	            legend: {
	                display: false // due to style limitation, add legend info as per design using legend template
	            },
	            title: {
	                        display: true,
	                        text: 'No data in chart.',
	                        textAlign : "center",
	                        textBaseline : "middle"

	   
	                    },
	            responsive: true,
	            scaleBeginAtZero: true,
	            // legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	        }
	    });
	    $("#pieChartLegend").html(window.barr.generateLegend());
	}
	else
	{

	        var dataPie = {
	        labels: ["Total Delivered","Total Rejected","Total Failure","Total Order Placed"],
	        datasets: [
	            {
	                data: [data.data[0]['Delivered'], data.data[0]['Rejected'],data.data[0]['Failure'],data.data[0]['OrderPlaced']],
	                backgroundColor: [
	                    "#3adc23",
	                    "#77b4db",
	                    "#e92020",
	                    "#f58220"
	                    
	                ]
	            }]
	    };
	    var ctxPie = document.getElementById("pie-chart").getContext("2d");
	    if(window.barr != undefined)
	     	window.barr.destroy();
	    
	    window.barr = new Chart(ctxPie,{
	        type: 'pie',
	        data: dataPie,
	        options: {
	            legend: {
	                display: false // due to style limitation, add legend info as per design using legend template
	            },
	            responsive: true,
	            scaleBeginAtZero: true,
	            // legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	        }
	    });
	    $("#pieChartLegend").html(window.barr.generateLegend());
	}
	}
	});

	}
	        $("#sortpieChat").on("change", function(){
	        //alert($(this).val());
	         loadsortpieChat($(this).val());
	});

	    // First Load Get Last Seven Days Recods
	function loaddata(value, selectvl, date1, date2){

		//alert(value, selectvl, date1, date2);
		console.log("",$("#sortChat").val(), $('#min').val(),$('#max').val());
	    $.ajax({

	            url: '{{ url('/super/chartjson') }}',
	            type: "POST",
	            data:{time:value, selectvl:selectvl, min:date1, max:date2},
	            success: function(data){
	            	
	            var OrderPlaced = data.OrderPlaced;
	            var Delivered = data.Delivered;
	            var Failure = data.Failure;
	            var Rejected = data.Rejected;

	             
		              	var timeFormat = 'MM/DD/YYYY HH:mm';
				
						function newDate(days) {
							return moment().add(days, 'd').toDate();
						}

						function newDateString(days) {
							return moment().add(days, 'd').format(timeFormat);
						}

						function newTimestamp(days) {
							return moment().add(days, 'd').unix();
						}
	            


	//alert(newDateString(5));


	var dataLine = {

	        labels: [],
	        datasets: [
	            {
	            	borderWidth: 1,
	                label: "Delivered",
	                fill: false,
	                lineTension: 0,
	                backgroundColor: "#3adc23",
	                borderColor: "#3adc23",
	                borderCapStyle: 'butt',
	                borderDash: [],
	                borderDashOffset: 0.0,
	                borderJoinStyle: 'miter',
	                pointBorderColor: "#3adc23",
	                pointBackgroundColor: "#3adc23",
	                pointBorderWidth: 3,
	                pointHoverRadius: 3,
	                pointHoverBackgroundColor: "#3adc23",
	                pointHoverBorderColor: "#3adc23",
	                pointHoverBorderWidth: 0,
	                pointRadius: 2,
	                pointHitRadius: 4,
	                data: Delivered,
	                spanGaps: false,
	            },
	            {
	            	borderWidth: 1,
	                label: "Failure",
	                fill: false,
	                lineTension: 0,
	                backgroundColor: "#e92020",
	                borderColor: "#e92020",
	                borderCapStyle: 'butt',
	                borderDash: [],
	                borderDashOffset: 0.0,
	                borderJoinStyle: 'miter',
	                pointBorderColor: "#e92020",
	                pointBackgroundColor: "#e92020",
	                pointBorderWidth: 3,
	                pointHoverRadius: 3,
	                pointHoverBackgroundColor: "#e92020",
	                pointHoverBorderColor: "#e92020",
	                pointHoverBorderWidth: 0,
	                pointRadius: 2,
	                pointHitRadius: 4,
	                data: Failure,
	                spanGaps: false,
	            },
	            {
	            	borderWidth: 1,
	                label: "Rejected",
	                fill: false,
	                lineTension: 0,
	                backgroundColor: "#7cb5ec",
	                borderColor: "#7cb5ec",
	                borderCapStyle: 'butt',
	                borderDash: [],
	                borderDashOffset: 0.0,
	                borderJoinStyle: 'miter',
	                pointBorderColor: "#7cb5ec",
	                pointBackgroundColor: "#7cb5ec",
	                pointBorderWidth: 3,
	                pointHoverRadius: 3,    
	                pointHoverBackgroundColor: "#7cb5ec",
	                pointHoverBorderColor: "#7cb5ec",
	                pointHoverBorderWidth: 0,
	                pointRadius: 2,
	                pointHitRadius: 4,
	                data: Rejected,
	                spanGaps: false,
	            },
	            {
	            	borderWidth: 1,
	                label: "Order Placed",
	                fill: false,
	                lineTension: 0,
	                backgroundColor: "#f58220",
	                borderColor: "#f58220",
	                borderCapStyle: 'butt',
	                borderDash: [],
	                borderDashOffset: 0.0,
	                borderJoinStyle: 'miter',
	                pointBorderColor: "#f58220",
	                pointBackgroundColor: "#f58220",
	                pointBorderWidth: 3,
	                pointHoverRadius: 3,    
	                pointHoverBackgroundColor: "#f58220",
	                pointHoverBorderColor: "#f58220",
	                pointHoverBorderWidth: 0,
	                pointRadius: 2,
	                pointHitRadius: 4,
	                data: OrderPlaced,
	                spanGaps: false,
	            }
	        ]
	    };

	             var ctxLine = document.getElementById("line-chart").getContext("2d");

	            if(OrderPlaced =="" && Delivered == "" && Failure =="" && Rejected =="")
	            {

	   
	   
	     if(window.bar != undefined)
	     	window.bar.destroy();
	     window.bar = new Chart(ctxLine, {
	        type: 'line',
	        data: "",
	        options: {

	            animation: {
	                duration: 10,
	            },
	             title: {
	                        display: true,
	                        text: 'No data in chart.',
	                        textAlign : "center",
	                        textBaseline : "middle"

	   
	                    },
	           scales: {
	                xAxes: [{
	                	 gridLines: {
	           display: false
	       },
							type: "time",
							display: true,
							time: {
								 unit: 'day',
								 //format: timeFormat,
								 round: 'day',
								 tooltipFormat: 'll',
								 min: $('#min').val(),
	                             max: $('#max').val()
							},
							scaleLabel: {
								display: true,
								labelString: 'Date'
							}
						}, ],
	                yAxes: [{
	                	display: true,
							scaleLabel: {
								display: true,
								labelString: 'value'
							}
							,
							ticks: {
	                            min: 0,
	                            max: 10,

	                            // forces step size to be 5 units
	                            stepSize: 1
	                        }
						}]
	            },
	            legend: {
	                display: false // due to style limitation, add legend info as per design using legend template
	            },
	            responsive: true,
	            scaleBeginAtZero: true,
	            
	        }
	    });
	     
	            }
	            else
	            {
	            



	     if(window.bar != undefined)
	     	window.bar.destroy();
	     window.bar = new Chart(ctxLine, {
	        type: 'line',
	        data: dataLine,
	        options: {
	            animation: {
	                duration: 10,
	            },
	           scales: {
	                xAxes: [{
	                	 gridLines: {
	           display: false
	       },
							type: "time",
							display: true,
							time: {
								 unit: 'day',
								 //format: timeFormat,
								 round: 'day',
								 tooltipFormat: 'll',
								 min: $('#min').val(),
	                             max: $('#max').val()
							},
							scaleLabel: {
								display: true,
								labelString: 'Date'
							}
						}, ],
	                yAxes: [{
	                	display: true,
							scaleLabel: {
								display: true,
								labelString: 'value'
							},
							ticks: {
	                            min: 0,
	                            max: 20,

	                            // forces step size to be 5 units
	                            stepSize: 1
	                        }
						}]
	            },
	            legend: {
	                display: false // due to style limitation, add legend info as per design using legend template
	            },
	            responsive: true,
	            scaleBeginAtZero: true,
	            // legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
	        }
	    });
	}

	    $("#lineChartLegend").html(window.bar.generateLegend());

	            },
	            error: function(xhr, status, error){
	                console.log(xhr);
	                console.log(status);
	                console.log(error);
	            },
	            beforeSend: function(xhr){
	                xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
	            }
	        });
	}

	loaddata("7 days");




	    //Datatables

	    var oTable = $('#dTableOrder').DataTable({
			"ajax": "{{ url('/super/json') }}",
			columns: [
			    { data: 'id' },
	            { data: 'order_date' }, 
	            { data: 'group_name' },
			    { data: 'customer_name' },
	            { data: 'order_status' },
	            { 
	            	data: 'null', 
	            	defaultContent:"<button type='button'  class='btn btn-primary-outline'>View More</button>" 
	            },
	            
	        ],
			"sDom": '<"top">rt<"bottom"p><"clear">',
			"stateSave": true,
			"lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
		    "iDisplayLength": 5,
		    "bSort" : false,
		    // "aaSorting": [[0, 'asc']],
		    "destroy": true,
			"language": {
			    "oPaginate": {
		          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
		          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
		        }
		    },
		    "createdRow": function( row, data, dataIndex ) {
		    	$(row).attr('id', data['id']);

		    	if(data['order_value']) {
					$(row).find('.order_value').text('$'+data['order_value']);
		    	}
		  	},



	  	});

			$('#sort').on("change", function() {
				
			    if($(this).val() == "Select Dispensary")
			    {
	            oTable.search("").draw()
			    }
			    else
			    {

			    oTable.search($(this).val()).draw();
			    }

			}); 


			$('#driver_sort').on("change", function() {
				
			    if($(this).val() == "Select Driver")
			    {
			    	
	            oTableD.search("").draw()
			    }
			    else
			    {

			    oTableD.search($(this).val()).draw();
			    }

			});     

			

	  	 $('#dTableOrder tbody').on( 'click', 'button', function (e) {
			   	

				e.preventDefault();
				var tblData = oTable.row(this).data();
				//var orderid = tblData['id'];
				var orderid = $(this).closest("tr").attr("id")
				var href = "{!! url('/super/orders/"+orderid+"') !!}";
				window.location.href = href;
			   });
		

	   var oTableD = $('#dTableOrderD').DataTable({
			"ajax": "{{ url('/super/allocated-drivers') }}",
			columns: [
	           
	            { 
	            	data: 'driver_name',
	            	
	            },
	            { data: 'order_id' },
	           
	            { data: 'order_date' },
	            {
	            	data: 'order_driver_status',
	            	orderable: false, 
	            	searchable: false, 
	            	defaultContent: "<div class='status'></div>"
	            },
	            { 
	            	data: 'action', 
	            	orderable: false, 
	            	searchable: false, 
	            	defaultContent:"<div class='btn-group' role='group'><a href='javascript:void(0)' class='btn btn-sm editdriver btn-warning btn-edit'><i class='glyphicon glyphicon-edit'></i></a></div>" 
	            }
	        ],
			"stateSave": true,
			"lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
		    "iDisplayLength": 5,
		    "aaSorting": [[0, 'desc']],
		    "destroy": true,
			"language": {
				"search": "_INPUT_",
		        "searchPlaceholder": "Search records ...",
		        "lengthMenu": "_MENU_",
			    "oPaginate": {
		          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
		          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
		        }
		    },
		    "createdRow": function( row, data, dataIndex ) {
		    	$(row).attr('id', 'row-'+data['order_id']);
		    	// if(data['order_driver_status'] != 'delivered') {
		    	// 	$(row).find('.btn-group').show();
		    		
		    	// }

		    	// if(data['deleted_at']) {
		    	// 	$(row).find('.status').text('Trashed');
		    	// 	$(row).find('.btn-group').find('.btn-danger').remove();
		    	// 	$(row).find('.btn-group').prepend("<button class='btn btn-sm btn-info po-restore'><i class='glyphicon glyphicon-eye-open'></i></button>");
		    	// } else {
		    	// 	$(row).find('.status').text('Published');
		    	// }

		    	
		  	}
	  	});

	    /*Select 2*/
	        $(".deriverlist").select2({
		  	  placeholder: "Select a Driver",
	  		  allowClear: true,

		    });	
	        /*Select 2*/
	        // $('#myModaldriver').on('show.bs.modal', function (e) {
	        // alert($(this).html());
	        // });
		    $('#dTableOrderD tbody').on( 'click', '.editdriver', function (e) {

			    var orderid = $(this).closest("tr").attr("id");
			   
			    var driver_text = $(this).closest("tr").find(".sorting_1").text()
			    orderid = orderid.split("-");
			    $(".oderid").text("Order Id : "+orderid[1]);
			    $(".Rejecteduser").text("Rejected by : "+driver_text);
			    $(".deriverlist").html($(".deriverlist2").html());
			    $(".deriverlist option").each(function(){
			      if($(this).text() == driver_text)
			      {
			      	//alert($(this).text());
			      	$(this).remove();
			      }

			    });

	            $(".select2-container--default .select2-selection--single").css("border", "1px solid #aaa");
			   	$("#myModaldriver").modal('show');

			    });

		        $(".Updatedriver").click(function(e){
	          
	              if($(".deriverlist").val()==null)
	              {
	              	e.preventDefault();
	              	$(".select2-container--default .select2-selection--single").css("border", "1px solid red");
	              }
	              else
	              {

                    var oderidd = $(".oderid").text();
                        oderidd = oderidd.split(":");
	              	$.ajax({

					            url: '{{ url('/super/allocatedriver') }}',
					            type: "POST",
					            data : {data : $(".deriverlist").val()},
					            success: function(data){
					            	if(data.data.length == 1)
					            	{
					            	if(data.data[0]["order_driver_status"] == "allocated")
					            	{
					            		alert('Driver already allocated');
					            	}
					            }
					            	else
					            	{

					            		$.ajax({

										            url: '{{ url('/super/addriver') }}',
										            type: "POST",
										            data : {driverid : $(".deriverlist").val(), oderidd : oderidd[1] },
										            success: function(data){

										            	$("#myModaldriver").modal("hide");
										            	$("#allocatednewdriver").modal("show");

$('#dTableOrderD').DataTable().ajax.reload(null, true);
										            	
										            }
	        			                       });
					            		
					            	}
					            }
	        			  });
	              	$(".select2-container--default .select2-selection--single").css("border", "1px solid #aaa");
	              	
	              }

		      });




	});
		$(document).on("click", '.refreshRecords', function (e) {
			$('#dTableOrder').DataTable().ajax.reload(null, true);
		});
		$(document).on("click", '.refreshRecordsD', function (e) {
			$('#dTableOrderD').DataTable().ajax.reload(null, true);
		});
	 </script>
@endsection