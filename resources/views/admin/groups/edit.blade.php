{{-- Super Admin Edit Group --}}
@extends('layouts.super-admin')

@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/dispensaries'); ?>>
			<a href="{{ url('/super/dispensaries') }}">Dispensaries</a>
		</li>
		<li <?php echo navActive('super/dispensaries/create'); ?>>
			<a href="{{ url('/super/dispensaries/create') }}">New Dispensary</a>
		</li>
		<li <?php echo navActive('super/dispensaries/edit/{id}'); ?>>
			<a href="{{ url('/super/dispensaries/edit/{id}') }}">Edit Dispensary</a>
		</li>
	</ul>
</nav>


<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5 class="m0">Edit Dispensary: {{ $group->name }} </h5>
				</div>
				<div class="col-lg-6"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				{!! Form::open(['method'=>'post', 'class'=>'form', 'id'=>'addDispensaries', 'files'=>true]) !!}

					<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
					    {!! Form::label('name', 'Group Name') !!}
					    {!! Form::text('name', $group->name, ['class' => 'form-control']) !!}
					    {!! $errors->first('name','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('type') ? 'has-error' :'' }}">
					    {!! Form::label('type', 'Group Type') !!}
					    {!! Form::text('type', $group->type, ['class' => 'form-control']) !!}
					    {!! $errors->first('type','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('logo') ? 'has-error' :'' }}">
					    {!! Form::label('logo', 'Group Logo') !!}
					    <div class="clearfix">
						    {!! Form::file('logo', ['class'=>'pull-left','id'=>'groupLogo']) !!}
						    {!! Form::hidden('prevLogo', $group->logo) !!}
						    @if(!empty($group->logo))
					    		<img class="pull-right" id="blah" src="#" alt="Group Logo Preview" />
					    	@endif
					    </div>
					</div>
					<div class="form-group {{ $errors->has('user') ? 'has-error' :'' }}">
					    {!! Form::label('user', 'Select Users') !!}
					    <a href="{{ url('/super/users/create') }}" class="help-block link pull-right">Add New User</a>
						<select name="user[]" id="user-select2" class="js-select-ws" multiple="multiple">
							<?php
								$getDA = getDispensaryAdmins();
								foreach ($getDA as $k => $v) {
									echo '<option value="'.$v->id.'">'.$v->first_name.' '.$v->last_name.'</option>';
								}
							?>
						</select>	
						{!! $errors->first('user','<span class="help-block">:message</span>') !!}								
					</div>
					<div class="form-group {{ $errors->has('location') ? 'has-error' :'' }}">
					    {!! Form::label('location', 'Select Locations') !!}
						<a href="{{ url('/super/locations/create') }}" class="help-block link pull-right">Add New Location</a>
						{{-- <select name="location[]" id="list-loc" class="js-select-ws" multiple="multiple"> --}}
						<select name="location" id="list-loc" class="js-select-ws">
							<?php
								$loc = getAllLocations();
								foreach ($loc as $k => $v) {
									echo '<option value="'.$v->id.'">'.$v->street_address.', '.$v->unit.', '.$v->city.', '.$v->state.', '.$v->zip_code.', '.$v->country.', '.$v->type.'</option>';
								}
							?>
						</select>
						{!! $errors->first('location','<span class="help-block">:message</span>') !!}
					</div>
				{!! Form::submit('Update', ['class'=>'btn btn-primary-outline']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@php 
	$userAttached = [];
	foreach ($group->user as $key => $user) {
		$userAttached[] = $user->id;
	}
	$listUser = json_encode($userAttached);

	$locAttached = [];
	foreach ($group->location as $key => $loc) {
		$locAttached[] = $loc->id;
	}
	$listLoc = json_encode($locAttached);
@endphp
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2-bootstrap.min.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
	$.fn.select2.defaults.set( "theme", "bootstrap" );

	var listUser = <?php echo $listUser; ?>;
	$("#user-select2").select2().val(listUser).trigger('change');

	var listLoc = <?php echo $listLoc; ?>;
	$("#list-loc").select2().val(listLoc).trigger('change');

	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	            $('#blah').attr({
	            	src: e.target.result,
	            	width: '128px',
	            	height: '64px'
	            });
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$("#groupLogo").change(function(){
	    readURL(this);
	});

	var isPrevLogo = '<?php echo $group->logo; ?>';
	if(isPrevLogo) {
		var previous_logo = '{{ asset('groups-logo/'.$group->logo) }}';
		if(previous_logo) {
			$('#blah').attr({
	        	src: previous_logo,
	        	width: '128px',
	        	height: '64px'
	        });
		}
	}
});
</script>
@endsection