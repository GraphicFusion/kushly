{{-- Super Admin Add New Groups/Dispensaries --}}
@extends('layouts.super-admin')

@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/dispensaries'); ?>>
			<a href="{{ url('/super/dispensaries') }}">Dispensaries</a>
		</li>
		<li <?php echo navActive('super/dispensaries/create'); ?>>
			<a href="{{ url('/super/dispensaries/create') }}">New Dispensary</a>
		</li>
	</ul>
</nav>

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5 class="m0">Create Dispensary</h5>
				</div>
				<div class="col-lg-6"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				{!! Form::open(['method'=>'post', 'url'=>'super/dispensaries/create', 'class'=>'form', 'id'=>'addDispensaries', 'files'=>true]) !!}
					<div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">
					    {!! Form::label('name', 'Group Name') !!}
					    {!! Form::text('name', '', ['class' => 'form-control']) !!}
					    {!! $errors->first('name','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('type') ? 'has-error' :'' }}">
					    {!! Form::label('type', 'Group Type') !!}
					    {!! Form::text('type', 'Dispensary', ['class' => 'form-control']) !!}
					    {!! $errors->first('type','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('logo') ? 'has-error' :'' }}">
					    {!! Form::label('logo', 'Group Logo') !!}
					    <div class="clearfix">
						    {!! Form::file('logo', ['class'=>'pull-left','id'=>'groupLogo']) !!}
					    	<span id="blah" class="pull-right"></span>
					    </div>
					</div>
					<div class="form-group {{ $errors->has('user') ? 'has-error' :'' }}">
					    {!! Form::label('user', 'Select Users') !!}
					    <a href="{{ url('/super/users/create') }}" class="help-block link pull-right">Add New User</a>
						<select name="user[]" class="js-select-ws" multiple="multiple">
							<?php
								$getDA = getDispensaryAdmins();
								foreach ($getDA as $k => $v) {
									echo '<option value="'.$v->id.'">'.$v->first_name.' '.$v->last_name.'</option>';
								}
							?>
						</select>
						{!! $errors->first('user','<span class="help-block">:message</span>') !!}								
					</div>
					<div class="form-group {{ $errors->has('location') ? 'has-error' :'' }}">
					    {!! Form::label('location', 'Select Locations') !!}
						<a href="{{ url('/super/locations/create') }}" class="help-block link pull-right">Add New Location</a>
						{{-- <select name="location[]" class="js-select-ws" multiple="multiple"> --}}
						<select name="location" class="js-select-ws">
							<?php
								$loc = getAllLocations();
								foreach ($loc as $k => $v) {
									echo '<option value="'.$v->id.'">'.$v->street_address.', '.$v->unit.', '.$v->city.', '.$v->state.', '.$v->zip_code.', '.$v->country.', '.$v->type.'</option>';
								}
							?>
						</select>
						{!! $errors->first('location','<span class="help-block">:message</span>') !!}
					</div>
				{!! Form::submit('Create', ['class'=>'btn btn-primary-outline']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('lib/select2/css/select2-bootstrap.min.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('lib/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
	// Customize select box via select2 js adding bootstrap stheme
	$.fn.select2.defaults.set( "theme", "bootstrap" );
	// Customize select box via select2 js
	$(".js-select-ws").select2();

	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
				$('#blah').html('<img src="'+e.target.result+'" width="128px" height="64px" />');
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$("#groupLogo").change(function(){
	    readURL(this);
	});

});
</script>
@endsection