{{-- Super Admin List Groups/Dispensary--}}
@extends('layouts.super-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/dispensaries'); ?>>
			<a href="{{ url('/super/dispensaries') }}">Dispensaries</a>
		</li>
		<li <?php echo navActive('super/dispensaries/create'); ?>>
			<a href="{{ url('/super/dispensaries/create') }}">New Dispensary</a>
		</li>
	</ul>
</nav>

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<h5 class="m0">List Dispensaries</h5>
				</div>
				<div class="col-sm-6">
					<a href="{{ url('/super/dispensaries/create') }}" class="btn btn-primary pull-right">Create New</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<table id="dTables" class="table" style="width: 100%;">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Type</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					{{-- Ajax Content Load --}}
				</table>
			</div>
		</div>
	</div>
</div>

@endsection


@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
$(document).ready(function($) {
	var oTable = $('#dTables').DataTable({
		"ajax": "{{ url('/super/dispensaries/json') }}",
		columns: [
            { data: 'id' },
            { data: 'name' },
            { data: 'type' },
            { data: 'status' },
            { 
            	data: 'action', 
            	orderable: false, 
            	searchable: false, 
            	defaultContent:"<div class='btn-group' role='group'><button class='btn btn-sm btn-danger po'><i class='glyphicon glyphicon-remove'></i></button><a href='javascript:void(0)' class='btn btn-sm btn-warning btn-edit'><i class='glyphicon glyphicon-edit'></i></a></div>" 
            }
        ],
		"stateSave": true,
		"lengthMenu": [ 10, 25, 50, 75, 100 ],
	    "iDisplayLength": 10,
	    "aaSorting": [[0, 'desc']],
	    "destroy": true,
		"language": {
			"search": "_INPUT_",
	        "searchPlaceholder": "Search records ...",
	        "lengthMenu": "_MENU_",
		    "oPaginate": {
	          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
	          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
	        }
	    },
	    "createdRow": function( row, data, dataIndex ) {
	    	$(row).attr('id', 'row-'+data['id']);

	    	if(data['status'] == 'Trashed') {
	    		$(row).find('.btn-group').find('.btn-danger').remove();
	    		$(row).find('.btn-group').prepend("<button class='btn btn-sm btn-info po-restore'><i class='glyphicon glyphicon-eye-open'></i></button>");
	    	}
	  	}
  	});

  	$('#dTables tbody').on( 'click', '.btn-edit', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        var href = "{{ url('/super/dispensaries/edit') }}"+"/"+data['id'];
        location.href = href;
    } );

    $('#dTables tbody').on( 'click', '.po', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        $('.btn').not(this).popover('hide');
        $(this).popover({
        	container: 'body',
        	trigger:'manual',
        	html: true,
        	placement: 'left',
        	title:'Are you sure!!!',
        	content:'<span class="help-block">If you delete this group, All the products related with this group also deleted.</span><a class="btn btn-sm btn-danger po-delete" href="{{ url('/super/dispensaries/delete') }}" data-uid="'+data['id']+'">Yes I`m sure</a> <button class="btn btn-sm po-close">No</button>'
        });
        $(this).popover('show');
    });

    $('#dTables tbody').on( 'click', '.po-restore', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        $('.btn').not(this).popover('hide');
        $(this).popover({
        	container: 'body',
        	trigger:'manual',
        	html: true,
        	placement: 'left',
        	title:'Restore this group!!!',
        	content:'<span class="help-block">Group related products are not self restored. So please check product list & restore manually.</span><a class="btn btn-sm btn-info po-restored" href="{{ url('/super/dispensaries/restored') }}" data-uid="'+data['id']+'">Yes, Restore this...</a> <button class="btn btn-sm po-close">No</button>'
        });
        $(this).popover('show');
    });

});

$(document).on("click", '.po-delete', function (e) {
	e.preventDefault();
	$.ajaxSetup({
	  	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});
	$.ajax({
		url: $(this).attr('href'),
		type: 'POST',
		data: {
			id: $(this).data('uid')
		},
	}).done(function(data) {
		// console.log(data);
		if(data.message == 'success') {
			$('.po').popover('hide');
			$('#dTables').DataTable().ajax.reload(null, false);
		}
	});
});

$(document).on("click", '.po-restored', function (e) {
	e.preventDefault();
	$.ajaxSetup({
	  	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});
	$.ajax({
		url: $(this).attr('href'),
		type: 'POST',
		data: {
			id: $(this).data('uid')
		},
	}).done(function(data) {
		// console.log(data);
		if(data.message == 'success') {
			$('.po-restore').popover('hide');
			$('#dTables').DataTable().ajax.reload(null, false);
		}
	});
});
</script>
@endsection