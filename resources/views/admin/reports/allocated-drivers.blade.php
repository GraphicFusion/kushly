{{-- Super Admin --}}
@extends('layouts.super-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/reports/allocated-drivers'); ?>>
			<a href="{{ url('/super/reports/allocated-drivers') }}">Allocated Driver</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-tb-30">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">

				<table id="dTableOrder" class="table table-striped">
					<thead>
						<tr>
							<th>Driver ID</th>
							<th>Name</th>
							<th>Order Status Detail</th>
						</tr>
					</thead>
					<tbody>
						
						@php
					    	$role = Sentinel::findRoleBySlug('driver');
					        $users = $role->users()->withTrashed()->with('roles')->get();
					        $driverIDs = '';
					        foreach ($users as $key => $user) {
					        	$driverIDs[] = [$user->id, $user->first_name.'&nbsp;'.$user->last_name];
					        }
					        // dd($driverIDs);
				        @endphp

				        @foreach ($driverIDs as $key => $d)
							<tr>
								<td>{{ $d[0] }}</td>
								<td>{{ $d[1] }}</td>
								<td>
									@php
										$orderDrivers = App\OrderDriver::select('driver_id','order_id','order_driver_status','estimate_time','estimate_status')->where('driver_id', $d)->get();
									@endphp
							        @if(sizeof($orderDrivers) > 0)
						        		<table class="table table-striped">
						        			<thead>
						        				<tr>
						        					<th>Order ID</th>
						        					<th>Status</th>
					        					</tr>
				        					</thead>
				        					<tbody>
									            @foreach ($orderDrivers as $key => $od)
									            	<tr>
									            		<td>{{ $od["order_id"] }}</td>
									            		<td>{{ title_case($od["order_driver_status"]) }}</td>
								            		</tr>
										        @endforeach
								        	</tbody>
							        	</table>
						        	@else
										No Order Allocated
						        	@endif
						        </td>
					        </tr>
				        @endforeach
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
$(document).ready(function($) {

	var oTable = $('#dTableOrder').DataTable({
		// "sDom": '<"top">rt<"bottom"p><"clear">',
		"stateSave": true,
		"lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
	    "iDisplayLength": 5,
	    "bSort" : false,
	    // "aaSorting": [[0, 'asc']],
	    "destroy": true,
		"language": {
		    "oPaginate": {
	          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
	          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
	        }
	    },
	});

	$('#search_dtable').keyup(function(){
        oTable.search($(this).val()).draw();
    });
});
</script>
@endsection