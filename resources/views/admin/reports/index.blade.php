{{-- Super Admin --}}
@extends('layouts.super-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/reports'); ?>>
			<a href="{{ url('/super/reports') }}">Reports</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1>Admin Reports</h1>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection