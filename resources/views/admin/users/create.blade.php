{{-- Super Admin - Create User --}}

@extends('layouts.super-admin')

@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/users'); ?>>
			<a href="{{ url('/super/users') }}">Users</a>
		</li>
		<li <?php echo navActive('super/users/create'); ?>>
			<a href="{{ url('/super/users/create') }}">Create User</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5>Create New User</h5>
				</div>
				<div class="col-lg-6"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				{!! Form::open(['method'=>'post', 'url'=>'super/users/create', 'class'=>'form', 'id'=>'adddriver']) !!}

					<div class="form-group {{ $errors->has('first_name') ? 'has-error' :'' }}">
					    {!! Form::label('first_name', 'First Name') !!}
					    {!! Form::text('first_name', '', ['class' => 'form-control']) !!}
					    {!! $errors->first('first_name','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('last_name') ? 'has-error' :'' }}">
					    {!! Form::label('last_name', 'Last Name') !!}
					    {!! Form::text('last_name', '', ['class' => 'form-control']) !!}
					    {!! $errors->first('last_name','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
					    {!! Form::label('email', 'E-Mail Address') !!}
					    {!! Form::text('email', '', ['class' => 'form-control']) !!}
					    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('password') ? 'has-error' :'' }}">
					    {!! Form::label('password', 'Password') !!}
					    {!! Form::password('password', ['class' => 'form-control']) !!}
					    {!! $errors->first('password','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('re_password') ? 'has-error' :'' }}">
					    {!! Form::label('re_password', 'Confirm Password') !!}
					    {!! Form::password('re_password', ['class' => 'form-control']) !!}
					    {!! $errors->first('re_password','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('phone_no') ? 'has-error' :'' }}">
					    {!! Form::label('phone_no', 'Phone No') !!}
					    {!! Form::text('phone_no', '', ['class' => 'form-control']) !!}
					    {!! $errors->first('phone_no','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('birthday') ? 'has-error' :'' }}">
					    {!! Form::label('birthday', 'Date of Birth') !!}
					    {!! Form::text('birthday', '', ['class' => 'form-control ll-skin-melon', 'id'=>'datepicker']) !!}
					    {!! $errors->first('birthday','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('roles') ? 'has-error' :'' }}">
					    {!! Form::label('roles', 'User Roles') !!}
						<div class="clearfix">
					    <?php
							$roles = \Sentinel::getRoles();
							$act = '';
							foreach ($roles as $key => $role) {
								echo '<div class="radio-inline"><label><input type="radio" name="roles" id="roles" value="'.$role->id.'">&nbsp;'.$role->name.'</label></div>';
							}
						?>
						</div>
						{!! $errors->first('roles','<span class="help-block">:message</span>') !!}
					</div>
				{!! Form::submit('Create', ['class'=>'btn btn-primary-outline']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection

@section('customJs')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#datepicker').datepicker({
	      	changeMonth: true,
	      	changeYear: true,
	      	yearRange: "1940:c",
	      	beforeShow: function(input, inst) {
				$('#ui-datepicker-div').wrap("<div class='datepicker ll-skin-melon hasDatepicker'></div>");
			},
			afterClose: function(input, inst) {
				$('#ui-datepicker-div').unwrap();
			}
	    });
	});
</script>
@endsection