{{-- List Users --}}
@extends('layouts.super-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/users'); ?>>
			<a href="{{ url('/super/users') }}">Users</a>
		</li>
		<li <?php echo navActive('super/users/create'); ?>>
			<a href="{{ url('/super/users/create') }}">Create User</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5>List Users</h5>
				</div>
				<div class="col-lg-6">
					<a href="{{ url('/super/users/create') }}" class="btn btn-primary pull-right">Create New User</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<table id="dTables" class="table" style="width:100%;">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone No</th>
							<th>Role</th>
							<th width="50px">Action</th>
						</tr>
					</thead>
					{{-- Ajax Content Load --}}
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
$(document).ready(function($) {
	var oTable = $('#dTables').DataTable({
		"ajax": "{{ url('/super/users/json') }}",
		columns: [
            { data: 'id' },
            { 
            	data: 'null',
            	render: function ( data, type, full, row ) {
                    return full['first_name']+' '+full['last_name'];
                }
            },
            { data: 'email' },
            { data: 'phone_no' },
            { 
        		data: 'roles', 
        		render: function (data, type, full, meta){
        			getdata = "";
					$.each(data, function (index, data) {
						getdata += '<label class="badge">'+data.name+'</label>&nbsp;';
					})
            		return getdata;
             	} 
         	},
            { 
            	data: 'action', 
            	orderable: false, 
            	searchable: false, 
            	defaultContent:"<div class='btn-group btn-action' role='group'><button class='btn btn-danger po'><i class='glyphicon glyphicon-remove'></i></button><a href='javascript:void(0)' class='btn btn-warning btn-edit'><i class='glyphicon glyphicon-edit'></i></a></div>" 
            }
        ],
		"stateSave": true,
		"lengthMenu": [ 10, 25, 50, 75, 100 ],
	    "iDisplayLength": 10,
	    "aaSorting": [[0, 'desc']],
	    "destroy": true,
		"language": {
			"search": "_INPUT_",
	        "searchPlaceholder": "Search records ...",
	        "lengthMenu": "_MENU_",
		    "oPaginate": {
	          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
	          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
	        }
	    },
	    "createdRow": function( row, data, dataIndex ) {
	    	$(row).attr('id', 'row-'+data['id']);
	  	}
  	});

  	$('#dTables tbody').on( 'click', '.btn-edit', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        var href = "{{ url('/super/users/edit') }}"+"/"+data['id'];
        $(this).attr('href', href);
    } );

    $('#dTables tbody').on( 'click', '.po', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        $('.btn').not(this).popover('hide');
        $(this).popover({
        	container: 'body',
        	trigger:'manual',
        	html: true,
        	placement: 'left',
        	title:'Are you sure!!!',
        	content:'<a class="btn btn-sm btn-danger po-delete" href="{{ url('/super/users/delete') }}" data-uid="'+data['id']+'">Yes I`m sure</a> <button class="btn btn-sm po-close">No</button>'
        });
        $(this).popover('show');
    });

});
$(document).on("click", '.po-delete', function (e) {
	e.preventDefault();
	$.ajaxSetup({
	  	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});
	$.ajax({
		url: $(this).attr('href'),
		type: 'POST',
		data: {
			id: $(this).data('uid')
		},
	}).done(function(data) {
		// console.log(data);
		if(data.message == 'success') {
			$('.po').popover('hide');
			$(this).parent('tr#row-'+data.id).css('background', 'red').fadeOut('slow');
			$('#dTables').DataTable().ajax.reload(null, false);
		}
	});
});
</script>
@endsection