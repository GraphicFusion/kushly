{{-- Edit user details --}}
@extends('layouts.super-admin')

@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/drivers'); ?>>
			<a href="{{ url('/super/drivers') }}">Drivers</a>
		</li>
		<li <?php echo navActive('super/drivers/create'); ?>>
			<a href="{{ url('/super/drivers/create') }}">Create New Account</a>
		</li>
		<li <?php echo navActive('super/drivers/edit/{id}'); ?>>
			<a href="{{ url('/super/drivers/edit/{id}') }}">Edit Driver Account</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5 class="m0">Edit Details: {{ $user->first_name.' '.$user->last_name }}</h5>
				</div>
				<div class="col-lg-6"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				{!! Form::open(['method'=>'post', 'url'=>'super/drivers/update/'.$user->id, 'class'=>'form', 'id'=>'edituser']) !!}
		
			    {!! Form::hidden('uid', $user->id) !!}
	    		<div class="form-group {{ $errors->has('first_name') ? 'has-error' :'' }}">
				    {!! Form::label('first_name', 'First Name') !!}
				    {!! Form::text('first_name', $user->first_name, ['class' => 'form-control']) !!}
				    {!! $errors->first('first_name','<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-group {{ $errors->has('last_name') ? 'has-error' :'' }}">
				    {!! Form::label('last_name', 'Last Name') !!}
				    {!! Form::text('last_name', $user->last_name, ['class' => 'form-control']) !!}
				    {!! $errors->first('last_name','<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
				    {!! Form::label('email', 'E-Mail Address') !!}
				    {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}
				    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-group {{ $errors->has('phone_no') ? 'has-error' :'' }}">
				    {!! Form::label('phone_no', 'Phone No') !!}
				    {!! Form::text('phone_no', $user->phone_no, ['class' => 'form-control numbersOnly']) !!}
				    {!! $errors->first('phone_no','<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-group {{ $errors->has('birthday') ? 'has-error' :'' }}">
				    {!! Form::label('birthday', 'Date of Birth') !!}
				    {!! Form::text('birthday', $user->birthday, ['class' => 'form-control ll-skin-melon', 'id'=>'datepicker']) !!}
				    {!! $errors->first('birthday','<span class="help-block">:message</span>') !!}
				</div>

				<div class="form-group {{ $errors->has('roles') ? 'has-error' :'' }}">

					<p>Assigned Role</p>

					<?php
						$assigned_roles = [];
						foreach($user->roles as $role):
							$assigned_roles[] = $role->id;
						endforeach;
					
						foreach($roles as $role):
							if(in_array($role->id, $assigned_roles)) {
								$chk = 'checked="checked"';
							} else {
								$chk = '';
							}
					?>	
							<div class="checkbox">
							    <label>
							      	<input type="checkbox" name="roles[]" {{ $chk }} value="{{ $role->id }}"> {{ $role->name }}
							    </label>
						  	</div>
					<?php
						endforeach;
					?>
					{!! $errors->first('roles','<span class="help-block">:message</span>') !!}
				</div>

				{!! Form::submit('Update', ['class'=>'btn btn-primary-outline']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
@endsection

@section('customJs')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#datepicker').datepicker({
	      	changeMonth: true,
	      	changeYear: true,
	      	yearRange: "1940:c",
	      	beforeShow: function(input, inst) {
				$('#ui-datepicker-div').wrap("<div class='datepicker ll-skin-melon hasDatepicker'></div>");
			},
			afterClose: function(input, inst) {
				$('#ui-datepicker-div').unwrap();
			}
	    });
	});
</script>
@endsection