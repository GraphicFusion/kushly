{{-- Super Admin Driver Listing--}}
@extends('layouts.super-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/drivers'); ?>>
			<a href="{{ url('/super/drivers') }}">Drivers</a>
		</li>
		<li <?php echo navActive('super/drivers/create'); ?>>
			<a href="{{ url('/super/drivers/create') }}">Create New Account</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<h5 class="m0">List Drivers</h5>
				</div>
				<div class="col-sm-6">
					<a href="{{ url('/super/drivers/create') }}" class="btn btn-primary pull-right">Create New Account</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<table class="table" id="dTables" style="width: 100%;">
				<thead>
					<tr>
						<td>ID</td>
						<td>Name</td>
						<td>Email</td>
						<td>Phone No</td>
						<td>Birthday</td>
						<td>Status</td>
						<td>Action</td>
					</tr>
				</thead>
				{{-- Ajax Content Load --}}
			</table>
		</div>
	</div>
</div>

@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
$(document).ready(function($) {
	var oTable = $('#dTables').DataTable({
		"ajax": "{{ url('/super/drivers/json') }}",
		columns: [
            { data: 'id' },
            { 
            	data: 'name',
            	render: function ( data, type, full, row ) {
                    return full['first_name']+' '+full['last_name'];
                }
            },
            { data: 'email' },
            { data: 'phone_no' },
            { data: 'birthday' },
            {
            	data: 'status',
            	orderable: false, 
            	searchable: false, 
            	defaultContent: "<div class='status'></div>"
            },
            { 
            	data: 'action', 
            	orderable: false, 
            	searchable: false, 
            	defaultContent:"<div class='btn-group' role='group'><button class='btn btn-sm btn-danger po'><i class='glyphicon glyphicon-remove'></i></button><a href='javascript:void(0)' class='btn btn-sm btn-warning btn-edit'><i class='glyphicon glyphicon-edit'></i></a></div>" 
            }
        ],
		"stateSave": true,
		"lengthMenu": [ 10, 25, 50, 75, 100 ],
	    "iDisplayLength": 10,
	    "aaSorting": [[0, 'desc']],
	    "destroy": true,
		"language": {
			"search": "_INPUT_",
	        "searchPlaceholder": "Search records ...",
	        "lengthMenu": "_MENU_",
		    "oPaginate": {
	          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
	          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
	        }
	    },
	    "createdRow": function( row, data, dataIndex ) {
	    	$(row).attr('id', 'row-'+data['id']);

	    	if(data['deleted_at']) {
	    		$(row).find('.status').text('Trashed');
	    		$(row).find('.btn-group').find('.btn-danger').remove();
	    		$(row).find('.btn-group').prepend("<button class='btn btn-sm btn-info po-restore'><i class='glyphicon glyphicon-eye-open'></i></button>");
	    	} else {
	    		$(row).find('.status').text('Published');
	    	}
	  	}
  	});

  	$('#dTables tbody').on( 'click', '.btn-edit', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        var href = "{{ url('/super/drivers/edit') }}"+"/"+data['id'];
        $(this).attr('href', href);
    } );

    $('#dTables tbody').on( 'click', '.po', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        $('.btn').not(this).popover('hide');
        $(this).popover({
        	container: 'body',
        	trigger:'manual',
        	html: true,
        	placement: 'left',
        	title:'Are you sure!!!',
        	content:'<a class="btn btn-sm btn-danger po-delete" href="{{ url('/super/drivers/delete') }}" data-uid="'+data['id']+'">Yes I`m sure</a> <button class="btn btn-sm po-close">No</button>'
        });
        $(this).popover('show');
    });

    $('#dTables tbody').on( 'click', '.po-restore', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        $('.btn').not(this).popover('hide');
        $(this).popover({
        	container: 'body',
        	trigger:'manual',
        	html: true,
        	placement: 'left',
        	title:'Restore this user!!!',
        	content:'<a class="btn btn-sm btn-info po-restored" href="{{ url('/super/drivers/restored') }}" data-uid="'+data['id']+'">Yes, Restore this...</a> <button class="btn btn-sm po-close">No</button>'
        });
        $(this).popover('show');
    });
});

$(document).on("click", '.po-delete', function (e) {
	e.preventDefault();
	$.ajaxSetup({
	  	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});
	$.ajax({
		url: $(this).attr('href'),
		type: 'POST',
		data: {
			id: $(this).data('uid')
		},
	}).done(function(data) {
		// console.log(data);
		if(data.message == 'success') {
			$('.po').popover('hide');
			$('#dTables').DataTable().ajax.reload(null, false);
		}
	});
});
$(document).on("click", '.po-restored', function (e) {
	e.preventDefault();
	$.ajaxSetup({
	  	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});
	$.ajax({
		url: $(this).attr('href'),
		type: 'POST',
		data: {
			id: $(this).data('uid')
		},
	}).done(function(data) {
		// console.log(data);
		if(data.message == 'success') {
			$('.po-restore').popover('hide');
			$('#dTables').DataTable().ajax.reload(null, false);
		}
	});
});
</script>
@endsection

