{{-- Super Admin Customer List --}}
@extends('layouts.super-admin')
<?php use App\User; use App\Identification; ?>
@section('content')
<style>
@media (min-width: 768px) {
	.modal-xl {
		width: 80%;
		max-width:1200px;
	}
}

@media (min-width: 992px) {
	.modal-lg {
		width: 80px;
	}
}
body {
    padding: 30px 0px;
}

#lightbox .modal-content {
    display: inline-block;
    text-align: center;   
}

#lightbox .close {
    opacity: 1;
    color: rgb(255, 255, 255);
    background-color: rgb(25, 25, 25);
    padding: 5px 8px;
    border-radius: 30px;
    border: 2px solid rgb(255, 255, 255);
    position: absolute;
    top: -15px;
    right: -55px;
    
    z-index:1032;
}
#verification-modal h4
{
	font-size:18px;
}
#verification-modal .modal-content
{	
	overflow-y: auto;
    overflow-x: hidden;
    max-height: 780px;
}
#lightbox .modal-body
{
	width:1200px;
	margin:100 auto;
}
#lightbox img
{
	width:100% !important;
	max-height: 100% !important;
	max-width: 100% !important;
}
@media(max-width:767px)
{
	#lightbox .modal-body
	{
		width:100%;
		margin:0px auto;
	}
	#lightbox .modal-dialog
	{
		margin:100px auto;
		width:200px !important;
	}
}
</style>

<!-- <nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/customers'); ?>>
			<a href="{{ url('/super/customers') }}">Customers</a>
		</li>
		<li <?php echo navActive('super/customers/create'); ?>>
			<a href="{{ url('/super/customers/create') }}">Create Customer</a>
		</li>
	</ul>
</nav>	 -->

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<h5>Customers List</h5>
				</div>
				<div class="col-sm-6">
					{{-- <a href="{{ url('/super/customers/create') }}" class="btn btn-primary pull-right mrtop">Create Customer Account</a> --}}
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="head">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label">Filter</label>
								<div class="col-sm-10">
									{!!Form::select('fileter',[''=>'Select Filter','4'=>'Incomplete','0'=>'Not Approved','1'=>'Approved','2'=>'Rejected','3'=>'Pending Appeal'],'',['class'=>'form-control','id'=>'searchFilter'])!!}
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="verification-modal" role="dialog">
			<div class="modal-dialog modal-xl">
				<div class="modal-content">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
								<h4 class="modal-title" ><b><span class="userName"> </span></b> 'Details</h4>
							</div>										
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-center">
								<h4>User Name :</h4>
							</div>
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-left">
								<h4><span class="userName"></span></h4>
							</div>
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-right">
								<h4>State Name  :</h4>
							</div>
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-center">
								<h4><span id="stateName"></span></h4>
							</div>	
						</div>
					</div>

					<div class="modal-body" id="verificationDetails">
						
					</div>

					<div class="modal-footer">
						<!--success msg start-->
						<div class="alert alert-success col-sm-6 statusMsg" id="successMsg" style="text-align: left">
						    <strong>Success!</strong> This alert box could indicate a successful or positive action.
						  </div>
						  <div class="clearfix"></div>
						<!--success msg end-->
						<!--error msg start-->
						<div class="alert alert-danger col-sm-6 statusMsg" id="errorMsg" style="text-align: left">
						    <strong>Error!</strong> Please try again later.
						 </div>
						<!--error msg end-->


						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
			</div>
		</div>
	</div>


<div class="row">
	<div class="col-lg-12">
		<table class="table" id="dTables" style="width: 100%;">
			<thead>
				<tr>
					<td>S.No</td>
					<td>ID</td>
					<td>Name</td>
					<td>Email</td>
					<td>State</td>
					<td>Phone No</td>
					<td>Status</td>
				</tr>
			</thead>
			<tbody id="verificationTable">
				<?php $i = 0 ?>
				@foreach($verifications as $verification)
				<?php $i++; ?>
				<tr>
					<td>{{$i}}</td>
					<td>{{$verification->user_id}}</td>
					<td><a href="#" class="verification-modal" rel="{{$verification->id}}">{{User::getUserDetail($verification->user_id,'first_name')}}  {{User::getUserDetail($verification->user_id,'last_name')}}</a></td>
					<td>{{User::getUserDetail($verification->user_id,'email')}}</td>
					<td>{{$verification->state}}</td>
					<td>{{User::getUserDetail($verification->user_id,'phone_no')}}</td>
					<td>{!!Identification::getVerificationStatus($verification->id)!!}</td>
				</tr>
				@endforeach
			</tbody>
			{{-- Ajax Content Load --}}
		</table>
	</div>
</div>
</div>

<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
        <div class="modal-content">
            <div class="modal-body">
                <img src="" alt="" />
            </div>
        </div>
    </div>
</div>


</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.statusMsg').hide();
		$('#dTables').dataTable();
		$(document).on('change','#searchFilter',function()
		{
			var value = $(this).val();
			//if(value==''){
			// location.reload();
			// return false;
		//}
		//$('#verificationTable').hide();
		$.ajax({
			type: 'GET',
			url: "{{ url('/super/verifications/json') }}",
			data: {value},
			dataType: 'json',
			success:function(data){
				console.log(data);
				if(data.count!=0){
					$('#verificationTable').show();
					var j=1;
					var oTable = $('#dTables').dataTable();
					oTable.fnClearTable();
					$.each(data.result,function(index,element){
						oTable.fnAddData([j,
							data.result[index]['id'],
							data.result[index]['name'],
							data.result[index]['email'],
							data.result[index]['state'],
							data.result[index]['phone'],
							data.result[index]['status']
							]);
						j++;
					});
					return false;
				}
				var oTable = $('#dTables').dataTable();
				oTable.fnClearTable();
				$('#verificationTable').addClass('.odd');

			},error:function(e){
				console.log(e.responseText);
			}
			});
		});


		$(document).on('click','.verification-modal',function(){
			$('.statusMsg').hide();
			var vid=$(this).attr('rel');
		$.ajax({
			type:'GET',
			url:"{{ url('/super/verifications/getdetails/json') }}",
			data:{vid},
			dataType:'json',
			success: function(data)
			{
				//console.log(data);
				if(data.status=='success'){
				$('.userName').text(data.userName);
				$('#stateName').text(data.stateName);
				$('#verification-modal').modal('show');	
				$('#verificationDetails').html(data.result);   
				}else{
					return false;
				}
			},
			error: function(e)
			{
				console.log(e);
				console.log(e.responseText);
			}
			});
		});

		$(document).on('click','.updateStatusButton',function(){
			$('.statusMsg').hide();
			$('.LoadingEmail').show();
			if ($('#sendEmail').is(':checked')) {
			var email = 'yes';}else{var email = 'yes';}
			var subType= $(this).attr('rel');
			var notes= $('#updateNotes').val();
			var id= $('#rowId').attr('rel');
			$.ajax({
				type:'GET',
				url:"{{ url('/super/verifications/updatestatus/json') }}",
				data:{subType,notes,id,email},
				dataType:'json',
				success: function(data)
				{
					$('.statusMsg').hide();
					console.log(data);
					if(data.status=='success'){
					$('.LoadingEmail').hide();
						$('#successMsg').text(data.msg).show();
						//$('#submitButtons').hide();
						$('.updateStatusButton').addClass('disabled');
						 $('#updateStatusForm').find("input[type=text], textarea").val("");
						 setTimeout(function(){ location.reload(); }, 5000);
						return false;
						//$('#verification-modal').modal('hide');
					}else{
						$('#errorMsg').show();
						$('.LoadingEmail').hide();
						return false;
					}
				},
				error: function(e)
				{
					$('.LoadingEmail').hide();
					console.log(e);
					console.log(e.responseText);
				}
			});	
			
		});	
//Light box started


    var $lightbox = $('#lightbox');
    
    //$('[data-target="#lightbox"]').on('click', function(event) {
    	$(document).on('click','[data-target="#lightbox"]',function(event){
        var $img = $(this).find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };
    
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
   //Light box ended

$("#lightbox").on("show", function () {
  $("body").addClass("modal-open");
}).on("hidden", function () {
  $("body").removeClass("modal-open")
});


});
</script>
@endsection

