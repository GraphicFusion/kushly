{{-- Edit Role --}}

@extends('layouts.super-admin')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			
			<div class="panel panel-primary m-t-30">
				<div class="panel-heading">Edit Role: <b>{{ $role->name }}</b></div>
				<div class="panel-body">

					{!! Form::open(['method'=>'post', 'url'=>'super/roles/edit', 'class'=>'form', 'id'=>'edituser']) !!}
			
				    {!! Form::hidden('rid', $role->id) !!}
		    		<div class="form-group">
					    {!! Form::label('name', 'Role Name') !!}
					    {!! Form::text('name', $role->name, ['class' => 'form-control', 'readonly' => true]) !!}
					</div>

					<p>Assigned Permissions</p>
					<div class="permissions">
						<div class="checkbox">
						    <label>
						    	<input type="hidden" name="perm[create]" value="0">
						      	<input type="checkbox" name="perm[create]" value="1" <?php if(!empty($role->permissions)): echo ($role->permissions['create']==1) ? 'checked="checked"' : ''; endif ?>> Create
						    </label>
					  	</div>
						<div class="checkbox">
						    <label>
						    	<input type="hidden" name="perm[edit]" value="0">
						      	<input type="checkbox" name="perm[edit]" value="1" <?php if(!empty($role->permissions)): echo ($role->permissions['edit']==1) ? 'checked="checked"' : ''; endif ?>> Edit
						    </label>
					  	</div>
						<div class="checkbox">
						    <label>
						    	<input type="hidden" name="perm[delete]" value="0">
						      	<input type="checkbox" name="perm[delete]" value="1" <?php if(!empty($role->permissions)): echo ($role->permissions['delete']==1) ? 'checked="checked"' : ''; endif ?>> Delete
						    </label>
					  	</div>
						<div class="checkbox">
						    <label>
						    	<input type="hidden" name="perm[view]" value="0">
						      	<input type="checkbox" name="perm[view]" value="1" <?php if(!empty($role->permissions)): echo ($role->permissions['view']==1) ? 'checked="checked"' : ''; endif ?>> View
						    </label>
					  	</div>
					</div>
			
					{!! Form::submit('Update', ['class'=>'btn btn-primary-outline']) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection