{{-- Roles --}}
@extends('layouts.super-admin')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			
			<div class="panel panel-primary m-t-30">
				<div class="panel-heading">Roles</div>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($roles as $role)
							<tr>
								<td></td>
								<td>{{ $role->name }}</td>
								<td>
									<div class="btn-group" role="group">
										<a href="{{ url('/super/roles/view/'.$role->id) }}" class="btn btn-sm btn-info">View</a>
										<a href="{{ url('/super/roles/edit/'.$role->id) }}" class="btn btn-sm btn-warning">Edit</a>
										<a href="{{ url('/super/roles/delete/'.$role->id) }}" class="btn btn-sm btn-danger">Delete</a>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				
				<div class="text-center">
					{{ $roles->links() }}
				</div>
			</div>

		</div>
	</div>
</div>
@endsection