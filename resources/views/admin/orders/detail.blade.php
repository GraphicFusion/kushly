{{-- Dispensary Admin --}}
@extends('layouts.super-admin')

@section('content')

<nav class="subnav">
	<ul class="list-inline">
		<li class="active">
			<a href="#x">Order</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="product-ordered-title">
					<span class="help-block">Order#</span>
					<h5>{{ $order->id }}</h5>
				</div>

				<div class="row">
					<div class="col-md-6">

						<div class="box">
							<div class="title">Customer</div>
							<div class="matter">
								@php
									$customer = App\User::select('first_name','last_name','email','phone_no')->where('id', $order->user_id)->first();
								@endphp
								<h5>{{ $customer->first_name.' '.$customer->last_name }}</h5>
								@if($order->customer_photo)
									<img class="customer_photo" width="150px" src="{{asset('/customer_photo/'.$order->customer_photo)}}" alt="" />
								@endif
								<div class="row">
									<div class="col-md-6">
										@php 
											$loc = App\Locations::where('id', $order->locations_id)->get()->first();
										@endphp
										<p>{{ $loc->street_address }}<br/>{{ $loc->city.', '.$loc->state.', '.$loc->zip_code }}</p>
									</div>
									<div class="col-md-6">
										<p>{{ $customer->email }}<br/>{{ $customer->phone_no }}</p>
									</div>
								</div>
							</div>
						</div>
						<div class="box">
							<div class="title">Driver</div>
							<div class="matter">
								@php
									$driver = App\OrderDriver::select('driver_id')->where('order_id', $order->id)->get()->first();
								@endphp
								@if(count($driver) > 0)
									@php 
										$driver = App\User::select('first_name','last_name','email','phone_no')->where('id', $driver->driver_id)->first();
									@endphp
									<h5>{{ $driver->first_name.' '.$driver->last_name }}</h5>
									<div class="row">
										<div class="col-md-6">
											<p>{{ $driver->email }}</p>
										</div>
										<div class="col-md-6">
											<p>{{ $driver->phone_no }}</p>
										</div>
									</div>
								@else
									<h5>Currently No Driver Allocated.</h5>
								@endif
							</div>
						</div>
						<div class="box">
							<div class="title">Status</div>
							<div class="matter">
								<h5>
									<?php
										$status = '';
										if($order->status == 1) { $status = 'Order Placed'; }
										if($order->status == 2) { $status = 'Allocated'; }
										if($order->status == 3) { $status = 'Rejected'; }
										if($order->status == 4) { $status = 'Failure'; }
										if($order->status == 5) { $status = 'Accepted'; }
										if($order->status == 6) { $status = 'Delivered'; }
										if($order->status == 7) { $status = 'Accepted by dispensary'; }
	    								if($order->status == 8) { $status = 'Pickup at dispensary'; }
										echo $status;
									?>
								</h5>
								<div class="row">
									<div class="col-md-6">
										<p class="help-block">Order Date/Time</p>
										<p><b>{{ $order->timestamp_order_made }}</b></p>
									</div>
									<div class="col-md-6">
										<p class="help-block">Delivery Date/Time</p>
										<p><b>{{ $order->timestamp_order_delivered }}</b></p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="box">
							<div class="title">Products Ordered</div>
							<div class="matter">
								<table class="table product-ordered-tbl">
									<tbody>
										@php
											$products = App\OrderProduct::where('order_id', $order->id)->get();
										@endphp
										@foreach($products as $product)
											@php
												$imgURL = '';
												$img = App\ProductImages::select('path')->where('product_id', $product['product_id'])->where('featured', 1)->first();
												if(count($img)>0) {
													$imgURL = url('product-images/'.$img->path);
												} else {
													$imgx = App\ProductImages::select('path')->where('product_id', $product['product_id'])->count();
													if($imgx == 0) {
														$imgURL = url('img/product-thumb.jpg');
													} else {
														$imgx = App\ProductImages::select('path')->where('product_id', $product['product_id'])->get()->first();
														$imgURL = url('product-images/'.$imgx->path);
													}
												}

												$pro = App\Product::select('product_name','type','strain_type')->where('id', $product['product_id'])->first();
											@endphp
											<tr>
												<td width="50%">
													<a href="{{ url('#x') }}"><img src="{{ $imgURL }}" width="84" height="55" alt=""></a>
													<p>
														<b>{{ $pro->product_name }}</b>
														<span class="help-block">
															@if(!empty($pro->type) && !empty($pro->strain_type))
																{{ $pro->type.'/'.$pro->strain_type }}
															@elseif(!empty($pro->type) && empty($pro->strain_type))
																{{ $pro->type }}
															@elseif(empty($pro->type) && !empty($pro->strain_type))
																{{ $pro->strain_type }}
															@endif
														</span>
													</p>
												</td>
												<th width="25%">{{ $product->quantity_per_unit }}</th>
												<th width="25%">${{ $product->unit_price }}</th>
											</tr>
										@endforeach
										
										<tr>
											<td colspan="3">
												<table class="table order-totals">
													<tbody>
														<tr>
															<td>Taxes</td>
															<th>${{ $order->taxes }}</th>
														</tr>
														<tr>
															<td>Delivery</td>
															<th>${{ $order->delivery }}</th>
														</tr>
														<tr>
															<td>Total</td>
															<th>${{ $order->order_value }}</th>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection