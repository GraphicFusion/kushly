{{-- Pages --}}

@extends('layouts.super-admin')

@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/manage-pages'); ?>>
			<a href="{{ url('/super/manage-pages') }}">Pages</a>
		</li>
		<li <?php echo navActive('super/manage-pages/add-new'); ?>>
			<a href="{{ url('/super/manage-pages/add-new') }}">Add New Page</a>
		</li>
		<li <?php echo navActive('super/manage-pages/edit/{slug}'); ?>>
			<a href="{{ url('/super/manage-pages/edit/{slug}') }}">Edit Page</a>
		</li>
	</ul>
</nav>

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5>Edit Page: {{ $page->page_title }}</h5>
				</div>
				<div class="col-lg-6"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				{!! Form::open(['method'=>'post', 'url'=>'super/manage-pages/edit/'.$page->id, 'class'=>'form', 'id'=>'editpage', 'files'=>true]) !!}

					<div class="form-group {{ $errors->has('page_title') ? 'has-error' :'' }}">
						{!! Form::label('page_title', 'Page Title') !!}
						{!! Form::text('page_title', $page->page_title, ['class' => 'form-control']) !!}
						{!! $errors->first('page_title','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {{ $errors->has('page_content') ? 'has-error' :'' }}">
						{!! Form::label('page_content', 'Page Content') !!}
						{!! Form::textarea('page_content', $page->page_content, ['class' => 'form-control summernote']) !!}
						{!! $errors->first('page_content','<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group">
						{!! Form::label('custom_css', 'Custom CSS') !!}
						{!! Form::textarea('custom_css', $page->custom_css, ['class' => 'form-control']) !!}
					</div>

				{!! Form::submit('Update', ['class'=>'btn btn-primary-outline']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('/lib/summernote/summernote.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('/lib/summernote/summernote.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
  	$('.summernote').summernote({
  		placeholder: 'Page Content ...',
  		height: 450
  	});
});
</script>
@endsection