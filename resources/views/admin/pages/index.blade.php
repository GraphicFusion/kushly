{{-- Pages --}}

@extends('layouts.super-admin')

@section('content')
<nav class="subnav">
	<ul class="list-inline">
		<li <?php echo navActive('super/manage-pages'); ?>>
			<a href="{{ url('/super/manage-pages') }}">Pages</a>
		</li>
		<li <?php echo navActive('super/manage-pages/add-new'); ?>>
			<a href="{{ url('/super/manage-pages/add-new') }}">Add New Page</a>
		</li>
	</ul>
</nav>	

<div class="bg-white p-b-30">
	<div class="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h5>Pages</h5>
				</div>
				<div class="col-lg-6">
					<a href="{{ url('/super/manage-pages/add-new') }}" class="btn btn-primary pull-right">Add Page</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<table id="dTables" class="table" width="100%" cellpadding="0">
					<thead>
						<tr>
							<td>ID</td>
							<td>Title</td>
							<td>Slug</td>
							<td width="15%">Action</td>
						</tr>
					</thead>
					{{-- Ajax Content Load --}}
				</table>					
			</div>
		</div>
	</div>
</div>
@endsection

@section('customCss')
<link rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
@endsection

@section('customJs')
<script src="{{ asset('js/dataTables.js') }}"></script>
<script type="text/javascript">
$(document).ready(function($) {
	var oTable = $('#dTables').DataTable({
		"ajax": "{{ url('/super/manage-pages/json') }}",
		"aoColumns": [
            { "data": "id" },
            { "data": "page_title" },
            { "data": "slug" },
            { "data": null }
        ],
	    "aoColumnDefs": [ 
	    	{ "orderable": false, "targets": 0 },
	    	{
                "targets": [ 2 ],
                "visible": false,
                "searchable": false
            },
	    	{
	    		"orderable": false,
	            "targets": -1,
	            "data": "null",
				"defaultContent": "<div class='btn-group' role='group'><button class='btn btn-sm btn-danger po'><i class='glyphicon glyphicon-remove'></i></button><a href='javascript:void(0)' class='btn btn-sm btn-warning btn-edit'><i class='glyphicon glyphicon-edit'></i></a></div>"
        	}
    	],
    	"stateSave": true,
		"lengthMenu": [ 10, 25, 50, 75, 100 ],
        "iDisplayLength": 10,
        "aaSorting": [[0, 'asc']],
        "destroy": true,
		"language": {
			"search": "_INPUT_",
	        "searchPlaceholder": "Search records ...",
	        "lengthMenu": "_MENU_",
    	    "oPaginate": {
	          	"sNext": '<i class="glyphicon glyphicon-arrow-right"></i>',
	          	"sPrevious": '<i class="glyphicon glyphicon-arrow-left"></i>',
	        }
	    },
	    "createdRow": function( row, data, dataIndex ) {
	    	$(row).attr('id', 'row-'+data['id']);
	  	}
    });

    $('#dTables tbody').on( 'click', '.btn-edit', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        var href = "{{ url('/super/manage-pages/edit') }}"+"/"+data['id'];
        $(this).attr('href', href);
    } );

    $('#dTables tbody').on( 'click', '.po', function () {
        var data = oTable.row( $(this).parents('tr') ).data();
        $('.btn').not(this).popover('hide');
        $(this).popover({
        	container: 'body',
        	trigger:'manual',
        	html: true,
        	placement: 'left',
        	title:'Are you sure!!!',
        	content:'<a class="btn btn-sm btn-danger po-delete" href="{{ url('/super/manage-pages/delete') }}" data-slug="'+data['slug']+'">Yes I`m sure</a> <button class="btn btn-sm po-close">No</button>'
        });
        $(this).popover('show');
    });

});

$(document).on("click", '.po-delete', function (e) {
	e.preventDefault();
	$.ajaxSetup({
	  	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});
	$.ajax({
		url: $(this).attr('href'),
		type: 'POST',
		data: {
			slug: $(this).data('slug')
		},
	}).done(function(data) {
		// console.log(data);
		if(data.message == 'success') {
			$('.po').popover('hide');
			$(this).parent('tr#row-'+data.id).css('background', 'red').fadeOut('slow');
			$('#dTables').DataTable().ajax.reload(null, false);
		}
	});
});
</script>
@endsection