{{-- {!! Form::open(['url'=>url('search'), 'method' => 'get', 'class' => 'form-search', 'id' => 'form-search']) !!} --}}
{!! Form::open(['method' => 'post', 'class' => 'form-search', 'id' => 'form-search']) !!}
    <div class="input-group">
        <input type="text" name="q" id="q" class="form-control" placeholder="Search ..." />
        <div class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <?php 
                    echo embed_svg(
                        $class = '', 
                        $svgurl = 'img/search-icon.svg', 
                        $height = '20px', 
                        $width = '20px', 
                        $link = ''
                    ); 
                ?>
            </button>
        </div>
    </div>
{!! Form::close() !!}