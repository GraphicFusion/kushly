{{-- Login Form --}}
@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				
				<div class="signin-from">
					<h3>Forgot Password</h3>
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					@if(!empty($msg))
						<div class="alert alert-success">{{ $msg }}</div>
					@endif

					{!! Form::open(['url' => 'forgot-password', 'method' => 'post', 'class' => 'form', 'id' => 'forgotPassForm']) !!}
						<div class="form-group">
						    {!! Form::label('email', 'E-Mail Address') !!}
						    {!! Form::text('email', '', ['class' => 'form-control']) !!}
						</div>

						{!! Form::submit('Reset Password', ['class'=>'btn btn-primary-outline']) !!}
					{!! Form::close() !!}
				</div>

			</div>
		</div>
	</div>
@endsection


@section('customJs')

@endsection