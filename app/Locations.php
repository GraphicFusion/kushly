<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Locations extends Model
{
    use SoftDeletes;
    public $table = 'locations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'street_address','city','state','zip_code','type','lat','lng'
	];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
	/**
     * Get the user with location
     */
    public function user()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * Get the group that owns the comment.
     */
    public function group()
    {
        return $this->belongsTo('App\Groups')->withTimestamps();
    }

    /**
     * Relation with orders
     */
    public function orders()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * get location by id
     * flag C for complete address, AR for return array
     */    
    public static function getLocation($id, $flag)
    {
        $l = static::select('street_address','city','state','zip_code')->where('id', $id)->first();
        if($flag == 'C') {
            return $l->street_address.', '.$l->city.', '.$l->state.', '.$l->zip_code;    
        } elseif ($flag == 'AR') {
            return $l;
        }        
    }
}