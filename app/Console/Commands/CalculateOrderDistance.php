<?php

namespace App\Console\Commands;

use App\Order;
use App\OrderDriver;
use App\OrderProduct;
use App\Locations;
use App\Product;
use App\Groups;
use App\DriverCurrentLocations;
use App\User;
use Sentinel;
use Illuminate\Console\Command;

class CalculateOrderDistance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate:distance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate distance between driver and the customer when order is accepted by the driver.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api_estimate = OrderDriver::select('id','api_estimate_time','estimate_time','cron_estimate_time')
            ->where('order_driver_status', 'accepted')
            ->get();

        $cronAcceptedTime = '';
        foreach($api_estimate as $key => $apie) {

            if(!empty($apie->cron_estimate_time)) {
              $cronAcceptedTime = $apie->cron_estimate_time;
            } elseif(!empty($apie->estimate_time) && empty($apie->cron_estimate_time)) {
              $cronAcceptedTime = $apie->estimate_time;
            } elseif(empty($apie->estimate_time) && empty($apie->cron_estimate_time)) {
              $cronAcceptedTime = $apie->api_estimate_time;
            } else {
              $cronAcceptedTime = $apie->api_estimate_time;
            }

            $hours = explode("hours", $cronAcceptedTime);
            $mins = explode("min", @$hours[1]);

            $totalTimimg = trim(trim(@$hours[0]).':'.trim(@$mins[0]).'Z');
            $lessInterval = trim('00:05').'Z';
            $total = strtotime($totalTimimg, 0);
            $interval = strtotime($lessInterval,0);
            $totalEstimate = $total - $interval;

            $rhours = $totalEstimate / 3600;
            $rminutes = ($totalEstimate % 3600) / 60;

            // echo sprintf("%d hours %d min", $rhours, $rminutes);

            // if($rhours == 0 && $rminutes == 0){
            //   // Mail
            // }

            if(($rhours >= 0) && ($rminutes >= 0)){
                \DB::table('order_driver')
                  ->where('id', $apie->id)
                  ->update([
                    'cron_estimate_time' => sprintf("%d hours %d min", 0, 0)
                  ]);
            } else {
                \DB::table('order_driver')
                  ->where('id', $apie->id)
                  ->update([
                    'cron_estimate_time' => sprintf("%d hours %d min", $rhours, $rminutes)
                  ]);
            }
        }

        $this->info('Estimate time updation cron is executed.');

        /*
        // Accepted orders by drivers with customer geo location
        $acceptedOrders = Order::select('orders.id as order_id','user_id','locations.lat','locations.lng')
          ->join('locations', 'locations.id', '=', 'orders.locations_id')
          ->where('orders.active', 1)
          ->where('orders.status', 5)
          ->orderBy('orders.id','asc')
          ->get();
        foreach ($acceptedOrders as $key => $ao) {
            // pr($ao);

            $od = OrderDriver::select('id','driver_id')->where('order_id', $ao->order_id)->first();
            $odLoc = DriverCurrentLocations::select('lat','lng')->where('driver_id', $od['driver_id'])->where('stat', 1)->first();

            // pr($odLoc['lat'].' '.$odLoc['lng']);
            $dst = GetDrivingDistance($odLoc['lat'], $ao->lat, $odLoc['lng'], $ao->lng, 'TM');

            // pr($dst);

            if(\DB::table('order_driver')
                ->where('id', $od->id)
                ->where('estimate_status',0)
                ->update([
                    'estimate_time' => $dst['time']
                ])) {
                $this->info('Order id:('.$ao->order_id.') estimate time updated.');
            }
        }
        */
    }
}
