<?php

namespace App\Console\Commands;

use App\User;
use App\Order;
use App\OrderDriver;
use App\OrderProduct;
use App\Locations;
use App\Product;
use App\Groups;
use App\DriverCurrentLocations;
use Sentinel;
use Illuminate\Console\Command;

class ReallocateRejectedOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reallocate:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reallocate order to driver when previous driver is not accepting.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get allocated and rejected orders
        $skipOrders = [];
        $allocats = [];
        $rejects = [];
        $accepts = [];
        $delivers = [];
        $failed = [];

        $skipDrivers = [];
        $skipDriversAllocate = [];
        $skipDriversRejects = [];

        $allocated = OrderDriver::select('order_id', 'driver_id')
              ->where('order_driver_status', 'allocated')
              ->get();
        if(sizeof($allocated) > 0)
        {
            foreach ($allocated as $key => $al) {
                $allocats[] = $al['order_id'];
                $skipDriversAllocate[] = $al['driver_id'];
            }
        }
        // pr($allocats);

        $rejected = OrderDriver::select('order_id', 'driver_id')
              ->where('order_driver_status', 'rejected')
              ->get();
        if(sizeof($rejected) > 0)
        {
            foreach ($rejected as $key => $r) {
                $rejects[] = $r['order_id'];
                // $skipDriversRejects[] = $r['driver_id'];
            }
        }
        // pr($rejects);

        $accepted = OrderDriver::select('order_id', 'driver_id')
              ->where('order_driver_status', 'accepted')
              ->get();
        if(sizeof($accepted) > 0)
        {
            foreach ($accepted as $key => $ac) {
                $accepts[] = $ac['order_id'];
            }
        }
        // pr($accepts);

        $delivered = OrderDriver::select('order_id', 'driver_id')
              ->where('order_driver_status', 'delivered')
              ->get();
        if(sizeof($delivered) > 0)
        {
            foreach ($delivered as $key => $d) {
                $delivers[] = $d['order_id'];
            }
        }
        // pr($delivers);

        $failer = OrderDriver::select('order_id', 'driver_id')
              ->where('order_driver_status', 'failer')
              ->get();
        if(sizeof($failer) > 0)
        {
            foreach ($failer as $key => $d) {
                $failed[] = $d['order_id'];
            }
        }
        // pr($failed);

        $role = Sentinel::findRoleBySlug('driver');
        $users = $role->users()->with('roles')->get();
        foreach ($users as $user) {
          if($user->api_login == 0) {
            $nlUser[] = $user->id;
          }
        }

        $skipOrders = array_unique(array_merge($allocats,$delivers,$accepts,$failed), SORT_REGULAR);
        // pr($skipOrders);
        $skipDrivers = array_unique(array_merge($skipDriversAllocate,$skipDriversRejects,$nlUser), SORT_REGULAR);

        // pr($skipDrivers);
        // get orders
        $orders = Order::where('active', 1)
              ->where('status','!=','Delivered')
              ->whereNotIn('id', $skipOrders)
              ->whereIn('id', $rejects)
              ->orderBy('id','asc')
              ->get();

        foreach ($orders as $key => $order) {
            // pr($order->id);

            // Order customer location
            $orderLocCust = Locations::where('id', $order->locations_id)->get();
            $cLoc = '';
            foreach ($orderLocCust as $locCust) {
                $cLoc = $locCust;
            }

            // get dispensary/group location
            $oProduct = $order->orderProducts;
            foreach ($oProduct as $key => $op) {
                $pgid = Product::select('group_id')->where('id', $op['product_id'])->first();
                $group = Groups::where('id', $pgid->group_id)->get()->first();
                $groupLoc = $group->location()->get()->first();
            }
            // pr(distance($cLoc->lat, $cLoc->lng, $groupLoc->lat, $groupLoc->lng, "M") . " Miles Distance between customer and dispensaey order id:".$order->id);

            $role = Sentinel::findRoleBySlug('driver');
            $users = $role->users()->with('roles')->get();
            
            $arr = [];
            foreach ($users as $user) {
                $dr = DriverCurrentLocations::where('stat', 1)->where('driver_id', $user->id)->whereNotIn('driver_id', $skipDrivers)->groupBy('driver_id')->get();
                foreach ($dr as $driver) {
                    $arr[] = [
                        'distance' => distance($driver->lat, $driver->lng, $groupLoc->lat, $groupLoc->lng, "M"),
                        'driver_id' => $user->id,
                        'order_id' => $order->id
                    ];
                }
            }

            // pr($arr);
            if(sizeof($arr) >= 1) {
                $allocatedDriver = min($arr);
                // pr($allocatedDriver);
                $allocation = new OrderDriver();
                $allocation->driver_id = $allocatedDriver['driver_id'];
                $allocation->order_id = $allocatedDriver['order_id'];
                $allocation->order_driver_status = 'allocated';
                $allocation->save();

                \DB::table('orders')
                  ->where('id', $allocatedDriver['order_id'])
                  ->update([
                    'status' => 2
                  ]);

                // API access key from Google API's Console
                // Ref: https://gist.github.com/MohammadaliMirhamed/7384b741a5c979eb13633dc6ea1269ce
                define( 'API_ACCESS_KEY', 'AAAAP7NiNdw:APA91bFkaiUYVMonv60aV8NWYKbmxzAGDpqk7UjTTxnziWxQYax8DW8X5Ee1-7PAkLNEWcWkbAPM5jknFCOFglnLjjxL7sxQeoa1WMNLkXK267OYgdHzHXbqBfftcnltK-8wDIENrBJx' );

                $fregid = User::select('firbaseregid')->where('id', $allocatedDriver['driver_id'])->first();

                $registrationIds = $fregid->firbaseregid;

                // prep the bundle
                $msg = array(
                  'body'  => 'Order id: '.$allocatedDriver['order_id'].' allocated to you. Please accept it as soon as possible.',
                  'title' => 'New order allocated',
                );

                $fields = array(
                  'to'    => $registrationIds,
                  'notification'  => $msg
                );

                $headers = array(
                  'Authorization: key=' . API_ACCESS_KEY,
                  'Content-Type: application/json'
                );
  
                // Send Reponse To FireBase Server  
                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );
                curl_close( $ch );

                $skipDrivers[] = $allocatedDriver['driver_id'];

                // success message.
                $this->info('Order id:('.$order->id.') is allocated');
            } else {
                $this->error('No driver is available for order id: ('.$order->id.')');
            }
        }
    }
}
