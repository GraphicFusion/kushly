<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Identification extends Model
{
    public $table = 'identifications';


    public static function getVerificationImage($id,$value)
    {
    	$user = Identification::where('user_id',$id)->first();
    	if(count($user)==1){
    		$check = $user->$value;
    		if($check=='NULL'||$check=='Null'||$check==''){
    			return $default = '/img/default.png';
    		}else{
    			return $check;
    		}
    	}
    }

    public static function getVerificationStatus($rowid)
    {
        $ver = Identification::where('id',$rowid)->first();
        if($ver->appeal_requested=='1'){
            return $status = '<button type="button" class="btn btn-basic btn-sm">Penting Appeal</button>';
        }elseif($ver->approved==0){
            return $status = '<button type="button" class="btn btn-info btn-sm">Not Approve</button>';
        }elseif($ver->approved==1){
            return $status = '<button type="button" class="btn btn-success btn-sm">Approved</button>';
        }elseif($ver->approved==2){
            return  $status =  '<button type="button" class="btn btn-danger btn-sm">Rejected</button>';
        }
    }
}
