<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public $table = 'product_price';

    protected $fillable = [
        'product_id', 'quantity_per_unit', 'unit_price', 'stock'
    ];

    /**
     * Get the group that owns the comment.
     */
    public function products()
    {
        return $this->belongsTo('App\Product')->withTimestamps();
    }

}
