<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends \Cartalyst\Sentinel\Users\EloquentUser
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'phone_no', 'birthday', 'profile_pic', 'api_token', 'api_login', 'firbaseregid', 'cardnumber', 'expdate', 'cardcode', 'bank', 'routingno', 'accountno',
        'anet_customer_profile_id','anet_customer_payment_profile_id','anet_card_last_four'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 're_password',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Vaildation rules for new customer registeration.
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required|min:2',
        'last_name' => 'required|min:2',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        're_password' => 'required|min:6|same:password',
        'phone_no' => 'required',
        'birthday' => 'required|date',
        'profile_pic' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        'stateIdFront' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        'stateIdBack' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        'medicalCardFront' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        'medicalCardBack' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
    ];

    /**
     * get user by email
     */    
    public static function byEmail($email)
    {
        return static::whereEmail($email)->first();
    }

    /**
     * Using accessor and mutators for db storage and retrive.
     */
    public function setBirthdayAttribute($value) 
    {
        $this->attributes['birthday'] = date('Y-m-d', strtotime($value) );
    }
    public function getBirthdayAttribute($value) 
    {
        return date('m/d/Y', strtotime($value) );
    }

    /**
     * The locations that belong to the user.
     */
    public function locations()
    {
        return $this->belongsToMany('App\Locations')->withTimestamps();
    }

    /**
     * Relation with groups
     */
    public function groups()
    {
        return $this->belongsToMany('App\Groups')->withTimestamps();
    }

    /**
     * Relation with orders
     */
    public function orders()
    {
        return $this->hasMany('App\Order', 'user_id', 'id');
    }

    /**
     *  relation between driver current location and driver
     */
    public function driverCurrentLocation()
    {
        return $this->belongsTo('App\DriverCurrentLocations', 'driver_id', 'id');
    }

    /**
     * get user email via user id
     */    
    public static function getEmailId($id)
    {
        return static::select('email')->where('id', $id)->first();
    }

    /**
     * get user name via id
     * flag = FN for First Name, LN for Last Name
     * if not flag then complete name return
     */    
    public static function getName($id, $flag)
    {
        switch ($flag) {
            case 'FN':
                $u = static::select('first_name')->where('id', $id)->first();
                return $u->first_name;
                break;
            case 'LN':
                $u = static::select('last_name')->where('id', $id)->first();
                return $u->last_name;
                break;
            
            default:
                $u = static::select('first_name', 'last_name')->where('id', $id)->first();
                return $u->first_name.' '.$u->last_name;
                break;
        }
    }

    /**
     * get user api token
     */    
    public static function existApiToken($token)
    {
        return static::select('id','api_token','api_login')->where('api_token', $token)->first();
    }

    /**
     * Get Authorize.net profile
     */
    public static function getAnetProfile()
    {
        $userID = getUserId(); // Current User
        $u = static::select('id','anet_customer_profile_id', 'anet_customer_payment_profile_id', 'anet_card_last_four')->where('id', $userID)->first();
        if(!empty($u->anet_customer_profile_id)) {
            return $u;
        } else {
            return false;
        }
    }

    //get user detail base user id
    public static function getUserDetail($id,$value)
    {

         $count = User::where('id',$id)->count();
        if($count==1){
            $values = User::where('id',$id)->first();
            return $values->$value;
        }
        return '';
        
    }
}
