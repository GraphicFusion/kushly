<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDriver extends Model
{
    public $table = 'order_driver';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'driver_id','order_id','order_accept_timestamp','rejection_reason','rejection_timestamp','failer_reason','failer_timestamp','order_driver_status','estimate_time','estimate_status', 'api_estimate_time', 'cron_estimate_time'
	];

    public function driver()
    {
        return $this->hasOne('App\User', 'id', 'driver_id');
    }

    public function order()
    {
    	return $this->belongsTo('App\Order', 'id', 'order_id');
    }
}
