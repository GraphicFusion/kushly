<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverCurrentLocations extends Model
{
    public $table = 'driver_current_locations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'driver_id','lat','lng','stat'
	];

	public function driver()
    {
        return $this->hasOne('App\User', 'id', 'driver_id');
    }
}
