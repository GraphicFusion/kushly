<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Session;

class CheckAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::check())
        {
            return $next($request);
        }
        Session::flash('danger', 'Login Required to complete this order. <a class="alert-link" href="'.makeURL('/signin').'">Click Here</a> to login.');
        return back();
    }
}
