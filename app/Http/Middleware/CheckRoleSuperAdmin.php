<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Session;

class CheckRoleSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::check())
        {
            $user_role = Sentinel::inRole('super-admin');
            if($user_role) {
                if(Session::get('activeRole') == 'super-admin') {
                    return $next($request);
                } else {
                    Session::flash('danger', 'Unauthorized Access!');
                    $redirectURL = '';
                    $currentSession = Session::get('activeRole');
                    switch ($currentSession) {
                        case 'super-admin':
                            $redirectURL = makeURL('/super');
                            break;
                        case 'dispensary-admin':
                            $redirectURL = makeURL('/dispensary/home');
                            break;
                        case 'driver':
                            $redirectURL = makeURL('/driver');
                            break;
                        case 'customer':
                            $redirectURL = makeURL('/');
                            break;
                    }
                    return redirect($redirectURL);
                }
            }
        }
        Session::flash('danger', 'Unauthorized Access!');
        return redirect('/');
    }
}
