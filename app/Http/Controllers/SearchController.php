<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class SearchController extends Controller
{
    public function __invoke(Request $request)
    {
    	$query = $request->q;
		// $data = Product::where('product_name','LIKE','%'.$query.'%')->get();
		$data = Product::like('product_name', $query)->get();
		return view('search-result', compact('data'));
    }
}
