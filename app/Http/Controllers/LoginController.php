<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Sentinel;
use Session;
use Cartalyst\Sentinel\Checkpoint\ThrottlingException;

class LoginController extends Controller
{
    /**
     * Show login form
     */
    public function getLogin()
    {
        if(Sentinel::check()) {
            $current_role = getUserRole();
            switch ($current_role) {
                case 'super-admin':
                    return redirect('super');
                    break;
                case 'dispensary-admin':
                    return redirect('dispensary/home');
                    break;
                case 'driver':
                    return redirect('driver');
                    break;
                case 'customer':
                    return redirect('/');
                    break;
                default:
                    return redirect('/');
                    break;
            }
        }
    	return view('signin');
    }

    /**
     * Perform authentication and check role for super admin
     * then redirect to admin area.
     */
    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('signin')
                    ->withErrors($validator)
                    ->withInput();
        } else {
            try {
                $rememberMe = false;
                if(isset($request->rememberMe))
                    $rememberMe = true;

                if($user = Sentinel::authenticate($request->all(), $rememberMe)) {
                    // Check role of current user if user havinf super admin role
                    // then redirect to admin area otherwise default home page.
                    $current_role = getUserRole();
                    switch ($current_role) {
                        case 'super-admin':
                            $request->session()->put('activeRole', $current_role);
                            Session::flash('success', 'Hello '.Sentinel::getUser()->first_name.', You are logged in as Super Admin.');
                            return redirect('super')->with([$user]);
                            break;
                        
                        case 'dispensary-admin':
                            $request->session()->put('activeRole', $current_role);
                            $assigned_groups = $user->groups;
                            if(count($assigned_groups) == 0) {
                                $msg  = 'Hello '.Sentinel::getUser()->first_name.', You are logged in as Dispensary Admin. But currently no dispensary allocated to your id';
                                return view('errors.no-dispensary-allocated', compact('msg'));
                            } else {
                                $session_gid = [];
                                $activeGroup = '';
                                foreach ($assigned_groups as $key => $group) {
                                    if($key == 0) {
                                        $activeGroup = $group->id;
                                    }
                                    $session_gid[] = $group->id;
                                }
                                // $request->session()->put('assigned_groups', $session_gid);
                                $request->session()->put('activeGroup', $activeGroup);

                                Session::flash('success', 'Hello '.Sentinel::getUser()->first_name.', You are logged in as Dispensary Admin.');
                                return redirect('dispensary/home')->with([$user]);
                            }
                            break;

                        case 'driver':
                            $request->session()->put('activeRole', $current_role);
                            Session::flash('success', 'Hello '.Sentinel::getUser()->first_name.', You are logged in as Driver.');
                            return redirect('driver')->with([$user]);
                            break;

                        case 'customer':
                            $request->session()->put('activeRole', $current_role);
                            Session::flash('success', 'Hello '.Sentinel::getUser()->first_name.', You are logged in as Customer.');
                            if(isset($request->ref)) {
                                if($request->ref == 'cart') {
                                    $cart = Session::get('addToCart');
                                    return redirect('cart')->with([$cart]);
                                }
                            } elseif(Session::get('back_ref')) {
                                $back_ref = Session::get('back_ref');
                                return redirect($back_ref);
                            } else {
                                return redirect('/products')->with([$user]);
								
                            }
                            break;
                    }

                } else {
                    Session::flash('danger','Error logging in, please try again!!!');
                    return redirect('signin');
                }
            } catch(ThrottlingException $e) {
                $delay = $e->getDelay();
                Session::flash('danger','You are banned for '.$delay.' seconds.');
                return redirect('signin');
            }
            
        }
    }

    /**
     * Perform logout
     */
    public function postLogout()
    {
        Session::forget('addToCart');
        Session::forget('addToCartLocation');
        Session::forget('back_ref');
        Session::forget('search_location');
        Session::put('addToCartGroup', 0);
        // Session::forget('assigned_groups');
        Session::forget('activeGroup');
        Session::forget('activeRole');

        Sentinel::logout(null, true);
        return redirect('signin');
    }
}
