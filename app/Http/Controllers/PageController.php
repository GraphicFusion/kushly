<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pages;

class PageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
    	$page = Pages::findBySlug($slug);
        return view('page', compact('page'));
    }
}
