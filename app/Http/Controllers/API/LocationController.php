<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DriverCurrentLocations;
use App\User;

class LocationController extends Controller
{
    public function currentLocation(Request $request)
    {
    	if($user = User::existApiToken($request->auth_code)) {
    		
    		$prevLoc = DriverCurrentLocations::where('driver_id', $user->id)->count();


    		if(sizeof($prevLoc) > 0) {

                // pr($prevLoc);
                // $lastLoca = DriverCurrentLocations::select('id','lat','lng')->where('driver_id', $user->id)->orderBy('id','desc')->first();

                // if( ($lastLoca['lat'] != $request->lat) || ($lastLoca['lng'] != $request->lng) ) {
                    $ploc = DriverCurrentLocations::where('driver_id', $user->id)->get();
                    foreach ($ploc as $loc) {
                        $loc->stat = 0;
                        $loc->save();
                    }
                    $dcl = new DriverCurrentLocations();
                    $dcl->driver_id = $user->id;
                    $dcl->lat = $request->lat;
                    $dcl->lng = $request->lng;
                    $dcl->stat = 1;
                    $dcl->save();
                    return response()->json([
                        'success' => '1',
                        'message' => 'Location updated.'
                    ]);
                // } else {
                //     return response()->json(['error' => 'Same location.']);
                // }
    		} else {
                // pr('test');
	    		$dcl = new DriverCurrentLocations();
	    		$dcl->driver_id = $user->id;
	    		$dcl->lat = $request->lat;
	    		$dcl->lng = $request->lng;
	    		$dcl->stat = 1;
	    		$dcl->save();
	    		return response()->json([
	    			'success' => '1',
	    			'message' => 'Location updated.'
    			]);
    		}

    	} else {
    		return response()->json(['error' => 'Auth Code not matched.']);
    	}
    }
}
