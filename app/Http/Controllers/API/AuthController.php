<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Reminder;
use App\User;

class AuthController extends Controller
{
    /**
     * Post login form
     */
    public function postLogin(Request $request)
    {
		$api_token = '';
		$sRoles = [];
        if(empty($request->email) || empty($request->password)) {
            return response()->json(['error' => 'Email and Password are required.']);
        } else {
            if($user = Sentinel::authenticate($request->all())) {
            
                $roles = Sentinel::findById(getUserId())->roles;
                foreach($roles as $role) {
                    $sRoles[] = $role->slug;
                }
                if(in_array('driver', $sRoles)) {
                    if($user->api_login == 1) {
                        return response()->json(['error' => 'User already logged in from other device.']);
                    } else {
                        $user->api_token = str_random(20).time().str_random(20);
                        $user->api_login = 1;
                        $user->firbaseregid = $request->firbaseregid;
                        $user->save();

                        return response()->json([
                            'success' => 1,
                            'auth_code' => $user->api_token,
                        ]);
                    }             
                } else {
                    return response()->json(['error' => 'User is not assigned driver role.']);
                }
            
            } else {
                return response()->json(['error' => 'Email or Password is incorrect']);
            }
        }
    }

    /**
     * Post logout form
     */
    public function logout(Request $request)
    {
        if($user = User::existApiToken($request->auth_code)) {
            if($user->api_login == 0) {
                return response()->json(['error' => 'User already logged out.']);
            } else {
                $user->api_token = null;
                $user->api_login = 0;
                $user->firbaseregid = null;
                $user->save();
                return response()->json(['response' => 'success']);
            }
        } else {
            return response()->json(['error' => 'Auth Code not matched.']);
        }
    }

    /**
     * Post Forget form
     */
    public function postForget(Request $request)
    {
        $user = User::whereEmail($request->email)->first();
        if(sizeof($user) == 0) {
            return response()->json(['error' => 'User not found.']);
        } else {
            $sentinel_user = Sentinel::findById($user->id);
            $reminder = Reminder::exists($sentinel_user) ?: Reminder::create($sentinel_user);
            // $this->sendEmail($user, $reminder->code);
            return response()->json(['success' => 'Reset code was sent to your email.']);
        }
    }

    /**
     * get user info
     */
    public function getDriver(Request $request)
    {
        if($user = User::existApiToken($request->auth_code)) {
            
            $driverInfo = [];
            $driver = User::select('id','first_name','last_name','email','phone_no')
                ->where('id',$user->id)
                ->first();

            $driverInfo = [
                'id' => $driver->id,
                'name' => $driver->first_name.' '.$driver->last_name,
                'email' => $driver->email,
                'phone_no' => $driver->phone_no
            ];
            
            return response()->json([
                'success' => '1',
                'driver_info' => $driverInfo
            ]);

        } else {
            return response()->json(['error' => 'Auth Code not matched.']);
        }   
    }
}
