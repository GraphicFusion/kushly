<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderProduct;
use App\OrderDriver;
use App\User;
use App\Locations;
use App\Product;
use App\Groups;
use DB;

class OrderController extends Controller
{

  public function listOrders(Request $request)
  {
    if($user = User::existApiToken($request->auth_code)) {

      $od = OrderDriver::select('order_id')->where('order_driver_status', 'allocated')->where('driver_id', $user->id)->get();

      $order_driver_d = [];
      foreach ($od as $key => $value) {
        $order_driver_d[] = $value['order_id'];
      }

      $orders = Order::select('orders.id', DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%d.%m.%Y") as order_date'), DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%h:%i %p") as order_time'), 'locations.street_address')
      ->join('locations', 'locations.id', '=', 'orders.locations_id')
      ->where('orders.active', 1)
      ->where('orders.status', 2)
      ->orderBy('orders.id','asc')
      ->whereIn('orders.id', $order_driver_d)
      ->get();

      if(sizeof($orders) > 0) {
        return response()->json([
          'success' => '1',
          'orders' => $orders
        ]);
      } else {
        return response()->json([
          'error' => 'No order allocated.'
        ]);  
      }

    } else {
      return response()->json([
        'error' => 'Auth Code not matched.'
      ]);
    }
  }

  /**
   * get order by id
   */
  public function getorder(Request $request)
  {
  	if($user = User::existApiToken($request->auth_code)) {

      $order = Order::select('id', 'locations_id', DB::raw('DATE_FORMAT(timestamp_order_made, "%d.%m.%Y") as order_date'), DB::raw('DATE_FORMAT(timestamp_order_made, "%h:%i %p") as order_time'))
      ->where('id', $request->order_id)
      ->get()->first();

      // Order customer location
      $orderLocCust = Locations::where('id', $order->locations_id)->first();

      $location_to = $orderLocCust->street_address.', '.$orderLocCust->unit.', '.$orderLocCust->city.', '.$orderLocCust->state.', '.$orderLocCust->zip_code.', '.$orderLocCust->country;

      $location_to_lat = $orderLocCust->lat;
      $location_to_lng = $orderLocCust->lng;

      // get dispensary/group location
       $oProduct = OrderProduct::select('product_id')->where('order_id',$request->order_id)->first();
        $gid = DB::table('products')->select('group_id')->where('id',$oProduct->product_id)->get();
        $group = Groups::where('id', $gid[0]->group_id)->first();
        $groupLoc = $group->location()->first();


      $location_from = $groupLoc->street_address.', '.$groupLoc->unit.', '.$groupLoc->city.', '.$groupLoc->state.', '.$groupLoc->zip_code.', '.$groupLoc->country;

      $location_from_lat = $groupLoc->lat;
      $location_from_lng = $groupLoc->lng;

      $ods = OrderDriver::select('order_driver_status','estimate_time','estimate_status','api_estimate_time','cron_estimate_time')->where('order_id', $request->order_id)->where('driver_id', $user->id)->first();

      $estimateTime = '';
      if(!empty($ods->cron_estimate_time)) {
        $estimateTime = $ods->cron_estimate_time;
      } elseif(!empty($ods->estimate_time) && empty($ods->cron_estimate_time)) {
        $estimateTime = $ods->estimate_time;
      } elseif(empty($ods->estimate_time) && empty($ods->cron_estimate_time)) {
        $estimateTime = $ods->api_estimate_time;
      } else {
        $estimateTime = $ods->api_estimate_time;
      }

    	return response()->json([
     		'success' => '1',
     		'order' => $order,
     		'order_products' => OrderProduct::where('order_id',$request->order_id)->get(),
        'location_from' => $location_from,
        'location_from_lat' => $location_from_lat,
        'location_from_lng' => $location_from_lng,
        'location_to' => $location_to,
        'location_to_lat' => $location_to_lat,
        'location_to_lng' => $location_to_lng,
        'order_driver_status' => $ods->order_driver_status,
        'estimate_time' => $estimateTime,
        'estimate_status' => $ods->estimate_status,
        'view_url' => url('driver/order/'.$request->order_id.'/detail')
   		]);
    } else {
      return response()->json([
        'error' => 'Auth Code not matched.'
      ]);
    }
  }

  /**
   * get all assignments
   */
  public function assignments(Request $request)
  {
    if($user = User::existApiToken($request->auth_code)) {

      $assignments = [];
      $oDrivers = OrderDriver::select('order_id')->where('driver_id', $user->id)->where('order_driver_status', 'accepted')->get();

      foreach ($oDrivers as $key => $od) {
        $assignments[] = $od->order_id;
      }

      if(sizeof($oDrivers) >= 1) {
        $orders = Order::select('orders.id', DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%d.%m.%Y") as order_date'), DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%h:%i %p") as order_time'), 'locations.street_address')
        ->join('locations', 'locations.id', '=', 'orders.locations_id')
        ->where('orders.active', 1)->where('orders.status', '!=', 6)
        ->whereIn('orders.id', $assignments)->get();
        return response()->json([
          'success' => '1',
          'orders' => $orders
        ]);
      } else {
        return response()->json([
          'error' => 'No Assignment Available.'
        ]);
      }

    } else {
      return response()->json([
        'error' => 'Auth Code not matched.'
      ]);
    }
  }

  /**
   *  Accept order as assignment
   */
  public function acceptOrder(Request $request)
  {
    if($user = User::existApiToken($request->auth_code)) {
      \DB::table('order_driver')
          ->where('order_id', $request->order_id)
          ->where('driver_id', $user->id)
          ->update([
            'order_accept_timestamp' => date('Y-m-d H:i:s'),
            'order_driver_status' => 'accepted'
          ]);

      \DB::table('orders')
          ->where('id', $request->order_id)
          ->update([
            'timestamp_order_delivered' => date('Y-m-d H:i:s'),
            'status' => 5
          ]);

      return response()->json([
        'success'=>'Order accepted by driver.'
      ]);
    } else {
      return response()->json(['error' => 'Auth Code not matched.']);
    }
  }

  /**
   * Reject order by driver then allocate same order to another driver
   */
  public function rejectOrder(Request $request)
  {
    if($user = User::existApiToken($request->auth_code)) {

      \DB::table('order_driver')
          ->where('order_id', $request->order_id)
          ->where('driver_id', $user->id)
          ->update([
            'rejection_reason' => $request->rejection_reason,
            'rejection_timestamp' => date('Y-m-d H:i:s'),
            'order_driver_status' => 'rejected'
          ]);

      \DB::table('orders')
          ->where('id', $request->order_id)
          ->update([
            'timestamp_order_delivered' => date('Y-m-d H:i:s'),
            'status' => 3
          ]);

      return response()->json([
        'success'=>'Reason updated for rejection.'
      ]);

    } else {
      return response()->json(['error' => 'Auth Code not matched.']);
    }
  }

  /**
   * Report failer on delivery
   */
  public function reportFailer(Request $request)
  {
    if($user = User::existApiToken($request->auth_code)) {

      \DB::table('order_driver')
          ->where('order_id', $request->order_id)
          ->where('driver_id', $user->id)
          ->update([
            'failer_reason' => $request->failer_reason ,
            'failer_timestamp' => date('Y-m-d H:i:s'),
            'order_driver_status' => 'failer'
          ]);

      \DB::table('orders')
          ->where('id', $request->order_id)
          ->update([
            'timestamp_order_delivered' => date('Y-m-d H:i:s'),
            'status' => 4
          ]);

      return response()->json([
        'success'=>'Order delivery failer reason updated.'
      ]);

    } else {
      return response()->json(['error' => 'Auth Code not matched.']);
    }
  }

  /**
   * Report failer on delivery
   */
  public function confirmDelivery(Request $request)
  {
    if($user = User::existApiToken($request->auth_code)) {

      \DB::table('order_driver')
          ->where('order_id', $request->order_id)
          ->where('driver_id', $user->id)
          ->update([
            'order_driver_status' => 'delivered'
          ]);

      \DB::table('orders')
          ->where('id', $request->order_id)
          ->update([
            'timestamp_order_delivered' => date('Y-m-d H:i:s'),
            'status' => 6
          ]);

      return response()->json([
        'success'=>'Order delivery status updated.'
      ]);

    } else {
      return response()->json(['error' => 'Auth Code not matched.']);
    }
  }

  /**
   * Get orders with failer/deliverd status
   */
  public function getOrderWithStatus(Request $request)
  {
    if($user = User::existApiToken($request->auth_code)) {
      
      $orderIDS = [];
      $odFailer = OrderDriver::select('order_id')
        ->where('order_driver_status', 'failer')
        ->where('driver_id', $user->id)
        ->get();

      $order_failer = [];
      foreach ($odFailer as $key => $value) {
        $order_failer[] = $value['order_id'];
      }

      $odDelivered = OrderDriver::select('order_id')
        ->where('order_driver_status', 'delivered')
        ->where('driver_id', $user->id)
        ->get();

      $order_delivered = [];
      foreach ($odDelivered as $key => $value) {
        $order_delivered[] = $value['order_id'];
      }

      $orderIDS = array_unique(array_merge($order_failer,$order_delivered));

      $orders = Order::select('orders.id', DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%d.%m.%Y") as order_date'), DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%h:%i %p") as order_time'), 'locations.street_address',DB::raw('DATE_FORMAT(orders.timestamp_order_delivered, "%d.%m.%Y") as delivered_date'),'order_driver.failer_reason',DB::raw('DATE_FORMAT(order_driver.failer_timestamp, "%d.%m.%Y") as failer_date'))
        ->join('locations', 'locations.id', '=', 'orders.locations_id')
        ->join('order_driver','order_driver.order_id','=','orders.id')
        ->where('orders.active', 1)
        ->orderBy('orders.id','asc')
        ->whereIn('orders.id', $orderIDS)->get();

      if(sizeof($orders) > 0) {
        return response()->json([
          'success' => '1',
          'orders' => $orders
        ]);
      } else {
        return response()->json([
          'error' => 'No order found.'
        ]);  
      }

    } else {
      return response()->json(['error' => 'Auth Code not matched.']);
    }
  }

  /**
   * Post estimate time by driver.
   */
  public function postEstimateTime(Request $request)
  {
    if($request->estimate_time < 1) {
      return response()->json(['error' => 'Estimate time required.']);
    }
    // function convertToHoursMins($time, $format = '%02d:%02d') {
    //   if ($time < 1) {
    //     return;
    //   }
    //   $hours = floor($time / 60);
    //   $minutes = ($time % 60);

    //   if($hours == 0) {
    //     return sprintf('%02d', $minutes).' minutes';
    //   } elseif ($minutes == 0) {
    //     return sprintf('%02d', $hours).' hours';
    //   } else {
    //     return sprintf($format, $hours, $minutes);
    //   }
    // }

    if($user = User::existApiToken($request->auth_code)) {

      $var = \DB::table('order_driver')
            ->select('estimate_status')
            ->where('order_id', $request->order_id)
            ->where('driver_id', $user->id)
            ->first();

      if($var->estimate_status == 0) {
        \DB::table('order_driver')
          ->where('order_id', $request->order_id)
          ->where('driver_id', $user->id)
          ->update([
            'estimate_time' => $request->estimate_time,
            // 'estimate_time' => convertToHoursMins($request->estimate_time, '%02d hours %02d minutes'),
            'estimate_status' => 1
          ]);

        return response()->json([
          'success' => 'Estimate time updated.',
          'estimate_time' => $request->estimate_time
          // 'estimate_time' => convertToHoursMins($request->estimate_time, '%02d hours %02d minutes')
        ]);
      } else {
        return response()->json(['error' => 'Estimate time already updated.']);
      }

    } else {
      return response()->json(['error' => 'Auth Code not matched.']);
    }
  }
}
