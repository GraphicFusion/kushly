<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Groups;
use App\Product;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/groups/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/groups/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'user' => 'required',
            'location' => 'required'
        ]);

        $group = new Groups;
        $group->name = $request->name;
        $group->type = $request->type;

        if( $request->hasFile('logo')){ 
            $image = $request->file('logo'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().'-'.$fileName;
            $request->logo->move(public_path('groups-logo'), $imageName);
            $group->logo = $imageName;
            $group->save();
        } else {
            $group->save();
        }

        foreach ($request->user as $key => $uid) {
            $group->user()->attach($uid);
        }

        // foreach ($request->location as $key => $loc) {
        //     $group->location()->attach($loc);
        // }
        $group->location()->attach($request->location);
        
        \Session::flash('success', 'Group created successfully.');
        return redirect('super/dispensaries');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Groups::findById($id);
        return view('admin/groups/edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'user' => 'required',
            'location' => 'required'
        ]);

        $group = Groups::findById($id);
        $group->name = $request->name;
        $group->type = $request->type;

        if( $request->hasFile('logo')){ 
            // Unlink previous logo
            unlink(public_path('groups-logo/'.$request->prevLogo));
            $image = $request->file('logo'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().'-'.$fileName;
            $request->logo->move(public_path('groups-logo'), $imageName);
            $group->logo = $imageName;
            $group->save();
        } else {
            $group->logo = $request->prevLogo;
            $group->save();
        }

        /**
         * Detach and attach users again under belongtomany relation
         */
        $users = $group->user()->get();
        foreach ($users as $key => $user) {
            $group->user()->detach($user->id);
        }
        foreach ($request->user as $key => $uid) {
            $group->user()->attach($uid);
        }

        /**
         * Detach and attach locations again under belongtomany relation
         */
        $locs = $group->location()->get();
        foreach ($locs as $key => $loc) {
            $group->location()->detach($loc->id);
        }
        foreach ($request->location as $key => $loc) {
            $group->location()->attach($loc);
        }
        
        \Session::flash('success', 'Group updated successfully.');
        return redirect('super/dispensaries');
    }

    /**
     * Soft Delete the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $group = Groups::findById($request->id);
        $group->delete();

        // Move all related product to trash
        $products = Product::where('group_id', $request->id)->get();
        foreach ($products as $key => $product) {
            $product->delete();
        }
        return response()->json(['message' => 'success', 'id' => $group->id], 200);
    }

    /**
     * Restore Soft Deleted resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request)
    {
        $group = Groups::withTrashed()->find($request->id);
        $group->restore();
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Get all pages for datatable response in json
     */
    public function getJSON()
    {
        $groups = Groups::select('id','name','type',
                \DB::raw("
                    IF(deleted_at IS NULL OR deleted_at = '', 'Published', 'Trashed')AS status
                ")
            )->with('user')->with('location')->withTrashed()->get();
        return response()->json(['data' => $groups], 200);
    }

    /**
     * Get group name
     */
    public function getGroupName(Request $request)
    {
        $group = Groups::select('name')->find($request->id)->first();
        return $group['name'];
    }
}
