<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Validator;
use App\User;
use App\Identification;
use Sentinel;
use App\Mail\EmailStatusToUser;
use Response;
use DB;
use Mail;

class VerificationController extends Controller
{
	public function index()
	{
		$verifications = Identification::get();
		return view('admin.verifications.index')->with('verifications',$verifications);
	} 

	public function getVerificationJSON()
	{
		$value= $_GET['value'];
		$lists = DB::table('identifications');
		if($value==''){
			$lists = $lists;
		}elseif($value==0){
			$lists = $lists->where('approved',$value);
		}elseif($value==1){
			$lists = $lists->where('approved',$value);
		}elseif($value==2){
			$lists = $lists->where('approved',$value);
		}elseif($value==3){
			$lists = $lists->where('appeal_requested',1);
		}elseif($value==4){
			$lists = $lists->where('state_id_front_path')->orWhere('state_id_front_path','NULL')
						   ->orWhereNull('state_id_back_path')->orWhere('state_id_back_path','NULL')
						   ->orWhereNull('med_card_front_path')->orWhere('med_card_front_path','NULL')
						   ->orWhereNull('med_card_back_path')->orWhere('med_card_back_path','NULL');
		}
				$lists = $lists->get();
		$result = [];
		$row = [];
		$i=0;
		if(count($lists)!=0)
		{
			foreach($lists as $list){
				$result[$i]['id']=$list->user_id;
				$result[$i]['name']='<a href="#" class="verification-modal" rel='.$list->id.'>'.ucfirst(User::getUserDetail($list->user_id,'first_name')).' '.User::getUserDetail($list->user_id,'last_name').'</a>';
				$result[$i]['email']= User::getUserDetail($list->user_id,'email');
				$result[$i]['state']=$list->state;
				$result[$i]['phone']= User::getUserDetail($list->user_id,'phone_no');
				$result[$i]['status']= Identification::getVerificationStatus($list->id);
				$i++;
			}
			return Response::json(array('success'=>true,'result'=>$result,'count'=>count($lists)));	
		}
			return Response::json(['count'=>count($lists)]);
	}


	public function getVerificationDetailsJSON(Request $req)
	{
		$vid = $req->vid;
		$verification = Identification::where('id',$vid)->first();
		if(count($verification)==1){
		$userName = User::getUserDetail($verification->user_id,'first_name').' '.User::getUserDetail($verification->user_id,'last_name');
		$html = '<div class="">
						<table class="table">
						<tbody>
						<tr>
						<td>UPLOAD LICENSE / STATE ID-FRONT</td>
						<td><a href="#" data-target="#lightbox" data-toggle="modal"><img src="'.Identification::getVerificationImage($verification->user_id,'state_id_front_path').'" alt="Medical Card" height="100" width="200"></a></td>
						</tr>
						<tr>
						<td>UPLOAD LICENSE / STATE ID-BACK</td>
						<td><a href="#" data-target="#lightbox" data-toggle="modal"><img src="'.Identification::getVerificationImage($verification->user_id,'state_id_back_path').'" alt="Medical Card" height="100" width="200"></a></td>
						</tr>
						<tr>
						<td>UPLOAD MEDICAL CARD-FRONT</td>
						<td><a href="#" data-target="#lightbox" data-toggle="modal"><img src="'.Identification::getVerificationImage($verification->user_id,'med_card_front_path').'" alt="Medical Card" height="100" width="200"></a></td>
						</tr>
						<tr>
						<td>UPLOAD MEDICAL CARD-BACK</td>
						<td><a href="#" data-target="#lightbox" data-toggle="modal"><img src="'.Identification::getVerificationImage($verification->user_id,'med_card_back_path').'" alt="Medical Card" height="100" width="200"></a></td>
						</tr>
						</tbody>
						</table>
						<hr/>
						<div class="container">
						<form class="col-sm-6" id="updateStatusForm">
						<div class="form-group col-sm-12">
						<label for="comment">Notes:</label>
						<textarea class="form-control" rows="5" id="updateNotes"></textarea><br>
						<input type="checkbox" checked name="vehicle" value="email" id="sendEmail">    Email Status to User
						<input type="hidden" name="rowId" id="rowId" rel="'.$vid.'">
						</div>
						<div class="form-group col-sm-12" id="submitButtons">
						<button type="button" class="btn btn-danger btn-md updateStatusButton" rel="reject">Reject</button>
						<button type="button" class="btn btn-success btn-md updateStatusButton" rel="approve">Approve</button>    
						<button type="button" class="btn btn btn-md updateStatusButton" rel="pending">Pending</button>
						<span class="LoadingEmail" style="display:none"><img src="/img/loading.gif" alt="Loading image" ></span>
						</div>
						</form>
						</div>'
						;
		return Response::json(['status'=>'success','result'=>$html,'userName'=>$userName,'stateName'=>$verification->state]);
		}
		return Response::json(['stauts'=>'failure']);
	}

	public function gupdateStatusJSON(Request $req)
	{
		$req->all();
		$status = Identification::find($req->id);
		if($req->subType=='reject'){
			$status->approved=2;
		}elseif($req->subType=='approve'){
			$status->approved = 1;
			$status->appeal_requested = NULL;
			$status->approved_on = date('Y-m-d');
		}else{
			$status->appeal_requested=1;
			$status->approved_on = 'NULL';
			$status->approved=0;
		}
		$status->notes= $req->notes;
		$status->save();
		if($status->save()){
			if($req->email=='yes'){
				Mail::to(User::getUserDetail($status->user_id,'email'))->send(new EmailStatusToUser);
			}
			return Response::json(['status'=>'success','msg'=>'Status changed successfully.']);
		}else{
			return Response::json(['status'=>'failure']);
		}		
	}



}