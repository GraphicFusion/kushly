<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDriver;
use App\DriverCurrentLocations;
use Sentinel;
use DB;

class ReportsController extends Controller
{
    public function index() 
    {
    	return view('admin/reports/index');
    }

    public function allocatedDrivers()
    {
        return view('admin.reports.allocated-drivers');
    }
}
