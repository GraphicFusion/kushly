<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Session;
use App\User;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.driver.index');
    }

    /**
     * Get all pages for datatable response in json
     */
    public function getDriversJSON()
    {
        $role = Sentinel::findRoleBySlug('driver');
        $users = $role->users()->withTrashed()->with('roles')->get();
        return response()->json(['data' => $users], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.driver.create-driver');
    }

    /**
     * Save record to db
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            're_password' => 'required|min:6|same:password',
            'phone_no' => 'required',
            'birthday' => 'required|date',
            'roles'=> 'required|in:1,2,4,5'
        ]);

        if($user = Sentinel::registerAndActivate($request->all())) {
            // Assign default role (Customer) to user
            $role = Sentinel::findRoleById($request->roles);
            $role->users()->attach($user);
            Session::flash('success', 'Driver account sucessfully created.');
            return redirect('super/drivers');
        } else {
            Session::flash('danger', 'Driver Account Creation Failed, Please try again!!!');
            return redirect('super/drivers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Sentinel::findById($id);
        $roles = Sentinel::getRoleRepository()->get();
        return view('admin.driver.edit-driver')->with(['user'=>$user, 'roles'=>$roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email|unique:users,id,'.$id,
            'phone_no' => 'required',
            'birthday' => 'required|date',
            'roles'=> 'required' 
        ]);

        $user = Sentinel::findById($id);
        $updates = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone_no' => $request->phone_no,
            'birthday' => $request->birthday
        ];
        $user = Sentinel::update($user, $updates);

        // Detach previous roles
        foreach ($user->roles as $assignedRole) {
            $aRole = Sentinel::findRoleById($assignedRole->id);
            $aRole->users()->detach($user);
        }

        // Attach updated roles
        foreach ($request->roles as $role) {
            $rol = Sentinel::findRoleById($role);
            $rol->users()->attach($user);
        }
        Session::flash('success','Driver details updated.');
        return redirect('/super/drivers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Delete User by id
     */
    public function deleteDriver(Request $request)
    {
        $user = Sentinel::findById($request->id);
        $user->delete();
        return response()->json(['message' => 'success', 'id' => $user->id], 200);
    }

    /**
     * Restore User
     */
    public function restoreDriver(Request $request)
    {
        $user = User::withTrashed()->find($request->id);
        $user->restore();
        return response()->json(['message' => 'success', 'id' => $user->id], 200);
    }

}
