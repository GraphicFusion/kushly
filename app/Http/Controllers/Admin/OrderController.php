<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use DB;

class OrderController extends Controller
{
    public function index()
    {
    	return view('admin/orders/index');
    }

    public function show($id)
    {
    	$order = Order::where('id', $id)->first();
    	return view('admin/orders/detail', compact('order'));
    }

    /**
     * Get all pages for datatable response in json
     */
    public function getOrdersJSON()
    {
        $orders = Order::select(
            'orders.id', 
            DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%m/%d/%Y") as order_date'),
            DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%h:%i %p") as order_time'),
            'orders.order_value',
            DB::raw("
                    CASE orders.status
                        WHEN '1' THEN 'Order Placed'
                        WHEN '2' THEN 'Allocated'
                        WHEN '3' THEN 'Rejected'
                        WHEN '4' THEN 'Failure'
                        WHEN '5' THEN 'Accepted'
                        WHEN '6' THEN 'Delivered'
                        WHEN '7' THEN 'Accepted by dispensary'
                        WHEN '8' THEN 'Pickup at dispensary'
                        ELSE 'NO'
                        END as order_status
                "),
            DB::raw("CONCAT_WS(' ',users.first_name, users.last_name) AS customer_name"), 
            'groups.name as group_name'
        )
        ->join('users', 'users.id','=','orders.user_id')
        ->join('order_products', 'order_products.order_id','=','orders.id')
        ->join('products', 'products.id','=','order_products.product_id')
        ->join('groups','groups.id','=','products.group_id')
        ->where('orders.active', 1)
        ->orderBy('orders.id','desc')
        ->get();

        return response()->json(['data' => $orders], 200);
    }

    public function getCustomerName(Request $request) 
    {
    	$customer = User::select('first_name', 'last_name')->where('id', $request->user_id)->get()->first();
    	$name = $customer->first_name.' '.$customer->last_name;
    	return response()->json(['name' => $name], 200);
    }
}
