<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Session;
use App\User;

class CustomerController extends Controller
{
    public function index()
    {
    	return view('admin.customers.index');
    }

    /**
     * Get all pages for datatable response in json
     */
    public function getCustomersJSON()
    {
        $role = Sentinel::findRoleBySlug('customer');
        $users = $role->users()->withTrashed()->with('roles')->get();
        return response()->json(['data' => $users], 200);
    }
        /**
     * Create User
     */
    public function createCustomer()
    {
        return view('admin.customers.create-customer');
    }

    /**
     * Create User
     */
    public function storeCustomer(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            're_password' => 'required|min:6|same:password',
            'phone_no' => 'required',
            'birthday' => 'required|date',
            'roles'=> 'required|in:1,2,4,5'
        ]);

        if($user = Sentinel::registerAndActivate($request->all())) {
            // Assign default role (Customer) to user
            $role = Sentinel::findRoleById($request->roles);
            $role->users()->attach($user);
            Session::flash('success', 'Customer account sucessfully created.');
            return redirect('/super/customers');
        } else {
            Session::flash('danger', 'Customer Account Creation Failed, Please try again!!!');
            return redirect('/super/customers');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Sentinel::findById($id);
        $roles = Sentinel::getRoleRepository()->get();
        return view('admin.customers.edit-customer')->with(['user'=>$user, 'roles'=>$roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email|unique:users,id,'.$id,
            'phone_no' => 'required',
            'birthday' => 'required|date',
            'roles'=> 'required' 
        ]);

        $user = Sentinel::findById($id);
        $updates = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone_no' => $request->phone_no,
            'birthday' => $request->birthday
        ];
        $user = Sentinel::update($user, $updates);

        // Detach previous roles
        foreach ($user->roles as $assignedRole) {
            $aRole = Sentinel::findRoleById($assignedRole->id);
            $aRole->users()->detach($user);
        }

        // Attach updated roles
        foreach ($request->roles as $role) {
            $rol = Sentinel::findRoleById($role);
            $rol->users()->attach($user);
        }
        Session::flash('success','Customer details updated.');
        return redirect('/super/customers');
    }

    /**
     * Delete User by id
     */
    public function deleteCustomer(Request $request)
    {
        $user = Sentinel::findById($request->id);
        $user->delete();
        return response()->json(['message' => 'success', 'id' => $user->id], 200);
    }

    /**
     * Restore User
     */
    public function restoreCustomer(Request $request)
    {
        $user = User::withTrashed()->find($request->id);
        $user->restore();
        return response()->json(['message' => 'success', 'id' => $user->id], 200);
    }
}
