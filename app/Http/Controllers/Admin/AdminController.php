<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDriver;
use DB;

class AdminController extends Controller
{
   public function index()
    {
    	return view('admin.index');
    }

     /**
     * Get all pages for datatable response in json
     */
    public function getOrdersJSON()
    {
        $orders = Order::select(
            'orders.id', 
            DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%m/%d/%Y") as order_date'),
            DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%h:%i %p") as order_time'),
            'orders.order_value',
            DB::raw("
                    CASE orders.status
                        WHEN '1' THEN 'Order Placed'
                        WHEN '2' THEN 'Allocated'
                        WHEN '3' THEN 'Rejected'
                        WHEN '4' THEN 'Failure'
                        WHEN '5' THEN 'Accepted'
                        WHEN '6' THEN 'Delivered'
                        WHEN '7' THEN 'Accepted by dispensary'
                        WHEN '8' THEN 'Pickup at dispensary'
                        ELSE 'NO'
                        END as order_status
                "),
            DB::raw("CONCAT_WS(' ',users.first_name, users.last_name) AS customer_name"), 
            'groups.name as group_name'
        )
        ->join('users', 'users.id','=','orders.user_id')
        ->join('order_products', 'order_products.order_id','=','orders.id')
        ->join('products', 'products.id','=','order_products.product_id')
        ->join('groups','groups.id','=','products.group_id')
        ->where('orders.active', 1)
        ->whereRaw('Date(orders.timestamp_order_made) = CURDATE()')
        ->orderBy('orders.id','desc')
        ->get();

        return response()->json(['data' => $orders], 200);
    }

    public function allocatedDrivers()
    {
    	$OrderDriver = OrderDriver::select(
    	 'orders.status','order_driver.*',
		    DB::raw('DATE_FORMAT(order_driver.created_at, "%m/%d/%Y") as order_date'),
		    DB::raw("CONCAT_WS(' ',users.first_name, users.last_name) AS driver_name")
            
    	)
    	->join('users', 'users.id','=','order_driver.driver_id')
        ->join('orders', 'orders.id','=','order_driver.order_id')
        ->whereIn('order_driver.order_driver_status', ['failure', 'rejected'])
    	->whereRaw('Date(order_driver.created_at) = CURDATE()')
        ->whereIn('orders.status', [3, 4])
    	->orderBy('order_driver.id','desc')
        ->get();

        return response()->json(['data' => $OrderDriver], 200);
    }

    public function chartdata()
    {
            if($_POST["time"] == "7 days")
                    {
                    $Failure = Order::select(
                    DB::raw('COUNT(id) AS y'),
                    DB::raw('DATE_FORMAT(timestamp_order_made, "%m/%d/%Y") as x'))
                    ->where('active', 1)
                    ->where('status', 4)
                    ->whereRaw('Date(timestamp_order_made) >= (CURDATE() - INTERVAL 7 DAY)')
                  //  ->orderBy('id','desc')
        	        ->groupBy('x')
        	        ->get();

        	        $Delivered = Order::select(
                    DB::raw('COUNT(id) AS y'),
                    DB::raw('DATE_FORMAT(timestamp_order_made, "%m/%d/%Y") as x'))
                    ->where('active', 1)
                    ->where('status', 6)
                    ->whereRaw('Date(timestamp_order_made) >= (CURDATE() - INTERVAL 7 DAY)')
                   // ->orderBy('id','desc')
        	        ->groupBy('x')
        	        ->get();

        	        $Rejected = Order::select(
                    DB::raw('COUNT(id) AS y'),
                    DB::raw('DATE_FORMAT(timestamp_order_made, "%m/%d/%Y") as x'))
                    ->where('active', 1)
                    ->where('status', 3)
                    ->whereRaw('Date(timestamp_order_made) >= (CURDATE() - INTERVAL 7 DAY)')
                   // ->orderBy('id','desc')

                    
        	        ->groupBy('x')
        	        ->get();

        	        $OrderPlaced = Order::select(
                    DB::raw('COUNT(id) AS y'),
                    DB::raw('DATE_FORMAT(timestamp_order_made, "%m/%d/%Y") as x'))
                    ->where('active', 1)
                    ->where('status', 1)
                    ->whereRaw('Date(timestamp_order_made) >= (CURDATE() - INTERVAL 7 DAY)')
        	      //  ->orderBy('id','desc')
        	        ->groupBy('x')
        	        ->get();
                    return response()->json(array('Delivered' => $Delivered,'Failure' => $Failure, 'Rejected' => $Rejected, 'OrderPlaced' => $OrderPlaced));
                   }
                   if($_POST["selectvl"]!="" && $_POST["min"]!="" && $_POST["max"]!="")
                   {
                    $Failure = Order::select(
                    DB::raw('COUNT(orders.id) AS y'),
                    DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%m/%d/%Y") as x'))
                    ->join('order_products', 'order_products.order_id','=','orders.id')
                    ->join('products', 'products.id','=','order_products.product_id')
                    ->join('groups', 'groups.id','=','products.group_id')
                    ->where('orders.active', 1)
                    ->where('orders.status', 4)
                    ->where('groups.name',$_POST["selectvl"])
                    ->whereBetween('orders.timestamp_order_made',[$_POST["min"],$_POST["max"]])
                  //  ->orderBy('orders.id','desc')
                    ->groupBy('x')
                    ->get();

                    $Delivered = Order::select(
                    DB::raw('COUNT(orders.id) AS y'),
                    DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%m/%d/%Y") as x'))
                    ->join('order_products', 'order_products.order_id','=','orders.id')
                    ->join('products', 'products.id','=','order_products.product_id')
                    ->join('groups', 'groups.id','=','products.group_id')
                    ->where('orders.active', 1)
                    ->where('orders.status', 6)
                    ->where('groups.name',$_POST["selectvl"])
                    ->whereBetween('orders.timestamp_order_made',[$_POST["min"],$_POST["max"]])
                   // ->orderBy('orders.id','desc')
                    ->groupBy('x')
                    ->get();

                    $Rejected = Order::select(
                    DB::raw('COUNT(orders.id) AS y'),
                    DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%m/%d/%Y") as x'))
                    ->join('order_products', 'order_products.order_id','=','orders.id')
                    ->join('products', 'products.id','=','order_products.product_id')
                    ->join('groups', 'groups.id','=','products.group_id')
                    ->where('orders.active', 1)
                    ->where('orders.status', 3)
                    ->where('groups.name',$_POST["selectvl"])
                    ->whereBetween('orders.timestamp_order_made',[$_POST["min"],$_POST["max"]])
                  //  ->orderBy('orders.id','desc')
                    ->groupBy('x')
                    ->get();

                    $OrderPlaced = Order::select(
                    DB::raw('COUNT(orders.id) AS y'),
                    DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%m/%d/%Y") as x'))
                    ->join('order_products', 'order_products.order_id','=','orders.id')
                    ->join('products', 'products.id','=','order_products.product_id')
                    ->join('groups', 'groups.id','=','products.group_id')
                    ->where('orders.active', 1)
                    ->where('orders.status', 1)
                    ->where('groups.name',$_POST["selectvl"])
                    ->whereBetween('orders.timestamp_order_made',[$_POST["min"],$_POST["max"]])
                  //  ->orderBy('orders.id','desc')
                    ->groupBy('x')
                    ->get();
                    return response()->json(array('Delivered' => $Delivered,'Failure' => $Failure, 'Rejected' => $Rejected, 'OrderPlaced' => $OrderPlaced));
                   }
        
               
    }

    public function piechartdata()
    {
             if($_POST["data"] == "")
             {
             $orders = Order::select(
             DB::raw("
                     COUNT(CASE status WHEN '1' THEN 'OrderPlaced' ELSE NULL END) as OrderPlaced,
                     COUNT(CASE status WHEN '3' THEN 'Rejected' ELSE NULL END) as Rejected,
                     COUNT(CASE status WHEN '4' THEN 'Failure' ELSE NULL END) as Failure,
                     COUNT(CASE status WHEN '6' THEN 'Delivered' ELSE NULL END) as Delivered")
             )
            
             ->where('orders.active', 1)
             ->orderBy('id','desc')
             ->get();

             return response()->json(['data' => $orders], 200);
         }
         
         elseif($_POST["data"]=="val")
         {
            $orders = Order::select(
             DB::raw("
                     COUNT(CASE status WHEN '1' THEN 'OrderPlaced' ELSE NULL END) as OrderPlaced,
                     COUNT(CASE status WHEN '3' THEN 'Rejected' ELSE NULL END) as Rejected,
                     COUNT(CASE status WHEN '4' THEN 'Failure' ELSE NULL END) as Failure,
                     COUNT(CASE status WHEN '6' THEN 'Delivered' ELSE NULL END) as Delivered")
             )
            
             ->where('orders.active', 1)
             ->orderBy('id','desc')
             ->get();

             return response()->json(['data' => $orders], 200);
         }
         else
         {
            $orders = Order::select(
             DB::raw("
                     COUNT(CASE orders.status WHEN '1' THEN 'OrderPlaced' ELSE NULL END) as OrderPlaced,
                     COUNT(CASE orders.status WHEN '3' THEN 'Rejected' ELSE NULL END) as Rejected,
                     COUNT(CASE orders.status WHEN '4' THEN 'Failure' ELSE NULL END) as Failure,
                     COUNT(CASE orders.status WHEN '6' THEN 'Delivered' ELSE NULL END) as Delivered")
             )
            ->join('order_products', 'order_products.order_id','=','orders.id')
            ->join('products', 'products.id','=','order_products.product_id')
            ->join('groups', 'groups.id','=','products.group_id')
            ->where('orders.active', 1)
            ->where('groups.name',$_POST["data"])
             
             
             ->get();

             return response()->json(['data' => $orders], 200);
         }

    }

   
    public function allocatedriver()
    {
        $OrderDriver = OrderDriver::select('*')
        //->whereNotIn('order_driver_status' , ['allocated'])
        ->where('driver_id', $_POST["data"])
        ->get();
        return response()->json(['data' => $OrderDriver], 200);
    }

    public function addriver()
    {
         DB::table('orders')
            ->where('id', $_POST["oderidd"])
            ->update([ 'status' => 2]);

         DB::table("order_driver")->insert(
                  ['driver_id' => $_POST["driverid"], 'order_id' => $_POST["oderidd"], 'order_driver_status' => "allocated", "created_at" => date('Y-m-d H:i:s')]
);
    }
   
    
}