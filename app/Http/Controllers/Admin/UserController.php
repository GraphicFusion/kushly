<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\User;
use Session;

class UserController extends Controller
{
	/**
	 * Get all users
	 */
    public function getUsers()
    {
    	$users = Sentinel::getUserRepository()->with('roles')->get();
    	return view('admin.users.index', compact('users'));
    }

    /**
     * Create User
     */
    public function createUser()
    {
        return view('admin.users.create');
    }

    /**
     * Create User
     */
    public function storeUser(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            're_password' => 'required|min:6|same:password',
            'phone_no' => 'required',
            'birthday' => 'required|date',
            'roles'=> 'required|in:1,2,4,5'
        ]);

        if($user = Sentinel::registerAndActivate($request->all())) {
            // Assign default role (Customer) to user
            $role = Sentinel::findRoleById($request->roles);
            $role->users()->attach($user);
            Session::flash('success', 'User account sucessfully created with the '.$role->name.' role.');
            return back();
        } else {
            Session::flash('danger', 'User Account Creation Failed, Please try again!!!');
            return back();
        }
    }


    /**
	 * Get user by id
	 */
    public function getUserDetails($id)
    {
    	$user = Sentinel::findById($id);
    	return view('admin.users.view')->with(['user'=>$user]);
    }

    /**
	 * edit specific user by id
	 */
    public function editUserDetails($id)
    {
    	$user = Sentinel::findById($id);
    	$roles = Sentinel::getRoleRepository()->get();
    	return view('admin.users.edit')->with(['user'=>$user, 'roles'=>$roles]);
    }

    /**
	 * Update user details by user id
	 */
    public function updateUserDetails(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email|unique:users,id,'.$id,
            'phone_no' => 'required',
            'birthday' => 'required|date',
            'roles'=> 'required' 
        ]);
        $user = Sentinel::findById($id);
    	$updates = [
    		'first_name' => $request->first_name,
    		'last_name' => $request->last_name,
    		'email' => $request->email,
    		'phone_no' => $request->phone_no,
    		'birthday' => $request->birthday
    	];
    	$user = Sentinel::update($user, $updates);

    	// Detach previous roles
		foreach ($user->roles as $assignedRole) {
			$aRole = Sentinel::findRoleById($assignedRole->id);
			$aRole->users()->detach($user);
		}

		// Attach updated roles
	   	foreach ($request->roles as $role) {
    		$rol = Sentinel::findRoleById($role);
            $rol->users()->attach($user);
    	}
		Session::flash('success','User details updated.');
    	return redirect('/super/users');
    }

    /**
     * Delete User by id
     */
    public function deleteUser(Request $request)
    {
        $user = Sentinel::findById($request->id);
        $user->delete();
        return response()->json(['message' => 'success', 'id' => $user->id], 200);
    }

    /**
     * Restore User
     */
    public function restoreUser(Request $request)
    {
        $user = User::withTrashed()->find($request->id);
        $user->restore();
        return response()->json(['message' => 'success', 'id' => $user->id], 200);
    }




    public function getRoles()
    {
    	$roles = Sentinel::getRoleRepository()->paginate(10);
    	return view('admin.roles.index', compact('roles'));
    }

    public function editRoles($id)
    {
        $role = Sentinel::findRoleById($id);
        return view('admin.roles.edit', compact('role'));
    }

    public function updateRole(Request $request)
    {
        $role = Sentinel::findRoleById($request->rid);
        foreach ($request->perm as $key => $value) {
            $val = ($value==1) ? true : false;
            $role->updatePermission($key, $val, true);
        }
        $role->save();
        return redirect('/super/roles/edit/'.$request->rid);
    }

    /**
     * Get all pages for datatable response in json
     */
    public function getUsersJSON()
    {
        $users = Sentinel::getUserRepository()->with('roles')->get();
        return response()->json(['data' => $users], 200);
    }







    public function getPermissions()
    {
    	return view('admin.permissions');
    }
}
