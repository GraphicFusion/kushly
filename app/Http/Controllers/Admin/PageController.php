<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pages;
use Session;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Pages::get();
        return view('admin.pages.index', compact('pages'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.addnew');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'page_title' => 'required',
            'page_content' => 'required'
        ]);
        $page = new Pages();
        $page->page_title = $request->page_title;
        $page->slug = $request->page_title;
        $page->page_content = $request->page_content;
        $page->custom_css = $request->custom_css;
        $page->save();
        Session::flash('success', 'New page created.');
        return redirect('/super/manage-pages');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Pages::find($id);
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'page_title' => 'required',
            'page_content' => 'required'
        ]);
        
        $page = Pages::find($id);
        $page->page_title = $request->page_title;
        $page->slug = $request->page_title;
        $page->page_content = $request->page_content;
        $page->custom_css = $request->custom_css;
        $page->save();
        Session::flash('success', 'Page updated successfully.');
        return redirect('/super/manage-pages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $page = Pages::findBySlug($request->slug);
        $page->delete();
        return response()->json(['message' => 'success', 'id' => $page->id], 200);
    }


    /**
     * Get all pages for datatable response in json
     */
    public function getPagesJSON()
    {
        $pages = Pages::select('id','page_title','slug')->get();
        return response()->json(['data' => $pages], 200);
    }

}
