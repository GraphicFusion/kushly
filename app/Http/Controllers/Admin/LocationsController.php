<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Locations;
use App\User;
use Sentinel;
use Session;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Locations::get();
        return view('admin.location.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.location.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'street_address' => 'required|min:5',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'zip_code' => 'required|numeric'
        ]);
        
        $loc = new Locations();
        $loc->street_address = $request->street_address;
        $loc->city = $request->city;
        $loc->state = $request->state;
        $loc->zip_code = $request->zip_code;
        $loc->country = $request->country;
        $loc->type = $request->type;
        $loc->lat = number_format(doubleval($request->lat), 7);
        $loc->lng = number_format(doubleval($request->lng), 7);
        $loc->save();
        $loc->user()->attach(Sentinel::getUser()->id);
        Session::flash('success','New location created successfully.');
        return redirect(request()->headers->get('referer'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Locations::where('id', $id)->first();
        return view('admin/location/edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'street_address' => 'required|min:5',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'zip_code' => 'required|numeric'
        ]);

        $loc = Locations::find($id);
        $loc->street_address = $request->street_address;
        $loc->city = $request->city;
        $loc->state = $request->state;
        $loc->zip_code = $request->zip_code;
        $loc->type = $request->type;
        $loc->country = $request->country;

        $loc->lat = number_format(doubleval($request->lat), 7);
        $loc->lng = number_format(doubleval($request->lng), 7);
        $loc->save();
        Session::flash('success', 'Location updated successfully.');
        return redirect(request()->headers->get('referer'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}