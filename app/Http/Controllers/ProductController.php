<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function singleProduct($id)
    {
        // Logic to get specific product details then redirect to single product view.
        $product = Product::where('id',$id)->get()->first();
        return view('product.single')->with(['product' => $product]);
    }
}
