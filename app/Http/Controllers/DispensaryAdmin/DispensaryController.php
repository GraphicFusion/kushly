<?php

namespace App\Http\Controllers\DispensaryAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Groups;
use App\Locations;
use Session;
use Sentinel;

class DispensaryController extends Controller
{
    public function index()
    {
		$groupID = Session::get('activeGroup');
    	$products = Product::withTrashed()->where('group_id', $groupID)->orderBy('id', 'desc')->get();
    	return view('dispensary/index', compact('products'));
    }

    public function edit($id)
    {
    	$group = Groups::findById($id);
        return view('dispensary/profile', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'location' => 'required'
        ]);

        $group = Groups::findById($request->id);
        $group->name = $request->name;
        $group->type = $request->type;

        if( $request->hasFile('logo')){ 
            // Unlink previous logo
            unlink(public_path('groups-logo/'.$request->prevLogo));
            $image = $request->file('logo'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $imageName = time().'-'.$fileName;
            $request->logo->move(public_path('groups-logo'), $imageName);
            $group->logo = $imageName;
            $group->save();
        } else {
            $group->logo = $request->prevLogo;
            $group->save();
        }

        /**
         * Detach and attach locations again under belongtomany relation
         */
        $locs = $group->location()->get();
        foreach ($locs as $key => $loc) {
            $group->location()->detach($loc->id);
        }
        // foreach ($request->location as $key => $loc) {
        //     $group->location()->attach($loc);
        // }

        $group->location()->attach($request->location);
        
        \Session::flash('success', 'Dispensary details updated successfully.');
        return redirect('dispensary/account/'.$group->id);
    }

    public function addLocation()
    {
    	return view('dispensary/add-location');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLocation(Request $request)
    {
        $this->validate($request, [
            'street_address' => 'required|min:5',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'zip_code' => 'required|numeric'
        ]);

    	$loc = new Locations();
    	$loc->street_address = $request->street_address;
    	$loc->city = $request->city;
    	$loc->state = $request->state;
    	$loc->zip_code = $request->zip_code;
        $loc->country = 'USA';
    	$loc->type = $request->type;

        // $completeAddress = $request->street_address.', '.$request->city.', '.$request->state.', '.$request->zip_code;
        // $location = getGeoLocation($completeAddress);

        // $completeAddress = $request->street_address.', '.$request->city.', '.$request->state;
        // $location = getGeoLocationByPin($completeAddress, $request->zip_code);

        // if($location == '0') {
        //     Session::flash('danger', 'Location is not Correct, please fill the correct location.');
        //     return back();
        // } else {
            $loc->lat = number_format(doubleval($request->lat), 7);
            $loc->lng = number_format(doubleval($request->lng), 7);
            $loc->save();

            $loc->user()->attach(Sentinel::getUser()->id);
            $groupID = Session::get('activeGroup');
            $group = Groups::findById($groupID);
            $group->location()->attach($loc->id);
            Session::flash('success', 'Location created successfully.');
            return redirect('/dispensary/account/'.Session::get('activeGroup'));
        // }
    }

    public function getLocations()
    {
        $groupID = Session::get('activeGroup');
        $group = Groups::findById($groupID);
        $locations = $group->location()->get();
        return view('dispensary/locations/list', compact('locations'));
    }

    public function editLocation($id)
    {
        $location = Locations::where('id', $id)->first();
        return view('dispensary/locations/edit-location', compact('location'));
    }
    public function updateLocation(Request $request, $id)
    {
        $this->validate($request, [
            'street_address' => 'required|min:5',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'zip_code' => 'required|numeric'
        ]);

        $loc = Locations::find($id);
        $loc->street_address = $request->street_address;
        $loc->city = $request->city;
        $loc->state = $request->state;
        $loc->zip_code = $request->zip_code;
        $loc->type = $request->type;


        // $completeAddress = $request->street_address.', '.$request->city.', '.$request->state.', '.$request->zip_code;
        // $location = getGeoLocation($completeAddress);

        // $completeAddress = $request->street_address.', '.$request->city.', '.$request->state;
        // $location = getGeoLocationByPin($completeAddress, $request->zip_code);
        
        // if($location == '0') {
        //     Session::flash('danger', 'Location is not Correct, please fill the correct location.');
        //     return back();
        // } else {
            $loc->lat = number_format(doubleval($request->lat), 7);
            $loc->lng = number_format(doubleval($request->lng), 7);
            $loc->save();
            Session::flash('success', 'Location updated successfully.');
            return redirect('/dispensary/manage-locations');
        // }
    }
}