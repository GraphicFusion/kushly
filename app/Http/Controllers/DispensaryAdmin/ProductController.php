<?php

namespace App\Http\Controllers\DispensaryAdmin;
use App\Mail\AddNewProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Price;
use App\ProductImages;
use Session;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Exceptions\Handler;

//use Mail;
//use App\Mail\AddNewProduct;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dispensary/product/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required', 
//            'description' => 'required', 

            'is_marijuana' => 'required', 
            'type' => 'required_if:is_marijuana,1', 
            'strain_type' => 'required_if:is_marijuana,1',

            // 'thc' => 'required_if:is_marijuana,1', 
            // 'cbd' => 'required_if:is_marijuana,1', 
            // 'cbn' => 'required_if:is_marijuana,1', 

            'group_id' => 'required', 
            //'quantity_per_unit.*' => 'required|alpha_num',
            // 'unit_price.*' => 'required|numeric', 
            // 'stock.*' => 'required|numeric'

            //'path' => 'image|mimes:jpeg,png|max:500000|dimensions:min_width=250,max_width=1500'
        ],[
            'product_name.required' => 'Product Name is required.', 
            'description.required' => 'Description is required.', 
            'is_marijuana.required' => 'Please select product is related with marijuana or not.', 
            'type.required_if' => 'Type is required when product related to marijuana.',
            'strain_type.required_if' => 'Strain Type is required when product related to marijuana.',
            'thc.required_if' => 'THC is required when product related to marijuana.',
            'cbd.required_if' => 'CBD is required when product related to marijuana.',
            'cbn.required_if' => 'CBN is required when product related to marijuana.',
            'group_id.required' => 'Please select dispensary.',

            'quantity_per_unit.*required' => 'Each variant size is required.',
            //'quantity_per_unit.*alpha_num' => 'Each variant size may only contain letters and numbers.',
            'unit_price.*required' => 'Each variant price is required.',
            'unit_price.*numeric' => 'Each variant price must be a number.', 
            'stock.*required' => 'Each variant inventory is required.',
            'stock.*.numeric' => 'Each variant inventory must be a number.'
        ]);


        $isMarijuana = $request->is_marijuana;
        if($isMarijuana == 1) {
            // Logical for marijuana product
            // dd($request->all());

            $product = new Product();
            $product->product_name = $request->product_name;
			$product->is_marijuana = 1;
            $product->description = $request->description;
            $product->group_id = $request->group_id;
            $product->active = 1;

            $product->type = $request->type;
            $product->strain_type = $request->strain_type;
            $product->thc = $request->thc;
            $product->cbd = $request->cbd;
            $product->cbn = $request->cbn;

            $product->save();

            // Price
            for ($i=0; $i < count($request->quantity_per_unit); $i++) { 
                $product_price = new Price();
                $product_price->product_id = $product->id;
                $product_price->quantity_per_unit = $request->quantity_per_unit[$i];
                $product_price->unit_price = $request->unit_price[$i];
                $product_price->stock = $request->stock[$i];
                $product_price->save();
            }

            $sortorder = '';
            $picture = '';

            if ($request->hasFile('path')) {
                $files = $request->file('path');
                foreach($files as $key => $file){
                    $sortorder = $key;
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $picture = date('His').'-'.$filename;
                    $file->move(public_path('product-images'), $picture);
                    $productImage = new ProductImages();
                    $productImage->product_id = $product->id;
                    $productImage->path = $picture;
                    $productImage->sortorder = $sortorder;
                    if($key == 0) {
                        $productImage->featured = 1;
                    }
                    $productImage->save();
                }
            }
            
            Session::flash('success', 'New product created successfully.');
            return redirect('dispensary/home');

        } elseif ($isMarijuana == 0) {
            // Logical for non marijuana product

            $product = new Product();
            $product->product_name = $request->product_name;
            $product->description = $request->description;
            $product->group_id = $request->group_id;
            $product->active = 1;

            $product->save();

            // Price
            for ($i=0; $i < count($request->quantity_per_unit); $i++) { 
                $product_price = new Price();
                $product_price->product_id = $product->id;
                $product_price->quantity_per_unit = $request->quantity_per_unit[$i];
                $product_price->unit_price = $request->unit_price[$i];
                $product_price->stock = $request->stock[$i];
                $product_price->save();
            }

            $sortorder = '';
            $picture = '';
            if ($request->hasFile('path')) {
                $files = $request->file('path');
                foreach($files as $key => $file){
                    $sortorder = $key;
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $picture = date('His').'-'.$filename;
                    $file->move(public_path('product-images'), $picture);
                    $productImage = new ProductImages();
                    $productImage->product_id = $product->id;
                    $productImage->path = $picture;
                    $productImage->sortorder = $sortorder;
                    if($key == 0) {
                        $productImage->featured = 1;
                    }
                    $productImage->save();
                }
            }
            Session::flash('success', 'New product created successfully.');

            // Send mail to admin for new product addition
            // \Mail::to(\Config::get('mail.admin_email'))->send(new AddNewProduct($product));

            // Product creation confirmation to dispensary
            // Mail::to()->send(new App\Mail\NewProductConfirmation($product));
            return redirect('dispensary/home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $product = Product::withTrashed()->find($id);
        return view('dispensary/product/edit')->with(['product' => json_decode($product)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => 'required', 
            'description' => 'required', 

            'is_marijuana' => 'required', 
            'type' => 'required_if:is_marijuana,1', 
            'strain_type' => 'required_if:is_marijuana,1',

            // 'thc' => 'required_if:is_marijuana,1', 
            // 'cbd' => 'required_if:is_marijuana,1', 
            // 'cbn' => 'required_if:is_marijuana,1', 

            'group_id' => 'required', 
            //'quantity_per_unit.*' => 'required|alpha_num',
            'unit_price.*' => 'required|numeric', 
            'stock.*' => 'required|numeric'

            //'path' => 'image|mimes:jpeg,png|max:500000|dimensions:min_width=250,max_width=1500'
        ],[
            'product_name.required' => 'Product Name is required.', 
            'description.required' => 'Description is required.', 
            'is_marijuana.required' => 'Please select product is related with marijuana or not.', 
            'type.required_if' => 'Type is required when product related to marijuana.',
            'strain_type.required_if' => 'Strain Type is required when product related to marijuana.',
            'thc.required_if' => 'THC is required when product related to marijuana.',
            'cbd.required_if' => 'CBD is required when product related to marijuana.',
            'cbn.required_if' => 'CBN is required when product related to marijuana.',
            'group_id.required' => 'Please select dispensary.',

            'quantity_per_unit.*required' => 'Each variant size is required.',
           // 'quantity_per_unit.*alpha_num' => 'Each variant size may only contain letters and numbers.',
            'unit_price.*required' => 'Each variant price is required.',
            'unit_price.*numeric' => 'Each variant price must be a number.', 
            'stock.*required' => 'Each variant inventory is required.',
            'stock.*.numeric' => 'Each variant inventory must be a number.'
        ]);

        $isMarijuana = $request->is_marijuana;
        if($isMarijuana == 1) {

            // dd($request->all());
            $product = Product::find($id);
            $product->product_name = $request->product_name;
            $product->description = $request->description;
            $product->group_id = $request->group_id;

            $product->type = $request->type;
            $product->strain_type = $request->strain_type;
            $product->thc = $request->thc;
            $product->cbd = $request->cbd;
            $product->cbn = $request->cbn;

            $product->save();

            // Price
            for ($i=0; $i < count($request->quantity_per_unit); $i++) { 
                if(!empty($request->variant_id[$i])) {
                    $product_price = Price::find($request->variant_id[$i]);
                    $product_price->quantity_per_unit = $request->quantity_per_unit[$i];
                    $product_price->unit_price = $request->unit_price[$i];
                    $product_price->stock = $request->stock[$i];
                    $product_price->save();
                } else {
                    $product_price = new Price();
                    $product_price->product_id = $product->id;
                    $product_price->quantity_per_unit = $request->quantity_per_unit[$i];
                    $product_price->unit_price = $request->unit_price[$i];
                    $product_price->stock = $request->stock[$i];
                    $product_price->save();
                }
            }

            // dd($request->image_id);

            // Product Images
            $sortorder = '';
            $picture = '';
            if(count($request->image_id) > 0) :
                foreach ($request->image_id as $key => $pImgID) {
                    $sortorder = $key;

                    // Update sortorder on previous images
                    if(!empty($pImgID)) {
                        $productImage = ProductImages::find($pImgID);
                        $productImage->sortorder = $sortorder;
                        $productImage->save();
                    }

                    // Add image with sortorder
                    if(empty($pImgID)) {
                        if ($request->hasFile('path')) {
                            $files = $request->file('path');
                            foreach($files as $file){
                                $filename = $file->getClientOriginalName();
                                $extension = $file->getClientOriginalExtension();
                                $picture = date('His').'-'.$filename;
                                // $picture = date('His').'-'.$id.'.'.$extension;
                                $file->move(public_path('product-images'), $picture);
                                $productImage = new ProductImages();
                                $productImage->product_id = $product->id;
                                $productImage->path = $picture;
                                $productImage->sortorder = $sortorder;
                                $productImage->save();
                            }
                        }
                    }
                }
            endif;

            Session::flash('success', 'Product updation complete.');
            return redirect('dispensary/home');

        } elseif ($isMarijuana == 0) {

            // dd($request->all());
            $product = Product::find($id);
            $product->product_name = $request->product_name;
            $product->description = $request->description;
            $product->group_id = $request->group_id;
            $product->save();

            // Price
            for ($i=0; $i < count($request->quantity_per_unit); $i++) { 
                if(!empty($request->variant_id[$i])) {
                    $product_price = Price::find($request->variant_id[$i]);
                    $product_price->quantity_per_unit = $request->quantity_per_unit[$i];
                    $product_price->unit_price = $request->unit_price[$i];
                    $product_price->stock = $request->stock[$i];
                    $product_price->save();
                } else {
                    $product_price = new Price();
                    $product_price->product_id = $product->id;
                    $product_price->quantity_per_unit = $request->quantity_per_unit[$i];
                    $product_price->unit_price = $request->unit_price[$i];
                    $product_price->stock = $request->stock[$i];
                    $product_price->save();
                }
            }

            // dd($request->image_id);

            // Product Images
            $sortorder = '';
            $picture = '';
            if(count($request->image_id) > 0) :
                foreach ($request->image_id as $key => $pImgID) {
                    $sortorder = $key;

                    // Update sortorder on previous images
                    if(!empty($pImgID)) {
                        $productImage = ProductImages::find($pImgID);
                        $productImage->sortorder = $sortorder;
                        $productImage->save();
                    }

                    // Add image with sortorder
                    if(empty($pImgID)) {
                        if ($request->hasFile('path')) {
                            $files = $request->file('path');
                            $a = 0;
                            foreach($files as $file){
                                $a++;
                                $filename = $file->getClientOriginalName();
                                $extension = $file->getClientOriginalExtension();
                               
                                
                                
                                $picture = $a.time().'-'.$filename;
                                 
                                $file->move(public_path('product-images'), $picture);
                                $productImage = new ProductImages();
                                $productImage->product_id = $product->id;
                                $productImage->path = $picture;
                                $productImage->sortorder = $sortorder;
                                $productImage->save();
                            }
                           
                        }
                    }
                }
            endif;
            
            Session::flash('success', 'Product updation complete.');
            return redirect('dispensary/home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Deactive given product.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Json Response
     */
    public function deactive(Request $request)
    {
        $product = Product::find($request->pid);
        $product->active = 0;
        $product->save();
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Active given product.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Json Response
     */
    public function active(Request $request)
    {
        $product = Product::find($request->pid);
        $product->active = 1;
        $product->save();
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Soft Delete given product.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Json Response
     */
    public function trash(Request $request)
    {
        $product = Product::find($request->pid);
        $product->delete();
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Restore the given trashed product.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Json Response
     */
    public function restore(Request $request)
    {
        $product = Product::withTrashed()->find($request->pid);
        $product->restore();
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Permanet Delete given product image.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Json Response
     */
    public function imagedelete(Request $request)
    {
        $product = ProductImages::find($request->imageId);
        if(!empty($product->path) && file_exists(public_path('product-images/'.$product->path))) {
            unlink(public_path('product-images/'.$product->path));
            
        }
         $product->delete();
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Product image marked as featured.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Json Response
     */
    public function imageFeatured(Request $request)
    {
        $gallery = new ProductImages();
        $productGallery = $gallery->select('id', 'featured')->where('product_id', $request->id)->get();
        foreach ($productGallery as $key => $pg) {
            if($pg['id'] == $request->imageId) {
                \DB::table('product_images')
                    ->where('id', $pg['id'])
                    ->update(['featured' => 1]);
            } else {
                \DB::table('product_images')
                    ->where('id', $pg['id'])
                    ->update(['featured' => 0]);
            }
        }
        return response()->json(['message' => 'success'], 200);
    }
    
}
