<?php

namespace App\Http\Controllers\DispensaryAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderProduct;
use App\OrderDriver;
use App\Product;
use App\Locations;
use App\Groups;
use DB;
use Session;

class OrderController extends Controller
{
    public function index()
    {
    	return view('dispensary/order/index');
    }

    public function show($id)
    {
        $order = Order::where('id', $id)->get()->first();
        return view('dispensary/order/detail', compact('order'));
    }

    /**
     * Get all pages for datatable response in json
     */
    public function getOrdersJSON()
    {
        $groupID = Session::get('activeGroup');
        $OP = OrderProduct::select('order_id', 'product_id')
            ->groupBy('order_id') 
            ->get();
        $ordersID = [];
        foreach ($OP as $key => $val) {
            $group = Product::select('group_id')->where('id',$val['product_id'])->first();
            if($group['group_id'] == $groupID) {
                $ordersID[] = $val['order_id'];
            }
        }
        // dd($ordersID);

        $orders = Order::select(
            'orders.id', 
            DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%m/%d/%Y") as order_date'),
            DB::raw('DATE_FORMAT(orders.timestamp_order_made, "%h:%i %p") as order_time'),
            'orders.order_value',
            DB::raw("
                    CASE orders.status
                        WHEN '1' THEN 'Order Placed'
                        WHEN '2' THEN 'Allocated'
                        WHEN '3' THEN 'Rejected'
                        WHEN '4' THEN 'Failure'
                        WHEN '5' THEN 'Accepted'
                        WHEN '6' THEN 'Delivered'
                        WHEN '7' THEN 'Accepted by dispensary'
                        WHEN '8' THEN 'Pickup at dispensary'
                        ELSE 'NO'
                        END as order_status
                "),
            DB::raw("CONCAT_WS(' ',users.first_name, users.last_name) AS customer_name"), 
            'groups.name as group_name'
        )
        ->join('users', 'users.id','=','orders.user_id')
        ->join('order_products', 'order_products.order_id','=','orders.id')
        ->join('products', 'products.id','=','order_products.product_id')
        ->join('groups','groups.id','=','products.group_id')
        ->where('orders.active', 1)
        ->whereIn('orders.id', $ordersID)
        ->orderBy('orders.id','desc')
        ->get();

        return response()->json(['data' => $orders], 200);
    }

    /**
     *  Order accept by dispensary. Then we add timestamp in db.
     */
    public function acceptByDispensary(Request $request)
    {
        DB::table('orders')
            ->where('id', $request->orderid)
            ->update([
                'timestamp_order_accepted_by_dispensary' => date('Y-m-d h:i:s'),
                'status' => 7
            ]);

        return response()->json(['message' => "success"]);
    }

    /**
     *  Order picked at dispensary.
     */
    public function pickedAtDispensary(Request $request)
    {
        DB::table('orders')
            ->where('id', $request->orderid)
            ->update([
                'timestamp_order_picked_up_at_dispensary' => date('Y-m-d h:i:s'),
                'status' => 8
            ]);

        return response()->json(['message' => "success"]);
    }

}