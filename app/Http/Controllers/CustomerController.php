<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Locations;
use App\User;
use App\Order;
use Validator;
use Sentinel;
use Session;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class CustomerController extends Controller
{
    /**
     * Redirect to customer profile
     */
    public function index()
    {
        if(Sentinel::check()) {
            return view('customer.profile');
        } else {
            Session::flash('info', 'Please login first to access this page.');
            return redirect('/signin');
        }
    }

    /**
     * Upload user profile image.
     */
    public function uploadUserImage(Request $request) 
    {
        if( $request->hasFile('profile_pic')){ 
            $image = $request->file('profile_pic'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();

            $imageName = time().'-'.$fileName;
            $request->profile_pic->move(public_path('profile-pics'), $imageName);

            $user = Sentinel::findById($request->id);
            $user = Sentinel::update($user, ['profile_pic' => $imageName]);

            Session::flash('success', $user->profile_pic.'&nbsp;Profile image successfully uploaded.');
            return redirect('/customer/profile');
        } else {
            Session::flash('danger','No Image Found.');
            return redirect('/customer/profile');
        }
    }

    /**
     * Delete user profile image.
     */
    public function delUserImage()
    {
        $user = Sentinel::findById(getUserId());
        if(empty($user->profile_pic)) {
            Session::flash('danger','User image found or Still not uploaded.');
            return redirect('/customer/profile');
        } else {
            unlink(public_path('profile-pics/'.$user->profile_pic));
            $user = Sentinel::update($user, ['profile_pic' => '']);
            Session::flash('success', 'Image successfully deleted.');
            return redirect('/customer/profile');
        }
    }

    /**
     * Redirect to add location form
     */
    public function getLocationForm()
    {
        return view('customer.form.location');
    }

    /**
     * Create new location and redirect to user profile my locations tab.
     */
    public function postLocationForm(Request $request)
    {
        $this->validate($request, [
            'street_address' => 'required|min:5',
            // 'city' => 'required|min:2',
            // 'state' => 'required|min:2',
            'zip_code' => 'required|numeric'
        ]);

        $loc = new Locations();
        $loc->street_address = $request->street_address;
        $loc->city = $request->city;
        $loc->state = $request->state;
        $loc->zip_code = $request->zip_code;
        $loc->country = $request->country;
        $loc->type = $request->type;

        // $completeAddress = $request->street_address.', '.$request->city.', '.$request->state.', '.$request->zip_code;
        // $location = getGeoLocation($completeAddress);

        // $completeAddress = $request->street_address.', '.$request->city.', '.$request->state;
        // $location = getGeoLocationByPin($completeAddress, $request->zip_code);

        // if($location == '0') {
        //     Session::flash('danger', 'Location is not Correct, please fill the correct location.');
        //     return back();
        // } else {
            // $loc->lat = number_format(doubleval($location['lat']), 7);
            // $loc->lng = number_format(doubleval($location['lng']), 7);
            $loc->lat = number_format(doubleval($request->lat), 7);
            $loc->lng = number_format(doubleval($request->lng), 7);
            $loc->save();
            $loc->user()->attach(Sentinel::getUser()->id);
            Session::flash('success', 'Location created successfully.');
            return redirect('/customer/profile#my-locations');
        // }
    }
    /**
     * Get location by ID
     */
    public function getlocation($id)
    {
        $loc = Locations::find($id);
        return view('customer/form/edit-location', compact('loc'));
    }

    /**
     * update location by ID
     */
    public function updateLocation(Request $request)
    {
        $this->validate($request, [
            'street_address' => 'required|min:5',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'zip_code' => 'required|numeric'
        ]);

        $loc = Locations::find($request->id);
        $loc->street_address = $request->street_address;
        // $loc->city = $request->city;
        // $loc->state = $request->state;
        // $loc->zip_code = $request->zip_code;
        $loc->type = $request->type;

        // $completeAddress = $request->street_address.', '.$request->city.', '.$request->state.', '.$request->zip_code;
        // $location = getGeoLocation($completeAddress);

        // $completeAddress = $request->street_address.', '.$request->city.', '.$request->state;
        // $location = getGeoLocationByPin($completeAddress, $request->zip_code);

        // if($location == '0') {
        //     Session::flash('danger', 'Location is not Correct, please fill the correct location.');
        //     return back();
        // } else {
            $loc->lat = number_format(doubleval($request->lat), 7);
            $loc->lng = number_format(doubleval($request->lng), 7);
            $loc->save();
            Session::flash('success', 'Location updated successfully.');
            return redirect('/customer/profile#my-locations');
        // }
    }

    /**
     * delete location by ID
     */
    public function deleteLocation(Request $request)
    {
        $loc = Locations::find($request->id);
        $loc = $loc->delete();
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * restore location by ID
     */
    public function restoreLocation(Request $request)
    {
        $loc = Locations::withTrashed()->find($request->id);
        $loc = $loc->restore();
        return response()->json(['message' => 'success'], 200);    
    }

    /**
     * View single order
     */
    public function viewOrder($id)
    {
        $order = Order::where('id', $id)->first();
        return view('customer/order-detail', compact('order'));
    }

    /**
     * ANET Profile
     */
    public function createANETProfile(Request $request)
    {
        /* Create a merchantAuthenticationType object with authentication details
           retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\Config::get('app.authLoginId'));
        $merchantAuthentication->setTransactionKey(\Config::get('app.authTransKey'));
        
        // Set the transaction's refId
        $refId = 'ref' . time();

        // Set credit card information for payment profile
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($request->cardnumber);
        $creditCard->setExpirationDate(date('Y-m', strtotime($request->expdate)));
        $creditCard->setCardCode($request->cardcode);
        $paymentCreditCard = new AnetAPI\PaymentType();
        $paymentCreditCard->setCreditCard($creditCard);

        //Get user info
        $currentUserID = getUserId();
        $user = Sentinel::findById($currentUserID);

        // Create the Bill To info for new payment type
        $billto = new AnetAPI\CustomerAddressType();
        $billto->setFirstName($user->first_name);
        $billto->setLastName($user->last_name);
        // $billto->setCompany("Souveniropolis");
        // $billto->setAddress("14 Main Street");
        // $billto->setCity("Pecan Springs");
        // $billto->setState("TX");
        // $billto->setZip("44628");
        // $billto->setCountry("USA");
        $billto->setPhoneNumber($user->phone_no);
        // $billto->setfaxNumber("999-999-9999");

        // Create a new Customer Payment Profile object
        $paymentprofile = new AnetAPI\CustomerPaymentProfileType();
        $paymentprofile->setCustomerType('individual');
        $paymentprofile->setBillTo($billto);
        $paymentprofile->setPayment($paymentCreditCard);
        $paymentprofile->setDefaultPaymentProfile(true);

        $paymentprofiles[] = $paymentprofile;

        // Create a new CustomerProfileType and add the payment profile object
        $customerprofile = new AnetAPI\CustomerProfileType();
        $customerprofile->setDescription("Customer ANET profile_pic");

        $customerprofile->setMerchantCustomerId("M_".$user->first_name);
        $customerprofile->setEmail($user->email);
        $customerprofile->setPaymentProfiles($paymentprofiles);

        // Assemble the complete transaction request
        $request = new AnetAPI\CreateCustomerProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setProfile($customerprofile);

        // Create the controller and get the response
        $controller = new AnetController\CreateCustomerProfileController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") ) {

            $paymentProfiles = $response->getCustomerPaymentProfileIdList();
            $userInfo = User::where('id', getUseriD());
            $userInfo->anet_customer_profile_id = $response->getCustomerProfileId();
            $userInfo->anet_customer_payment_profile_id = $paymentProfiles[0];
            $userInfo->save();
            Session::flash('success', 'Authorize.net Customer Profile Created.');

        } else {
            
            $errorMessages = $response->getMessages()->getMessage();
            Session::flash('error','ERROR : Invalid response, Response : '.$errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText());

        }
    }

    /**
     * Delete ANET customer and payment profile
     */
    public function deleteANETProfile(Request $request)
    {
        $userID = $request->user_id;
        $customerProfileId= $request->profileID; 
        $customerpaymentprofileid = $request->paymentProfileID;

        /**
         * Create a merchantAuthenticationType object with 
         * authentication details retrieved from the constants file 
         */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\Config::get('app.authLoginId'));
        $merchantAuthentication->setTransactionKey(\Config::get('app.authTransKey'));

        switch ($request->anet_profile) {
            case 'completeProfile':
                // Delete Payment profile
                $requestPProfile = new AnetAPI\DeleteCustomerPaymentProfileRequest();
                $requestPProfile->setMerchantAuthentication($merchantAuthentication);
                $requestPProfile->setCustomerProfileId($customerProfileId);
                $requestPProfile->setCustomerPaymentProfileId($customerpaymentprofileid);
                $controllerPP = new AnetController\DeleteCustomerPaymentProfileController($requestPProfile);
                $responsePP = $controllerPP->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);

                if (($responsePP != null) && ($responsePP->getMessages()->getResultCode() == "Ok")) {
                    Session::flash('success', 'Delete Customer Payment Profile Successfully.');

                    // $usr = new User::find($userID);
                    // $usr->anet_customer_profile_id = '';
                    // $usr->anet_customer_payment_profile_id = '';
                    // $usr->save();

                    // Delete an existing customer profile  
                    $requestCProfile = new AnetAPI\DeleteCustomerProfileRequest();
                    $requestCProfile->setMerchantAuthentication($merchantAuthentication);
                    $requestCProfile->setCustomerProfileId( $customerProfileId );

                    $controllerCP = new AnetController\DeleteCustomerProfileController($requestCProfile);
                    $responseCP = $controllerCP->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
                    if (($responseCP != null) && ($responseCP->getMessages()->getResultCode() == "Ok")) {
                        Session::flash('Success','Delete Customer Profile Successfully.');
                    } else {
                        $errorMessages = $responseCP->getMessages()->getMessage();
                        Session::flash('danger', 'Failed! Delete Customer Profile. Error: '.$errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText());
                    }
                    
                } else {
                    $errorMessages = $responsePP->getMessages()->getMessage();
                    Session::flash('danger', 'Failed! Delete Customer Payment Profile. Error: '.$errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText());
                }
            break;

            case 'paymentProfileOnly':
                $requestPP = new AnetAPI\DeleteCustomerPaymentProfileRequest();
                $requestPP->setMerchantAuthentication($merchantAuthentication);
                $requestPP->setCustomerProfileId($customerProfileId);
                $requestPP->setCustomerPaymentProfileId($customerpaymentprofileid);
                $controllerPP = new AnetController\DeleteCustomerPaymentProfileController($requestPP);
                $responsePP = $controllerPP->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
                if (($responsePP != null) && ($responsePP->getMessages()->getResultCode() == "Ok")) {
                    Session::flash('success', 'Delete Customer Payment Profile Successfully.');
                    // $usr = new User::find($userID);
                    // $usr->anet_customer_payment_profile_id = '';
                    // $usr->save();
                } else {
                    $errorMessages = $responsePP->getMessages()->getMessage();
                    Session::flash('danger', 'Failed! Delete Customer Payment Profile. Error: '.$errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText());
                }
            break;
        }
    }
}