<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Session;

class LandingController extends Controller
{
    public function getProducts() 
    {
        return view('home');
    }  
    public function postSearch(Request $request) 
    {
		$location = $request->locations;
		$request->session()->put('search_location', $location);
		$request->session()->put('back_ref', '/products');

		return redirect('/products');
	
    }
}