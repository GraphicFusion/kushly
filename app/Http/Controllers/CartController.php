<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use Sentinel;
use Validator;
use App\Order;
use App\OrderProduct;
use App\OrderDriver;
use App\Locations;
use App\User;
use App\Product;
use App\Groups;
use App\DriverCurrentLocations;
use App\Price;
use response;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define('IV_SIZE', mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

class iCanPay {

    private $_API_URL;
    private $_3DSv_API_URL;
    private $_SECRET_KEY;
    private $_PARAMS;
    private $_API_TYPE;

    function __construct($secretKey, $params = array(), $type = 'API') {
        if (($this->_mcrypt_exists = function_exists('mcrypt_encrypt')) === FALSE) {
            die('The Encrypt library requires the Mcrypt extension.');
        }

        $this->_PARAMS = $params;
        $this->_SECRET_KEY = $secretKey;
        $this->_API_URL = 'https://pay.icanpay.cn.com/pay/authorize_payment';
        $this->_3DSv_API_URL = 'https://pay.icanpay.cn.com/pay/authorize3dsv_payment';
        $this->_API_TYPE = $type;

        $this->validatePayload();
    }
    public function payment() {
        $payload = array(
            "ccn" => $this->_PARAMS['ccn'],
            "expire" => $this->_PARAMS['exp_month'] . '/' . $this->_PARAMS['exp_year'],
            "cvc" => $this->_PARAMS['cvc_code'],
            "firstname" => $this->_PARAMS['firstname'],
            "lastname" => $this->_PARAMS['lastname']
        );
        $encripted_card_info = $this->encrypt($payload);
        $this->_PARAMS['card_info'] = $encripted_card_info;

        unset($this->_PARAMS['ccn']);
        unset($this->_PARAMS['cvc_code']);
        unset($this->_PARAMS['firstname']);
        unset($this->_PARAMS['lastname']);
        unset($this->_PARAMS['exp_year']);
        unset($this->_PARAMS['exp_month']);
        $this->_PARAMS['customerip'] = $_SERVER['REMOTE_ADDR'];
        if ($this->_API_TYPE == '3DSV') {
            $this->_PARAMS['success_url'] = urlencode($this->_PARAMS['success_url']);
            $this->_PARAMS['fail_url'] = urlencode($this->_PARAMS['fail_url']);
            $this->_PARAMS['notify_url'] = urlencode($this->_PARAMS['notify_url']);
        }

        $signature = "";
        ksort($this->_PARAMS);

        foreach ($this->_PARAMS as $key => $val) {
            if ($key != "signature") {
                $signature .= $val;
            }
        }
        $signature = $signature . $this->_SECRET_KEY;
        $signature = strtolower(sha1($signature));
        $this->_PARAMS['signature'] = $signature;        
        if ($this->_API_TYPE == 'API') {
            $this->_PARAMS['tr_mode'] = 'API';            
            $response = $this->post_request();
            parse_str($response, $output);
            return json_encode($output);
        } elseif ($this->_API_TYPE == '3DSV') {
            $this->_PARAMS['tr_mode'] = 'API3DSv';            
            $requestDataJson = json_encode($this->_PARAMS);
            $base64_encode = base64_encode($requestDataJson);
            $final_param = array('request' => $base64_encode);
            $url_args = http_build_query($final_param);

            $response = array(
                'status' => 1,
                'redirect_url' => $this->_3DSv_API_URL . '?' . $url_args
            );
            return json_encode($response);
        }
    }

    private function encrypt($payload = array()) {        
        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $this->_SECRET_KEY);
        $sKey = substr($string, 0, 16);
        
        $iv = mcrypt_create_iv(IV_SIZE, MCRYPT_DEV_URANDOM);
        $crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $sKey, $this->array_implode_with_keys($payload), MCRYPT_MODE_CBC, $iv);
        $combo = $iv . $crypt;
        $encryptdata = base64_encode($iv . $crypt);
        return $encryptdata;
    }

    private function validatePayload() {
        $payload = $this->_PARAMS;
        if (!is_array($payload)) {
            die('Data to be encripted must be in array format');
        }
        if ($this->_API_TYPE == 'API') {
            $required_parameter = array('authenticate_id', 'authenticate_pw', 'orderid', 'transaction_type', 'amount', 'currency', 'ccn', 'exp_month', 'exp_year', 'cvc_code', 'firstname', 'lastname', 'email', 'street', 'city', 'zip', 'state', 'country', 'phone');
        } elseif ($this->_API_TYPE == '3DSV') {
            $required_parameter = array('authenticate_id', 'authenticate_pw', 'orderid', 'transaction_type', 'amount', 'currency', 'ccn', 'exp_month', 'exp_year', 'cvc_code', 'firstname', 'lastname', 'email', 'street', 'city', 'zip', 'state', 'country', 'phone', 'dob', 'success_url', 'fail_url', 'notify_url');
        }

        foreach ($required_parameter as $key) {
            if (empty($payload[$key])) {
                die($key . ' must have a value');
            }
        }
        if ($payload['ccn']) {
            $ccn = preg_replace('/[^0-9]/', '', $payload['ccn']);
            if ((strlen($ccn) < 13) || (strlen($ccn) > 16)) {
                die($payload['ccn'] . ' is invalid card');
            }
        }
        if ($payload['cvc_code']) {
            $cvc_code = preg_replace('/[^0-9]/', '', $payload['cvc_code']);
            if ((strlen($cvc_code) < 3) || (strlen($cvc_code) > 4)) {
                die($payload['cvc_code'] . ' is invalid cvc code');
            }
        }
        if ($this->validDate($payload['exp_year'], $payload['exp_month']) == FALSE) {
            die('Expiry date must be valid');
        }
        return TRUE;
    }

    private function validDate($year, $month) {
        $year = '20' . $year;
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        if (!preg_match('/^20\d\d$/', $year)) {
            return FALSE;
        }
        if (!preg_match('/^(0[1-9]|1[0-2])$/', $month)) {
            return FALSE;
        }
        // past date
        if ($year < date('Y') || $year == date('Y') && $month < date('m')) {
            return FALSE;
        }
        return TRUE;
    }

    private function array_implode_with_keys($array) {
        $return = '';
        if (count($array) > 0) {
            foreach ($array as $key => $value) {
                $return .= $key . '||' . $value . '__';
            }
            $return = substr($return, 0, strlen($return) - 2);
        }
        return $return;
    }

    private function post_request() {
        $data_stream = http_build_query($this->_PARAMS);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_stream);
        curl_setopt($ch, CURLOPT_URL, $this->_API_URL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result_str = curl_exec($ch);
        if (curl_errno($ch) != 0) {
            $result_str = 'curl_error=' . curl_errno($ch) . '&status=0';
        }
        curl_close($ch);
        return $result_str;
    }
}

class CartController extends Controller
{
	/**
	 * Add to cart ajax handler
	 */
    public function addToCart(Request $request)
    {
    	$request->all();
		$currentAddToCart = $request->session()->get('addToCart');
		
		$addToCartGroup = $request->group_id;

		$sessionGroup = '';

		if(Session::has('addToCartGroup')) {
			$sessionGroup = $request->session()->get('addToCartGroup');
		} else {
			$sessionGroup = '';
		}
		//return $addToCartGroup.'--'.$sessionGroup;

		if($addToCartGroup != $sessionGroup && !empty($sessionGroup)){
			return response()->json([
				'error' => 'danger',
				'message' => 'Only same dispensary is allowed in single cart. You can place another order if required.'
			], 200);
		} 
		else 
		{
			if(!empty($currentAddToCart)) {
				array_push($currentAddToCart, [
					'product_id' => $request->product_id,
					'user_id' => $request->user_id,
					'variant_id' => $request->variant_id,
					'quantity' => $request->quantity,
					'quantity_per_unit' => $request->quantity_per_unit,
					'unit_price' => $request->unit_price
				]);
				$request->session()->put('addToCart', $currentAddToCart);
			} else {
				$AddToCart[] = [
					'product_id' => $request->product_id,
					'user_id' => $request->user_id,
					'variant_id' => $request->variant_id,
					'quantity' => $request->quantity,
					'quantity_per_unit' => $request->quantity_per_unit,
					'unit_price' => $request->unit_price
				];
				$request->session()->put('addToCart', $AddToCart);
			}
			$request->session()->put('addToCartGroup', $request->group_id);
			return response()->json([
				'message' => 'success', 
				'count' => sizeof($request->session()->get('addToCart'))
			], 200);
		}
    }

    /**
	 * Add to cart ajax handler
	 */
    public function addToCartLocation(Request $request)
    {
    	$locId = $request->location_id;
		$request->session()->put('addToCartLocation', $locId);
		return response()->json([
			'message' => 'success', 
			'location' => 'Cart Location Updated'
		], 200);
    }

	/**
	 * Add to cart ajax handler
	 */
    public function updateAddToCart(Request $request)
    {
    	$currentAddToCart = $request->session()->get('addToCart');
    	//return $currentAddToCart;
    	$updatedcart = array();
		if(!empty($currentAddToCart)) {
			foreach ($currentAddToCart as &$cart) {
				if($cart['product_id'] == $request->product_id){
					$cart['variant_id'] = $request->variant_id;
					$cart['unit_price'] = $request->unit_price;
					$cart['quantity_per_unit'] = $request->quantity_per_unit;
				}
				$updatedcart[] = $cart;
			}
		}
		Session::set('addToCart', $updatedcart);

		$carts = Session::get('addToCart');
        return view('modals/view-cart', compact('carts'));
    }

    /**
     * Cart Page
     */
    public function cart()
    {
    	$cart = Session::get('addToCart');
    	return view('cart/index', compact('cart'));
    }

    /**
     * Cart Payment Process
     */
    public function getcartpayment()
    {
    	$cart = Session::get('addToCart');
    	$cartLocation = Session::get('addToCartLocation');
    	if(!Session::has('addToCartLocation')){
    		$user_id = '';
			foreach ($cart as $key => $pr) {
				$user_id = $pr['user_id'];
			}
			if(empty($user_id)) {
				$user_id = getUserId();
			}
			$user = Sentinel::findById($user_id);
			$location = $user->locations()->get()->first();
			if(count($location)!=0){
			$cartLocation = $location->id;
			}else{
			//$loc = Locations::where('deleted_at','NULL')->first();
			$cartLocation = 1;
			}
    	}
 		
    	return view('cart/payment')->with([
    		'cart' => $cart, 
    		'location' => $cartLocation, 
    		'cartOptions' => Session::get('cartOptions')
		]);
    }

    /**
     * Cart Payment Process
     */
    public function cartpayment(Request $request)
    {
    	$cart = Session::get('addToCart');
    	$cartLocation = Session::get('addToCartLocation');
    	if(!Session::has('addToCartLocation')){
    		$user_id = '';
			foreach ($cart as $key => $pr) {
				$user_id = $pr['user_id'];
			}
			if(empty($user_id)) {
				$user_id = getUserId();
			}
			$user = Sentinel::findById($user_id);
			$location = $user->locations()->get()->first();
			if(count($location)!=0){
			$cartLocation = $location->id;
			}else{
			//$loc = Locations::where('deleted_at','NULL')->first();
			$cartLocation = 1;
			}
    	}
 		
    	$cartOptions = [
    		'taxes' => $request->taxes,
    		'delivery' => $request->delivery,
    		'order_value' => $request->order_value
    	];
 		Session::put('cartOptions', $cartOptions);
 		Session::put('cartProcessPayment', 'start');

    	return view('cart/payment')->with([
    		'cart' => $cart, 
    		'location' => $cartLocation, 
    		'cartOptions' => Session::get('cartOptions')
		]);
    }

    /**
     * Cart Place Order with Payment
     */
    public function placeorder(Request $request)
    {			
		 global $transactionid;
		 global $status;
		 global $errorcode;
		 global $errormessage;
		 global $amount;
		 global $currency;
		 global $orderid;
		 global $descriptor;
    	if(Session::get('cartProcessPayment') == 'start'){

    	$this->validate($request, [
			'payoptions' => 'required|in:cc,bank',
	    ],[
	    	'payoptions.required' => 'Please select one payment option to complete cart process.'
	    ]);

			$authResponse = []; 
			
			//Customer Address
		$loc = Locations::where('id', $request->location)->get()->first();
		if($request->payoptions == 'cc'){
		
			$this->validate($request, [
				'payoptions' => 'required|in:cc,bank',
				'cardnumber' => 'required|numeric',
    			'expdate' => 'required',
    			'cardcode' => 'required|numeric',
		    ],[
		    	'cardnumber.required' => 'The card number is required.',
		    	'cardnumber.numeric' => 'The card number must be a number.',
		    	'expdate.required' => 'The card expiry date is required.',
		    	'cardcode.required' => 'The card code is required.',
		    	'cardcode.numeric' => 'The card code must be a number.',
		    ]);
			
			$user_id = $request->user_id;
			$first_name = $request->first_name; 
			$last_name = $request->last_name; 
			$email = $request->email;
			if($email !=""){
			   $email;
			}
			else{
			   $email = "websolution807@gmail.com";
			}
			$phone = $request->phone;
			if($phone !=""){
			   $phone;
			}
			else{
			   $phone = 857057598;
			}
			$street_address = $loc->street_address;
			$city = $loc->city;
			if($city !=""){
			   $city;
			}
			else{
			   $city = "TUCSON";
			}
			
			$state = $loc->state;
			if($state !=""){
			   $state;
			}
			else{
			   $state = "AZ";
			}		
			$zipcode = $loc->zip_code;
			$zip_code = 85705;
			$order_value = $request->order_value;
			$location = $request->location;
			$taxes = $request->taxes;
			$delivery = $request->delivery;
			$payoptions = $request->payoptions;
			$cardnumber = $request->cardnumber;
			$product_id = $request->product_id;
			$productid = 0;
			foreach($product_id as $products_id){
			  	$productid+= $products_id;
			}
			$order_id = $user_id + $productid; 
			$expdate = $request->expdate;			
			$final_date = explode("/",$expdate);			
			$month = $final_date[0];
			$ye_ar = $final_date[1];
			$year = substr($ye_ar, 2); 
			
			$cardcode = $request->cardcode; 
			
			$quantity = $request->quantity;
			$Sum_quantity = 0;
			foreach($quantity as $quantites){
			  	$Sum_quantity+= $quantites;;
			}
			$quantity_per_unit = $request->quantity_per_unit; 	//array
			
			$unit_price = $request->unit_price;
			$Sum_unit_price = 0;
			foreach($unit_price as $unit_prices){
			  	$Sum_unit_price+= $unit_prices;;
			}
			$params = array(
				'authenticate_id'=>'5f01e4c93efc13fef85db3397bcd2372',
				'authenticate_pw'=>'de6c1c1894bee93e7f8bbe3d9029a27c',
				'orderid'=> $order_id,
				'transaction_type'=>'a',
				'amount'=> $Sum_unit_price,
				'currency'=>'USD',
				'ccn'=> $cardnumber,
				'exp_month'=> $month,
				'exp_year'=> $year,
				'cvc_code'=> $cardcode,
				'firstname'=> $first_name,
				'lastname'=> $last_name,
				'email'=> $email,
				'street'=> $street_address,
				'city'=> $city,
				'zip'=> $zip_code,
				'state'=> $state,
				'country'=>'USA',
				'phone'=> $phone
			);			
			$sec_key = "59e736017cc8c6.72822554";  //Secret Key Of icanpay
			$pay = new iCanPay($sec_key, $params, 'API');
			$response = $pay->payment(); 
			$data = json_decode($response);
			
			if ($data->status == 1) {
				 $transactionid = $data->transactionid;
				 $status = $data->status;
				 $errorcode = $data->errorcode;
				 $errormessage = $data->errormessage;
				 $amount = $data->amount;
				 $currency = $data->currency;
				 $orderid = $data->orderid;
				 $descriptor = $data->descriptor;
			}else{
				 $status = $data->status;
				 $errorcode = $data->errorcode;
				 $errormessage = $data->errormessage;
			}			
			$order = new Order();
			$order->locations_id = $request->location;
			$order->user_id = $request->user_id;
			$order->timestamp_order_made = date('Y-m-d H:i:s');
			$order->active = 1;
			$order->status = 1;
			$order->order_value = $request->order_value;
			$order->taxes = $request->taxes;
			$order->delivery = $request->delivery;

			$order->an_transaction_id = $transactionid;
			$order->an_response_code = $errorcode;
			$order->an_message_code = $errormessage;
			$order->an_auth_code = $status;
			$order->an_description = $descriptor;

			$order->payoptions = $request->payoptions;
			$order->cardnumber = $request->cardnumber;
			$order->expdate = $request->expdate;
			$order->cardcode = $request->cardcode;
			$order->save();

			for ($i=0; $i < count($request->product_id); $i++) { 
				$orderProduct = new OrderProduct();
				$orderProduct->order_id = $order->id;
				$orderProduct->product_id = $request->product_id[$i];
				$orderProduct->quantity = $request->quantity[$i];
				$orderProduct->quantity_per_unit = $request->quantity_per_unit[$i];
				$orderProduct->unit_price = $request->unit_price[$i];
				$orderProduct->save();
			}
			
			if($errorcode == ""){
				$errorcode = 200;
				$errormessage = "Success";
				$authResponse = [
					"Order Id" => $order->id,
					"Successfully created transaction with Transaction ID" => $transactionid,
					"Transaction Response Code" => $errorcode,
					"Message Code" => $errormessage,
					"Auth Code" => $status,
					"Description" => $descriptor
				];
            }
			else{
				Session::flash('danger', 'Transaction Failed');
				if($errormessage != "Success") {
					$authResponse = [
						"Error Code:" => $errorcode,
						"Error Message:" =>  $errormessage
					];
				}
			}			
			// Clean add to cart
			Session::forget('addToCart');
			Session::forget('addToCartLocation');
			Session::forget('addToCartGroup');
			Session::forget('repeatCart');
			Session::forget('cartOptions');
			Session::put('cartProcessPayment', 'complete');
			Session::flash('success', 'Your order is placed successfully.');			     
		}		
		return view('cart/thankyou', compact('authResponse'));
	    Session::put('authResponse', $authResponse);
	    return redirect('cart/thankyou');

		/* else{
			Session::flash('info', 'No direct access for requested page.');
    		return redirect('/');
		} */
		/* endif; */
		}
    }
	
    public function thankyou()
    {
    	$authResponse = Session::get('authResponse');
    	return view('/cart/thankyou', compact('authResponse'));
    }

	/**
	 * Repeat order add to cart ajax handler
	 */
    public function repeatAddToCart(Request $request)
    {
		// Clear previous cart value to fix the confliction between location and other.
    	Session::forget('addToCart');
        Session::forget('addToCartLocation');

		// dd($request->all());
        if(!empty($request->locations_id)) {
			$request->session()->put('addToCartLocation', $request->locations_id);
			$request->session()->put('repeatCart', 'active');
			$AddToCart = [];
			for ($i=0; $i < sizeof($request->product_id); $i++) { 
				$AddToCart[] = [
					'product_id' => $request->product_id[$i],
					'user_id' => $request->user_id[$i],
					'variant_id' => $request->variant_id[$i],
					'quantity' => $request->quantity[$i],
					'quantity_per_unit' => $request->quantity_per_unit[$i],
					'unit_price' => $request->unit_price[$i]
				];
			}
			$request->session()->put('addToCart', $AddToCart);
			return response()->json([
				'message' => 'success', 
				'count' => sizeof($request->session()->get('addToCart'))
			], 200);

        } else {
        	$error = 'Location not avaliable.';
			return response()->json([
				'message' => 'danger', 
				'error' => $error
			], 200);
        }
    }

}			

	    	