<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Mail;
use App\Identification;
use Sentinel;
use Session;
use Response;
use App\Libraries\UploadHandler;

class RegistrationController extends Controller
{
	/**
	 * Redirect to sign up form.
	 */
    public function getSignup() {
    	return view('signup');
    }

    /**
     * Validates the input and create new user as customer
     */
    public function postSignup(Request $request) {


        $input = $request->all();
	    $validator = Validator::make($request->all(), User::$rules);

        if ($validator->fails()) {
            return redirect('signup')
                    ->withErrors($validator)
                    ->withInput();
        } else {
            if($user = Sentinel::registerAndActivate($request->all())) {
                
                // Assign default role (Customer) to user
                $role = Sentinel::findRoleBySlug('customer');
                $role->users()->attach($user);
                $request->session()->put('activeRole', 'customer');


                $data = [
                    'text' => "
                    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
                    <html xmlns='http://www.w3.org/1999/xhtml'>
                        <p>Hello and Welcome to Kushly. - </p>
                    </html>",
                    'subject' => "Welcome to Kushly"
                ];
                $subject = $data['subject'];
                Mail::send('welcome-email', $data, function ($message) use ($subject, $user) {
                    $message->from('info@kushly.sonderdev.com', 'Kushly');
                    $message->subject($subject);
                    $message->to($user->email, $user->first_name );
                    $message->to('wjgrass@gmail.com', 'wjgrass');
                    //$message->setBody($text, 'text/html'); 
                });                    

                // if($request->stateIdFronthidden!=''|| $request->stateIdBackhidden!='' ||$request->medicalCardFronthidden!='' || $request->medicalCardBackhidden!=''){
                //     return 'true';
                // }else{
                //     return 'false';
                // }
                $identification= new Identification();
                $identification->user_id= $user->id;

                 if($request->stateIdFronthidden!=''){
                   $parts = parse_url($request->stateIdFronthidden);
                   $stateIdFront =  $parts['path'];
                }else{ $stateIdFront='NULL'; }

                 if($request->stateIdBackhidden!=''){
                   $parts = parse_url($request->stateIdBackhidden);
                   $stateIdBack =  $parts['path'];
                }else{ $stateIdBack='NULL'; }

                 if($request->medicalCardFronthidden!=''){
                   $parts = parse_url($request->medicalCardFronthidden);
                   $medicalCardFront =  $parts['path'];
                }else{ $medicalCardFront='NULL'; }

                 if($request->medicalCardBackhidden!=''){
                   $parts = parse_url($request->medicalCardBackhidden);
                   $medicalCardBack =  $parts['path'];
                }else{ $medicalCardBack='NULL'; }

                $identification->state_id_front_path = $stateIdFront;
                $identification->state_id_back_path = $stateIdBack;
                $identification->med_card_front_path = $medicalCardFront;
                $identification->med_card_back_path = $medicalCardBack;
                $identification->save();

                // Send welcome mail to customer
                // Mail::to($request->email)->send(new App\Mail\CustomerWelcomeMessage($user));

                Session::flash('success', 'Registration complete sucessfully, Please login...');
                return redirect('signin');
            } else {
                Session::flash('danger', 'Registration not complete, Please try again!!!');
                return redirect('signup');
            }
        }

    }

    public function customerfileupdate(Request $req)
    {
        $year = date('Y');
        $month = date('m');
        $options = array('accept_file_types' => '/\.(gif|jpe?g|png)$/i',
                'upload_dir'=>'customers/'.$year.'/'.$month.'/', 
                'upload_url'=>'customers/'.$year.'/'.$month.'/', 
                'image_versions' => array(),
            );
        $upload_handler = new UploadHandler($options);
    }
}
