<?php

namespace App\Http\Controllers\Driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderProduct;
use App\OrderDriver;
use App\Product;
use App\Groups;
use App\Locations;

use App\DriverCurrentLocations;
use App\User;
use Session;
use Sentinel;

class DriverController extends Controller
{
    /**
     * View all orders
     */
    public function index()
    {
        $od = OrderDriver::select('order_id')
            ->where('order_driver_status', 'allocated')
            ->where('driver_id', getUserId())
            ->get();

        $orders = Order::where('active', 1)->where('status','!=','Delivered')->orderBy('id','asc')->whereIn('id', $od)->get();

        return view('driver.index', compact('orders'));
    }
    
    /**
     * View all assignments
     */
    public function assignments()
    {
        $od = OrderDriver::select('order_id')->where('order_driver_status', 'accepted')->where('driver_id', getUserId())->get();
        if(sizeof($od) >= 1) {
            $orders = Order::where('active', 1)->whereIn('id', $od)->get();
            return view('driver.index', compact('orders'));
        } else {
            Session::flash('danger', 'No Assignment Available');
            return redirect('/driver');
        }
    }

    /**
     * Redirect to map view page
     */
    public function mapview(Request $request)
    {
        $geo = $request->all();
    	return view('driver.map', compact('geo'));
    }
    

    /**
     * Redirect to driver account page
     */
    public function profile()
    {
    	return view('driver.profile');
    }

    /**
     * Get specific order via id
     */
    public function getOrder($id)
    {
        $order = Order::where('id', $id)->first();
        return view('driver.order-detail', compact('order'));
    }

    /**
     * Get specific order via id
     * then redirect to order detail page
     */
    public function getOrderDetail($id)
    {
        $order = Order::where('id', $id)->first();
        return view('driver.order-detail-brief', compact('order'));
    }

    /**
     * Ajax handler to submit assignment accepted by driver
     */
    public function acceptAssignment(Request $request)
    {
        $od = new OrderDriver();
        $od->order_id = $request->order_id;
        $od->driver_id = $user->id;
        $od->order_accept_timestamp = date('Y-m-d H:i:s');
        $od->save();
        return response()->json(['message'=>'success'], 200);
    }
	/**
     * Save customer photo
     */
    public function saveCustomerPhoto(Request $request)
    { 
		$data = $request->all();
		$this->validate($request, [
            'customer_photo' => 'mimes:jpeg,jpg,bmp,png'
        ]);
		
		if( $request->hasFile('customer_photo')){ 
            $image = $request->file('customer_photo'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();

            $imageName = time().'-'.$fileName;
            $request->customer_photo->move(public_path('customer_photo'), $imageName);
			$order = Order::where('id', $data['order_id'])->first();
			$res = $order->update(['customer_photo' => $imageName]);
			
            Session::flash('success', 'Customer photo successfully uploaded.');
        }
		return redirect('driver/order/'.$data['order_id']);
        
    }
}
