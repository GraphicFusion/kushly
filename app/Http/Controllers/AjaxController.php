<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Sentinel;
use Validator;
use App\User;
use App\Product;
use App\Price;
use App\Order;
use App\OrderDriver;
use App\DriverCurrentLocations;
use App\Identification;

class AjaxController extends Controller
{
    /**
     * Product popup slider in homepage
     */
    public function loadProductModalSlider(Request $request) 
    {
		return view('modals.products-slider')->with(['id' => $request->id]);
    }

    /**
     * Load more product in homepage product grid
     */
    public function loadMoreProducts(Request $request)
    {
        $query = Product::where('active', 1);
        if(!empty($request->groupID)) {
            $query->where('group_id', $request->groupID);
        }
        if(!empty($request->viewAll)) {
            // pr($request->viewAll);
        }
        if (!empty($request->options)) { 
            // if(strpos($request->options, ',') !== false) {
            //     $options = explode(',', $request->options);
            //     $query->whereIn('type', $options);
            //     $query->orWhereIn('strain_type', $options);
            // } else {
            //     $query->where('type', $request->options);
            //     $query->orWhere('strain_type', $request->options);
            // }

            $productIDs = '';
            if(strpos($request->options, ',') !== false) {
                $options = explode(',', $request->options);

                $pidT = [];
                $pidTypeX = Product::select('id')->whereIn('type', $options)->get();
                foreach ($pidTypeX as $key => $type) {
                    $pidT[] = $type->id;
                }
                // pr($pidT);

                $pidS = [];
                $pidSTypeX = Product::select('id')->whereIn('strain_type', $options)->get();
                foreach ($pidSTypeX as $key => $stype) {
                    $pidS[] = $stype->id;
                }
                // pr($pidS);

                $productIDs = array_unique(array_merge($pidT, $pidS), SORT_REGULAR);
                // pr($productIDs);

            } else {

                $pidT = [];
                $pidType = Product::select('id')->where('type', $request->options)->get();
                foreach ($pidType as $key => $type) {
                    $pidT[] = $type->id;
                }
                // pr($pidT);

                $pidS = [];
                $pidSType = Product::select('id')->where('strain_type', $request->options)->get();
                foreach ($pidSType as $key => $stype) {
                    $pidS[] = $stype->id;
                }
                // pr($pidS);

                $productIDs = array_unique(array_merge($pidT, $pidS), SORT_REGULAR);
                // pr($productIDs);
            }

            if(sizeof($productIDs) > 0) {
                $query->whereIn('id', $productIDs);
            }
        }
        if(isset($request->q) && !empty($request->q)) {
            $query->where('product_name', 'LIKE', "%$request->q%");
        }
        $query->with(['prices' => function ($qur) {
            $qur->orderBy('unit_price', 'asc');
        }]);

        $productList = $query->offset($request->offset)->limit($request->nop)->orderBy('id', 'desc')->get();
        $productListCount = $query->orderBy('id', 'desc')->count();
        return view('product.response-product')->with(['products' => $productList, 'count' => $productListCount]);
    }

    /**
     * Product filter
     * @param Request instance
     */
    public function productFilter(Request $request)
    {
        $query = Product::where('active', 1);
        if(!empty($request->groupID)) {
            $query->where('group_id', $request->groupID);
        }
        if(!empty($request->viewAll)) {
            // pr($request->viewAll);
        }
        if(!empty($request->options)) { 
            // if(strpos($request->options, ',') !== false) {
            //     $options = explode(',', $request->options);
            //     $query->whereIn('type', $options);
            //     $query->orWhereIn('strain_type', $options);
            // } else {
            //     $query->where('type', $request->options);
            //     $query->orWhere('strain_type', $request->options);
            // }
            $productIDs = '';
            if(strpos($request->options, ',') !== false) {
                $options = explode(',', $request->options);

                $pidT = [];
                $pidTypeX = Product::select('id')->whereIn('type', $options)->get();
                foreach ($pidTypeX as $key => $type) {
                    $pidT[] = $type->id;
                }
                // pr($pidT);

                $pidS = [];
                $pidSTypeX = Product::select('id')->whereIn('strain_type', $options)->get();
                foreach ($pidSTypeX as $key => $stype) {
                    $pidS[] = $stype->id;
                }
                // pr($pidS);

                $productIDs = array_unique(array_merge($pidT, $pidS), SORT_REGULAR);
                // pr($productIDs);

            } else {

                $pidT = [];
                $pidType = Product::select('id')->where('type', $request->options)->get();
                foreach ($pidType as $key => $type) {
                    $pidT[] = $type->id;
                }
                // pr($pidT);

                $pidS = [];
                $pidSType = Product::select('id')->where('strain_type', $request->options)->get();
                foreach ($pidSType as $key => $stype) {
                    $pidS[] = $stype->id;
                }
                // pr($pidS);

                $productIDs = array_unique(array_merge($pidT, $pidS), SORT_REGULAR);
                // pr($productIDs);
            }

            if(sizeof($productIDs) > 0) {
                $query->whereIn('id', $productIDs);
            }
        }
        if(isset($request->q) && !empty($request->q)) {
            $query->where('product_name', 'LIKE', "%$request->q%");
        }

        // if(isset($request->locations) && !empty($request->locations)) {

        // }

        $query->with(['prices' => function ($qur) {
            $qur->orderBy('unit_price', 'asc');
        }]);

        $productList = $query->limit($request->nop)->orderBy('id', 'desc')->get();

        $productListCount = $query->orderBy('id', 'desc')->count();
        return view('product.response-product')->with(['products' => $productList, 'count' => $productListCount]);
    }

    /**
     * Location wise search in between 50 miles of area.
     */
    public function locSearch(Request $request)
    {
        dd($request->all());
        // echo getGeoLocationByCity($request->location);

        // $query = "SELECT *, (((acos(sin((".$latitude."*pi()/180)) * sin((`Latitude`*pi()/180))+cos((".$latitude."*pi()/180)) * cos((`Latitude`*pi()/180)) * cos(((".$longitude."- `Longitude`)* pi()/180))))*180/pi())*60*1.1515) as distance FROM `MyTable` HAVING distance >= ".$distance."";
    }

    /**
     * Load update form for user details.
     */
    public function updateCustomerForm(Request $request)
    {
        $user_id = $request->id;
        $action = $request->action;
        $user = Sentinel::findById($user_id);
        switch ($action) {
            case 'name':
                $data['id'] = $user_id;
                $data['first_name'] = $user->first_name;
                $data['last_name'] = $user->last_name;
                return view('ajax.customer-name-form')->with(['data' => $data]);
                break;
            case 'phone':
                $data['id'] = $user_id;
                $data['phone_no'] = $user->phone_no;
                return view('ajax.customer-phoneno-form')->with(['data' => $data]);
                break;
            case 'email':
                $data['id'] = $user_id;
                $data['email'] = $user->email;
                return view('ajax.customer-email-form')->with(['data' => $data]);
                break;
            case 'dob':
                $data['id'] = $user_id;
                $data['birthday'] = $user->birthday ;
                return view('ajax.customer-birthday-form')->with(['data' => $data]);
            case 'ccinfo':
                $data['id'] = $user_id;
                $data['cardnumber'] = $user->cardnumber;
                $data['expdate'] = $user->expdate;
                $data['cardcode'] = $user->cardcode;
                return view('ajax.customer-credit-card-form')->with(['data' => $data]);
                break;
            case 'bankinfo':
                $data['id'] = $user_id;
                $data['bank'] = $user->bank;
                $data['routingno'] = $user->routingno;
                $data['accountno'] = $user->accountno;
                return view('ajax.customer-bank-info')->with(['data' => $data]);
                break;
                case 'stateidfront':
                 $data['id']= $user_id;
                  return view('ajax.customer-stateidfront-form')->with(['data' => $data]);
                  break;
              case 'stateidback':
                $data['id']= $user_id;
                 return view('ajax.customer-stateidback-form')->with(['data' => $data]);
                 break;
              case 'medicalcardfront':
                $data['id']= $user_id;
                 return view('ajax.customer-medicalcardfront-form')->with(['data' => $data]);
                 break;
             case 'medicalcardback':
                $data['id']= $user_id;
                 return view('ajax.customer-medicalcardback-form')->with(['data' => $data]);
                 break;
        }
    }

    /**
     * Save updated user details
     */
    public function updateCustomerDetails(Request $request)
    {
        $user_id = $request->id;
        $action = $request->action;
        $user = Sentinel::findById($user_id);
        switch ($action) {
            case 'name':
            $details = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name
            ];
            $user = Sentinel::update($user, $details);
            return response()->json([
                'message' => 'Success!',
                'action' => 'name',
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
            ], 200);
            break;
            case 'phone':
            $details = [
                'phone_no' => $request->phone_no
            ];
            $user = Sentinel::update($user, $details);
            return response()->json([
                'message' => 'Success!',
                'action' => 'phone',
                'phone_no' => $user->phone_no
            ], 200);
            break;
            case 'email':
            $details = [
                'email' => $request->email
            ];
            $user = Sentinel::update($user, $details);
            return response()->json([
                'message' => 'Success!',
                'action' => 'email',
                'email' => $user->email
            ], 200);
            break;
            case 'birthday':
            $details = [
                'birthday' => $request->birthday
            ];
            $user = Sentinel::update($user, $details);
            return response()->json([
                'message' => 'Success!',
                'action' => 'dob',
                'birthday' => $user->birthday
            ], 200);
            break;
            case 'ccinfo':
            $details = [
                'cardnumber' => $request->cardnumber,
                'expdate' => $request->expdate,
                'cardcode' => $request->cardcode
            ];
            $user = Sentinel::update($user, $details);
            return response()->json([
                'message' => 'Success!',
                'action' => 'ccinfo',
                'cardnumber' => $user->cardnumber,
                'expdate' => $user->expdate,
                'cardcode' => $user->cardcode
            ], 200);
            break;
            case 'bankinfo':
            $details = [
                'bank' => $request->bank,
                'routingno' => $request->routingno,
                'accountno' => $request->accountno
            ];
            $user = Sentinel::update($user, $details);
            return response()->json([
                'message' => 'Success!',
                'action' => 'bankInfo',
                'bank' => $user->bank,
                'routingno' => $user->routingno,
                'accountno' => $user->accountno
            ], 200);
            break;

             case 'stateidfront':
         if($request->stateIdFront!='')
         {
             $user = Identification::where('user_id',$user->id)->first();
             if(count($user)==1)
             {
                 $user->state_id_front_path= $request->stateIdFront;
                 $user->save();
             }else{
                 $user = new Identification();
                 $user->user_id=Sentinel::findById(getUserId())->id;
                 $user->state_id_front_path= $request->stateIdFront;
                 $user->save();
             }
                 return response()->json([
                 'message' => 'Success!',
                 'action' => 'stateIdFront',
                 'stateidfront' => $user->state_id_front_path
             ], 200);
             break;
         }
         case 'stateidback':
             if($request->stateIdBack!='')
              {
                $user = Identification::where('user_id',$user->id)->first();
               if(count($user)==1)
               {
                   $user->state_id_back_path= $request->stateIdBack;
                   $user->save();
               }else{
                   $user = new Identification();
                   $user->user_id=Sentinel::findById(getUserId())->id;
                   $user->state_id_back_path= $request->stateIdBack;
                   $user->save();
               }
                   return response()->json([
                   'message' => 'Success!',
                   'action' => 'stateIdBack',
                   'stateidback' => $user->state_id_back_path
               ], 200);
               break;
           }
      
       case 'medicalcardfront':
           if($request->medicalCardFront!='')
           {
               $user = Identification::where('user_id',$user->id)->first();
               if(count($user)==1)
               {
                   $user->med_card_front_path= $request->medicalCardFront;
                   $user->save();
               }else{
                   $user = new Identification();
                   $user->user_id=Sentinel::findById(getUserId())->id;
                   $user->med_card_front_path= $request->medicalCardFront;
                   $user->save();
               }
                   return response()->json([
                   'message' => 'Success!',
                   'action' => 'medicalCardFront',
                   'medicalcardfront' => $user->med_card_front_path
               ], 200);
               break;
           }
      case 'medicalcardback':
      if($request->medicalCardBack!='')
            {
                $user = Identification::where('user_id',$user->id)->first();
                if(count($user)==1)
                {
                    $user->med_card_back_path= $request->medicalCardBack;
                    $user->save();
                }else{
                    $user = new Identification();
                    $user->user_id=Sentinel::findById(getUserId())->id;
                    $user->med_card_back_path= $request->medicalCardBack;
                    $user->save();
                }
                    return response()->json([
                    'message' => 'Success!',
                    'action' => 'medicalCardBack',
                    'medicalcardback' => $user->med_card_back_path
                ],200); 
                break;
            }




                        }
                    }

    /**
     * Load view cart
     */
    public function loadViewCart(Request $request) {
        $carts = $request->session()->get('addToCart');
        return view('modals/view-cart', compact('carts'));
    }

    /**
     * Remove product from cart
     */
    public function removeProductFromCart(Request $request)
    {
        $rKey = $request->key;
        $carts = $request->session()->get('addToCart');
        unset($carts[$request->key]);
        $request->session()->set('addToCart', $carts);

        $product_in_cart = sizeof($request->session()->get('addToCart'));
        if($product_in_cart == 0) {
            Session::forget('repeatCart');
            Session::put('addToCartGroup', 0);
        }

        return response()->json([
            'message' => 'success', 
            'count' => $product_in_cart
        ], 200);
    }

    public function updateCartAfterRemove() {
        $carts = $request->session()->get('addToCart');
        $count = sizeof($carts);
        return response()->json(['count' => $count]);
    }

    /**
     * Activate role
     */
    public function aRole(Request $request) 
    {
        $newRole = $request->role;
        if($newRole == 'dispensary-admin') {
            $request->session()->put('activeRole', $newRole);
            $request->session()->put('activeGroup', $request->group);
        } else {
            $request->session()->put('activeRole', $newRole);
            $request->session()->forget('activeGroup');
        }
    }

    /**
     * Google Map API to get lat-lng using pin code
     */
    public function getGM(Request $request)
    {
        return getGeoLocationByPin($request->address, $request->pin, 'FL');
    }
}