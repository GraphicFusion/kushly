<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Session;
use App\Mail\ContactMail;

class ContactController extends Controller
{
    /**
     * Submit Contact Request
     *
     * @param  Request
     * @return Success Info
     */
    public function __invoke(Request $request)
    {
		$this->validate($request, [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'email' => 'required|email',
			'phone_no' => 'required',
			'message' => 'required'
		]);

		$contact = Contact::create($request->all());

		Session::flash('success', 'Contact query sent successfully.');

		\Mail::to(\Config::get('mail.admin_email'))->send(new ContactMail($contact));

		return back();
    }
}
