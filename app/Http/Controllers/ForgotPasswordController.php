<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Reminder;
use Mail;
use Sentinel;
use Session;
use Validator;

class ForgotPasswordController extends Controller
{
    public function getForm()
    {
    	return view('forgot-password');
    }
    
    public function postForm(Request $request)
    {
    	$user = User::whereEmail($request->email)->first();

    	$sentinel_user = Sentinel::findById($user->id);

    	if(count($user) == 0) {
    		Session::flash('success','Reset code was sent to your email.');
            return redirect('signin');
    	}

    	$reminder = Reminder::exists($sentinel_user) ?: Reminder::create($sentinel_user);

    	$this->sendEmail($user, $reminder->code);
        
    	Session::flash('success','Reset code was sent to your email.');
            return redirect('signin');
    }

    public function resetPassword($email, $code)
    {
    	$user = User::byEmail($email);
    	$sentinel_user = Sentinel::findById($user->id);
    	if(count($user) == 0){
			abort(404);
    	}

		if($reminder = Reminder::exists($sentinel_user)) {
			if($code == $reminder->code) {
				return view('reset-password')->with([
						'email' => $email,
						'code' => $code
					]);
			} else {
				return redirect('/');
			}
		} else {
			return 0;
		}
    }

    public function postResetPassword(Request $request, $email, $code)
    {
    	$this->validate($request, [
    		'password' => 'required|min:6',
        	're_password' => 'required|min:6|same:password'
		]);

		$user = User::byEmail($email);
    	$sentinel_user = Sentinel::findById($user->id);
    	if(count($user) == 0)
			abort(404);

		if($reminder = Reminder::exists($sentinel_user)) {
			if($code == $reminder->code) {
				Reminder::complete($sentinel_user, $code, $request->password);
				Session::flash('success', 'Please login with your new password.');
				return redirect('/signin');
			} else {
				return redirect('/');
			}
		} else {
			return 0;
		}
    }

    public function sendEmail($user, $code)
    {
    	Mail::send('emails.forgetpassword', [
			'user' => $user,
			'code' => $code
		], function ($message) use ($user) {
			$message->from('noreply@tekkiwebsolutions.com', 'Tekki');
			$message->to($user->email);
			$message->subject("Hello $user->first_name, reset your password.");
		});
    }
}
