<?php
/*
 * embed_svg helper function is used to embed svg in frontend
 * because <img /> tag only works with the mozila and chrome browser, safari dosen't support this
 * <object></opject> tag works in almost major browsers like mozila, chrome and safari
 */
function embed_svg($class, $svgurl, $height = '20px', $width = '20px', $link = '') {
	$path = App::make('url')->to($svgurl);
	$obj = '<div class="object-wrapper '.$class.'" style="height:'.$height.'; width:'.$width.';">';
	$obj .= '<object class="svg-object" type="image/svg+xml" height="'.$height.'" width="'.$width.'" data="'.$path.'">Your browser doesnot support SVG</object>';
	if(!empty($link)) {	
		$obj .= '<a class="object-link" href="'.$link.'"></a>';
	}
	$obj .= '</div>';
	return $obj;
}

function makeURL($url){
	return App::make('url')->to($url);
}

/**
 * Make nav active with route
 */
function navActive($url) {
	if (Request::is('page/*')) {
		$current_request = Request::url();
		$uri = explode("/",$current_request);  
		if(end($uri) == $url) {
			return 'class="active"';
		}
	} else {
		$route = Route::getCurrentRoute()->getPath();
		if ($url == $route) {
			return 'class="active"';
		}
	}
}

/**
 * Return current user id
 */
function getUserId() {
	if(Sentinel::check())
		return Sentinel::getUser()->id;
}

/**
 * Check user role
 */
function checkRole($role) {
	if(Sentinel::check()){
		if(Sentinel::inRole($role) && Session::get('activeRole') == $role) {
			return true;
		}
	}
	return false;
}

/**
 * get user role first
 */
function getUserRole() {
	$roles = Sentinel::findById(getUserId())->roles;
	foreach ($roles as $role) {
		return $role->slug;
	}
}

/**
 * get active role from session
 */
function getActiveRole() {
	if(Session::has('activeRole'))
        return Session::get('activeRole');
}

/**
 * Use print_r in php
 */
function pr($x) {
	echo '<pre>';
	print_r($x);
	echo '</pre>';
}

/**
 * Get all user with dispensary admin role
 */
function getDispensaryAdmins() {
	$role = Sentinel::findRoleBySlug('dispensary-admin');
    $users = $role->users()->with('roles')->get();
    return $users;
}

function getAllLocations() {
	$loc = App\Locations::all();
	return $loc;
}
function getCurrentGroupLocations() {
	$groupID = Session::get('activeGroup');
    $group = App\Groups::findById($groupID);
    $locations = $group->location()->get();
    return $locations;
}

/**
 * Reference http://www.geodatasource.com
 * calculate distance between two geo location
 */
function distance($lat1, $lon1, $lat2, $lon2, $unit) {
  	$theta = $lon1 - $lon2;
  	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  	$dist = acos($dist);
  	$dist = rad2deg($dist);
  	$miles = $dist * 60 * 1.1515;
  	$unit = strtoupper($unit);
	if ($unit == "K") {
    	return (number_format($miles * 1.609344, 2));
  	} else if ($unit == "N") {
		return (number_format($miles * 0.8684, 2));
    } else {
        return number_format($miles, 2);
  	}
}

// Create GEO Location on given address using google map geocode api.
function getGeoLocation($addr)
{
	$cleanAddress = str_replace (" ", "+", $addr);
	$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$cleanAddress."&sensor=false";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $details_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$geoloc = json_decode(curl_exec($ch), true);
	switch ($geoloc['status']) {
		case 'ZERO_RESULTS':
			return 0;
			break;
		case 'OK':
			return $geoloc['results'][0]['geometry']['location'];
			break;
	}
}

function getGeoLocationByPin($addr, $pin, $flag = '')
{
	$cleanAddress = str_replace (" ", "+", $addr);
	$pincode = trim($pin);
	$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$cleanAddress."&components=postal_code:".$pincode."&sensor=true";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $details_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$geoloc = json_decode(curl_exec($ch), true);
	switch ($geoloc['status']) {
		case 'ZERO_RESULTS':
			return 0;
			break;
		case 'OK':
			if(!empty($flag) && $flag == 'FL') {
				return $geoloc['results'][0];
			} else {
				return $geoloc['results'][0]['geometry']['location'];
			}
			break;
	}
}


// Calculate distence and duration between two geolocations
function GetDrivingDistance($lat1, $lat2, $long1, $long2, $flag)
{
	$distance="";
    $duration="";
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=en";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);

    // pr($response_a);

    $status = $response_a['rows'][0]['elements'][0]['status'];

    // pr($status);
	if($status == 'OK') {
	    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
	    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
	    if($flag == 'TM') {
		    if (strpos($time, 'hours') == false) {
			    return array('time' => '00 hours '.$time);
			} else {
				return array('time' => $time);
			}
	    } elseif ($flag == 'DS') {
	   	    return array('distance' => $dist);
	    } else {
		    return array('distance' => $dist, 'time' => $time);
	    }
	} else {
		if($flag == 'TM') {
		    return array('time' => '0 hours 0 mins');
	    } elseif ($flag == 'DS') {
	   	    return array('distance' => '00');
	    } else {
		    return array('distance' => '00', 'time' => '0 hours 0 mins');
	    }
	}
}

// Convert timestemp to formated time, hours - minutes and seconds.
function secondsToTime($seconds) {
  $dtF = new \DateTime("@0");
  $dtT = new \DateTime("@$seconds");
  return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
}  

/**
 * Get Geo Location by city
 */
function getGeoLocationByCity($city)
{
	$locality = trim($city);
	$details_url = "http://maps.googleapis.com/maps/api/geocode/json?locality=".$locality."&components=locality:".$locality."&sensor=true";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $details_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$geoloc = json_decode(curl_exec($ch), true);
	switch ($geoloc['status']) {
		case 'ZERO_RESULTS':
			return 0;
			break;
		case 'OK':
			return $geoloc['results'][0]['geometry']['location'];
			break;
	}
}

?>