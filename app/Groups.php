<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sentinel;

class Groups extends Model
{
    use SoftDeletes;
    public $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'name', 'type'
	];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
     * relation with user
     */
    public function user()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

	/**
     * relation with location
     */
    public function location()
    {
    	return $this->belongsToMany('App\Locations')->withTimestamps();
    }

    /**
     * Get the group that owns the comment.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product')->withTimestamps();
    }


	/**
     * find by id
     */
    public static function findById($id)
	{
		return static::where('id', $id)
			->with('user')
			->with('location')
			->first();
	}

    public static function getAllGroups()
    {
        return static::select('id', 'name')->where('type', 'Dispensary')->get();
    }

}
