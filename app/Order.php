<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
 	use SoftDeletes;
    public $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'locations_id','user_id','timestamp_order_made','active','status','order_value','taxes','delivery', 'an_transaction_id','an_response_code','an_message_code','an_auth_code','an_description', 'payoptions','cardnumber','expdate','cardcode','bank','routingno','accountno', 'customer_photo'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Get the user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the user with location
     */
    public function locations()
    {
        return $this->hasOne('App\Locations', 'id', 'locations_id');
    }

    /**
     * Raltion with products 
     */
    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct', 'order_id', 'id');
    }

    public function orderDriver()
    {
        return $this->hasOne('App\OrderDriver', 'order_id', 'id');
    }
    
    /**
     * Using accessor and mutators for db storage and retrive.
     */
    public function setTimestampOrderMadeAttribute($value) 
    {
        $this->attributes['timestamp_order_made'] = date('Y-m-d H:i:s', strtotime($value));
    }
    public function getTimestampOrderMadeAttribute($value) 
    {
        if(!is_null($value)) {
            return date('m/d/Y H:i A', strtotime($value) );
        }
    }

    public function setTimestampOrderDeliveredAttribute($value) 
    {
        $this->attributes['timestamp_order_delivered'] = date('Y-m-d H:i:s', strtotime($value));
    }
    public function getTimestampOrderDeliveredAttribute($value) 
    {
        if(!is_null($value)) {
            return date('m/d/Y H:i A', strtotime($value) );
        } else {
            return 'Not delivered';
        }
    }
}
