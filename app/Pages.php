<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
	use SoftDeletes;

    public $table = 'pages';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'page_title','page_content','custom_css'
	];

	/**
	 * Create a page slug.
	 *
	 * @param  string $title
	 * @return string
	 */
	public function setSlugAttribute($value)
	{
	    $temp = str_slug($value, '-');
	    // if(!Pages::all()->where('slug',$temp)->isEmpty()){
	    //     $i = 1;
	    //     $newslug = $temp . '-' . $i;
	    //     while(!Pages::all()->where('slug',$newslug)->isEmpty()){
	    //         $i++;
	    //         $newslug = $temp . '-' . $i;
	    //     }
	    //     $temp =  $newslug;
	    // }
	    $this->attributes['slug'] = $temp;
	}

	public static function findBySlug($slug)
	{
		return static::where('slug', $slug)->first();
	}
}
