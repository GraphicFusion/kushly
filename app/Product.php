<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';

    protected $fillable = [
        'product_name', 
        'group_id', 
        'active', 
        'sku', 
        'vendorID', 
        'supplierID', 
        'color', 
        'size', 
        'description', 
        'is_marijuana', 
        'type', 
        'thc', 
        'cbd', 
        'cbn', 
        'strain_type'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the user with location
     */
    public function group()
    {
        return $this->hasOne('App\Groups');
    }

    /**
     * Get product prices.
     */
    public function prices()
    {
        return $this->hasMany('App\Price');
    }

    /**
     * Get the group that owns the comment.
     */
    public function picture()
    {
        return $this->hasMany('App\ProductImages')->orderBy('sortorder');
    }

    /**
     * Query scope on like condition
     */
    public function scopeLike($query, $field, $value)
    {
        return $query->where($field, 'LIKE', "%$value%");
    }

    /**
     * Relation with order
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}