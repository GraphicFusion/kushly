<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    public $table = 'product_images';

    protected $fillable = [
        'product_id', 'path', 'sortorder'
    ];

    /**
     * Get the group that owns the comment.
     */
    public function products()
    {
        return $this->belongsTo('App\Product')->withTimestamps();
    }
}
