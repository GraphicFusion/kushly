<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
	public $table = 'order_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
		'order_id','product_id','quantity','quantity_per_unit','unit_price'
	];

	/**
     * Relation with order
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * relation with products
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }


}
