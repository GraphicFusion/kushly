<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            [
            	'street_address' => 'Lorem ipsum dolor sit amet',
	            'unit' => 'consectetur adipisicing elit',
	            'city' => 'Huntsville',
	            'state' => 'Alabama',
	            'zip_code' => '35801',
	            'country' => 'US',
	            'type' => 'home',
	            'note' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        	],[
            	'street_address' => 'Lorem ipsum dolor sit amet',
	            'unit' => 'consectetur adipisicing elit',
	            'city' => 'Miami',
	            'state' => 'Florida',
	            'zip_code' => '33124',
	            'country' => 'US',
	            'type' => 'office',
	            'note' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        	],[
            	'street_address' => 'Lorem ipsum dolor sit amet',
	            'unit' => 'consectetur adipisicing elit',
	            'city' => 'Austin',
	            'state' => 'Texas',
	            'zip_code' => '78701',
	            'country' => 'US',
	            'type' => 'public',
	            'note' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        	]
        ]);

        DB::table('locations_user')->insert([
            [
            	'locations_id' => 1,
            	'user_id' => 2
        	],[
            	'locations_id' => 2,
            	'user_id' => 2
        	],[
            	'locations_id' => 3,
            	'user_id' => 2
        	]
        ]);
    }
}
