<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
            	'slug' => 'super-admin',
            	'name' => 'Super Admin'
        	],[
            	'slug' => 'dispensary-admin',
            	'name' => 'Dispensary Admin'
        	],[
            	'slug' => 'dispensary-employee',
            	'name' => 'Dispensary Employee'
        	],[
            	'slug' => 'driver',
            	'name' => 'Driver'
        	],[
            	'slug' => 'customer',
            	'name' => 'Customer'
        	],
        ]);
    }
}
