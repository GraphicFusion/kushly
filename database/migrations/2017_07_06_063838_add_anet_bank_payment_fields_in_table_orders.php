<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnetBankPaymentFieldsInTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('payoptions')->nullable();
            $table->string('cardnumber')->nullable();
            $table->string('expdate')->nullable();
            $table->string('cardcode')->nullable();
            $table->string('bank')->nullable();
            $table->string('routingno')->nullable();
            $table->string('accountno')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('payoptions');
            $table->string('cardnumber');
            $table->string('expdate');
            $table->string('cardcode');
            $table->string('bank');
            $table->string('routingno');
            $table->string('accountno');
        });
    }
}
