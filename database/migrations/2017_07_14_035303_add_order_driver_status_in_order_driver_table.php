<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderDriverStatusInOrderDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_driver', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_driver_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_driver', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_driver_status')->nullable();
        });
    }
}
