<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayOptionFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('cardnumber')->nullable();
            $table->string('expdate')->nullable();
            $table->string('cardcode')->nullable();
            $table->string('bank')->nullable();
            $table->string('routingno')->nullable();
            $table->string('accountno')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('cardnumber')->nullable();
            $table->string('expdate')->nullable();
            $table->string('cardcode')->nullable();
            $table->string('bank')->nullable();
            $table->string('routingno')->nullable();
            $table->string('accountno')->nullable();
        });
    }
}
