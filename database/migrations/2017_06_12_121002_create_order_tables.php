<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('locations_id');
            $table->unsignedInteger('user_id');
            $table->timestamp('timestamp_order_made')->nullable();
            $table->timestamp('timestamp_order_accepted_by_dispensary')->nullable();
            $table->timestamp('timestamp_order_picked_up_at_dispensary')->nullable();
            $table->timestamp('timestamp_order_delivered')->nullable();
            $table->boolean('active')->default(1);
            $table->enum('status', ['Pending', 'Delivered', 'Cancelled', 'Hold']);
            $table->float('order_value', 8, 2)->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('locations_id')->references('id')->on('locations');
            $table->foreign('user_id')->references('id')->on('users');
        });
        
        Schema::create('order_products', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('product_id');
            $table->integer('quantity');
            $table->string('quantity_per_unit');
            $table->float('unit_price', 8, 2);

            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('order_driver', function (Blueprint $table) {
            $table->unsignedInteger('driver_id');
            $table->unsignedInteger('order_id');
            $table->timestamp('cancelled_time')->nullable();

            $table->timestamps();
            $table->foreign('driver_id')->references('id')->on('users');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_products');
        Schema::dropIfExists('order_driver');
    }
}
