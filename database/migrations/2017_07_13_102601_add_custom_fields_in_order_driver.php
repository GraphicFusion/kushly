<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomFieldsInOrderDriver extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_driver', function (Blueprint $table) {
            $table->timestamp('order_accept_timestamp')->nullable();
            $table->string('rejection_reason')->nullable();
            $table->timestamp('rejection_timestamp')->nullable();
            $table->string('failer_reason')->nullable();
            $table->timestamp('failer_timestamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_driver', function (Blueprint $table) {
            $table->timestamp('order_accept_timestamp')->nullable();
            $table->string('rejection_reason')->nullable();
            $table->timestamp('rejection_timestamp')->nullable();
            $table->string('failer_reason')->nullable();
            $table->timestamp('failer_timestamp')->nullable();
        });
    }
}
