<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiEstimateTimeFieldsInOrderDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_driver', function (Blueprint $table) {
            $table->string('api_estimate_time')->nullable();
            $table->string('cron_estimate_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_driver', function (Blueprint $table) {
            $table->string('api_estimate_time')->nullable();
            $table->string('cron_estimate_time')->nullable();
        });
    }
}
