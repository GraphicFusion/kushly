<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('status')->default(1)->comment('1=order placed, 2=allocated, 3=rejected, 4=failure, 5=accepted, 6=delivered');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('status')->default(1)->comment('1=order placed, 2=allocated, 3=rejected, 4=failure, 5=accepted, 6=delivered');
        });
    }
}
