<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdentificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('state',['AZ','CA','NV']);
            $table->string('state_id_front_path')->nullable();
            $table->string('state_id_back_path')->nullable();
            $table->string('med_card_front_path')->nullable();
            $table->string('med_card_back_path')->nullable();
            $table->smallInteger('approved')->default('0');
            $table->string('approved_on')->nullable();
            $table->string('appeal_requested')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identifications');
    }
}
