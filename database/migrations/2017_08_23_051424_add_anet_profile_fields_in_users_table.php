<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnetProfileFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('anet_customer_profile_id')->nullable();
            $table->string('anet_customer_payment_profile_id')->nullable();
            $table->string('anet_card_last_four')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('anet_customer_profile_id')->nullable();
            $table->string('anet_customer_payment_profile_id')->nullable();
            $table->string('anet_card_last_four')->nullable();
        });
    }
}
