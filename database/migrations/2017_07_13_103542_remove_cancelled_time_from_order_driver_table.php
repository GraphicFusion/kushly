<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCancelledTimeFromOrderDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_driver', function (Blueprint $table) {
            $table->dropColumn('cancelled_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_driver', function (Blueprint $table) {
            $table->timestamp('cancelled_time')->nullable();
        });
    }
}
