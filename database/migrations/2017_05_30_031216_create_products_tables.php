<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_name');
            $table->integer('group_id')->unsigned();
            $table->boolean('active')->default(0);
            $table->bigInteger('sku')->nullable();
            $table->bigInteger('vendorID')->nullable();
            $table->bigInteger('supplierID')->nullable();
            $table->string('color')->nullable();
            $table->string('size')->nullable();
            $table->text('description');

            $table->boolean('is_marijuana')->default(0);
            $table->string('type')->nullable();
            $table->string('thc')->nullable();
            $table->string('cbd')->nullable();
            $table->string('cbn')->nullable();
            $table->string('strain_type')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('group_id')->references('id')->on('groups');
        });

        Schema::create('product_price', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->float('quantity_per_unit', 8, 2);
            $table->float('unit_price', 8, 2);
            $table->float('stock', 8, 2);
            $table->nullableTimestamps();

            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('path');
            $table->integer('sortorder')->nullable();
            $table->nullableTimestamps();

            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_price');
        Schema::dropIfExists('product_images');
    }
}
