<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnetFieldsInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('an_transaction_id')->nullable();
            $table->string('an_response_code')->nullable();
            $table->string('an_message_code')->nullable();
            $table->string('an_auth_code')->nullable();
            $table->string('an_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('an_transaction_id');
            $table->string('an_response_code');
            $table->string('an_message_code');
            $table->string('an_auth_code');
            $table->string('an_description');
        });
    }
}
